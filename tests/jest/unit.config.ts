import type { Config } from '@jest/types';

import defaultConfig from './base.config';

const config: Config.InitialOptions = {
    ...defaultConfig,
    testMatch: ['<rootDir>/tests/unit/**/*.spec.ts'],
    collectCoverage: true,
};

export default config;
