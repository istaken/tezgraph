import fs from 'fs';
import path from 'path';
import { DependencyContainer } from 'tsyringe';

import { App } from '../../src/app';
import { createDIContainer } from '../../src/dependency-injection-container';
import { guard } from '../../src/utils/guard';
import { safeJsonStringify } from '../../src/utils/safe-json-stringifier';

export function readFileText(filePath: string): string {
    return fs.readFileSync(filePath).toString();
}

export function writeFileText(filePath: string, text: string): void {
    const dirPath = path.dirname(filePath);
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath);
    }
    fs.writeFileSync(filePath, text);
}

export function deleteFileIfExists(filePath: string): void {
    if (fs.existsSync(filePath)) {
        fs.unlinkSync(filePath);
    }
}

export async function delay(millis: number): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(resolve, millis);
    });
}

export async function waitUntil(checkState: () => void): Promise<void> {
    const maxWaitMillis = 3_000;
    const waitIntervalMillis = 100;

    let waitCount = 0;
    let lastError = '';

    while (waitCount++ <= maxWaitMillis / waitIntervalMillis) {
        await delay(waitIntervalMillis);
        try {
            checkState();
            return;
        } catch (error: any) {
            lastError = error.toString();
        }
    }
    throw new Error(`Condition wasn't fulfilled within ${maxWaitMillis} ms. ${lastError}`);
}

export async function waitUntilFileContains(filePath: string, expected: string[]): Promise<void> {
    await waitUntil(() => {
        const contents = readFileText(filePath);
        for (const str of expected) {
            if (!contents.includes(str)) {
                throw new Error(`Expected file ${filePath} to contain:\n\n\t${str}`);
            }
        }
    });
}

export function getPort(app: App): number {
    const address = app.httpServer.address();
    if (typeof address !== 'object' || address === null) {
        throw new Error(`Failed to determine app port from address ${safeJsonStringify(address)}.`);
    }
    return address.port;
}

export function createTestDIContainer(envOverrides: NodeJS.ProcessEnv = {}): DependencyContainer {
    return createDIContainer({
        ...process.env,
        ...envOverrides,
    });
}

export async function startTestApp(diContainer: DependencyContainer = createTestDIContainer()): Promise<App> {
    const app = diContainer.resolve(App);
    await app.start();

    guard.notNullish(app.httpServer);
    return app;
}
