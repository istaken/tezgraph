/*
 * Mock `NoUnusedFragmentsRule` because of the way how we provide all fragments in test queries
 */
jest.mock('graphql/validation/rules/NoUnusedFragmentsRule', () => ({
    NoUnusedFragmentsRule: () => ({
        OperationDefinition() {
            return false;
        },
        FragmentDefinition() {
            return false;
        },
        Document: {
            leave() {
                // Do nothing
            },
        },
    }),
}));
