import { gql } from 'apollo-server-express';

import { balanceUpdateFragment } from './balance-update-fragment';
import { bigMapDiffFragment } from './big-map-diff-fragment';
import { blockFragment } from './block-fragment';
import { contractResultParentFragment } from './contract-result-parent-fragment';
import { delegationResultFragment } from './delegation-result-fragment';
import { endorsementMetadataFragment } from './endorsement-metadata-fragment';
import { inlinedEndorsementFragment } from './inlined-endorsement-fragment';
import { internalOperationResultFragment } from './internal-operation-result-fragment';
import { lazyStorageDiffFragment } from './lazy-storage-diff-fragment';
import { moneyOperationFragment } from './money-operation-fragment';
import { operationFragment } from './operation-fragment';
import { operationResultFragment } from './operation-result-fragment';
import { originationResultFragment } from './origination-result-fragment';
import { registerGlobalConstantResultFragment } from './register-global-constant-result-fragment';
import { revealResultFragment } from './reveal-result-fragment';
import { scriptedContractsFragment } from './scripted-contracts-fragment';
import { setDepositsLimitResultFragment } from './set-deposits-limit-result-fragment';
import { transactionParametersFragment } from './transaction-parameters-fragment';
import { transactionResultFragment } from './transaction-result-fragment';

export const allFragments = gql`
    ${balanceUpdateFragment}
    ${bigMapDiffFragment}
    ${blockFragment}
    ${contractResultParentFragment}
    ${delegationResultFragment}
    ${endorsementMetadataFragment}
    ${inlinedEndorsementFragment}
    ${internalOperationResultFragment}
    ${lazyStorageDiffFragment}
    ${moneyOperationFragment}
    ${operationFragment}
    ${operationResultFragment}
    ${originationResultFragment}
    ${registerGlobalConstantResultFragment}
    ${revealResultFragment}
    ${scriptedContractsFragment}
    ${setDepositsLimitResultFragment}
    ${transactionParametersFragment}
    ${transactionResultFragment}
`;
