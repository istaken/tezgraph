import { gql } from 'apollo-server-express';

export const bigMapDiffFragment = gql`
    fragment BigMapDiffFragment on BigMapDiff {
        action
        ... on BigMapUpdate {
            big_map
            key_hash
            key
            value
        }
        ... on BigMapRemove {
            big_map
        }
        ... on BigMapCopy {
            source_big_map
            destination_big_map
        }
        ... on BigMapAlloc {
            big_map
            key_type
            value_type
        }
    }
`;
