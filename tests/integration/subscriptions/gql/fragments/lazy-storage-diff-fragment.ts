import { gql } from 'apollo-server-express';

export const lazyStorageDiffFragment = gql`
    fragment LazyBigMapUpdatesFragment on LazyBigMapUpdateItem {
        key_hash
        key
        value
    }
    fragment LazySaplingStateDiffUpdatesFragment on LazySaplingStateDiffUpdates {
        commitments_and_ciphertexts {
            commitment
            ciphertext {
                cv
                epk
                payload_enc
                nonce_enc
                payload_out
                nonce_out
            }
        }
        nullifiers
    }
    fragment LazyStorageDiffFragment on LazyStorageDiff {
        kind
        id
        ... on LazyBigMapStorageDiff {
            big_map_diff {
                ... on LazyBigMapAlloc {
                    action
                    key_type
                    value_type
                    updates {
                        ...LazyBigMapUpdatesFragment
                    }
                }
                ... on LazyBigMapCopy {
                    action
                    source
                    updates {
                        ...LazyBigMapUpdatesFragment
                    }
                }
                ... on LazyBigMapRemove {
                    action
                }
                ... on LazyBigMapUpdate {
                    action
                    updates {
                        ...LazyBigMapUpdatesFragment
                    }
                }
            }
        }
        ... on LazySaplingStateStorageDiff {
            sapling_state_diff {
                ... on LazySaplingStateAlloc {
                    action
                    memo_size
                    updates {
                        ...LazySaplingStateDiffUpdatesFragment
                    }
                }
                ... on LazySaplingStateCopy {
                    action
                    source
                    updates {
                        ...LazySaplingStateDiffUpdatesFragment
                    }
                }
                ... on LazySaplingStateRemove {
                    action
                }
                ... on LazySaplingStateUpdate {
                    action
                    updates {
                        ...LazySaplingStateDiffUpdatesFragment
                    }
                }
            }
        }
    }
`;
