import { gql } from 'apollo-server-express';

export const transactionResultFragment = gql`
    fragment TransactionResultFragment on TransactionNotificationResult {
        ...OperationResultFragment
        storage
        balance_updates {
            ...BalanceUpdateFragment
        }
        originated_contracts
        storage_size
        paid_storage_size_diff
        allocated_destination_contract
        big_map_diff {
            ...BigMapDiffFragment
        }
        lazy_storage_diff {
            ...LazyStorageDiffFragment
        }
    }
`;
