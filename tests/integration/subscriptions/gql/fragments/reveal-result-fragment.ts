import { gql } from 'apollo-server-express';

export const revealResultFragment = gql`
    fragment RevealResultFragment on RevealResult {
        ...OperationResultFragment
    }
`;
