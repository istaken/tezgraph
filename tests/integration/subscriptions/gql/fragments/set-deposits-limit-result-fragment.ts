import { gql } from 'apollo-server-express';

export const setDepositsLimitResultFragment = gql`
    fragment SetDepositsLimitResultFragment on SetDepositsLimitNotificationResult {
        ...OperationResultFragment
    }
`;
