import { gql } from 'apollo-server-express';

export const contractResultParentFragment = gql`
    fragment ContractResultParentFragment on ContractResultParent {
        ... on OriginationNotification {
            balance
        }
        ... on InternalOriginationResult {
            balance
        }
        ... on TransactionNotification {
            destination
        }
        ... on InternalTransactionResult {
            destination
        }
    }
`;
