import { gql } from 'apollo-server-express';

export const internalOperationResultFragment = gql`
    fragment InternalOperationResultFragment on InternalOperationResult {
        kind
        source
        nonce
        ... on InternalDelegationResult {
            delegate
            result {
                ...DelegationResultFragment
            }
        }
        ... on InternalOriginationResult {
            balance
            delegate
            script {
                ...ScriptedContractsFragment
            }
            result {
                ...OriginationResultFragment
            }
        }
        ... on InternalRegisterGlobalConstantResult {
            value
            result {
                ...RegisterGlobalConstantResultFragment
            }
        }
        ... on InternalRevealResult {
            public_key
            result {
                ...RevealResultFragment
            }
        }
        ... on InternalTransactionResult {
            amount
            destination
            parameters {
                ...TransactionParametersFragment
            }
            result {
                ...TransactionResultFragment
            }
        }
    }
`;
