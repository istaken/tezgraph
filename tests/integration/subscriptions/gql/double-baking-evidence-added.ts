import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const doubleBakingEvidenceAdded = gql`
    ${allFragments}
    fragment HeaderFragment on DoubleBakingEvidenceBlockHeader {
        level
        proto
        predecessor
        timestamp
        validation_pass
        operations_hash
        fitness
        context
        priority
        proof_of_work_nonce
        seed_nonce_hash
        signature
    }
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $delegate: NullableAddressArrayFilter
    ) {
        doubleBakingEvidenceAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                delegate: $delegate
            }
        ) {
            ...OperationFragment
            bh1 {
                ...HeaderFragment
            }
            bh2 {
                ...HeaderFragment
            }
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
            }
        }
    }
`;
