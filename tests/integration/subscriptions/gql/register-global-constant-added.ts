import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const registerGlobalConstantAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $status: NullableOperationResultStatusFilter
    ) {
        registerGlobalConstantAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                source: $source
                status: $status
            }
        ) {
            ...OperationFragment
            ...MoneyOperationFragment
            value
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
                internal_operation_results {
                    ...InternalOperationResultFragment
                }
                operation_result {
                    ...RegisterGlobalConstantResultFragment
                }
            }
        }
    }
`;
