import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const delegationAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $delegate: NullableAddressFilter
        $status: NullableOperationResultStatusFilter
    ) {
        delegationAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                source: $source
                delegate: $delegate
                status: $status
            }
        ) {
            ...OperationFragment
            ...MoneyOperationFragment
            delegate
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
                internal_operation_results {
                    ...InternalOperationResultFragment
                }
                operation_result {
                    ...DelegationResultFragment
                }
            }
        }
    }
`;
