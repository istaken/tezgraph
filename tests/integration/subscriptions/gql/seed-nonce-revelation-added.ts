import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const seedNonceRevelationAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter # Specific parameters. # (nothing)
    ) {
        seedNonceRevelationAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch

                # Specific parameters.
                # (nothing)
            }
        ) {
            ...OperationFragment
            level
            nonce
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
            }
        }
    }
`;
