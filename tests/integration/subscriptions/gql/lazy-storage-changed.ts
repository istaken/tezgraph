import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const lazyStorageChanged = gql`
    ${allFragments}
    subscription (
        $replayFromBlockLevel: Int
        $kind: LazyStorageDiffKindFilter
        $id: LazyStorageIdFilter
        $action: LazyStorageDiffActionFilter
        $big_map_key: NullableLazyStorageBigMapKeyFilter
        $source: AddressFilter
        $destination: NullableAddressFilter
    ) {
        lazyStorageChanged(
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                kind: $kind
                id: $id
                action: $action
                big_map_key: $big_map_key
                source: $source
                destination: $destination
            }
        ) {
            lazy_storage_diff {
                ...LazyStorageDiffFragment
            }
            operation {
                source
            }
            parent {
                ...ContractResultParentFragment
            }
        }
    }
`;
