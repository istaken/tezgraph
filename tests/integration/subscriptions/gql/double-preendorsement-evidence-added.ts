import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const doublePreendorsementEvidenceAdded = gql`
    ${allFragments}
    fragment InlinedPreendorsementFragment on InlinedPreendorsement {
        branch
        signature
        operations {
            kind
            level
            slot
            round
            block_payload_hash
        }
    }
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $delegate: NullableAddressArrayFilter
    ) {
        doublePreendorsementEvidenceAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                delegate: $delegate
            }
        ) {
            ...OperationFragment
            op1 {
                ...InlinedPreendorsementFragment
            }
            op2 {
                ...InlinedPreendorsementFragment
            }
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
            }
        }
    }
`;
