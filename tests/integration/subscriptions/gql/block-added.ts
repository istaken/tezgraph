import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const blockAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $source: AddressFilter
        $delegate: NullableAddressFilter
        $originated_contract: NullableAddressArrayFilter
        $status: NullableOperationResultStatusFilter
    ) {
        blockAdded(replayFromBlockLevel: $replayFromBlockLevel) {
            ...BlockFragment
            originations(
                filter: {
                    # Common parameters.
                    hash: $hash
                    protocol: $protocol
                    branch: $branch
                    # Specific parameters.
                    source: $source
                    delegate: $delegate
                    originated_contract: $originated_contract
                    status: $status
                }
            ) {
                kind
                source
                fee
            }
        }
    }
`;
