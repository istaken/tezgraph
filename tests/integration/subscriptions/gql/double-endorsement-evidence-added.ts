import { gql } from 'apollo-server-express';

import { allFragments } from './fragments/all-fragments';

export const doubleEndorsementEvidenceAdded = gql`
    ${allFragments}
    subscription (
        # Common parameters.
        $includeMempool: Boolean
        $replayFromBlockLevel: Int
        $hash: NullableOperationHashFilter
        $protocol: ProtocolHashFilter
        $branch: BlockHashFilter
        # Specific parameters.
        $delegate: NullableAddressArrayFilter
    ) {
        doubleEndorsementEvidenceAdded(
            # Common parameters.
            includeMempool: $includeMempool
            replayFromBlockLevel: $replayFromBlockLevel
            filter: {
                hash: $hash
                protocol: $protocol
                branch: $branch
                # Specific parameters.
                delegate: $delegate
            }
        ) {
            ...OperationFragment
            op1 {
                ...InlinedEndorsementFragment
            }
            op2 {
                ...InlinedEndorsementFragment
            }
            slot
            metadata {
                balance_updates {
                    ...BalanceUpdateFragment
                }
            }
        }
    }
`;
