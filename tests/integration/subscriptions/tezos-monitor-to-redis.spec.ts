import { ModuleName } from '../../../src/modules/module';
import { delay } from '../helpers';
import {
    createSubscriptionsTestHelper,
    getRedisPubSub,
    readTestDataFile,
    updateReceivedOn,
    writeReceivedDataFileJson,
} from './subscriptions-test-helper';

describe('Tezos monitor publishing to Redis PubSub', () => {
    const helper = createSubscriptionsTestHelper([ModuleName.TezosMonitor, ModuleName.RedisPubSub]);

    it('should receive notifications from Tezos node, process and publish them to Redis PubSub', async () => {
        const iterator = getRedisPubSub().asyncIterator('BLOCKS');

        await delay(500);
        helper.blockMonitorStream.push(readTestDataFile('block-monitor.json'));

        const result = await iterator.next();

        const fileName = 'block-redis.json';
        const expectedNotification = JSON.parse(readTestDataFile(fileName));
        updateReceivedOn(result.value, expectedNotification);

        writeReceivedDataFileJson(fileName, result.value);
        expect(result.done).toBeFalsy();
        expect(result.value).toEqual(expectedNotification);
    });
});
