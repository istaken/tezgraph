import * as tmp from 'tmp';

import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { createRootLogger, debugFilePaths, Logger, LoggerFactory, LogLevel } from '../../../src/utils/logging';
import { LogFormat } from '../../../src/utils/logging/log-config';
import { MetricsContainer } from '../../../src/utils/metrics/metrics-container';
import { Clock } from '../../../src/utils/time/clock';
import { deleteFileIfExists, readFileText, waitUntilFileContains } from '../helpers';

describe(createRootLogger.name, () => {
    let logger: Logger;
    let tmpDir: tmp.DirResult;
    let tmpFilePath: string;

    beforeEach(() => {
        tmpDir = tmp.dirSync();
        tmpFilePath = `${tmpDir.name}/test.jsonl`;
        deleteFileIfExists(debugFilePaths.json);
        deleteFileIfExists(debugFilePaths.messages);

        const envConfig = {
            logging: {
                file: {
                    minLevel: LogLevel.Warning,
                    format: LogFormat.Json,
                    path: tmpFilePath,
                },
                console: null,
            },
            enableDebug: true,
        } as EnvConfig;

        const rootLogger = createRootLogger(envConfig);
        const factory = new LoggerFactory(rootLogger, new Clock(), new MetricsContainer());
        logger = factory.getLogger('IntTests');
    });

    afterEach(() => {
        logger.close();
    });

    it('should write logger files correctly', async () => {
        logger.logDebug('lol');
        logger.logWarning('omg {meme}', { meme: 'troll' });

        const expectedWarnEntry = ['"message":"omg {meme}"', '"category":"IntTests"', '"level":"warning"'];

        await waitUntilFileContains(tmpFilePath, expectedWarnEntry);
        expect(readFileText(tmpFilePath)).not.toContain('lol'); // Debug should not be written here.

        await waitUntilFileContains(
            debugFilePaths.json,
            expectedWarnEntry.concat(['"message":"lol"', '"level":"debug"']),
        );
        await waitUntilFileContains(debugFilePaths.messages, ['[IntTests] lol', '[IntTests] omg meme "troll"']);
    });
});
