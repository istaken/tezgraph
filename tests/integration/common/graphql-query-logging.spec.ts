import supertest from 'supertest';

import { App } from '../../../src/app';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { createTestDIContainer, deleteFileIfExists, waitUntilFileContains } from '../helpers';

describe('GraphQL Query Logging', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;
    const logFilePath = 'logs/logger_test.jsonl';

    beforeAll(async () => {
        deleteFileIfExists(logFilePath);
        const diContainer = createTestDIContainer({
            LOG_CONSOLE_FORMAT: 'messages',
            LOG_FILE_MIN_LEVEL: 'debug',
            LOG_FILE_FORMAT: 'json',
            LOG_FILE_PATH: logFilePath,
        });
        app = diContainer.resolve(App);
        await app.start();
        request = supertest(app.httpServer);
    });

    afterAll(async () => {
        await app.stop();
    });

    it('should log start and end for single graphql account query call', async () => {
        await sendAccountsQueryGraphQLRequest();
        const expectedDebugEntry = [
            `"category":"QueryLogger","level":"debug","message":"Query.accounts request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"Query.accounts request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.address request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.operations request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.address request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.operations request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"DelegationRecord.hash request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"DelegationRecord.hash request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
        ];
        await waitUntilFileContains(logFilePath, expectedDebugEntry);
    });

    it('should log start and end for single graphql operation query call', async () => {
        await sendOperationQueryGraphQLRequest();
        const expectedDebugEntry = [
            `"category":"QueryLogger","level":"debug","message":"Query.operations request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"Query.operations request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"RevealRecord.hash request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"RevealRecord.source request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"RevealRecord.hash request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"RevealRecord.source request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"RevealRecord.block request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"BlockRecord.timestamp request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"BlockRecord.level request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.address request received - {requestId}","data":{"requestId":`,
            `"category":"QueryLogger","level":"debug","message":"AccountRecord.address request resolved - {requestId}, {duration}, {res}","data":{"requestId":`,
        ];
        await waitUntilFileContains(logFilePath, expectedDebugEntry);
    });

    async function sendAccountsQueryGraphQLRequest() {
        const res = await request.post(appUrlPaths.graphQL).send({
            query: /* Graphql */ `
                query AccountsQuery {
                    accounts(first: 1, filter: { addresses: ["tz1Z8ns4YVqXR4pw5My1E4iRdxVdXhUwqB4i"] }) {
                        edges {
                            node {
                                address
                                operations(first: 1, filter: {relationship_type: source}) {
                                    edges {
                                        cursor
                                        node {
                                            hash
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            `,
        });

        expect(res.status).toBe(200);
        expect(res.body.errors).toBeUndefined();
    }

    async function sendOperationQueryGraphQLRequest() {
        const res = await request.post(appUrlPaths.graphQL).send({
            query: /* Graphql */ `
                query OperationsQuery {
                    operations(first: 1, filter: { hash: "ooomsUiGgn4Tn33cBnS47cHTNYP28xic6zv8gCrSxjQduxpQyPg" }) {
                        edges {
                            node {
                                hash
                                source {
                                    address
                                }
                                ... on DelegationRecord {
                                    batch_position
                                    metadata {
                                        operation_result {
                                            consumed_milligas
                                        }
                                    }
                                }
                                ... on RevealRecord {
                                    block {
                                        timestamp
                                        level
                                    }
                                }
                            }
                        }
                    }
                }
            `,
        });

        expect(res.status).toBe(200);
        expect(res.body.errors).toBeUndefined();
    }
});
