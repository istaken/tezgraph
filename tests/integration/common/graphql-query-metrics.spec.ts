import { Registry } from 'prom-client';
import supertest from 'supertest';

import { App } from '../../../src/app';
import { appUrlPaths } from '../../../src/utils/http-handler';
import { createTestDIContainer } from '../helpers';

describe('Prometheus GraphQL Query Metrics', () => {
    let app: App;
    let request: supertest.SuperTest<supertest.Test>;
    let promClientRegistry: Registry;

    beforeAll(async () => {
        const diContainer = createTestDIContainer();
        app = diContainer.resolve(App);
        promClientRegistry = diContainer.resolve(Registry);
        await app.start();
        request = supertest(app.httpServer);
    });

    beforeEach(() => {
        promClientRegistry.resetMetrics();
    });

    afterAll(async () => {
        await app.stop();
    });

    describe('graphql_query_type_field_total', () => {
        it('should correctly return metric for single graphql account query call', async () => {
            await sendAccountsQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toInclude(
                'graphql_query_type_field_total{type="AccountRecord",field="address",caller="external"} 1',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="AccountRecord",field="activated",caller="external"} 1',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="AccountRecord",field="operations",caller="external"} 1',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="OperationRecord",field="hash",caller="external"} 1',
            );
            expect(text).not.toInclude('graphql_query_type_field_error_total{field="');
        });

        it('should correctly count subsequent graphql operation query call', async () => {
            await sendOperationQueryGraphQLRequest();
            await sendOperationQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toInclude(
                'graphql_query_type_field_total{type="OperationRecord",field="hash",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="OperationRecord",field="source",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="AccountRecord",field="address",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="DelegationRecord",field="batch_position",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="DelegationRecord",field="metadata",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="DelegationRecordMetadata",field="operation_result",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="DelegationResultRecord",field="consumed_milligas",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="RevealRecord",field="block",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="BlockRecord",field="level",caller="external"} 2',
            );
            expect(text).toInclude(
                'graphql_query_type_field_total{type="BlockRecord",field="timestamp",caller="external"} 2',
            );
            expect(text).not.toInclude('graphql_query_type_field_error_total{field="');
        });
    });

    describe('graphql_query_off_chain_source_type_total', () => {
        it('should correctly return a metric for the off chain source type used', async () => {
            await sendAccountsExternalCallQueryGraphQLRequest();

            const text = await getMetrics();

            expect(text).toInclude('getTokenMetadataFromTaquito",type="taquito-tzip12-rpc",status="failed"} 2'); // KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8, KT1EtmVGhh3t5V2RBevyXjVkCkL5cdNkv6NA
            expect(text).toInclude(
                'findContractMetadataFromTaquito",type="taquito-tzip16-https",status="succeeded"} 2',
            ); // KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8, KT1EtmVGhh3t5V2RBevyXjVkCkL5cdNkv6NA
            expect(text).toInclude('findContractMetadataFromTaquito",type="taquito-tzip16-ipfs",status="succeeded"} 1'); // KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD
            expect(text).toInclude('getTokenMetadataFromTaquito",type="taquito-tzip12-rpc",status="succeeded"} 1'); // KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD
            expect(text).toInclude('getDelegationAmountFromTaquito",type="taquito-rpc",status="succeeded"} 1'); // Tz1cNakLoctaqaCtBZSQGZs5RgAciYqB7cNf
        });
    });

    async function getMetrics() {
        const res = await request.get(appUrlPaths.metrics);

        expect(res.status).toBe(200);
        return res.text;
    }

    async function sendAccountsQueryGraphQLRequest() {
        const res = await request.post(appUrlPaths.graphQL).send({
            query: /* Graphql */ `
                query AccountsQuery {
                    accounts(first: 1, filter: { addresses: ["tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ"] }) {
                        edges {
                            node {
                                address
                                activated
                                operations(first: 10, filter: { relationship_type: source }) {
                                    edges {
                                        cursor
                                        node {
                                            hash
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            `,
        });

        expect(res.status).toBe(200);
        expect(res.body.errors).toBeUndefined();
    }

    async function sendAccountsExternalCallQueryGraphQLRequest() {
        const res = await request.post(appUrlPaths.graphQL).send({
            query: /* Graphql */ `
            query AccountQuery {
                accounts(
                  first: 4
                  filter: { addresses: ["KT1VCaAEpuKF6EmqpA2VyvkiYLMGJ7XFnHi8", "KT1EtmVGhh3t5V2RBevyXjVkCkL5cdNkv6NA", "KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD","tz1cNakLoctaqaCtBZSQGZs5RgAciYqB7cNf"] }
                ) {
                  edges {
                    node {
                      address
                      token_metadata(token_id: 2009) {
                        decimals
                        token_id
                        name
                      },
                      operations (first: 1, filter: {
                        relationship_type: source
                        kind:delegation
                      }) {
                        edges {
                          node {
                            ... on DelegationRecord {
                              amount
                            }
                          }
                        }
                      }
                      contract_metadata {
                        name
                        description
                      }
                    }
                  }
                }
              }
            `,
        });

        expect(res.status).toBe(200);
    }

    async function sendOperationQueryGraphQLRequest() {
        const res = await request.post(appUrlPaths.graphQL).send({
            query: /* Graphql */ `
                query OperationsQuery {
                    operations(first: 1, filter: { hash: "ooomsUiGgn4Tn33cBnS47cHTNYP28xic6zv8gCrSxjQduxpQyPg" }) {
                        edges {
                            node {
                                hash
                                source {
                                    address
                                }
                                ... on DelegationRecord {
                                    batch_position
                                    metadata {
                                        operation_result {
                                            consumed_milligas
                                        }
                                    }
                                }
                                ... on RevealRecord {
                                    block {
                                        timestamp
                                        level
                                    }
                                }
                            }
                        }
                    }
                }
            `,
        });

        expect(res.status).toBe(200);
        expect(res.body.errors).toBeUndefined();
    }
});
