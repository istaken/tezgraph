/* eslint-disable max-lines */
import { gql } from 'apollo-server-express';

import { DateRangeFilter } from '../../../../src/modules/queries-graphql/repositories/date-range.utils';
import { OperationRecordKind } from '../../../../src/modules/queries-graphql/resolvers/types/operation-record-kind';
import { IntRangeFilter } from '../../../../src/modules/queries-graphql/resolvers/types/range-filters';
import { createTestGraphQLClientUtils } from '../utils/test-client';

const testUtils = createTestGraphQLClientUtils();

describe('Operation Resolver', () => {
    describe('General', () => {
        const badDates = ['A non-date string'];
        const query = gql`
            query Operations($start: String, $end: String) {
                operations(first: 20, filter: { timestamp: { gte: $start, lte: $end } }) {
                    page_info {
                        has_next_page
                        end_cursor
                    }
                    edges {
                        node {
                            kind
                            hash
                            block {
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                        }
                    }
                }
            }
        `;
        badDates.forEach((badDate) => {
            it('should return proper error for wrong DateTime texts', async () => {
                const res = await testUtils.executeOperation({
                    query,
                    variables: { start: badDate },
                });
                expect(res.errors).toBeArrayOfSize(1);
                expect(res.errors?.[0]?.extensions?.error).toMatch(
                    `The value '${badDate}' is not a supported date and time format. The suggested format is Iso 8601.`,
                );
                expect(res.errors?.[0]?.message).toContain(res.errors?.[0]?.extensions?.error);
            });
        });
    });

    describe('Endorsement operation', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    total_count
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on EndorsementRecord {
                                reward
                                deposit
                                metadata {
                                    delegate {
                                        address
                                    }
                                    slots
                                }
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'opYtDuR9crXqDKp562yBisGnspgwgCZRaYw8BDsicrk9evkytWA';

        it('should return endorsement operation data', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });
    });

    describe('Reveal operation', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on RevealRecord {
                                fee
                                counter
                                gas_limit
                                storage_limit
                                public_key
                                metadata {
                                    operation_result {
                                        consumed_gas
                                        consumed_milligas
                                        errors
                                        status
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'ooomsUiGgn4Tn33cBnS47cHTNYP28xic6zv8gCrSxjQduxpQyPg';

        it('should return reveal operation data', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });
    });

    describe('Origination operation', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on OriginationRecord {
                                burned
                                delegate {
                                    address
                                }
                                balance
                                fee
                                counter
                                gas_limit
                                storage_limit
                                storage_size
                                contract {
                                    script {
                                        code
                                        storage
                                    }
                                }
                                contract_address
                                metadata {
                                    operation_result {
                                        consumed_gas
                                        consumed_milligas
                                        errors
                                        status
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'oowHaMcwCGFjswygaB1fNiJFPLrF79EySKVQys9oDNYtc6AzSMc';

        it('should return origination operation data', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });
    });

    describe('Transaction operation', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on TransactionRecord {
                                fee
                                counter
                                gas_limit
                                storage_limit
                                storage_size
                                amount
                                parameters {
                                    entrypoint
                                    value
                                    canonical_value
                                    michelson_value
                                }
                                destination {
                                    address
                                }
                                metadata {
                                    operation_result {
                                        consumed_gas
                                        consumed_milligas
                                        errors
                                        status
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo';

        it('should return transaction operation data', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data?.operations).toMatchSnapshot();
        });
    });

    describe('Delegation operation', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on DelegationRecord {
                                fee
                                counter
                                gas_limit
                                amount
                                storage_limit
                                delegate {
                                    address
                                }
                                metadata {
                                    operation_result {
                                        consumed_gas
                                        consumed_milligas
                                        errors
                                        status
                                    }
                                }
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'oo6NZT12G8yNk6hRjusR3Qr7hhssW5x6Gh266tLgmwhMkG6FXLQ';

        it('should return delegation operation data', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });
    });

    describe('Multiple operations', () => {
        const testQuery = gql`
            query OperationsQuery($filter: OperationsFilter!) {
                operations(filter: $filter, first: 10) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                            ... on TransactionRecord {
                                fee
                                counter
                                gas_limit
                                storage_limit
                                amount
                                parameters {
                                    entrypoint
                                    value
                                    canonical_value
                                    michelson_value
                                }
                                destination {
                                    address
                                }
                            }
                            ... on RevealRecord {
                                fee
                                counter
                                gas_limit
                                storage_limit
                                public_key
                            }
                        }
                    }
                }
            }
        `;
        const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';

        it('should return a list of operations data for single hash', async () => {
            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: { filter: { hash } },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data?.operations.edges).toHaveLength(4);
            const nodes = res.data?.operations.edges.map((it: any) => it.node);
            expect(nodes).toMatchSnapshot();
        });
    });

    describe('sorting', () => {
        describe('default', () => {
            const testQuery = gql`
                query OperationsQuery($filter: OperationsFilter!) {
                    operations(filter: $filter, first: 10) {
                        total_count
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            it('should sort operations by chronological order desc (default)', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'chronological_order',
                            direction: 'desc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should sort operations by chronological order desc (explicit)', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'chronological_order',
                            direction: 'asc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should sort operations by chronological order asc', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: { filter: { timestamp } },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('hash', () => {
            const testQuery = gql`
                query OperationsQuery($filter: OperationsFilter!, $order_by: OperationsOrderByInput!) {
                    operations(filter: $filter, order_by: $order_by, first: 10) {
                        total_count
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            it('should sort operations by hash asc', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'hash',
                            direction: 'asc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should sort operations by hash desc', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'hash',
                            direction: 'desc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('source', () => {
            const testQuery = gql`
                query OperationsQuery($filter: OperationsFilter!, $order_by: OperationsOrderByInput!) {
                    operations(filter: $filter, order_by: $order_by, first: 10) {
                        total_count
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                hash
                                batch_position
                                internal
                                source {
                                    address
                                }
                            }
                        }
                    }
                }
            `;

            it('should sort operations by source address asc', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'asc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should sort operations by source address desc', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'desc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });
    });

    describe('pagination', () => {
        it('should return proper list of operation when after and first is used', async () => {
            const testQuery = gql`
                query OperationsQuery($after: Cursor!, $first: Int!) {
                    operations(after: $after, first: $first) {
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                block {
                                    level
                                    timestamp
                                }
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: {
                    after: 'ooSnZKPdAW7mxrf8GjBDgGyeniduYu1fSG3DMU8SrGXfS1Z5bjN:1:0', // 13005130000028
                    first: 3,
                },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });

        it('should return proper list of operation when before and last is used', async () => {
            const testQuery = gql`
                query OperationsQuery($before: Cursor!, $last: Int!) {
                    operations(before: $before, last: $last) {
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            const res = await testUtils.executeOperation({
                query: testQuery,
                variables: {
                    before: 'ooSnZKPdAW7mxrf8GjBDgGyeniduYu1fSG3DMU8SrGXfS1Z5bjN:1:0', // 13005130000028
                    last: 3,
                },
            });

            expect(res.errors).toBeUndefined();
            expect(res.data).toMatchSnapshot();
        });

        describe('with custom orderBy', () => {
            it('should return last records before selected one sorted by source address ascending', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $before: Cursor!
                        $last: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, before: $before, last: $last) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                cursor
                                node {
                                    __typename
                                    hash
                                    batch_position
                                    internal
                                    source {
                                        address
                                    }
                                }
                            }
                        }
                    }
                `;
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        before: 'ooTVmXPWTcnbHfn1iz9EFXjRKAZssbqyUjqGJkMgRqmnSNTi4zM:0:0', // 12827890000018
                        last: 3,
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'asc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return last records before selected one sorted by source address descending', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $before: Cursor!
                        $last: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, before: $before, last: $last) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                cursor
                                node {
                                    __typename
                                    hash
                                    batch_position
                                    internal
                                    source {
                                        address
                                    }
                                }
                            }
                        }
                    }
                `;
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        before: 'ooobECWi4Hr6HfddhqyuA6Sfo5VMkyVaZwF5s4KRCqexjmqLDs8:0:0', // 12827890000004
                        last: 3,
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'desc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return first records after selected one sorted by source address ascending', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $after: Cursor!
                        $first: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, after: $after, first: $first) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                cursor
                                node {
                                    __typename
                                    hash
                                    batch_position
                                    internal
                                    source {
                                        address
                                    }
                                }
                            }
                        }
                    }
                `;
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        after: 'ooobECWi4Hr6HfddhqyuA6Sfo5VMkyVaZwF5s4KRCqexjmqLDs8:0:0', // 12827890000004
                        first: 3,
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'asc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return first records after selected one sorted by source address descending', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $after: Cursor!
                        $first: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, after: $after, first: $first) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                cursor
                                node {
                                    __typename
                                    hash
                                    batch_position
                                    internal
                                    source {
                                        address
                                    }
                                }
                            }
                        }
                    }
                `;
                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:00:10.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        after: 'ooTVmXPWTcnbHfn1iz9EFXjRKAZssbqyUjqGJkMgRqmnSNTi4zM:0:0', // 12827890000018
                        first: 3,
                        filter: { timestamp },
                        order_by: {
                            field: 'source',
                            direction: 'desc',
                        },
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should properly paginate over sorted results when columns value is equal', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $after: Cursor!
                        $first: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, after: $after, first: $first) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                node {
                                    hash
                                    batch_position
                                    internal
                                    block {
                                        level
                                    }
                                }
                            }
                        }
                    }
                `;

                const timestamp: DateRangeFilter = {
                    gte: '2021-01-01T00:00:00.000Z',
                    lte: '2021-01-01T00:02:00.000Z',
                };

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { timestamp },
                        order_by: {
                            field: 'chronological_order',
                            direction: 'desc',
                        },
                        after: 'onwru99FepqyAgypPMjXaFbeDEGM2cD7frRnrHjpKdcVQWCxL4d:1:0', // 12827900000033
                        first: 3,
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should properly paginate when operation hash and batch_position are not unique', async () => {
                const testQuery = gql`
                    query OperationsQuery(
                        $filter: OperationsFilter!
                        $order_by: OperationsOrderByInput!
                        $after: Cursor!
                        $first: Int!
                    ) {
                        operations(filter: $filter, order_by: $order_by, after: $after, first: $first) {
                            total_count
                            page_info {
                                has_previous_page
                                has_next_page
                                start_cursor
                                end_cursor
                            }
                            edges {
                                cursor
                                node {
                                    hash
                                    batch_position
                                    internal
                                    block {
                                        level
                                    }
                                }
                            }
                        }
                    }
                `;

                const res = await testUtils.executeOperation({
                    query: testQuery,
                    variables: {
                        filter: { hash: 'ooghYAMi4BFPsPwW25GSWCkZDYq1uYsBBMYhdJBceE6r4rc9xNw' },
                        order_by: {
                            field: 'chronological_order',
                            direction: 'desc',
                        },
                        after: 'ooghYAMi4BFPsPwW25GSWCkZDYq1uYsBBMYhdJBceE6r4rc9xNw:0:4',
                        first: 3,
                    },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });
    });

    describe('filtering', () => {
        const fullTestQuery = gql`
            query OperationsQuery($filter: OperationsFilter!, $first: Int, $last: Int) {
                operations(filter: $filter, first: $first, last: $last) {
                    total_count
                    page_info {
                        has_previous_page
                        has_next_page
                        start_cursor
                        end_cursor
                    }
                    edges {
                        cursor
                        node {
                            __typename
                            hash
                            batch_position
                            internal
                            kind
                            sender {
                                address
                            }
                            receiver {
                                address
                            }
                            block {
                                hash
                                level
                                timestamp
                            }
                            source {
                                address
                            }
                        }
                    }
                }
            }
        `;

        describe('internal', () => {
            it('should return operations with selected hash, batch_position and internal', async () => {
                const hash = 'oniwdBLj3jP8kHivHwrZwxdRE8d7VXfTKi27BimafXygwN5ukMJ';
                const batch_position = 0;
                const internal = 2;
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, batch_position, internal }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('batch_position', () => {
            it('should return operations filtered by batch_position', async () => {
                const hash = 'ooqjCjGrJu5casEhGXUq9usTRf5bmB12xHv5trzr1C7WeBb4GjB';
                const batch_position = 2;
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { batch_position, hash }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('kind', () => {
            it('should return operations of selected kind', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const kind = 'reveal';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, kind }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('level', () => {
            it('should return operations of selected level', async () => {
                const level = 1300513;
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { level }, first: 1 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('block', () => {
            it('should return operations of selected block hash', async () => {
                const block_hash = 'BMU5LYNswNSKymTFXkxroXXZk7p4EvsVPG2jUJTdyyw8dZEzhLw';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { block_hash }, first: 1 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('source', () => {
            it('should return operations of selected sources', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const sources = ['KT1V4Vp7zhynCNuaBjWMpNU535Xm2sgqkz6M'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, sources }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('status', () => {
            it('should return operations of the selected status', async () => {
                const senders = ['tz1Me1MGhK7taay748h4gPnX2cXvbgL6xsYL'];
                const receivers = ['KT1BmFQWFuiJPe6KVBMD8EDUMjNsgk9vFVyt'];
                const kind = 'transaction';
                const entrypoint = 'default';
                const status = 'applied';
                const res = await testUtils.executeOperation({
                    query: gql`
                        query OperationsQuery($filter: OperationsFilter!, $first: Int, $last: Int) {
                            operations(filter: $filter, first: $first, last: $last) {
                                total_count
                                page_info {
                                    has_previous_page
                                    has_next_page
                                    start_cursor
                                    end_cursor
                                }
                                edges {
                                    node {
                                        hash
                                        block {
                                            timestamp
                                        }
                                        ... on TransactionRecord {
                                            metadata {
                                                operation_result {
                                                    status
                                                }
                                            }
                                            parameters {
                                                entrypoint
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    `,
                    variables: { filter: { senders, receivers, kind, entrypoint, status }, last: 5 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('senders', () => {
            it('should return operations of selected senders', async () => {
                const hash = 'onoBBWBgUNixT9iyhLMF4ZffuaRiqm4hgFma44Lysd2rE3umWYf';
                const senders = ['tz1eCuUshhy61jCHTg3gCHhGm1Tt1MYcPiyb', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, senders }, first: 3 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('receivers', () => {
            it('should return operations of selected receivers', async () => {
                const hash = 'onoBBWBgUNixT9iyhLMF4ZffuaRiqm4hgFma44Lysd2rE3umWYf';
                const receivers = ['tz1eCuUshhy61jCHTg3gCHhGm1Tt1MYcPiyb', 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, receivers }, first: 3 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('senders and receivers', () => {
            it('should return operations of with the specified senders and receivers', async () => {
                const hash = 'op7fsRrmKVxjThjYux2bwrg91RqNeX928ifxCCpZnWbvAHdcnTf';
                const senders = ['tz3gGCrSvKfJpUd3w6ckSvBFbRJ5RjWU9zEw'];
                const receivers = ['KT1WnkYcBK7gyChXjSvZ4wqE7TqsU5UYL5uS'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, senders, receivers }, first: 3 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('endorsement_delegates', () => {
            it('should return Endorsement operations of selected delegates', async () => {
                const level = 2130000;
                const endorsement_delegates = [
                    'tz1WnfXMPaNTBmH7DBPwqCWs9cPDJdkGBTZ8',
                    'tz1Ldzz6k1BHdhuKvAtMRX7h5kJSMHESMHLC',
                ];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { endorsement_delegates, level }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if endorsement_delegate does not match', async () => {
                const level = 2130000;
                const endorsement_delegates = ['tz1VsPcXRar3GKaAW34S5NiNHUKvEaR7Z7q9'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { level, endorsement_delegates }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('delegation_delegates', () => {
            it('should return Delegation operations of selected delegates', async () => {
                const delegation_delegates = [
                    'tz1Wit2PqodvPeuRRhdQXmkrtU8e8bRYZecd',
                    'tz1U2ufqFdVkN2RdYormwHtgm3ityYY1uqft',
                ];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { delegation_delegates }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if delegation_delegates does not match', async () => {
                const hash = 'ongmCE4AABhEn1nv931KwpeWDHaJ5PATLKYk8UuiR77MrkfKBpy';
                const delegation_delegates = ['tz1VsPcXRar3GKaAW34S5NiNHUKvEaR7Z7q9'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, delegation_delegates }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('public_key', () => {
            it('should return Reveal operations of selected public_key', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const public_key = 'edpkvWvcmkAikWmDSTgUpija5wBZphW7SUQaTWGbnLArJYdUhYWM61';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, public_key }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if public_key does not match', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const public_key = 'edpku4dqwQ7RwzifYhAngykCSgtTvddyxEh8BMey3kGYNxtNfetGdJ';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, public_key }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('originated_contracts', () => {
            it('should return Origination operations of selected originated_contracts', async () => {
                const originated_contracts = [
                    'KT1HedYVpGi5N2NVTXhRY5zw6PRjf8NFy5te',
                    'KT1Lpg5e9aMVL9k4UwVkssftjctWgnMmYiyi',
                ];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { originated_contracts }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if originated_contracts does not match', async () => {
                const hash = 'oowHaMcwCGFjswygaB1fNiJFPLrF79EySKVQys9oDNYtc6AzSMc';
                const originated_contracts = ['tz1VsPcXRar3GKaAW34S5NiNHUKvEaR7Z7q9'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, originated_contracts }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('entrypoint', () => {
            it('should return Transaction operations of selected entrypoint', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const entrypoint = 'runEntrypointLambda';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, entrypoint }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if entrypoint does not match', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const entrypoint = 'default';
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, entrypoint }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('destinations', () => {
            it('should return Transaction operations of selected destinations', async () => {
                const hash = 'oofHTrWoKf1r1jhjYSUz7psbp4u7Hw2e6UBuUswbwKfAEZdse2b';
                const destinations = ['KT19kCpYFxrNPegMBYKKH44szaqv8offqBRz', 'tz1hFhmqKNB7hnHVHAFSk9wNqm7K9GgF2GDN'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, destinations }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return nothing if destinations does not match', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const destinations = ['KT1HedYVpGi5N2NVTXhRY5zw6PRjf8NFy5te'];
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, destinations }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('timestamp', () => {
            const simpleTestQuery = gql`
                query OperationsQuery($filter: OperationsFilter!) {
                    operations(filter: $filter, first: 10) {
                        total_count
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                block {
                                    timestamp
                                }
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            it('should return operations in selected period', async () => {
                const timestamp: DateRangeFilter = {
                    gte: '2021-02-02T02:02:00.000Z',
                    lte: '2021-02-02T02:02:22.000Z',
                };
                const res = await testUtils.executeOperation({
                    query: simpleTestQuery,
                    variables: { filter: { timestamp } },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('level_range', () => {
            const simpleTestQuery = gql`
                query OperationsQuery($filter: OperationsFilter!) {
                    operations(filter: $filter, first: 5) {
                        total_count
                        page_info {
                            has_previous_page
                            has_next_page
                            start_cursor
                            end_cursor
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                block {
                                    timestamp
                                    level
                                }
                                hash
                                batch_position
                                internal
                            }
                        }
                    }
                }
            `;

            it('should return operations in selected period', async () => {
                const level_range: IntRangeFilter = {
                    gte: 2130000,
                    lte: 2130001,
                };
                const res = await testUtils.executeOperation({
                    query: simpleTestQuery,
                    variables: { filter: { level_range } },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('consumed_milligas', () => {
            it('should return Transaction operations with consumed_milligas gte', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const consumed_milligas = { gte: '50000000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, consumed_milligas }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return Transaction operations with consumed_milligas lte', async () => {
                const hash = 'onhXpF55XQ1e9d3Epmb5oS6gH4jimfEjKQ6kAcuMrmahcKoCfjJ';
                const consumed_milligas = { lte: '50000000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, consumed_milligas }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('amount', () => {
            it('should return Transaction operations with amount gte', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const amount = { gte: '500000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, amount }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return Transaction operations with amount lte', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const amount = { lte: '500000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, amount }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });
        });

        describe('fee', () => {
            it('should return Transaction operations with fee gte', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const fee = { gte: '10000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, fee }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return empty array if there is no Transaction operations with fee gte ', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const fee = { gte: '20000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, fee }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });

            it('should return Transaction operations with fee lte', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const fee = { lte: '20000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, fee }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data).toMatchSnapshot();
            });

            it('should return empty array if there is no Transaction operations with fee lte ', async () => {
                const hash = 'ooaQEJpnuYnBbd4DurWUdsdLUD6GyzMMZf1yLKPmN7gGSPEQwaq';
                const fee = { lte: '10000' };
                const res = await testUtils.executeOperation({
                    query: fullTestQuery,
                    variables: { filter: { hash, fee }, first: 10 },
                });

                expect(res.errors).toBeUndefined();
                expect(res.data?.operations).toBeNull();
            });
        });

        describe('Operation kinds', () => {
            const query = gql`
                query test($kind: OperationRecordKind!) {
                    operations(
                        first: 10
                        filter: { kind: [$kind] }
                        order_by: { field: chronological_order, direction: asc }
                    ) {
                        edges {
                            node {
                                kind
                                hash
                                block {
                                    level
                                }
                                source {
                                    address
                                }
                            }
                        }
                    }
                }
            `;
            for (const kind in OperationRecordKind) {
                const isValueProperty = parseInt(kind, 10) >= 0;
                if (isValueProperty) {
                    continue;
                }
                it(`should filter operations of kind ${kind}`, async () => {
                    const res = await testUtils.executeOperation({
                        query,
                        variables: {
                            kind,
                        },
                    });
                    expect(res.errors).toBeUndefined();
                    expect(res.data).toMatchSnapshot();
                });
            }
        });
    });
});
