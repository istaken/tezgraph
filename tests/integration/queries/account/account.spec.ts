import { gql } from 'apollo-server-express';

import { executeTestQuery } from './account.utils';

const accountWithAllScalarFieldQuery = gql`
    query AccountsQuery($filter: AccountFilter, $first: Int, $after: Cursor, $before: Cursor, $last: Int) {
        accounts(first: $first, filter: $filter, after: $after, before: $before, last: $last) {
            edges {
                node {
                    address
                    public_key
                    activated
                    script {
                        code
                        storage
                    }
                }
            }
        }
    }
`;

const accountTestQuery = gql`
    query AccountsQuery($filter: AccountFilter, $first: Int, $after: Cursor, $before: Cursor, $last: Int) {
        accounts(first: $first, filter: $filter, after: $after, before: $before, last: $last) {
            edges {
                node {
                    address
                }
            }
        }
    }
`;

const accountWithTotalCountTestQuery = gql`
    query AccountsQuery($addresses: [Address!], $first: Int, $after: Cursor, $before: Cursor, $last: Int) {
        accounts(
            first: $first
            filter: { addresses: $addresses }
            after: $after
            before: $before
            last: $last
            order_by: { field: address, direction: desc }
        ) {
            total_count
            edges {
                node {
                    address
                }
            }
        }
    }
`;

const accountOperationsTestQuery = gql`
    query AccountsQuery($address: Address!, $last: Int, $relationship_type: AccountToOperationRelationshipType!) {
        accounts(first: 1, filter: { addresses: [$address] }) {
            total_count
            edges {
                node {
                    address
                    activated
                    operations(
                        first: $last
                        filter: { relationship_type: $relationship_type }
                        order_by: { field: chronological_order, direction: asc }
                    ) {
                        total_count
                        page_info {
                            start_cursor
                            end_cursor
                            has_next_page
                            has_previous_page
                        }
                        edges {
                            cursor
                            node {
                                __typename
                                hash
                                batch_position
                                internal
                                kind
                                block {
                                    hash
                                    level
                                    timestamp
                                }
                                source {
                                    address
                                }
                                ... on DelegationRecord {
                                    fee
                                    counter
                                    gas_limit
                                    storage_limit
                                    delegate {
                                        address
                                    }
                                    metadata {
                                        operation_result {
                                            consumed_gas
                                            consumed_milligas
                                            errors
                                            status
                                        }
                                    }
                                }
                                ... on TransactionRecord {
                                    fee
                                    counter
                                    gas_limit
                                    storage_limit
                                    amount
                                    destination {
                                        address
                                    }
                                    metadata {
                                        operation_result {
                                            consumed_gas
                                            consumed_milligas
                                            errors
                                            status
                                        }
                                    }
                                    parameters {
                                        entrypoint
                                        value
                                        canonical_value
                                        michelson_value
                                    }
                                }
                                ... on OriginationRecord {
                                    fee
                                    counter
                                    gas_limit
                                    storage_limit
                                    contract_address
                                    delegate {
                                        address
                                    }
                                    metadata {
                                        operation_result {
                                            consumed_gas
                                            consumed_milligas
                                            errors
                                            status
                                        }
                                    }
                                }
                                ... on RevealRecord {
                                    fee
                                    counter
                                    gas_limit
                                    storage_limit
                                    public_key
                                    metadata {
                                        operation_result {
                                            consumed_gas
                                            consumed_milligas
                                            errors
                                            status
                                        }
                                    }
                                }
                                ... on EndorsementRecord {
                                    metadata {
                                        delegate {
                                            address
                                        }
                                        slots
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`;

describe('Account Resolvers', () => {
    it('should return multiple accounts in correct order', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                addresses: ['tz1W3VYi3gSt9Xgfw6WNV1aGZRPH2BsskMEY', 'tz1d1yGG5GAPZZuX8jhd14KgBd6SNYA8UDag'],
                first: 10,
            },
            accountWithTotalCountTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should limit accounts using first', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                first: 3,
            },
            accountWithAllScalarFieldQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should paginate using after', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                first: 3,
                after: 'KT18b2Pv5Jbi5LLXYxpEWHSkwxz53W79unYH',
            },
            accountTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should paginate using before', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                last: 3,
                before: 'KT18amZmM5W7qDWVt2pH6uj7sCEd3kbzLrHT',
            },
            accountTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('should error if page size is too large', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                first: 1000,
            },
            accountTestQuery,
        );
        expect(res.errors?.[0]?.extensions?.error).toMatch('Cannot return more than 100 results in a page');
        expect(res.errors?.[0]?.message).toContain(res.errors?.[0]?.extensions?.error);
    });

    it('should return an error explaining that the given address string is not a valid Base58Check encoded hash', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1W3VYi3gSt9Xgfw6WNV1aGZRPH21nva11d',
                last: 1,
            },
            accountTestQuery,
        );
        expect(res.errors).toMatchSnapshot();
    });

    it('should return an error explaining the given address string must be a valid tz1/tz2/tz3/KT1 address with a length of 36', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'foobar',
                last: 1,
            },
            accountTestQuery,
        );
        expect(res.errors).toMatchSnapshot();
    });
});

describe('Operations Field', () => {
    it('Should return operations for which this account is the source', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1f5ZTSYkNgjMWurw4Uk8GH7gsLW6S1dNeb',
                last: 1,
                relationship_type: 'source',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the destination', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1f5ZTSYkNgjMWurw4Uk8GH7gsLW6S1dNeb',
                last: 1,
                relationship_type: 'destination',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the sender', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1f5ZTSYkNgjMWurw4Uk8GH7gsLW6S1dNeb',
                last: 1,
                relationship_type: 'sender',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the receiver', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1f5ZTSYkNgjMWurw4Uk8GH7gsLW6S1dNeb',
                last: 1,
                relationship_type: 'receiver',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the endorsement_delegate', async () => {
        /*
         * If we use a random account, new delegations to it will change total_count and we have to maintain the snapshot for the test to pass.
         * We should find an account which has many delegations, but one that for a long time no one is delegating to.
         * We user the following query to see which accounts have many delegations, use the last column to find one that's inactive for a long time
         * (and therefore we can expect it to change less frequently
         *
         * select e.delegate_id, count(*), max(operation_id)  from c.endorsement e
         * group by e.delegate_id
         * order by count(*) desc
         *
         */
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1Yju7jmmsaUiG9qQLoYv35v5pHgnWoLWbt',
                last: 5,
                relationship_type: 'endorsement_delegate',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the delegation_delegate', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'tz1gk3TDbU7cJuiBRMhwQXVvgDnjsxuWhcEA',
                last: 5,
                relationship_type: 'delegation_delegate',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should return operations for which this account is the contract', async () => {
        const res = await executeTestQuery(
            'transaction',
            {
                address: 'KT1HedYVpGi5N2NVTXhRY5zw6PRjf8NFy5te',
                last: 1,
                relationship_type: 'originated_contract',
            },
            accountOperationsTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should filter accounts based on address prefix', async () => {
        const res = await executeTestQuery(
            'account',
            {
                filter: {
                    address_prefixes: ['KT18aq', 'tz3ZS8y'],
                },
                first: 10,
            },
            accountTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });

    it('Should filter accounts based on address prefix and is_contract', async () => {
        const res = await executeTestQuery(
            'account',
            {
                filter: {
                    is_contract: true,
                    address_prefixes: ['KT18aq', 'tz3ZS8y'],
                },
                first: 10,
            },
            accountTestQuery,
        );
        expect(res.errors).toBeUndefined();
        expect(res.data).toMatchSnapshot();
    });
});
