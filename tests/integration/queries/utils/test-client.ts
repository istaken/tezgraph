import { ApolloServer } from 'apollo-server-express';

import { ApolloServerFactory } from '../../../../src/bootstrap/apollo-server-factory';
import { ModuleName } from '../../../../src/modules/module';
import { Names } from '../../../../src/utils/configuration/env-config';
import { createTestDIContainer } from '../../helpers';

interface TestUtils {
    executeOperation: typeof ApolloServer['prototype']['executeOperation'];
}

export function createTestGraphQLClientUtils() {
    const diContainer = createTestDIContainer({
        ...process.env,
        [Names.Modules]: ModuleName.QueriesGraphQL,
    });
    const apolloServerFactory = diContainer.resolve(ApolloServerFactory);
    let apolloServer: ApolloServer;
    const testUtils = {} as TestUtils;

    beforeAll(async () => {
        apolloServer = await apolloServerFactory.create();
        testUtils.executeOperation = apolloServer.executeOperation.bind(apolloServer);
    });

    afterAll(async () => {
        await apolloServer.stop();
    });

    return testUtils;
}
