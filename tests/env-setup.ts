import 'reflect-metadata';

import { addEnvVarsFromFile } from '../src/utils/configuration/env-vars-loader';

addEnvVarsFromFile('.env.test.local');
addEnvVarsFromFile('.env.test');
