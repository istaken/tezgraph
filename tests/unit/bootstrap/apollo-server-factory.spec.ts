import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';
import { DependencyContainer } from 'tsyringe';

import {
    ApolloServerFactory,
    CustomApolloServer,
    CustomApolloServerConfig,
} from '../../../src/bootstrap/apollo-server-factory';
import { GraphQLAuthChecker } from '../../../src/bootstrap/graphql-auth-checker';
import { GraphQLErrorHandler } from '../../../src/bootstrap/graphql-error-handler';
import { ResolverContext } from '../../../src/bootstrap/resolver-context';
import { VersionGraphQLResolver } from '../../../src/utils/app-version/version-graphql-resolver';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { Logger } from '../../../src/utils/logging';
import { UuidGenerator } from '../../../src/utils/uuid-generator';

describe(ApolloServerFactory.name, () => {
    let target: ApolloServerFactory;
    let envConfig: Writable<EnvConfig>;
    let container: DependencyContainer;
    let errorHandler: GraphQLErrorHandler;
    let authChecker: GraphQLAuthChecker;
    let uuidGenerator: UuidGenerator;
    let logger: Logger;

    beforeEach(() => {
        envConfig = {
            subscriptionKeepAliveMillis: 11,
            graphQLDepthLimit: 22,
            graphQLComplexityLimit: 33,
            enableDebug: true,
        } as EnvConfig;
        container = {} as DependencyContainer;
        errorHandler = mock(GraphQLErrorHandler);
        authChecker = mock(GraphQLAuthChecker);
        uuidGenerator = mock(UuidGenerator);
        logger = mock(Logger);

        target = new ApolloServerFactory(
            envConfig,
            [VersionGraphQLResolver],
            [],
            container,
            instance(errorHandler),
            instance(authChecker),
            instance(uuidGenerator),
            instance(logger),
        );
    });

    it('should create server correctly configured', async () => {
        const serverConfig = await act();

        expect(serverConfig.validationRules).toHaveLength(2);
        expect(serverConfig.introspection).toBe(true);
    });

    it.each([
        ['valid connection', {}],
        ['undefined connection', undefined],
    ])('should prepare context correctly with %s', async (_desc, connection) => {
        const serverConfig: any = await act();
        const originalContext = { req: {}, res: {}, connection } as unknown as ExpressContext;
        when(uuidGenerator.generate()).thenReturn('uuid');

        const finalContext: ResolverContext = serverConfig.context(originalContext);

        expect(finalContext.container).toBe(container);
        expect(finalContext.requestId).toBe('uuid');
        expect(finalContext.req).toBe(originalContext.req);
        expect(finalContext.res).toBe(originalContext.res);
        expect(finalContext.connection).toBe(connection);
    });

    it('should not add depth limit if zero configured', async () => {
        envConfig.graphQLDepthLimit = 0;

        const serverConfig = await act();

        expect(serverConfig.validationRules).toHaveLength(1);
    });

    it('should not add complexity limit if zero configured', async () => {
        envConfig.graphQLComplexityLimit = 0;

        const serverConfig = await act();

        expect(serverConfig.validationRules).toHaveLength(1);
    });

    async function act(): Promise<CustomApolloServerConfig> {
        const server = await target.create();

        expect(server).toBeInstanceOf(CustomApolloServer);
        return (server as any).config;
    }
});
