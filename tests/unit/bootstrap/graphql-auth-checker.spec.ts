import { Writable } from 'ts-essentials';

import { apiKeyHeader, GraphQLAuthChecker } from '../../../src/bootstrap/graphql-auth-checker';
import { ResolverContext } from '../../../src/bootstrap/resolver-context';
import { EnvConfig } from '../../../src/utils/configuration/env-config';
import { expectToThrow } from '../mocks';

describe(GraphQLAuthChecker.name, () => {
    let target: GraphQLAuthChecker;
    let context: ResolverContext;
    let envConfig: Writable<EnvConfig>;

    const act = () => target.check(context);

    beforeEach(() => {
        context = {} as ResolverContext;
        envConfig = { apiKey: 'lol' } as EnvConfig;
        target = new GraphQLAuthChecker(envConfig);
    });

    it('should pass if request API key matches configured one', () => {
        setupRequestApiKey('lol');

        expect(act()).toBe(true);
    });

    it.each([
        ['missing', undefined],
        ['empty', ''],
        ['not matching', 'omg'],
    ])('should fail if request API key is %s', (_desc, requestApiKey) => {
        setupRequestApiKey(requestApiKey);

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([`'${apiKeyHeader}'`, `'${requestApiKey ?? ''}'`]);
        expect(error.message).not.toInclude('lol');
    });

    it('should pass if authorization disabled', () => {
        envConfig.apiKey = null;

        expect(act()).toBe(true);
    });

    function setupRequestApiKey(apiKey: string | undefined) {
        context = { connection: { context: { [apiKeyHeader]: apiKey } } } as ResolverContext;
    }
});
