import { instance, mock, when } from 'ts-mockito';

import {
    BallotNotification,
    BlockNotification,
    Filter,
    OperationNotification,
    ProposalsNotification,
    RevealNotification,
} from '../../../src/entity/subscriptions';
import { createOperationsOnBlockResolverClass } from '../../../src/modules/subscriptions-subscriber/subscriptions/operations-on-block-resolver-factory';
import { asReadonly } from '../../../src/utils/conversion';

const TargetResolver = createOperationsOnBlockResolverClass(BallotNotification, null!);

describe(createOperationsOnBlockResolverClass.name, () => {
    let target: any;

    let ballot1: BallotNotification;
    let ballot2: BallotNotification;
    let block: BlockNotification;

    beforeEach(() => {
        target = new TargetResolver();

        ballot1 = new BallotNotification();
        ballot2 = new BallotNotification();
        block = {
            operations: asReadonly<OperationNotification>([
                ballot1,
                new RevealNotification(),
                ballot2,
                new ProposalsNotification(),
            ]),
        } as BlockNotification;
    });

    it('should expose operations of particular type', () => {
        const result = target.ballots(block);

        expect(result).toEqual([ballot1, ballot2]);
    });

    it('should apply given filter', () => {
        const filter = mock<Filter<BallotNotification>>();
        when(filter.passes(ballot1)).thenReturn(false);
        when(filter.passes(ballot2)).thenReturn(true);

        const result = target.ballots(block, instance(filter));

        expect(result).toEqual([ballot2]);
    });
});
