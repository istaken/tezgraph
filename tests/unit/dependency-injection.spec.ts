import { ApolloServer } from 'apollo-server-express';
import { DependencyContainer } from 'tsyringe';

import { App } from '../../src/app';
import { ApolloServerFactory } from '../../src/bootstrap/apollo-server-factory';
import { graphQLResolversDIToken } from '../../src/bootstrap/graphql-resolver-type';
import { CompositeModuleName, ModuleName } from '../../src/modules/module';
import { Names } from '../../src/utils/configuration/env-config';
import { createTestDIContainer } from '../integration/helpers';

describe('Dependency injection', () => {
    let container: DependencyContainer;

    beforeAll(() => {
        container = createTestDIContainer({
            [Names.Modules]: [ModuleName.QueriesGraphQL, CompositeModuleName.SubscriptionsGraphQL].join(),
            [Names.DatabaseConnectionString]:
                'postgresql://username:password@8.8.8.8:5432/indexer-v9-8-mainnet?schema=c&connection_limit=50&application_name=test',
        });
    });

    it('should build GraphQL schema correctly', async () => {
        const apolloServerFactory = container.resolve(ApolloServerFactory);

        const server = await apolloServerFactory.create();

        expect(server).toBeInstanceOf(ApolloServer);
    });

    it('should resolve all app dependencies correctly', () => {
        expect(() => container.resolve(App)).not.toThrow();
    });

    it('should resolve all GraphQL resolvers', () => {
        for (const resolverType of container.resolveAll(graphQLResolversDIToken)) {
            expect(container.resolve(resolverType)).toBeInstanceOf(resolverType);
        }
    });
});
