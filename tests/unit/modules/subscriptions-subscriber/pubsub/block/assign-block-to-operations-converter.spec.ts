import { BlockNotification, OperationKind } from '../../../../../../src/entity/subscriptions';
import { AssignBlockToOperationsConverter } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/assign-block-to-operations-converter';
import { asReadonly, deepFreeze } from '../../../../../../src/utils/conversion';

describe(AssignBlockToOperationsConverter.name, () => {
    const target = new AssignBlockToOperationsConverter();

    it('should assign block to operations of cloned block', () => {
        const inputBlock = deepFreeze({
            hash: 'SomeHash',
            operations: asReadonly([{ kind: OperationKind.ballot }, { kind: OperationKind.reveal }]),
        }) as BlockNotification;

        const block = target.convert(inputBlock);

        expect(block.hash).toBe('SomeHash');
        expect(block.operations[0]?.kind).toBe(OperationKind.ballot);
        expect(block.operations[0]?.block).toBe(block);
        expect(block.operations[1]?.kind).toBe(OperationKind.reveal);
        expect(block.operations[1]?.block).toBe(block);
    });
});
