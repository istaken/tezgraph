import { AbortSignal } from 'abort-controller';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../../src/entity/subscriptions';
import { BlockPubSubProvider } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-provider';
import { AsyncIterableProvider, ExternalPubSub, externalTriggers } from '../../../../../../src/utils';

describe(BlockPubSubProvider.name, () => {
    let target: AsyncIterableProvider<BlockNotification>;
    let externalPubSub: ExternalPubSub;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        target = new BlockPubSubProvider(instance(externalPubSub));
    });

    it('should iterate blocks', () => {
        const pubSubIterable = {} as AsyncIterableIterator<BlockNotification>;
        const abortSignal = {} as AbortSignal;
        when(externalPubSub.iterate(deepEqual([externalTriggers.blocks]), abortSignal)).thenReturn(pubSubIterable);

        const iterable = target.iterate(abortSignal);

        expect(iterable).toBe(pubSubIterable);
    });
});
