import { anything, instance, mock, verify } from 'ts-mockito';

import { BlockNotification, OperationKind } from '../../../../../../src/entity/subscriptions';
import { PubSubPublisher } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/block-pubsub-processor';
import { OperationsPubSubPublisher } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/block/operations-pubsub-publisher';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { asReadonly } from '../../../../../../src/utils/conversion';
import { mockOperation } from '../../../../mocks';

describe(OperationsPubSubPublisher.name, () => {
    let target: PubSubPublisher;
    let subscriberPubSub: SubscriberPubSub;

    beforeEach(() => {
        subscriberPubSub = mock(SubscriberPubSub);
        target = new OperationsPubSubPublisher();
    });

    it('should publish block and all related items', async () => {
        const operation1 = mockOperation({ kind: OperationKind.ballot });
        const operation2 = mockOperation({ kind: OperationKind.reveal });
        const block = { operations: asReadonly([operation1, operation2]) } as BlockNotification;

        await target.publish(block, instance(subscriberPubSub));

        verify(subscriberPubSub.publish(anything(), anything())).times(2);
        verify(subscriberPubSub.publish(subscriberTriggers.blockOperations[OperationKind.ballot], operation1)).once();
        verify(subscriberPubSub.publish(subscriberTriggers.blockOperations[OperationKind.reveal], operation2)).once();
    });
});
