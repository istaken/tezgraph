import { AbortSignal } from 'abort-controller';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import { MempoolOperationGroup } from '../../../../../../src/entity/subscriptions';
import { MempoolPubSubProvider } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-pubsub-provider';
import { AsyncIterableProvider, ExternalPubSub, externalTriggers } from '../../../../../../src/utils';

describe(MempoolPubSubProvider.name, () => {
    let target: AsyncIterableProvider<MempoolOperationGroup>;
    let externalPubSub: ExternalPubSub;

    beforeEach(() => {
        externalPubSub = mock(ExternalPubSub);
        target = new MempoolPubSubProvider(instance(externalPubSub));
    });

    it('should iterate mempool operation groups', () => {
        const pubSubIterable = {} as AsyncIterableIterator<MempoolOperationGroup>;
        const abortSignal = {} as AbortSignal;
        when(externalPubSub.iterate(deepEqual([externalTriggers.mempoolOperationGroups]), abortSignal)).thenReturn(
            pubSubIterable,
        );

        const iterable = target.iterate(abortSignal);

        expect(iterable).toBe(pubSubIterable);
    });
});
