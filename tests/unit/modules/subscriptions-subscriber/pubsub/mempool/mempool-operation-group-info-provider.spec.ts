import { MempoolOperationGroup, OperationNotification } from '../../../../../../src/entity/subscriptions';
import { MempoolOperationGroupInfoProvider } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/mempool/mempool-operation-group-info-provider';

describe(MempoolOperationGroupInfoProvider.name, () => {
    const target = new MempoolOperationGroupInfoProvider();

    it('should get info about mempool operation group', () => {
        const group: MempoolOperationGroup = [
            { operation_group: { signature: 's1' } } as OperationNotification,
            { operation_group: { signature: 's2' } } as OperationNotification,
        ];

        const info = target.getInfo(group);

        expect(info).toEqual({
            signature: 's1',
            operationCount: 2,
        });
    });
});
