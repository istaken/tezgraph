import { BlockHeaderResponse } from '@taquito/rpc';
import { capture, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification } from '../../../../../src/entity/subscriptions';
import { HeadBlockProvider } from '../../../../../src/modules/subscriptions-subscriber/helpers/head-block-provider';
import { TezosRpcClient } from '../../../../../src/rpc/tezos-rpc-client';
import { ExternalPubSub, externalTriggers } from '../../../../../src/utils';

describe(HeadBlockProvider.name, () => {
    let target: HeadBlockProvider;
    let pubSub: ExternalPubSub;
    let rpcClient: TezosRpcClient;

    beforeEach(() => {
        pubSub = mock(ExternalPubSub);
        rpcClient = mock(TezosRpcClient);
        target = new HeadBlockProvider(instance(pubSub), instance(rpcClient));
    });

    it(`should provide last monitor header`, async () => {
        const block = { hash: 'hh', header: { level: 66 } } as BlockNotification;
        const onNewBlock = capture((_t: any, x: (b: BlockNotification) => void) =>
            pubSub.subscribe(externalTriggers.blocks, x),
        ).last()[1];

        onNewBlock(block);
        const header = await target.getHeader();

        expect(header).toEqual({ hash: 'hh', level: 66 });
        verify(rpcClient.getHeadBlockHeader()).never();
    });

    it(`should get block from RPC if nothing from monitor yet`, async () => {
        const rpcHeader = { hash: 'aa', level: 77 } as BlockHeaderResponse;
        when(rpcClient.getHeadBlockHeader()).thenResolve(rpcHeader);

        const header = await target.getHeader();

        expect(header).toBe(rpcHeader);
        expect(await target.getHeader()).toBe(header); // Should be cached.
        verify(rpcClient.getHeadBlockHeader()).once();
    });
});
