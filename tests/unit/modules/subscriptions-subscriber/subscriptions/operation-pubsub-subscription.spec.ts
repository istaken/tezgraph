import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import {
    EndorsementNotification,
    OperationArgs,
    OperationKind,
    OperationNotification,
} from '../../../../../src/entity/subscriptions';
import {
    SubscriberPubSub,
    subscriberTriggers,
} from '../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import {
    OperationSubscription,
    OperationSubscriptionOptions,
    PubSubOperationSubscription,
} from '../../../../../src/modules/subscriptions-subscriber/subscriptions/operation-pubsub-subscription';
import { PubSubTrigger } from '../../../../../src/utils';
import { DefaultConstructor } from '../../../../../src/utils/reflection';

describe(PubSubOperationSubscription.name, () => {
    let target: OperationSubscription;
    let pubSub: SubscriberPubSub;

    let options: Writable<OperationSubscriptionOptions>;
    let pubSubIterable: AsyncIterableIterator<EndorsementNotification>;

    beforeEach(() => {
        pubSub = mock(SubscriberPubSub);
        target = new PubSubOperationSubscription(instance(pubSub));

        options = {
            itemType: EndorsementNotification as DefaultConstructor<OperationNotification>,
            args: { includeMempool: false },
        } as OperationSubscriptionOptions;

        pubSubIterable = {} as AsyncIterableIterator<EndorsementNotification>;
        when(pubSub.iterate(anything())).thenReturn(pubSubIterable);
    });

    it('should iterate block operations of specified kind', () => {
        runTest([subscriberTriggers.blockOperations[OperationKind.endorsement]]);
    });

    it('should include mempool if requested', () => {
        options.args = { includeMempool: true } as OperationArgs<EndorsementNotification>;

        runTest([
            subscriberTriggers.blockOperations[OperationKind.endorsement],
            subscriberTriggers.mempoolOperations[OperationKind.endorsement],
        ]);
    });

    function runTest(expectedTriggers: PubSubTrigger<OperationNotification>[]) {
        const iterable = target.subscribe(options);

        expect(iterable).toBe(pubSubIterable);
        verify(pubSub.iterate(deepEqual(expectedTriggers))).once();
    }
});
