import { instance, mock, when } from 'ts-mockito';

import { SubscriberPubSub } from '../../../../../../src/modules/subscriptions-subscriber/pubsub/subscriber-pub-sub';
import { PubSubSubscription } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/pubsub-subscription';
import { PubSubTrigger } from '../../../../../../src/utils';

describe(PubSubSubscription.name, () => {
    let pubSub: SubscriberPubSub;
    let triggers: PubSubTrigger<string>[];
    let target: PubSubSubscription<string>;

    beforeEach(() => {
        pubSub = mock(SubscriberPubSub);
        triggers = [new PubSubTrigger<string>('FOOS'), new PubSubTrigger<string>('BARS')];
        target = new PubSubSubscription<string>(instance(pubSub), triggers);
    });

    it('should iterate given triggers', () => {
        const pubSubIterable = {} as AsyncIterableIterator<string>;
        when(pubSub.iterate(triggers)).thenReturn(pubSubIterable);

        const iterable = target.subscribe(null!);

        expect(iterable).toBe(pubSubIterable);
    });
});
