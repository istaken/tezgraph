import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification, ReplayFromBlockArgs } from '../../../../../../src/entity/subscriptions';
import { FromBlockExtractor } from '../../../../../../src/modules/subscriptions-subscriber/from-block-extractors/extractor';
import { ReplayBlockProvider } from '../../../../../../src/modules/subscriptions-subscriber/helpers/replay-block-provider';
import { ReplayFromBlockSubscription } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/replay-from-block-subscription';
import {
    Subscription,
    SubscriptionOptions,
} from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/subscription';
import { iterableToArray, toAsyncIterable } from '../../../../../../src/utils/collections/async-iterable-utils';
import { LogLevel } from '../../../../../../src/utils/logging';
import { TestLogger } from '../../../../mocks';

class Foo {}

describe(ReplayFromBlockSubscription.name, () => {
    let target: Subscription<string, ReplayFromBlockArgs>;
    let innerSubscription: Subscription<string, ReplayFromBlockArgs>;
    let itemExtractor: FromBlockExtractor<string>;
    let replayBlockProvider: ReplayBlockProvider;
    let logger: TestLogger;

    let options: Writable<SubscriptionOptions<string, ReplayFromBlockArgs>>;

    const act = async () => iterableToArray(target.subscribe(options));

    beforeEach(() => {
        innerSubscription = mock<Subscription<string, ReplayFromBlockArgs>>();
        itemExtractor = mock<FromBlockExtractor<string>>();
        replayBlockProvider = mock(ReplayBlockProvider);
        logger = new TestLogger();
        target = new ReplayFromBlockSubscription(
            instance(replayBlockProvider),
            logger,
            instance(itemExtractor),
            instance(innerSubscription),
        );

        options = {
            args: {},
            itemType: Foo,
            context: { requestId: 'req' },
        } as SubscriptionOptions<string, ReplayFromBlockArgs>;
    });

    it(`should replay from specified block`, async () => {
        options.args = { replayFromBlockLevel: 66 } as ReplayFromBlockArgs;
        const block1 = { header: { level: 100 } } as BlockNotification;
        const block2 = { header: { level: 101 } } as BlockNotification;
        when(replayBlockProvider.iterateFrom(66, 'req')).thenReturn(toAsyncIterable([block1, block2]));
        when(itemExtractor.extract(block1)).thenReturn(['history1.1', 'history1.2']);
        when(itemExtractor.extract(block2)).thenReturn(['history2']);
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(['fresh1', 'fresh2']));

        const items = await act();

        expect(items).toEqual(['history1.1', 'history1.2', 'history2', 'fresh1', 'fresh2']);

        const requestId = options.context.requestId;
        const itemType = 'Foo';
        logger.verifyLoggedCount(5);
        logger.logged(0).verify(LogLevel.Information, { blockLevel: 66, itemType, requestId });
        logger.logged(1).verify(LogLevel.Debug, { item: 'history1.1', blockLevel: 100, itemType, requestId });
        logger.logged(2).verify(LogLevel.Debug, { item: 'history1.2', blockLevel: 100, itemType, requestId });
        logger.logged(3).verify(LogLevel.Debug, { item: 'history2', blockLevel: 101, itemType, requestId });
        logger.logged(4).verify(LogLevel.Debug, { itemType, requestId });
    });

    it(`should iterate fresh blocks if no replay block specified`, async () => {
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(['fresh1', 'fresh2']));

        const items = await act();

        expect(items).toEqual(['fresh1', 'fresh2']);
        verify(replayBlockProvider.iterateFrom(anything(), anything())).never();
        logger.verifyNothingLogged();
    });
});
