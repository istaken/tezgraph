import { Writable } from 'ts-essentials';

import { ResolverContext } from '../../../../../../src/bootstrap/resolver-context';
import { LoggingSubscriptionListener } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/logging-subscription-listener';
import { SubscriptionOptions } from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/subscription';
import { LogLevel } from '../../../../../../src/utils/logging';
import { nameof } from '../../../../../../src/utils/reflection';
import { TestLogger } from '../../../../mocks';

class Foo {
    value: string | undefined;
}

describe(LoggingSubscriptionListener.name, () => {
    let target: LoggingSubscriptionListener<Foo, object>;
    let logger: TestLogger;

    let options: Writable<SubscriptionOptions<Foo, object>>;

    beforeEach(() => {
        logger = new TestLogger();
        target = new LoggingSubscriptionListener(logger);

        options = {
            subscriptionName: 'fooAdded',
            context: {
                requestId: 'rr',
                connection: {
                    query: 'qq',
                    variables: { ['var' as string]: 123 as any },
                },
            } as ResolverContext,
            itemType: Foo,
            args: { filter: 'ff' },
        };
    });

    describe(nameof<LoggingSubscriptionListener<Foo, object>>('onConnectionOpen'), () => {
        it('should log subscription info', () => {
            target.onConnectionOpen(options);

            logger.loggedSingle().verify(LogLevel.Information, {
                subscription: 'fooAdded',
                itemType: 'Foo',
                requestId: 'rr',
                args: { filter: 'ff' },
                rawQuery: 'qq',
                rawQueryVariables: { var: 123 },
            });
        });

        it('should handle no connection', () => {
            options.context = {} as ResolverContext;

            target.onConnectionOpen(options);

            expect(logger.loggedSingle().data).toContainEntries([
                ['rawQuery', undefined],
                ['rawQueryVariables', undefined],
            ]);
        });
    });

    describe(nameof<LoggingSubscriptionListener<Foo, object>>('onConnectionOpen'), () => {
        it('should log subscription basics', () => {
            target.onConnectionClose(options);

            logger.loggedSingle().verify(LogLevel.Information, {
                subscription: 'fooAdded',
                requestId: 'rr',
            });
        });
    });
});
