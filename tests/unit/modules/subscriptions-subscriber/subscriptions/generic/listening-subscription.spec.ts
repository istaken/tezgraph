import { anything, instance, mock, verify, when } from 'ts-mockito';

import {
    ListeningSubscription,
    SubscriptionEventsListener,
} from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/generic/listening-subscription';
import {
    Subscription,
    SubscriptionOptions,
} from '../../../../../../src/modules/subscriptions-subscriber/subscriptions/subscription';
import { iterableToArray, toAsyncIterable } from '../../../../../../src/utils/collections/async-iterable-utils';
import { expectToThrowAsync } from '../../../../mocks';

describe(ListeningSubscription.name, () => {
    let target: Subscription<string, object>;
    let listeners: SubscriptionEventsListener<string, object>[];
    let innerSubscription: Subscription<string, object>;

    let options: SubscriptionOptions<string, object>;

    beforeEach(() => {
        innerSubscription = mock<Subscription<string, object>>();
        listeners = [
            mock<SubscriptionEventsListener<string, object>>(),
            mock<SubscriptionEventsListener<string, object>>(),
        ];
        target = new ListeningSubscription(listeners.map(instance), instance(innerSubscription));

        options = { subscriptionName: 'fooAdded' } as SubscriptionOptions<string, object>;
        when(innerSubscription.subscribe(options)).thenReturn(toAsyncIterable(['lol', 'wtf']));
    });

    it('should invoke listeners before subscribed', () => {
        target.subscribe(options);

        for (const listener of listeners) {
            verify(listener.onConnectionOpen!(options)).once();
            verify(listener.onItemPublish!(anything(), anything())).never();
            verify(listener.onConnectionClose!(anything())).never();
        }
    });

    it('should invoke listeners for each published item', async () => {
        const items = await iterableToArray(target.subscribe(options));

        expect(items).toEqual(['lol', 'wtf']);
        for (const listener of listeners) {
            verify(listener.onConnectionOpen!(options)).once();
            verify(listener.onItemPublish!('lol', options)).once();
            verify(listener.onItemPublish!('wtf', options)).once();
            verify(listener.onConnectionClose!(anything())).never();
        }
    });

    it('should log subscription close if iterator.return()', async () => {
        const iterable = target.subscribe(options);

        await iterable.return!();

        verifyListenersOnCloseInvoked();
    });

    it('should log subscription closing if iterator.throw()', async () => {
        const iterable = target.subscribe(options);
        const testError = new Error('lol');

        const error = await expectToThrowAsync(async () => iterable.throw!(testError));

        expect(error).toBe(testError);
        verifyListenersOnCloseInvoked();
    });

    function verifyListenersOnCloseInvoked() {
        for (const listener of listeners) {
            verify(listener.onConnectionOpen!(options)).once();
            verify(listener.onItemPublish!(anything(), anything())).never();
            verify(listener.onConnectionClose!(options)).once();
        }
    }
});
