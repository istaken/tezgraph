import { BlockNotification, OperationKind } from '../../../../../src/entity/subscriptions';
import { OperationExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/operation-extractor';
import { asReadonly } from '../../../../../src/utils/conversion';
import { mockOperation } from '../../../mocks';

describe(OperationExtractor.name, () => {
    it('should extract operations of particular kind', () => {
        const op1 = mockOperation({ kind: OperationKind.ballot });
        const op2 = mockOperation({ kind: OperationKind.reveal });
        const op3 = mockOperation({ kind: OperationKind.ballot });
        const op4 = mockOperation({ kind: OperationKind.proposals });

        const block = { operations: asReadonly([op1, op2, op3, op4]) } as BlockNotification;
        const target = new OperationExtractor(OperationKind.ballot);

        const results = Array.from(target.extract(block));

        expect(results).toEqual([op1, op3]);
    });
});
