import { difference } from 'lodash';

import {
    BigMapDiff,
    ContractResultParent,
    DelegationNotification,
    InternalOperationKind,
    InternalOperationResult,
    OperationKind,
    OriginationNotification,
    RevealNotification,
    TransactionNotification,
} from '../../../../../src/entity/subscriptions';
import { BigMapDiffExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/big-map-diff-extractor';
import { asReadonly } from '../../../../../src/utils/conversion';
import { getEnumValues } from '../../../../../src/utils/reflection';
import { mockBigMapDiff } from '../../../mocks';
import {
    describeOperationsWithoutRespectiveNotifications,
    itShouldReturnEmptyIfNullMetadata,
    itShouldReturnEmptyIfNullRespectiveProperties,
    runTest,
} from './extractor-test-helper';

describe(BigMapDiffExtractor.name, () => {
    const target = new BigMapDiffExtractor();

    let diff1: BigMapDiff;
    let diff2: BigMapDiff;
    let diff3: BigMapDiff;
    let diff4: BigMapDiff;
    let diff5: BigMapDiff;

    let internalParent1: ContractResultParent;
    let internalParent2: ContractResultParent;
    let internalResults: readonly InternalOperationResult[];

    beforeEach(() => {
        diff1 = mockBigMapDiff();
        diff2 = mockBigMapDiff();
        diff3 = mockBigMapDiff();
        diff4 = mockBigMapDiff();
        diff5 = mockBigMapDiff();

        internalParent1 = {
            kind: InternalOperationKind.transaction,
            result: { big_map_diff: asReadonly([diff1, diff2]) },
        } as ContractResultParent;
        internalParent2 = {
            kind: InternalOperationKind.origination,
            result: { big_map_diff: asReadonly([diff3]) },
        } as ContractResultParent;
        internalResults = [
            internalParent1,
            { kind: InternalOperationKind.transaction, result: {} }, // Undefined diffs.
            internalParent2,
            { kind: InternalOperationKind.reveal, result: {} }, // Unrelated operation.
        ] as InternalOperationResult[];
    });

    const kindsWithInternalResults = [OperationKind.delegation, OperationKind.reveal];
    type OperationWithInternalResults = DelegationNotification | RevealNotification;

    describe.each(kindsWithInternalResults)('%s', (kind) => {
        it('should extract diffs from internal results', () => {
            const operation = mockOperation(internalResults);
            runTest(target, operation, [
                { big_map_diff: diff1, parent: internalParent1, operation },
                { big_map_diff: diff2, parent: internalParent1, operation },
                { big_map_diff: diff3, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(internal_operation_results?: readonly InternalOperationResult[]) {
            return {
                kind,
                metadata: { internal_operation_results },
            } as OperationWithInternalResults;
        }
    });

    const kindsWithResults = [OperationKind.origination, OperationKind.transaction];
    type OperationWithResults = OriginationNotification | TransactionNotification;

    // eslint-disable-next-line jest/no-identical-title
    describe.each(kindsWithResults)('%s', (kind) => {
        it('should extract diffs from operation result and internal results', () => {
            const operation = mockOperation([diff4, diff5], internalResults);
            runTest(target, operation, [
                { big_map_diff: diff4, parent: operation, operation },
                { big_map_diff: diff5, parent: operation, operation },
                { big_map_diff: diff1, parent: internalParent1, operation },
                { big_map_diff: diff2, parent: internalParent1, operation },
                { big_map_diff: diff3, parent: internalParent2, operation },
            ]);
        });

        itShouldReturnEmptyIfNullRespectiveProperties(target, mockOperation());
        itShouldReturnEmptyIfNullMetadata(target, kind);

        function mockOperation(
            big_map_diff?: readonly BigMapDiff[],
            internal_operation_results?: readonly InternalOperationResult[],
        ) {
            return {
                kind,
                metadata: {
                    operation_result: { big_map_diff },
                    internal_operation_results,
                },
            } as OperationWithResults;
        }
    });
    describeOperationsWithoutRespectiveNotifications(
        target,
        difference(getEnumValues(OperationKind), [...kindsWithInternalResults, ...kindsWithResults]),
    );
});
