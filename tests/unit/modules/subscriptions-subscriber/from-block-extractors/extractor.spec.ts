import { spy, when } from 'ts-mockito';

import { BlockNotification, OperationNotification } from '../../../../../src/entity/subscriptions';
import { FromOperationExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/extractor';
import { asReadonly } from '../../../../../src/utils/conversion';
import { mockOperation } from '../../../mocks';

class TargetExtractor extends FromOperationExtractor<string> {
    extractFromOperation(_operation: OperationNotification): Iterable<string> {
        throw new Error('Method not implemented.');
    }
}

describe(FromOperationExtractor.name, () => {
    it('should extract from all operations', () => {
        const op1 = mockOperation();
        const op2 = mockOperation();
        const block = { operations: asReadonly([op1, op2]) } as BlockNotification;

        const target = new TargetExtractor();
        const targetSpy = spy(target);
        when(targetSpy.extractFromOperation(op1)).thenReturn(['val1.1', 'val1.2']);
        when(targetSpy.extractFromOperation(op2)).thenReturn(['val2.1', 'val2.2']);

        const results = Array.from(target.extract(block));

        expect(results).toEqual(['val1.1', 'val1.2', 'val2.1', 'val2.2']);
    });
});
