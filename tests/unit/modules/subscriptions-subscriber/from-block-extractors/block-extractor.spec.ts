import { BlockNotification } from '../../../../../src/entity/subscriptions';
import { BlockExtractor } from '../../../../../src/modules/subscriptions-subscriber/from-block-extractors/block-extractor';

describe(BlockExtractor.name, () => {
    it('should yield block itself', () => {
        const block = { hash: 'SomeHash' } as BlockNotification;
        const target = new BlockExtractor();

        const results = Array.from(target.extract(block));

        expect(results).toEqual([block]);
    });
});
