import * as rpc from '@taquito/rpc';

import {
    getRpcSignature,
    RpcMempoolOperationGroup,
} from '../../../../../src/modules/tezos-monitor/mempool/rpc-mempool-operation-group';

describe(getRpcSignature.name, () => {
    it('should get signature from first operation in the group', () => {
        const group = {
            signature: 'sig',
            contents: [{ kind: rpc.OpKind.BALLOT }],
        } as RpcMempoolOperationGroup;

        const signature = getRpcSignature(group);

        expect(signature).toBe('sig');
    });

    it('should get signature from inlined endorsement if endorsement with slot', () => {
        const group = {
            signature: 'sig',
            contents: [
                {
                    kind: rpc.OpKind.ENDORSEMENT_WITH_SLOT,
                    endorsement: { signature: 'endorsement-sig' },
                },
            ],
        } as RpcMempoolOperationGroup;

        const signature = getRpcSignature(group);

        expect(signature).toBe('endorsement-sig');
    });

    it.each([
        ['no operation', { signature: 'sig' } as RpcMempoolOperationGroup],
        ['no signature', { contents: [{ kind: rpc.OpKind.BALLOT }] } as RpcMempoolOperationGroup],
        [
            'white-space signature',
            { signature: '  ', contents: [{ kind: rpc.OpKind.BALLOT }] } as RpcMempoolOperationGroup,
        ],
    ])('should throw if %s in the group', (_desc, group) => {
        expect(() => getRpcSignature(group)).toThrow();
    });
});
