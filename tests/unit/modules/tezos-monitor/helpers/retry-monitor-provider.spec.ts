import { AbortSignal } from 'abort-controller';
import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, spy, verify, when } from 'ts-mockito';

import { MonitorDelayHelper } from '../../../../../src/modules/tezos-monitor/helpers/monitor-delay-helper';
import {
    healthMessages,
    RetryMonitorProvider,
} from '../../../../../src/modules/tezos-monitor/helpers/retry-monitor-provider';
import { AsyncIterableProvider } from '../../../../../src/utils';
import { iterableToArray, take, toAsyncIterable } from '../../../../../src/utils/collections/async-iterable-utils';
import { ComponentHealthState } from '../../../../../src/utils/health/component-health-state';
import { HealthStatus } from '../../../../../src/utils/health/health-check';
import { LogLevel } from '../../../../../src/utils/logging';
import { MetricsContainer } from '../../../../../src/utils/metrics/metrics-container';
import { expectToThrowAsync, TestAbortError, TestLogger } from '../../../mocks';

describe(RetryMonitorProvider, () => {
    let target: AsyncIterableProvider<number>;
    let innerProvider: AsyncIterableProvider<number>;
    let healthState: ComponentHealthState;
    let delayHelper: MonitorDelayHelper;
    let logger: TestLogger;
    let metricsContainer: MetricsContainer;
    let infiniteLoopsSpy: Counter<string>;
    let reconnectsSpy: Counter<string>;
    let retriesSpy: Counter<string>;
    let abortSignal: AbortSignal;

    const act = async (count: number) => iterableToArray(take(target.iterate(abortSignal), count));

    beforeEach(() => {
        innerProvider = mock<AsyncIterableProvider<number>>();
        healthState = mock(ComponentHealthState);
        delayHelper = mock(MonitorDelayHelper);
        logger = new TestLogger();
        metricsContainer = new MetricsContainer();
        target = new RetryMonitorProvider(
            instance(innerProvider),
            instance(healthState),
            instance(delayHelper),
            logger,
            metricsContainer,
        );
        infiniteLoopsSpy = spy(metricsContainer.tezosMonitorInfiniteLoopCount);
        reconnectsSpy = spy(metricsContainer.tezosMonitorReconnectCount);
        retriesSpy = spy(metricsContainer.tezosMonitorRetryCount);
        abortSignal = {} as AbortSignal;
    });

    it('should continuously iterate data', async () => {
        when(innerProvider.iterate(abortSignal)).thenReturn(toAsyncIterable([42, 43, 44]));

        const items = await act(3);

        expect(items).toEqual([42, 43, 44]);
        logger.verifyNothingLogged();

        verify(healthState.set(anything(), anything())).times(3);
        verify(healthState.set(HealthStatus.Healthy, healthMessages.healthy)).times(3);

        verify(delayHelper.wait()).never();
        verify(delayHelper.reset()).times(3);
    });

    it.each([
        [false, HealthStatus.Degraded, LogLevel.Debug, healthMessages.temporaryFailure],
        [true, HealthStatus.Unhealthy, LogLevel.Error, healthMessages.persistentFailure],
    ])(
        'should retry on failure if it is %s persistent',
        async (isPersistentError, expectedStatus, expectedLevel, expectedReason) => {
            const error = new Error('Oops');
            when(innerProvider.iterate(abortSignal))
                .thenThrow(error)
                .thenReturn(toAsyncIterable([42]));
            when(delayHelper.isInInfiniteLoop).thenReturn(isPersistentError);
            when(delayHelper.wait()).thenResolve([100, 1]);
            when(healthState.name).thenReturn('abc');

            const items = await act(1);

            expect(items).toEqual([42]);
            logger.loggedSingle().verify(expectedLevel, { error });

            verify(healthState.set(anything(), anything())).twice();
            verify(healthState.set(expectedStatus, deepEqual({ error, reason: expectedReason }))).calledBefore(
                healthState.set(HealthStatus.Healthy, healthMessages.healthy),
            );

            if (isPersistentError) {
                verify(infiniteLoopsSpy.inc()).once();
            } else {
                verify(reconnectsSpy.inc()).once();
            }

            verify(retriesSpy.inc(deepEqual({ endpoint: 'abc', delay: '100', index: '1' }))).once();

            verify(delayHelper.wait()).calledBefore(delayHelper.reset());
        },
    );

    it('should retry on connection end', async () => {
        when(innerProvider.iterate(abortSignal))
            .thenReturn(toAsyncIterable([42]))
            .thenReturn(toAsyncIterable([43]));
        when(delayHelper.wait()).thenResolve([100, 1]);

        const items = await act(2);

        expect(items).toEqual([42, 43]);
        logger.loggedSingle(LogLevel.Debug);
        expect(logger.logged(0).data?.error.toString()).toContain('ended');
    });

    it('should end on connection abort', async () => {
        const abortError = new TestAbortError();
        when(innerProvider.iterate(abortSignal)).thenThrow(abortError);

        const error = await expectToThrowAsync(async () => iterableToArray(target.iterate(abortSignal)));

        expect(error).toBe(abortError);
        logger.loggedSingle().verify(LogLevel.Information).verifyMessageIncludesAll('aborted');
    });
});
