import { anything, instance, mock, verify, when } from 'ts-mockito';

import { MonitorDelayHelper } from '../../../../../src/modules/tezos-monitor/helpers/monitor-delay-helper';
import { TezosMonitorEnvConfig } from '../../../../../src/modules/tezos-monitor/tezos-monitor-env-config';
import { asReadonly } from '../../../../../src/utils/conversion';
import { SleepHelper } from '../../../../../src/utils/time/sleep-helper';

describe(MonitorDelayHelper.name, () => {
    let sleepHelper: SleepHelper;
    let target: MonitorDelayHelper;

    beforeEach(() => {
        const envConfig = { rpcMonitorReconnectDelaysMillis: asReadonly([100, 200, 300]) } as TezosMonitorEnvConfig;
        sleepHelper = mock(SleepHelper);
        when(sleepHelper.sleep(anything())).thenResolve();
        target = new MonitorDelayHelper(envConfig, instance(sleepHelper));
    });

    it(`should wait first delay`, async () => {
        const [delay, index] = await target.wait();

        expect(target.isInInfiniteLoop).toBe(false);
        expect(delay).toBe(100);
        expect(index).toBe(1);
        verify(sleepHelper.sleep(anything())).once();
        verify(sleepHelper.sleep(100)).once();
    });

    it(`should wait third delay`, async () => {
        await target.wait();
        await target.wait();
        const [delay, index] = await target.wait();

        expect(target.isInInfiniteLoop).toBe(true);
        expect(delay).toBe(300);
        expect(index).toBe(3);
        verify(sleepHelper.sleep(anything())).times(3);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
        verify(sleepHelper.sleep(200)).calledBefore(sleepHelper.sleep(300));
    });

    it(`should wait last delay`, async () => {
        await target.wait();
        await target.wait();
        await target.wait();
        await target.wait();
        const [delay, index] = await target.wait();

        expect(target.isInInfiniteLoop).toBe(true);
        expect(delay).toBe(300);
        expect(index).toBe(5);
        verify(sleepHelper.sleep(anything())).times(5);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
        verify(sleepHelper.sleep(200)).calledBefore(sleepHelper.sleep(300));
        verify(sleepHelper.sleep(300)).times(3);
    });

    it(`should wait second delay after reset`, async () => {
        await target.wait();
        await target.wait();
        target.reset();
        await target.wait();
        const [delay, index] = await target.wait();

        expect(target.isInInfiniteLoop).toBe(false);
        expect(delay).toBe(200);
        expect(index).toBe(2);
        verify(sleepHelper.sleep(anything())).times(4);
        verify(sleepHelper.sleep(100)).times(2);
        verify(sleepHelper.sleep(200)).times(2);
        verify(sleepHelper.sleep(100)).calledBefore(sleepHelper.sleep(200));
    });
});
