import { deserialize, serialize } from '../../../../src/modules/redis-pubsub/redis-pubsub-serializer';

describe('RedisPubSubSerializer', () => {
    it('should serialize and deserialize object with various types correctly', () => {
        const input = {
            str: 'Hello TezGraph',
            num: 123,
            bigNum: BigInt(456),
            numArray: [1, 2],
            bigNumArray: [BigInt(4), BigInt(5)],
            date: new Date('2021-01-21T10:36:55.045Z'),
            dateStr: '2020-03-11T13:35:45.144Z',
            dateArray: [new Date('2031-01-21T10:36:55.045Z')],
            dateStrArray: ['2040-03-11T13:35:45.144Z'],
            nested: { meme: 'omg' },
            primitiveArray: [1, 2],
            objArray: [{ name: 'James Bond' }],
        };

        const json = serialize(input);
        const output = deserialize(json);

        expect(() => JSON.parse(json)).not.toThrow();
        expect(output).toEqual(input);
    });
});
