import * as rpc from '@taquito/rpc';

import { TransactionParameters } from '../../../../../src/entity/common/transaction';
import { TransactionParametersConverter } from '../../../../../src/rpc/converters/common';
import { mockMichelson } from '../../../mocks';

describe(TransactionParametersConverter.name, () => {
    const target = new TransactionParametersConverter();

    it('should convert correctly', () => {
        const rpcParams: rpc.TransactionOperationParameter = {
            entrypoint: 'ee',
            value: mockMichelson(),
        };

        const params = target.convert(rpcParams);

        expect(params).toEqual<TransactionParameters>(rpcParams);
    });
});
