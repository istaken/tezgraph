import * as rpc from '@taquito/rpc';

import {
    LazyBigMapAlloc,
    LazyBigMapCopy,
    LazyBigMapDiff,
    LazyBigMapRemove,
    LazyBigMapStorageDiff,
    LazyBigMapUpdate,
    LazyBigMapUpdateItem,
    LazySaplingStateAlloc,
    LazySaplingStateCopy,
    LazySaplingStateDiff,
    LazySaplingStateDiffUpdates,
    LazySaplingStateRemove,
    LazySaplingStateStorageDiff,
    LazySaplingStateUpdate,
    LazyStorageDiff,
    LazyStorageDiffAction,
    LazyStorageDiffKind,
} from '../../../../../src/entity/subscriptions';
import { LazyStorageDiffConverter } from '../../../../../src/rpc/converters/common';
import { typed } from '../../../../../src/utils/reflection';

describe(LazyStorageDiffConverter.name, () => {
    const target = new LazyStorageDiffConverter();

    describe(LazyStorageDiffKind.big_map, () => {
        itShouldConvertBigMap({
            input: {
                action: LazyStorageDiffAction.alloc,
                key_type: { prim: 'kt' } as rpc.MichelsonV1Expression,
                value_type: { prim: 'vt' } as rpc.MichelsonV1Expression,
                updates: getBigMapUpdates(),
            },
            expected: typed<LazyBigMapAlloc>({
                graphQLTypeName: LazyBigMapAlloc.name,
                action: LazyStorageDiffAction.alloc,
                key_type: { prim: 'kt' } as rpc.MichelsonV1Expression,
                value_type: { prim: 'vt' } as rpc.MichelsonV1Expression,
                updates: getBigMapUpdates(),
            }),
        });

        itShouldConvertBigMap({
            input: {
                action: LazyStorageDiffAction.copy,
                source: '2211',
                updates: getBigMapUpdates(),
            },
            expected: typed<LazyBigMapCopy>({
                graphQLTypeName: LazyBigMapCopy.name,
                action: LazyStorageDiffAction.copy,
                source: BigInt('2211'),
                updates: getBigMapUpdates(),
            }),
        });

        itShouldConvertBigMap({
            input: { action: LazyStorageDiffAction.remove },
            expected: typed<LazyBigMapRemove>({
                graphQLTypeName: LazyBigMapRemove.name,
                action: LazyStorageDiffAction.remove,
            }),
        });

        itShouldConvertBigMap({
            input: {
                action: LazyStorageDiffAction.update,
                updates: getBigMapUpdates(),
            },
            expected: typed<LazyBigMapUpdate>({
                graphQLTypeName: LazyBigMapUpdate.name,
                action: LazyStorageDiffAction.update,
                updates: getBigMapUpdates(),
            }),
        });

        function getBigMapUpdates(): (LazyBigMapUpdateItem & rpc.LazyStorageDiffUpdatesBigMap)[] {
            return [
                { key_hash: 'kh1', key: { prim: 'k1' }, value: { prim: 'v1' } },
                { key_hash: 'kh2', key: { prim: 'k2' }, value: { prim: 'v2' } },
            ];
        }

        function itShouldConvertBigMap(options: { input: rpc.LazyStorageDiffBigMapItems; expected: LazyBigMapDiff }) {
            itShouldConvert({
                input: {
                    kind: LazyStorageDiffKind.big_map,
                    id: '11',
                    diff: options.input,
                },
                expected: typed<LazyBigMapStorageDiff>({
                    graphQLTypeName: LazyBigMapStorageDiff.name,
                    kind: LazyStorageDiffKind.big_map,
                    id: BigInt('11'),
                    big_map_diff: options.expected,
                }),
            });
        }
    });

    describe(LazyStorageDiffKind.sapling_state, () => {
        itShouldConvertSaplingState({
            input: {
                action: LazyStorageDiffAction.alloc,
                memo_size: 5511,
                updates: getRpcSaplingStateUpdates(),
            },
            expected: typed<LazySaplingStateAlloc>({
                graphQLTypeName: LazySaplingStateAlloc.name,
                action: LazyStorageDiffAction.alloc,
                memo_size: 5511,
                updates: getSaplingStateUpdates(),
            }),
        });

        itShouldConvertSaplingState({
            input: {
                action: LazyStorageDiffAction.copy,
                source: '6611',
                updates: getRpcSaplingStateUpdates(),
            },
            expected: typed<LazySaplingStateCopy>({
                graphQLTypeName: LazySaplingStateCopy.name,
                action: LazyStorageDiffAction.copy,
                source: BigInt(6611),
                updates: getSaplingStateUpdates(),
            }),
        });

        itShouldConvertSaplingState({
            input: { action: LazyStorageDiffAction.remove },
            expected: typed<LazySaplingStateRemove>({
                graphQLTypeName: LazySaplingStateRemove.name,
                action: LazyStorageDiffAction.remove,
            }),
        });

        itShouldConvertSaplingState({
            input: {
                action: LazyStorageDiffAction.update,
                updates: getRpcSaplingStateUpdates(),
            },
            expected: typed<LazySaplingStateUpdate>({
                graphQLTypeName: LazySaplingStateUpdate.name,
                action: LazyStorageDiffAction.update,
                updates: getSaplingStateUpdates(),
            }),
        });

        function getRpcSaplingStateUpdates(): rpc.LazyStorageDiffUpdatesSaplingState {
            return {
                commitments_and_ciphertexts: [
                    [
                        'cc1',
                        {
                            cv: 'cv1',
                            epk: 'epk1',
                            payload_enc: 'p_enc1',
                            nonce_enc: 'n_enc1',
                            payload_out: 'p_out1',
                            nonce_out: 'n_out1',
                        },
                    ],
                ],
                nullifiers: ['n1', 'n2'],
            };
        }

        function getSaplingStateUpdates(): LazySaplingStateDiffUpdates {
            return {
                commitments_and_ciphertexts: [
                    {
                        commitment: 'cc1',
                        ciphertext: {
                            cv: 'cv1',
                            epk: 'epk1',
                            payload_enc: 'p_enc1',
                            nonce_enc: 'n_enc1',
                            payload_out: 'p_out1',
                            nonce_out: 'n_out1',
                        },
                    },
                ],
                nullifiers: ['n1', 'n2'],
            };
        }

        function itShouldConvertSaplingState(options: {
            input: rpc.LazyStorageDiffSaplingStateItems;
            expected: LazySaplingStateDiff;
        }) {
            itShouldConvert({
                input: {
                    kind: LazyStorageDiffKind.sapling_state,
                    id: '22',
                    diff: options.input,
                },
                expected: typed<LazySaplingStateStorageDiff>({
                    graphQLTypeName: LazySaplingStateStorageDiff.name,
                    kind: LazyStorageDiffKind.sapling_state,
                    id: BigInt('22'),
                    sapling_state_diff: options.expected,
                }),
            });
        }
    });

    function itShouldConvert(options: { input: rpc.LazyStorageDiff; expected: LazyStorageDiff }) {
        it(`should convert ${options.input.kind} ${options.input.diff.action} correctly`, () => {
            const diff = target.convert(options.input);

            expect(diff).toEqual(options.expected);
        });
    }
});
