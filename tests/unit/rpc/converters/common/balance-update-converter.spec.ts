import * as rpc from '@taquito/rpc';

import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
} from '../../../../../src/entity/subscriptions';
import {
    BalanceUpdateConverter,
    RpcBalanceUpdate,
} from '../../../../../src/rpc/converters/common/balance-update-converter';

describe(BalanceUpdateConverter.name, () => {
    const target = new BalanceUpdateConverter();

    it('should convert correctly', () => {
        const input: RpcBalanceUpdate = {
            kind: 'contract',
            change: '222',
            origin: 'block',
            contract: 'ccc',
            category: rpc.METADATA_BALANCE_UPDATES_CATEGORY.BLOCK_FEES,
            delegate: 'ddd',
            cycle: 111,
        };

        const update = target.convert(input);

        expect(update).toEqual<BalanceUpdate>({
            kind: BalanceUpdateKind.contract,
            change: BigInt(222),
            origin: BalanceUpdateOrigin.block,
            contract: 'ccc',
            category: BalanceUpdateCategory.block_fees,
            delegate: 'ddd',
            cycle: 111,
        });
    });

    it('should convert correctly if nulls', () => {
        const input: RpcBalanceUpdate = {
            kind: 'accumulator',
            change: '222',
            origin: 'migration',
        };

        const update = target.convert(input);

        expect(update).toEqual<BalanceUpdate>({
            kind: BalanceUpdateKind.accumulator,
            change: BigInt(222),
            origin: BalanceUpdateOrigin.migration,
            contract: null,
            category: null,
            delegate: null,
            cycle: null,
        });
    });
});
