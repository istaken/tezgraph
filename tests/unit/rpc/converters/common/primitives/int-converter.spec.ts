import { IntConverter } from '../../../../../../src/rpc/converters/common/primitives/int-converter';
import { expectToThrow } from '../../../../mocks';

describe(IntConverter.name, () => {
    const target = new IntConverter();

    it.each([-456, 0, 123])('should convert %s to int', (input) => {
        const result = target.convert(input);

        expect(result).toBe(input);
    });

    it.each([
        null,
        undefined,
        1.23, // Float.
        '123', // Int but inside a string.
        '',
    ])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
