import { MichelsonConverter } from '../../../../../../src/rpc/converters/common/primitives/michelson-converter';
import { expectToThrow } from '../../../../mocks';

describe(MichelsonConverter.name, () => {
    const target = new MichelsonConverter();

    it.each([{ prim: 'obj' }, [{ prim: 'array' }]])('should convert %s to int', (input) => {
        const result = target.convert(input);

        expect(result).toBe(input);
    });

    it.each([null, undefined, 'abc', new Date()])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
