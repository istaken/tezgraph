import { BooleanConverter } from '../../../../../../src/rpc/converters/common/primitives/boolean-converter';
import { expectToThrow } from '../../../../mocks';

describe(BooleanConverter.name, () => {
    const target = new BooleanConverter();

    it.each([true, false])('should guard %s value', (input) => {
        const result = target.convert(input);

        expect(result).toBe(input);
    });

    it.each([
        null,
        undefined,
        0, // Falsy.
        '', // Falsy.
        1,
        'true',
        'false',
    ])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
