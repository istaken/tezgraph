import { StringConverter } from '../../../../../../src/rpc/converters/common/primitives/string-converter';
import { expectToThrow } from '../../../../mocks';

describe(StringConverter.name, () => {
    const target = new StringConverter();

    it.each([
        '',
        '  ', // White-space.
        'abc',
    ])('should convert %s correctly', (input) => {
        const result = target.convert(input);

        expect(result).toBe(input);
    });

    it.each([null, undefined, 0, false])('should throw if %s', (input: any) => {
        expectToThrow(() => target.convert(input));
    });
});
