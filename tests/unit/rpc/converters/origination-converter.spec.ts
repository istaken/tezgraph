import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind, OriginationNotification } from '../../../../src/entity/subscriptions';
import { OriginationNotificationMetadata } from '../../../../src/entity/subscriptions/operations/origination/origination-notification';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { ScriptedContractsConverter } from '../../../../src/rpc/converters/common/scripted-contracts-converter';
import { OriginationResultConverter } from '../../../../src/rpc/converters/operation-results/origination-result-converter';
import { OriginationConverter, RpcOrigination } from '../../../../src/rpc/converters/origination-converter';
import { mockScriptedContracts } from '../../mocks';
import { mockBaseProperties } from './rpc-mocks';

describe(OriginationConverter.name, () => {
    let target: OriginationConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: OriginationResultConverter;
    let scriptConverter: ScriptedContractsConverter;

    const act = (o: RpcOrigination) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(OriginationResultConverter);
        scriptConverter = mock(ScriptedContractsConverter);
        target = new OriginationConverter(instance(metadataConverter), resultConverter, instance(scriptConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcOrigination = {
            kind: rpc.OpKind.ORIGINATION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            balance: '555',
            delegate: 'ddd',
            script: 'rpcScriptedContracts' as any,
            metadata: null!,
        };
        const metadata: OriginationNotificationMetadata = 'mockedMetadata' as any;
        const script = mockScriptedContracts();
        when(metadataConverter.convert(rpcOperation, resultConverter, OriginationNotificationMetadata.name)).thenReturn(
            metadata,
        );
        when(scriptConverter.convertToNonNullable(rpcOperation.script)).thenReturn(script);

        const operation = act(rpcOperation);

        expect(operation).toEqual<OriginationNotification>({
            graphQLTypeName: OriginationNotification.name,
            kind: OperationKind.origination,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            balance: BigInt(555),
            delegate: 'ddd',
            script,
            ...mockBaseProperties(),
            metadata,
        });
    });
});
