import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OperationKind, RevealNotification, RevealNotificationMetadata } from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { RevealResultConverter } from '../../../../src/rpc/converters/operation-results/reveal-result-converter';
import { RevealConverter } from '../../../../src/rpc/converters/reveal-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(RevealConverter.name, () => {
    let target: RevealConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: RevealResultConverter;
    let rpcOperation: rpc.OperationContentsAndResultReveal;

    const act = () => target.convert(rpcOperation, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(RevealResultConverter);
        target = new RevealConverter(instance(metadataConverter), resultConverter);
    });

    it(`should convert values correctly`, () => {
        rpcOperation = {
            kind: rpc.OpKind.REVEAL,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            public_key: 'kkk',
            metadata: null!,
        };
        const metadata: RevealNotificationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convert(rpcOperation, resultConverter, RevealNotificationMetadata.name)).thenReturn(
            metadata,
        );

        const operation = act();

        expect(operation).toEqual<RevealNotification>({
            graphQLTypeName: RevealNotification.name,
            kind: OperationKind.reveal,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            public_key: 'kkk',
            ...mockBaseProperties(),
            metadata,
        });
    });
});
