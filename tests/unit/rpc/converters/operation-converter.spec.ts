import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { ActivateAccountConverter } from '../../../../src/rpc/converters/activate-account-converter';
import { BallotConverter } from '../../../../src/rpc/converters/ballot-converter';
import { DelegationConverter } from '../../../../src/rpc/converters/delegation-converter';
import { DoubleBakingEvidenceConverter } from '../../../../src/rpc/converters/double-baking-evidence-converter';
import { DoubleEndorsementEvidenceConverter } from '../../../../src/rpc/converters/double-endorsement-evidence-converter';
import { DoublePreendorsementEvidenceConverter } from '../../../../src/rpc/converters/double-preendorsement-evidence-converter';
import { EndorsementConverter } from '../../../../src/rpc/converters/endorsement-converter';
import { EndorsementWithSlotConverter } from '../../../../src/rpc/converters/endorsement-with-slot-converter';
import {
    BaseOperationProperties,
    OperationConverter,
    RpcOperation,
} from '../../../../src/rpc/converters/operation-converter';
import { OriginationConverter } from '../../../../src/rpc/converters/origination-converter';
import { PreendorsementConverter } from '../../../../src/rpc/converters/preendorsement-converter';
import { ProposalsConverter } from '../../../../src/rpc/converters/proposals-converter';
import { RegisterGlobalConstantConverter } from '../../../../src/rpc/converters/register-global-constant-converter';
import { RevealConverter } from '../../../../src/rpc/converters/reveal-converter';
import { SeedNonceRevelationConverter } from '../../../../src/rpc/converters/seed-nonce-revelation-converter';
import { SetDepositsLimitConverter } from '../../../../src/rpc/converters/set-deposits-limit-converter';
import { TransactionConverter } from '../../../../src/rpc/converters/transaction-converter';
import { expectToThrow } from '../../mocks';
import { mockBaseProperties } from './rpc-mocks';

describe(OperationConverter.name, () => {
    let target: OperationConverter;
    let activateAccountConverter: ActivateAccountConverter;
    let ballotConverter: BallotConverter;
    let delegationConverter: DelegationConverter;
    let doubleBakingEvidenceConverter: DoubleBakingEvidenceConverter;
    let doubleEndorsementEvidenceConverter: DoubleEndorsementEvidenceConverter;
    let doublePreendorsementEvidenceConverter: DoublePreendorsementEvidenceConverter;
    let endorsementConverter: EndorsementConverter;
    let endorsementWithSlotConverter: EndorsementWithSlotConverter;
    let preendorsementConverter: PreendorsementConverter;
    let originationConverter: OriginationConverter;
    let proposalsConverter: ProposalsConverter;
    let registerGlobalConstantConverter: RegisterGlobalConstantConverter;
    let revealConverter: RevealConverter;
    let seedNonceRevelationConverter: SeedNonceRevelationConverter;
    let setDepositsLimitConverter: SetDepositsLimitConverter;
    let transactionConverter: TransactionConverter;

    beforeEach(() => {
        activateAccountConverter = mock(ActivateAccountConverter);
        ballotConverter = mock(BallotConverter);
        delegationConverter = mock(DelegationConverter);
        doubleBakingEvidenceConverter = mock(DoubleBakingEvidenceConverter);
        doubleEndorsementEvidenceConverter = mock(DoubleEndorsementEvidenceConverter);
        doublePreendorsementEvidenceConverter = mock(DoublePreendorsementEvidenceConverter);
        endorsementConverter = mock(EndorsementConverter);
        endorsementWithSlotConverter = mock(EndorsementWithSlotConverter);
        originationConverter = mock(OriginationConverter);
        preendorsementConverter = mock(PreendorsementConverter);
        proposalsConverter = mock(ProposalsConverter);
        registerGlobalConstantConverter = mock(RegisterGlobalConstantConverter);
        revealConverter = mock(RevealConverter);
        seedNonceRevelationConverter = mock(SeedNonceRevelationConverter);
        setDepositsLimitConverter = mock(SetDepositsLimitConverter);
        transactionConverter = mock(TransactionConverter);
        target = new OperationConverter(
            instance(activateAccountConverter),
            instance(ballotConverter),
            instance(delegationConverter),
            instance(doubleBakingEvidenceConverter),
            instance(doubleEndorsementEvidenceConverter),
            instance(doublePreendorsementEvidenceConverter),
            instance(endorsementConverter),
            instance(endorsementWithSlotConverter),
            instance(originationConverter),
            instance(preendorsementConverter),
            instance(proposalsConverter),
            instance(registerGlobalConstantConverter),
            instance(revealConverter),
            instance(seedNonceRevelationConverter),
            instance(setDepositsLimitConverter),
            instance(transactionConverter),
        );
    });

    executeTest(rpc.OpKind.ACTIVATION, () => activateAccountConverter);
    executeTest(rpc.OpKind.BALLOT, () => ballotConverter);
    executeTest(rpc.OpKind.DELEGATION, () => delegationConverter);
    executeTest(rpc.OpKind.DOUBLE_BAKING_EVIDENCE, () => doubleBakingEvidenceConverter);
    executeTest(rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE, () => doubleEndorsementEvidenceConverter);
    executeTest(rpc.OpKind.DOUBLE_PREENDORSEMENT_EVIDENCE, () => doublePreendorsementEvidenceConverter);
    executeTest(rpc.OpKind.ENDORSEMENT, () => endorsementConverter);
    executeTest(rpc.OpKind.ENDORSEMENT_WITH_SLOT, () => endorsementWithSlotConverter);
    executeTest(rpc.OpKind.ORIGINATION, () => originationConverter);
    executeTest(rpc.OpKind.PREENDORSEMENT, () => preendorsementConverter);
    executeTest(rpc.OpKind.PROPOSALS, () => proposalsConverter);
    executeTest(rpc.OpKind.REGISTER_GLOBAL_CONSTANT, () => registerGlobalConstantConverter);
    executeTest(rpc.OpKind.REVEAL, () => revealConverter);
    executeTest(rpc.OpKind.SEED_NONCE_REVELATION, () => seedNonceRevelationConverter);
    executeTest(rpc.OpKind.SET_DEPOSITS_LIMIT, () => setDepositsLimitConverter);
    executeTest(rpc.OpKind.TRANSACTION, () => transactionConverter);

    function executeTest<TRpc extends RpcOperation, TOperation>(
        kind: rpc.OpKind,
        getConverter: () => { convert(r: TRpc, b: BaseOperationProperties): TOperation },
    ) {
        it(`should delegate to correct converter if ${kind} operation`, () => {
            const rpcOperation = { kind } as TRpc;
            const convertedResult = {} as TOperation;
            const baseProps = mockBaseProperties();
            when(getConverter().convert(rpcOperation, baseProps)).thenReturn(convertedResult);

            const result = target.convert(rpcOperation, baseProps);

            expect(result).toBe(convertedResult);
        });
    }

    it(`should return null if ${rpc.OpKind.FAILING_NOOP} operation`, () => {
        const rpcOperation = { kind: rpc.OpKind.FAILING_NOOP } as rpc.OperationContentsFailingNoop;

        const result = target.convert(rpcOperation, mockBaseProperties());

        expect(result).toBeNull();
    });

    it('should throw if unsupported operation', () => {
        const rpcOperation = { kind: 'wtf' as any } as rpc.OperationContentsFailingNoop;

        expectToThrow(() => target.convert(rpcOperation, mockBaseProperties()));
    });
});
