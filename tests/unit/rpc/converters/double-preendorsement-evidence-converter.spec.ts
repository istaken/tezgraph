import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    DoublePreendorsementEvidenceNotification,
    InlinedPreendorsementKind,
    OperationKind,
    SimpleOperationMetadata,
} from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common';
import {
    DoublePreendorsementEvidenceConverter,
    RpcDoublePreendorsementEvidence,
} from '../../../../src/rpc/converters/double-preendorsement-evidence-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(DoublePreendorsementEvidenceConverter.name, () => {
    let target: DoublePreendorsementEvidenceConverter;
    let metadataConverter: OperationMetadataConverter;

    const act = (e: RpcDoublePreendorsementEvidence) => target.convert(e, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        target = new DoublePreendorsementEvidenceConverter(instance(metadataConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcDoublePreendorsementEvidence = {
            kind: rpc.OpKind.DOUBLE_PREENDORSEMENT_EVIDENCE,
            op1: {
                branch: '1_bb',
                signature: '1_ss',
                operations: {
                    block_payload_hash: '1_hh',
                    kind: rpc.OpKind.PREENDORSEMENT,
                    level: 1_111,
                    round: 1_222,
                    slot: 1_333,
                },
            },
            op2: {
                branch: '2_bb',
                signature: '2_ss',
                operations: {
                    block_payload_hash: '2_hh',
                    kind: rpc.OpKind.PREENDORSEMENT,
                    level: 2_111,
                    round: 2_222,
                    slot: 2_333,
                },
            },
        };
        const metadata: SimpleOperationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);

        const operation = act(rpcOperation);

        expect(operation).toEqual<DoublePreendorsementEvidenceNotification>({
            graphQLTypeName: DoublePreendorsementEvidenceNotification.name,
            kind: OperationKind.double_preendorsement_evidence,
            metadata,
            ...mockBaseProperties(),
            op1: {
                branch: '1_bb',
                signature: '1_ss',
                operations: {
                    block_payload_hash: '1_hh',
                    kind: InlinedPreendorsementKind.preendorsement,
                    level: 1_111,
                    round: 1_222,
                    slot: 1_333,
                },
            },
            op2: {
                branch: '2_bb',
                signature: '2_ss',
                operations: {
                    block_payload_hash: '2_hh',
                    kind: InlinedPreendorsementKind.preendorsement,
                    level: 2_111,
                    round: 2_222,
                    slot: 2_333,
                },
            },
        });
    });
});
