import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    DoubleEndorsementEvidenceNotification,
    OperationKind,
    SimpleOperationMetadata,
} from '../../../../src/entity/subscriptions';
import { InlinedEndorsementConverter } from '../../../../src/rpc/converters/common/inlined-endorsement-converter';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import {
    DoubleEndorsementEvidenceConverter,
    RpcDoubleEndorsement,
} from '../../../../src/rpc/converters/double-endorsement-evidence-converter';
import { mockInlinedEndorsement } from '../../mocks';
import { mockBaseProperties } from './rpc-mocks';

describe(DoubleEndorsementEvidenceConverter.name, () => {
    let target: DoubleEndorsementEvidenceConverter;
    let metadataConverter: OperationMetadataConverter;
    let inlinedEndorsementConverter: InlinedEndorsementConverter;

    const act = (o: RpcDoubleEndorsement) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        inlinedEndorsementConverter = mock(InlinedEndorsementConverter);
        target = new DoubleEndorsementEvidenceConverter(
            instance(metadataConverter),
            instance(inlinedEndorsementConverter),
        );
    });

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcDoubleEndorsement = {
            kind: rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE,
            op1: 'rpcInlinedEndorsement1' as any,
            op2: 'rpcInlinedEndorsement2' as any,
            slot: 11,
            metadata: null!,
        };
        const metadata: SimpleOperationMetadata = 'mockedMetadata' as any;
        const inlEndorsement1 = mockInlinedEndorsement();
        const inlEndorsement2 = mockInlinedEndorsement();
        when(metadataConverter.convertSimple(rpcOperation)).thenReturn(metadata);
        when(inlinedEndorsementConverter.convert(rpcOperation.op1)).thenReturn(inlEndorsement1);
        when(inlinedEndorsementConverter.convert(rpcOperation.op2)).thenReturn(inlEndorsement2);

        const operation = act(rpcOperation);

        expect(operation).toEqual<DoubleEndorsementEvidenceNotification>({
            graphQLTypeName: DoubleEndorsementEvidenceNotification.name,
            kind: OperationKind.double_endorsement_evidence,
            op1: inlEndorsement1,
            op2: inlEndorsement2,
            slot: 11,
            metadata,
            ...mockBaseProperties(),
        });
    });
});
