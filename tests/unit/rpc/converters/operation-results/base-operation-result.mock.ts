import * as rpc from '@taquito/rpc';

import { OperationResult, OperationResultStatus } from '../../../../../src/entity/common/operation';

export const basePropertiesMock = {
    get rpc() {
        return {
            status: 'applied' as rpc.OperationResultStatusEnum,
            consumed_gas: '111',
            consumed_milligas: '111000',
            errors: [{ id: 'xx', kind: 'yy' }],
        };
    },
    get expected(): Omit<OperationResult, 'graphQLTypeName'> {
        return {
            status: OperationResultStatus.applied,
            consumed_gas: BigInt(111),
            consumed_milligas: BigInt(111000),
            errors: [{ id: 'xx', kind: 'yy' }],
        };
    },
};
