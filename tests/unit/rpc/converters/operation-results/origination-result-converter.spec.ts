import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { OriginationNotificationResult } from '../../../../../src/entity/subscriptions/operations/origination/origination-notification';
import {
    BalanceUpdateConverter,
    BigMapDiffConverter,
    LazyStorageDiffConverter,
} from '../../../../../src/rpc/converters/common';
import { OriginationResultConverter } from '../../../../../src/rpc/converters/operation-results/origination-result-converter';
import { mockBalanceUpdate, mockBigMapDiff, mockLazyStorageDiff } from '../../../mocks';
import { basePropertiesMock } from './base-operation-result.mock';

describe(OriginationResultConverter.name, () => {
    let target: OriginationResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;
    let lazyStorageDiffConverter: LazyStorageDiffConverter;

    const act = (r: rpc.OperationResultOrigination) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        bigMapDiffConverter = mock(BigMapDiffConverter);
        lazyStorageDiffConverter = mock(LazyStorageDiffConverter);
        target = new OriginationResultConverter(
            instance(balanceUpdateConverter),
            instance(bigMapDiffConverter),
            instance(lazyStorageDiffConverter),
        );
    });

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultOrigination = {
            ...basePropertiesMock.rpc,
            originated_contracts: ['c1', 'c2'],
            storage_size: '222',
            paid_storage_size_diff: '333',
            balance_updates: 'rpcBalanceUpdates' as any,
            big_map_diff: 'rpcBigMapDiffs' as any,
            lazy_storage_diff: 'rpcLazyStorageDiffs' as any,
        };
        const balanceUpdates = [mockBalanceUpdate()];
        const bigMapDiffs = [mockBigMapDiff()];
        const lazyStorageDiffs = [mockLazyStorageDiff()];
        when(balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates)).thenReturn(balanceUpdates);
        when(bigMapDiffConverter.convertNullableArray(rpcResult.big_map_diff)).thenReturn(bigMapDiffs);
        when(lazyStorageDiffConverter.convertNullableArray(rpcResult.lazy_storage_diff)).thenReturn(lazyStorageDiffs);

        const result = act(rpcResult);

        expect(result).toEqual<OriginationNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: OriginationNotificationResult.name,
            originated_contracts: ['c1', 'c2'],
            storage_size: BigInt(222),
            paid_storage_size_diff: BigInt(333),
            balance_updates: balanceUpdates,
            big_map_diff: bigMapDiffs,
            lazy_storage_diff: lazyStorageDiffs,
        });
    });
});
