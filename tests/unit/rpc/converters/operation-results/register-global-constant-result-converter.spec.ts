import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import { RegisterGlobalConstantNotificationResult } from '../../../../../src/entity/subscriptions';
import { BalanceUpdateConverter } from '../../../../../src/rpc/converters/common';
import { RegisterGlobalConstantResultConverter } from '../../../../../src/rpc/converters/operation-results/register-global-constant-result-converter';
import { mockBalanceUpdate } from '../../../mocks';
import { basePropertiesMock } from './base-operation-result.mock';

describe(RegisterGlobalConstantResultConverter.name, () => {
    let target: RegisterGlobalConstantResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    const act = (r: rpc.OperationResultRegisterGlobalConstant) => target.convert(r);

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new RegisterGlobalConstantResultConverter(instance(balanceUpdateConverter));
    });

    it(`should convert values correctly`, () => {
        const rpcResult: rpc.OperationResultRegisterGlobalConstant = {
            ...basePropertiesMock.rpc,
            storage_size: '222',
            global_address: 'glb-addr',
            balance_updates: 'rpcBalanceUpdates' as any,
        };
        const balanceUpdates = [mockBalanceUpdate()];
        when(balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates)).thenReturn(balanceUpdates);

        const result = act(rpcResult);

        expect(result).toEqual<RegisterGlobalConstantNotificationResult>({
            ...basePropertiesMock.expected,
            graphQLTypeName: RegisterGlobalConstantNotificationResult.name,
            storage_size: BigInt(222),
            balance_updates: balanceUpdates,
            global_address: 'glb-addr',
        });
    });
});
