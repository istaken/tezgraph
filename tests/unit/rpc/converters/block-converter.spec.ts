import * as rpc from '@taquito/rpc';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BlockNotification, OperationNotification } from '../../../../src/entity/subscriptions';
import { BlockConverter } from '../../../../src/rpc/converters/block-converter';
import { OperationGroupConverter } from '../../../../src/rpc/converters/operation-group-converter';
import { LogLevel } from '../../../../src/utils/logging';
import { expectToThrow, mockOperation, TestLogger } from '../../mocks';
import { getRandomDate } from '../../mocks/test-clock';

describe(BlockConverter.name, () => {
    let target: BlockConverter;
    let operationGroupConverter: OperationGroupConverter;
    let logger: TestLogger;

    beforeEach(() => {
        operationGroupConverter = mock(OperationGroupConverter);
        logger = new TestLogger();
        target = new BlockConverter(instance(operationGroupConverter), logger);
    });

    it('should convert values correctly', () => {
        const blockProps = getBlockOwnProperties();
        const receivedFromTezosOn = getRandomDate();

        const failedRpcOpGroup = 'bullshit' as unknown as rpc.OperationEntry;
        const rpcGroupError = new Error('oops');
        when(operationGroupConverter.convert(failedRpcOpGroup, anything())).thenThrow(rpcGroupError);

        const [rpcOpGroup1, op1, op2] = setupGroup('s1');
        const [rpcOpGroup2, op3, op4] = setupGroup('s2');
        const rpcBlock = {
            ...blockProps,
            operations: [[rpcOpGroup1, failedRpcOpGroup], [rpcOpGroup2]],
        } as rpc.BlockResponse;

        const block = target.convert(rpcBlock, receivedFromTezosOn);

        expect(block).toEqual<BlockNotification>({
            ...blockProps,
            operations: [op1, op2, op3, op4],
            received_from_tezos_on: receivedFromTezosOn,
        });
        verifyConversion(rpcOpGroup1, block);
        verifyConversion(rpcOpGroup2, block);
        verifyConversion(failedRpcOpGroup, block);
        logger.verifyLoggedCount(3);
        logger.logged(0).verify(LogLevel.Debug, { hash: 'SomeHash' }).verifyMessageIncludesAll('Converting');
        logger.logged(1).verify(LogLevel.Error, { hash: 'SomeHash', rpcGroup: failedRpcOpGroup, error: rpcGroupError });
        logger
            .logged(2)
            .verify(LogLevel.Debug, { hash: 'SomeHash', level: 11, operationCount: 4 })
            .verifyMessageIncludesAll('Converted');
    });

    function setupGroup(signature: string): [rpc.OperationEntry, OperationNotification, OperationNotification] {
        const rpcOpGroup = { signature } as rpc.OperationEntry;
        const op1 = mockOperation();
        const op2 = mockOperation();

        when(operationGroupConverter.convert(rpcOpGroup, anything())).thenReturn([op1, op2]);
        return [rpcOpGroup, op1, op2];
    }

    function verifyConversion(rpcGroup: rpc.OperationEntry, block: BlockNotification) {
        verify(operationGroupConverter.convert(rpcGroup, block)).once();
    }

    it('should throw if no operation converted correctly', () => {
        const rpcOpGroup = { signature: 'ss' } as rpc.OperationEntry;
        const rpcBlock = {
            ...getBlockOwnProperties(),
            operations: [[rpcOpGroup]],
        } as rpc.BlockResponse;
        when(operationGroupConverter.convert(rpcOpGroup, anything())).thenThrow(new Error('oops'));

        const error = expectToThrow(() => target.convert(rpcBlock, getRandomDate()));

        expect(error.message).toIncludeMultiple(['all operation groups', `'SomeHash'`]);
    });

    function getBlockOwnProperties() {
        return {
            hash: 'SomeHash',
            protocol: 'pp',
            chain_id: 'cc',
            header: {
                level: 11,
                proto: 22,
                predecessor: 'rr',
                timestamp: new Date('2020-09-24T21:54:33.000Z'),
                signature: 'ss',
                validation_pass: 33,
                operations_hash: 'oh',
                fitness: ['f1', 'f2'],
                context: 'cc',
                priority: 44,
                proof_of_work_nonce: 'pw',
                seed_nonce_hash: 'sh',
                liquidity_baking_escape_vote: true,
                payload_hash: 'p-haha',
                payload_round: 55,
            },
        };
    }
});
