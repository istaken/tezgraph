import * as rpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { BalanceUpdate, OperationKind, PreendorsementNotification } from '../../../../src/entity/subscriptions';
import { BalanceUpdateConverter } from '../../../../src/rpc/converters/common';
import { PreendorsementConverter, RpcPreendorsement } from '../../../../src/rpc/converters/preendorsement-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(PreendorsementConverter.name, () => {
    let target: PreendorsementConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let rpcOperation: Writable<RpcPreendorsement>;

    const act = (e: RpcPreendorsement) => target.convert(e, mockBaseProperties());

    beforeEach(() => {
        balanceUpdateConverter = mock(BalanceUpdateConverter);
        target = new PreendorsementConverter(instance(balanceUpdateConverter));

        rpcOperation = {
            kind: rpc.OpKind.PREENDORSEMENT,
            slot: 111,
            level: 222,
            round: 333,
            block_payload_hash: 'haha',
            metadata: {
                balance_updates: 'rpcBalanceUpdates' as any,
                delegate: 'ddd',
                preendorsement_power: 444,
            },
        };
    });

    it(`should convert values correctly`, () => {
        const balance_updates: BalanceUpdate[] = 'mockedBalanceUpdates' as any;
        when(balanceUpdateConverter.convertArray(rpcOperation.metadata!.balance_updates)).thenReturn(balance_updates);

        const operation = act(rpcOperation);

        expect(operation).toEqual<PreendorsementNotification>({
            graphQLTypeName: PreendorsementNotification.name,
            kind: OperationKind.preendorsement,
            slot: 111,
            level: 222,
            round: 333,
            block_payload_hash: 'haha',
            metadata: {
                balance_updates,
                delegate: 'ddd',
                preendorsement_power: 444,
            },
            ...mockBaseProperties(),
        });
    });

    it.each([null, undefined])(`should handle %s metadata`, (metadata) => {
        rpcOperation.metadata = metadata!;

        const operation = act(rpcOperation);

        expect(operation.metadata).toBeNull();
        verify(balanceUpdateConverter.convertArray(anything())).never();
    });
});
