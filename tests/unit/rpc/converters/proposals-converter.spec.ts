import * as rpc from '@taquito/rpc';

import { OperationKind, ProposalsNotification } from '../../../../src/entity/subscriptions';
import { ProposalsConverter, RpcProposals } from '../../../../src/rpc/converters/proposals-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(ProposalsConverter.name, () => {
    const target = new ProposalsConverter();

    const act = (o: RpcProposals) => target.convert(o, mockBaseProperties());

    it(`should convert values correctly`, () => {
        const rpcOperation: RpcProposals = {
            kind: rpc.OpKind.PROPOSALS,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
        };

        const operation = act(rpcOperation);

        expect(operation).toEqual<ProposalsNotification>({
            graphQLTypeName: ProposalsNotification.name,
            kind: OperationKind.proposals,
            source: 'sss',
            period: 111,
            proposals: ['ppp'],
            ...mockBaseProperties(),
        });
    });
});
