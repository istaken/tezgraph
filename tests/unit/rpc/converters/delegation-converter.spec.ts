import * as rpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    DelegationNotification,
    DelegationNotificationMetadata,
    OperationKind,
} from '../../../../src/entity/subscriptions';
import { OperationMetadataConverter } from '../../../../src/rpc/converters/common/operation-metadata-converter';
import { DelegationConverter, RpcDelegation } from '../../../../src/rpc/converters/delegation-converter';
import { DelegationResultConverter } from '../../../../src/rpc/converters/operation-results/delegation-result-converter';
import { mockBaseProperties } from './rpc-mocks';

describe(DelegationConverter.name, () => {
    let target: DelegationConverter;
    let metadataConverter: OperationMetadataConverter;
    let resultConverter: DelegationResultConverter;

    const act = (o: RpcDelegation) => target.convert(o, mockBaseProperties());

    beforeEach(() => {
        metadataConverter = mock(OperationMetadataConverter);
        resultConverter = mock(DelegationResultConverter);
        target = new DelegationConverter(instance(metadataConverter), resultConverter);
    });

    it('should convert values correctly', () => {
        const rpcOperation: RpcDelegation = {
            kind: rpc.OpKind.DELEGATION,
            source: 'sss',
            fee: '111',
            counter: '222',
            gas_limit: '333',
            storage_limit: '444',
            delegate: 'ddd',
            metadata: null!,
        };
        const metadata: DelegationNotificationMetadata = 'mockedMetadata' as any;
        when(metadataConverter.convert(rpcOperation, resultConverter, DelegationNotificationMetadata.name)).thenReturn(
            metadata,
        );

        const operation = act(rpcOperation);

        expect(operation).toEqual<DelegationNotification>({
            graphQLTypeName: DelegationNotification.name,
            kind: OperationKind.delegation,
            source: 'sss',
            fee: BigInt(111),
            counter: BigInt(222),
            gas_limit: BigInt(333),
            storage_limit: BigInt(444),
            delegate: 'ddd',
            ...mockBaseProperties(),
            metadata,
        });
    });
});
