import { BlockNotification, OperationGroup, OperationOrigin } from '../../../../src/entity/subscriptions';
import { BaseOperationProperties } from '../../../../src/rpc/converters/operation-converter';

export function mockBaseProperties() {
    return {
        operation_group: { protocol: 'ppp' } as OperationGroup,
        block: { hash: 'SomeHash' } as BlockNotification,
        origin: OperationOrigin.block,
    } as BaseOperationProperties;
}
