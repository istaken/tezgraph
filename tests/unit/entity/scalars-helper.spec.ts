import { UserInputError } from 'apollo-server-express';
import { GraphQLScalarType, IntValueNode, StringValueNode } from 'graphql';
import { spy, when } from 'ts-mockito';

import {
    base58CheckError,
    bigIntError,
    createBase58HashScalar,
    createScalar,
    parseBigInt,
    parsePositiveBigInt,
    positiveBigIntError,
    ScalarConfig,
    parseCursor,
} from '../../../src/entity/scalars-helper';
import { nameof } from '../../../src/utils/reflection';
import { expectToThrow } from '../mocks';

describe('Scalars helper', () => {
    const validBigIntCases: [string, bigint][] = [
        ['0', BigInt(0)],
        ['2', BigInt(2)],
        ['900124577199254740991', BigInt('900124577199254740991')],
    ];
    const invalidBigIntCases = [
        ['', bigIntError],
        ['  ', bigIntError],
        ['ab', bigIntError],
        ['2.34', bigIntError],
    ];

    describe(parseBigInt.name, () => {
        it.each([...validBigIntCases, ['-5', BigInt(-5)]])(`should parse '%s' to %s`, (input, expected) => {
            expect(parseBigInt(input)).toEqual(expected);
        });

        it.each(invalidBigIntCases)(`should fail parse '%s'`, (input: string, error: string) => {
            expect(() => parseBigInt(input)).toThrow(error);
        });
    });

    describe(parsePositiveBigInt.name, () => {
        it.each(validBigIntCases)(`should parse '%s' to %s'`, (input, expected) => {
            expect(parsePositiveBigInt(input)).toEqual(BigInt(expected));
        });

        it.each([...invalidBigIntCases, ['-5', positiveBigIntError]])(`should fail parse '%s'`, (input, error) => {
            expect(() => parsePositiveBigInt(input)).toThrow(error);
        });
    });

    describe(createScalar.name, () => {
        let target: GraphQLScalarType;
        let config: ScalarConfig<number>;
        let configSpy: ScalarConfig<number>;

        beforeEach(() => {
            config = {
                name: 'foo',
                description: 'foo desc',
                parseValue: parseFloat,
            };
            configSpy = spy(config);
            target = createScalar(config);
        });

        it('should expose properties correctly', () => {
            expect(target.name).toBe('foo');
            expect(target.description).toBe('foo desc');
        });

        describe(nameof<GraphQLScalarType>('serialize'), () => {
            it('should convert value to string', () => {
                const serialized = target.serialize(123);

                expect(serialized).toBe('123');
            });
        });

        describe(nameof<GraphQLScalarType>('parseValue'), () => {
            it('should parse from string', () => {
                when(configSpy.parseValue('xxx')).thenReturn(111);

                const value = target.parseValue('xxx');

                expect(value).toBe(111);
            });

            it('should throw if not string input', () => {
                runThrowTest({
                    act: () => target.parseValue(66),
                    actualValue: 66,
                    expectedError: 'Expected a string but there is number.',
                });
            });

            it('should throw if further parsing throws', () => {
                when(configSpy.parseValue('zzz')).thenThrow(new Error('wtf'));

                runThrowTest({
                    act: () => target.parseValue('zzz'),
                    actualValue: 'zzz',
                    expectedError: 'wtf',
                });
            });
        });

        describe(nameof<GraphQLScalarType>('parseLiteral'), () => {
            it('should parse from string', () => {
                const node = { kind: 'StringValue', value: 'yyy' } as StringValueNode;
                when(configSpy.parseValue('yyy')).thenReturn(222);

                const value = target.parseLiteral(node, undefined);

                expect(value).toBe(222);
            });

            it('should throw if not string node input', () => {
                const node = { kind: 'IntValue', value: '66' } as IntValueNode;

                runThrowTest({
                    act: () => target.parseLiteral(node, undefined),
                    actualValue: node,
                    expectedError: 'Expected a string but there is IntValue.',
                });
            });

            it('should throw if further parsing throws', () => {
                const node = { kind: 'StringValue', value: 'aaa' } as StringValueNode;
                when(configSpy.parseValue('aaa')).thenThrow(new Error('wtf'));

                runThrowTest({
                    act: () => target.parseLiteral(node, undefined),
                    actualValue: node,
                    expectedError: 'wtf',
                });
            });
        });

        function runThrowTest(options: { expectedError: string; actualValue: any; act(): any }) {
            const error = expectToThrow(() => options.act());

            expect(error.message).toBe(options.expectedError);
            expect(error).toBeInstanceOf(UserInputError);
            expect((error as UserInputError).extensions).toEqual({
                actualValue: options.actualValue,
                scalarType: 'foo',
                code: 'BAD_USER_INPUT',
            });
        }
    });

    describe(createBase58HashScalar.name, () => {
        function getTarget(supportedPrefixes?: readonly { readonly prefix: string; readonly length: number }[]) {
            return createBase58HashScalar({
                name: 'bar',
                description: 'bar desc',
                supportedPrefixes,
            });
        }

        it('should expose properties correctly', () => {
            const target = getTarget();

            expect(target.name).toBe('bar');
            expect(target.description).toBe('bar desc');
        });

        it.each([
            ['prefix not validated', undefined],
            ['prefix validated', [{ prefix: 'BK', length: 51 }]],
        ])('should parse correct base 58 hash if %s', (_desc, prefixes) => {
            const input = 'BKsdCv5tsbZ8bYYW5pfkhxDrusgGrEbCczH7GetyofAZPPRgnjn';
            const target = getTarget(prefixes);

            const parsed = target.parseValue(input);

            expect(parsed).toEqual(input);
        });

        it(`should throw if not correct base 58 hash`, () => {
            const target = getTarget();

            expect(() => target.parseValue('abc')).toThrow(base58CheckError);
        });

        it.each([
            ['invalid prefix', 'OPsdCv5tsbZ8bYYW5pfkhxDrusgGrEbCczH7GetyofAZPPRgnjn'],
            ['invalid length', 'BKSomeBlockHash'],
        ])('should throw if %s', (_desc, input) => {
            const target = getTarget([
                { prefix: 'BK', length: 51 },
                { prefix: 'TZ1', length: 36 },
            ]);

            const error = expectToThrow(() => target.parseValue(input));

            expect(error.message).toIncludeMultiple(['Invalid hash', `'BK' with length 51`, `'TZ1' with length 36`]);
        });
    });

    describe(parseCursor.name, () => {
        it(`should throw user input error due to invalid operation hash length'`, () => {
            const input = 'opBeBXu9MZFqHXcYZy3oiL6ah1r9GcmrMHqWLnkY5UwMQ1pB:1';
            const expectedError = `Argument "cursor" of type "string?" is in an invalid format. The valid cursor format is "operation_hash:operation_id".`;
            expect(() => parseCursor(input)).toThrow(expectedError);
        });

        it(`should throw user input error due to invalid operation hash prefix'`, () => {
            const input = 'xp5PU5PNtR1iev3wRWZiUGQKW1RA5nrLrQkV2j81NQmhzhmvDbA:1';
            const expectedError = `Argument "cursor" of type "string?" contains an invalid cursor hash. An cursor hash should start with`;
            expect(() => parseCursor(input)).toThrow(expectedError);
        });

        it(`should throw user input error due to invalid batch position'`, () => {
            const input = 'op5PU5PNtR1iev3wRWZiUGQKW1RA5nrLrQkV2j81NQmhzhmvDbA:x';
            const expectedError = `Argument "cursor" of type "string?" is in an invalid format. The valid cursor format is "operation_hash:operation_id".`;
            expect(() => parseCursor(input)).toThrow(expectedError);
        });
    });
});
