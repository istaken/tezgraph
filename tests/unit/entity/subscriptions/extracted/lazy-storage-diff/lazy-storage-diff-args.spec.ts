import { AddressFilter, NullableAddressFilter } from '../../../../../../src/entity/subscriptions/common-filters';
import { LazyBigMapUpdateItem } from '../../../../../../src/entity/subscriptions/extracted/lazy-storage-diff/lazy-big-map-diff';
import {
    LazyStorageDiffAction,
    LazyStorageDiffKind,
} from '../../../../../../src/entity/subscriptions/extracted/lazy-storage-diff/lazy-storage-diff';
import {
    bigMapUpdateIncludeKey,
    getBigMapUpdates,
    LazyStorageDiffActionFilter,
    LazyStorageDiffFilter,
    LazyStorageDiffKindFilter,
    lazyStorageIdEquals,
    LazyStorageIdFilter,
    NullableLazyStorageBigMapKeyFilter,
} from '../../../../../../src/entity/subscriptions/extracted/lazy-storage-diff/lazy-storage-diff-args';
import { LazyStorageDiffNotification } from '../../../../../../src/entity/subscriptions/extracted/lazy-storage-diff/lazy-storage-diff-notification';
import { LazyStorageDiff } from '../../../../../../src/entity/subscriptions/extracted/lazy-storage-diff/lazy-storage-diff-union';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { getEnumValues, nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../../operations/mocks';

describe(lazyStorageIdEquals.name, () => {
    describe(`${LazyStorageDiffKind.big_map} ${LazyStorageDiffAction.copy}`, () => {
        it.each([
            [false, 11, 22, 33],
            [true, 11, 11, 33],
            [true, 11, 22, 11],
            [true, 11, 11, 11],
        ])('should return %s for %s equals id %s and source %s', (expectedEquals, filterValue, id, source) => {
            const diff = {
                kind: LazyStorageDiffKind.big_map,
                id: BigInt(id),
                big_map_diff: {
                    action: LazyStorageDiffAction.copy,
                    source: BigInt(source),
                },
            } as LazyStorageDiff;

            const equals = lazyStorageIdEquals(BigInt(filterValue), diff);

            expect(equals).toBe(expectedEquals);
        });
    });

    for (const kind of getEnumValues(LazyStorageDiffKind)) {
        for (const action of getEnumValues(LazyStorageDiffAction)) {
            if (kind === LazyStorageDiffKind.big_map && action === LazyStorageDiffAction.copy) {
                continue;
            }
            describe(`${kind} ${action}`, () => {
                it.each([
                    [false, 11, 22],
                    [true, 11, 11],
                ])('should return %s for %s equals id %s', (expectedEquals, filterValue, id) => {
                    const diff = {
                        kind,
                        id: BigInt(id),
                        sapling_state_diff: {
                            action,
                            source: BigInt(11),
                        },
                        big_map_diff: {
                            action,
                            source: BigInt(11),
                        },
                    } as unknown as LazyStorageDiff;

                    const equals = lazyStorageIdEquals(BigInt(filterValue), diff);

                    expect(equals).toBe(expectedEquals);
                });
            });
        }
    }
});

describe(bigMapUpdateIncludeKey.name, () => {
    it.each([
        [true, { prim: 'p1' }],
        [true, { prim: 'p2' }],
        [false, { prim: 'not-found' }],
    ])('should return %s if any update has key %s', (expectedIncluded, key) => {
        const updates = [
            { key: { prim: 'p1' } } as LazyBigMapUpdateItem,
            { key: { prim: 'p2' } } as LazyBigMapUpdateItem,
        ];

        const included = bigMapUpdateIncludeKey(updates, key);

        expect(included).toBe(expectedIncluded);
    });
});

describe(`${LazyStorageDiffFilter.name}.${nameof<LazyStorageDiffFilter>('passes')}()`, () => {
    for (const [
        expectedPassed,
        kindResult,
        idResult,
        actionResult,
        bigMapKeyResult,
        sourceResult,
        destinationResult,
    ] of getFilterTestCases(6)) {
        it(
            `should return ${expectedPassed} if filters are kind=${kindResult}, id=${idResult}, action=${actionResult},` +
                ` big_map_key=${bigMapKeyResult}, source=${sourceResult}, destination=${destinationResult}`,
            () => {
                const updates = [{ key: { prim: 'k1' } } as LazyBigMapUpdateItem];
                const notification = {
                    lazy_storage_diff: {
                        kind: LazyStorageDiffKind.big_map,
                        big_map_diff: {
                            action: LazyStorageDiffAction.update,
                            updates: asReadonly(updates),
                        },
                    },
                    parent: {
                        source: 'src',
                        destination: 'dst',
                    },
                } as LazyStorageDiffNotification;
                const target = create(LazyStorageDiffFilter, {
                    kind: mockFilter(LazyStorageDiffKindFilter, kindResult, notification.lazy_storage_diff.kind),
                    id: mockFilter(LazyStorageIdFilter, idResult, notification.lazy_storage_diff),
                    action: mockFilter(LazyStorageDiffActionFilter, actionResult, LazyStorageDiffAction.update),
                    big_map_key: mockFilter(NullableLazyStorageBigMapKeyFilter, bigMapKeyResult, updates),
                    source: mockFilter(AddressFilter, sourceResult, notification.parent.source),
                    destination: mockFilter(NullableAddressFilter, destinationResult, notification.parent.destination),
                });

                const passed = target.passes(notification);

                expect(passed).toBe(expectedPassed);
            },
        );
    }
});

describe(getBigMapUpdates.name, () => {
    it('should get big map updates if big map diff', () => {
        const updates = [{ key: { prim: 'k1' } } as LazyBigMapUpdateItem];
        const diff = {
            kind: LazyStorageDiffKind.big_map,
            big_map_diff: { updates: asReadonly(updates) },
        } as LazyStorageDiff;

        const result = getBigMapUpdates(diff);

        expect(result).toBe(updates);
    });

    it('should get undefined if not big map diff', () => {
        const diff = {
            kind: LazyStorageDiffKind.sapling_state,
            sapling_state_diff: { updates: {} },
        } as LazyStorageDiff;

        const result = getBigMapUpdates(diff);

        expect(result).toBeUndefined();
    });
});
