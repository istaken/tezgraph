import {
    AddressFilter,
    NullableAddressFilter,
    NullableMichelsonFilter,
} from '../../../../../../src/entity/subscriptions/common-filters';
import {
    BigMapCopy,
    BigMapDiff,
    BigMapDiffAction,
} from '../../../../../../src/entity/subscriptions/extracted/big-map-diff/big-map-diff';
import {
    BigMapDiffActionFilter,
    BigMapDiffBigMapFilter,
    BigMapDiffFilter,
    bigMapEquals,
} from '../../../../../../src/entity/subscriptions/extracted/big-map-diff/big-map-diff-args';
import { BigMapDiffNotification } from '../../../../../../src/entity/subscriptions/extracted/big-map-diff/big-map-diff-notification';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../../operations/mocks';

describe(bigMapEquals.name, () => {
    describe.each([BigMapDiffAction.alloc, BigMapDiffAction.remove, BigMapDiffAction.update])('%s', (action) => {
        it.each([
            [true, 11, 11],
            [false, 11, 22],
        ])('should return %s for %s equals %s', (expectedEquals, filterValue, bigMap) => {
            const diff = {
                action,
                big_map: BigInt(bigMap),
            } as BigMapDiff;

            const equals = bigMapEquals(BigInt(filterValue), diff);

            expect(equals).toBe(expectedEquals);
        });
    });

    describe(BigMapDiffAction.copy, () => {
        it.each([
            [false, 11, 22, 33],
            [true, 22, 22, 33],
            [true, 33, 22, 33],
        ])(
            'should return %s for %s equals source %s or destination %s',
            (expectedEquals, filterValue, sourceBigMap, destinationBigMap) => {
                const diff = {
                    action: BigMapDiffAction.copy,
                    source_big_map: BigInt(sourceBigMap),
                    destination_big_map: BigInt(destinationBigMap),
                } as BigMapCopy;

                const equals = bigMapEquals(BigInt(filterValue), diff);

                expect(equals).toBe(expectedEquals);
            },
        );
    });
});

describe(`${BigMapDiffFilter.name}.${nameof<BigMapDiffFilter>('passes')}()`, () => {
    for (const [
        expectedPassed,
        bigMapResult,
        keyResult,
        actionResult,
        sourceResult,
        destinationResult,
    ] of getFilterTestCases(5)) {
        it(
            `should return ${expectedPassed} if filters are big_map=${bigMapResult}, key=${keyResult},` +
                ` action=${actionResult}, source=${sourceResult}, destination=${destinationResult}`,
            () => {
                const notification = {
                    big_map_diff: {
                        action: BigMapDiffAction.update,
                        key: { prim: 'kk' },
                    },
                    parent: {
                        source: 'src',
                        destination: 'dst',
                    },
                } as BigMapDiffNotification;
                const target = create(BigMapDiffFilter, {
                    big_map: mockFilter(BigMapDiffBigMapFilter, bigMapResult, notification.big_map_diff),
                    key: mockFilter(NullableMichelsonFilter, keyResult, notification.big_map_diff.key),
                    action: mockFilter(BigMapDiffActionFilter, actionResult, notification.big_map_diff.action),
                    source: mockFilter(AddressFilter, sourceResult, notification.parent.source),
                    destination: mockFilter(NullableAddressFilter, destinationResult, notification.parent.destination),
                });

                const passed = target.passes(notification);

                expect(passed).toBe(expectedPassed);
            },
        );
    }
});
