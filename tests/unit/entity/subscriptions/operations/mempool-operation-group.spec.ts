import { EndorsementWithSlotNotification } from '../../../../../src/entity/subscriptions/operations/endorsements/endorsement-notification';
import {
    getSignature,
    MempoolOperationGroup,
} from '../../../../../src/entity/subscriptions/operations/mempool-operation-group';
import { OperationKind } from '../../../../../src/entity/subscriptions/operations/operation-kind';
import { OperationNotification } from '../../../../../src/entity/subscriptions/operations/operation-notification-union';

describe(getSignature.name, () => {
    it('should get signature from first operation in the group', () => {
        const group: MempoolOperationGroup = [
            { operation_group: { signature: 'sig1' } } as OperationNotification,
            { operation_group: { signature: 'sig2' } } as OperationNotification,
        ];

        const signature = getSignature(group);

        expect(signature).toBe('sig1');
    });

    it('should get signature from inlined endorsement if endorsement with slot', () => {
        const endorsement = {
            kind: OperationKind.endorsement_with_slot,
            operation_group: {},
            endorsement: { signature: 'endorsement-sig' },
        } as EndorsementWithSlotNotification;

        const signature = getSignature([endorsement]);

        expect(signature).toBe('endorsement-sig');
    });

    it.each([
        ['no operation', []],
        ['no signature', [{} as OperationNotification]],
        ['white-space signature', [{ operation_group: { signature: '  ' } } as OperationNotification]],
    ])('should throw if %s in the group', (_desc, group) => {
        expect(() => getSignature(group)).toThrow();
    });
});
