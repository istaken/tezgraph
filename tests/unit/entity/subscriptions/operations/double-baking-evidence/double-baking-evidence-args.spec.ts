import { NullableAddressArrayFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import {
    BalanceUpdate,
    BalanceUpdateKind,
} from '../../../../../../src/entity/subscriptions/extracted/balance-update/balance-update';
import { DoubleBakingEvidenceSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/double-baking-evidence/double-baking-evidence-args';
import { DoubleBakingEvidenceNotification } from '../../../../../../src/entity/subscriptions/operations/double-baking-evidence/double-baking-evidence-notification';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${DoubleBakingEvidenceSpecificFilter.name}.${nameof<DoubleBakingEvidenceSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: {
                    balance_updates: asReadonly([
                        { kind: BalanceUpdateKind.freezer, delegate: 'd1' } as BalanceUpdate,
                        { kind: BalanceUpdateKind.contract } as BalanceUpdate,
                        { kind: BalanceUpdateKind.freezer, delegate: 'd2' } as BalanceUpdate,
                    ]),
                },
            } as DoubleBakingEvidenceNotification;
            const target = create(DoubleBakingEvidenceSpecificFilter, {
                delegate: mockFilter(NullableAddressArrayFilter, delegateResult, ['d1', 'd2']),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
