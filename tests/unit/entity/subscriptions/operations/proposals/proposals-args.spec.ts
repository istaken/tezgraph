import { AddressFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { NullableProtocolHashArrayFilter } from '../../../../../../src/entity/subscriptions/common-filters/hash-filters';
import { ProposalsSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/proposals/proposals-args';
import { ProposalsNotification } from '../../../../../../src/entity/subscriptions/operations/proposals/proposals-notification';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${ProposalsSpecificFilter.name}.${nameof<ProposalsSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, proposalsResult] of getFilterTestCases(2)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, proposals=${proposalsResult}`, () => {
            const operation = {
                source: 'sss',
                proposals: asReadonly(['p1', 'p2']),
            } as ProposalsNotification;
            const target = create(ProposalsSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                proposals: mockFilter(NullableProtocolHashArrayFilter, proposalsResult, operation.proposals),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
