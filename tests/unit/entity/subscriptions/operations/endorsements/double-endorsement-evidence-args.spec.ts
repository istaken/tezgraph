import {
    BalanceUpdate,
    BalanceUpdateKind,
    DoubleEndorsementEvidenceNotification,
    NullableAddressArrayFilter,
} from '../../../../../../src/entity/subscriptions';
import { DoubleEndorsementEvidenceSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/endorsements/double-endorsement-evidence-args';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${DoubleEndorsementEvidenceSpecificFilter.name}.${nameof<DoubleEndorsementEvidenceSpecificFilter>(
    'passes',
)}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: {
                    balance_updates: asReadonly([
                        { kind: BalanceUpdateKind.freezer, delegate: 'd1' } as BalanceUpdate,
                        { kind: BalanceUpdateKind.contract } as BalanceUpdate,
                        { kind: BalanceUpdateKind.freezer, delegate: 'd2' } as BalanceUpdate,
                    ]),
                },
            } as DoubleEndorsementEvidenceNotification;
            const target = create(DoubleEndorsementEvidenceSpecificFilter, {
                delegate: mockFilter(NullableAddressArrayFilter, delegateResult, ['d1', 'd2']),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
