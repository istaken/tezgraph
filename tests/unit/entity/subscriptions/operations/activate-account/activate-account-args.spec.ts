import { AddressFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { ActivateAccountSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/activate-account/activate-account-args';
import { ActivateAccountNotification } from '../../../../../../src/entity/subscriptions/operations/activate-account/activate-account-notification';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${ActivateAccountSpecificFilter.name}.${nameof<ActivateAccountSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, pkhResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are pkh=${pkhResult}`, () => {
            const operation = {
                pkh: 'ppp',
            } as ActivateAccountNotification;
            const target = create(ActivateAccountSpecificFilter, {
                pkh: mockFilter(AddressFilter, pkhResult, operation.pkh),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
