import { OperationResultStatus } from '../../../../../../src/entity/common/operation';
import { SetDepositsLimitNotification } from '../../../../../../src/entity/subscriptions';
import { AddressFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { NullableOperationResultStatusFilter } from '../../../../../../src/entity/subscriptions/common-filters/operation-result-status-filter';
import { SetDepositsLimitSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/set-deposits-limit/set-deposits-limit-args';
import { create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${SetDepositsLimitSpecificFilter.name}.${nameof<SetDepositsLimitSpecificFilter>('passes')}()`, () => {
    for (const [expectedPassed, sourceResult, statusResult] of getFilterTestCases(2)) {
        it(`should return ${expectedPassed} if filters are source=${sourceResult}, status=${statusResult}`, () => {
            const operation = {
                source: 'sss',
                metadata: { operation_result: { status: OperationResultStatus.backtracked } },
            } as SetDepositsLimitNotification;
            const target = create(SetDepositsLimitSpecificFilter, {
                source: mockFilter(AddressFilter, sourceResult, operation.source),
                status: mockFilter(
                    NullableOperationResultStatusFilter,
                    statusResult,
                    operation.metadata!.operation_result.status,
                ),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
