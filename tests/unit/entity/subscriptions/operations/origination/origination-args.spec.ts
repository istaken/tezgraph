import { OperationResultStatus } from '../../../../../../src/entity/common/operation';
import {
    AddressFilter,
    NullableAddressArrayFilter,
    NullableAddressFilter,
} from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import { NullableOperationResultStatusFilter } from '../../../../../../src/entity/subscriptions/common-filters/operation-result-status-filter';
import { OriginationSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/origination/origination-args';
import { OriginationNotification } from '../../../../../../src/entity/subscriptions/operations/origination/origination-notification';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${OriginationSpecificFilter.name}.${nameof<OriginationSpecificFilter>('passes')}()`, () => {
    for (const [
        expectedPassed,
        sourceResult,
        delegateResult,
        originatedContractResult,
        statusResult,
    ] of getFilterTestCases(4)) {
        it(
            `should return ${expectedPassed} if filters are source=${sourceResult}, delegate=${delegateResult},` +
                ` originated_contract=${originatedContractResult}, status=${statusResult}`,
            () => {
                const contracts = asReadonly(['c1', 'c2']);
                const operation = {
                    source: 'sss',
                    delegate: 'ddd',
                    metadata: {
                        operation_result: {
                            originated_contracts: contracts,
                            status: OperationResultStatus.backtracked,
                        },
                    },
                } as OriginationNotification;
                const target = create(OriginationSpecificFilter, {
                    source: mockFilter(AddressFilter, sourceResult, operation.source),
                    delegate: mockFilter(NullableAddressFilter, delegateResult, operation.delegate),
                    originated_contract: mockFilter(NullableAddressArrayFilter, originatedContractResult, contracts),
                    status: mockFilter(
                        NullableOperationResultStatusFilter,
                        statusResult,
                        operation.metadata!.operation_result.status,
                    ),
                });

                const passed = target.passes(operation);

                expect(passed).toBe(expectedPassed);
            },
        );
    }
});
