import { NullableAddressArrayFilter } from '../../../../../../src/entity/subscriptions/common-filters/address-filters';
import {
    BalanceUpdate,
    BalanceUpdateKind,
} from '../../../../../../src/entity/subscriptions/extracted/balance-update/balance-update';
import { DoublePreendorsementEvidenceSpecificFilter } from '../../../../../../src/entity/subscriptions/operations/preendorsements/double-preendorsement-evidence-args';
import { DoublePreendorsementEvidenceNotification } from '../../../../../../src/entity/subscriptions/operations/preendorsements/double-preendorsement-evidence-notification';
import { asReadonly, create } from '../../../../../../src/utils/conversion';
import { nameof } from '../../../../../../src/utils/reflection';
import { getFilterTestCases, mockFilter } from '../mocks';

describe(`${DoublePreendorsementEvidenceSpecificFilter.name}.${nameof<DoublePreendorsementEvidenceSpecificFilter>(
    'passes',
)}()`, () => {
    for (const [expectedPassed, delegateResult] of getFilterTestCases(1)) {
        it(`should return ${expectedPassed} if filters are delegate=${delegateResult}`, () => {
            const operation = {
                metadata: {
                    balance_updates: asReadonly([
                        { kind: BalanceUpdateKind.freezer, delegate: 'd1' } as BalanceUpdate,
                        { kind: BalanceUpdateKind.contract } as BalanceUpdate,
                        { kind: BalanceUpdateKind.freezer, delegate: 'd2' } as BalanceUpdate,
                    ]),
                },
            } as DoublePreendorsementEvidenceNotification;
            const target = create(DoublePreendorsementEvidenceSpecificFilter, {
                delegate: mockFilter(NullableAddressArrayFilter, delegateResult, ['d1', 'd2']),
            });

            const passed = target.passes(operation);

            expect(passed).toBe(expectedPassed);
        });
    }
});
