import { Filter } from '../../../../../src/entity/subscriptions/common-filters/filter';
import { makeNullable } from '../../../../../src/entity/subscriptions/common-filters/nullable-filter';
import { nameof, Nullish } from '../../../../../src/utils/reflection';
import { TestOptions } from './filter-test-helper';

class TargetFilter extends makeNullable(
    class DummyFilter implements Filter<string> {
        baseShouldPass = true;
        baseCalls: string[] = [];

        passes(value: string): boolean {
            this.baseCalls.push(value);
            return this.baseShouldPass;
        }
    },
) {}

interface TargetFilterInterface extends TargetFilter {
    baseShouldPass: boolean;
    baseCalls: string[];
}

describe(`${makeNullable.name}().${nameof<TargetFilter>('passes')}()`, () => {
    describe('base filter', () => {
        for (const baseShouldPass of [true, false]) {
            runTest(`super.${nameof<TargetFilter>('passes')}()`, {
                value: 'omg',
                filter: { baseShouldPass },
                expectedPassed: baseShouldPass,
                expectedBaseCalled: true,
            });
        }

        runTest('matches because of defaults in base test class', {
            value: 'omg',
            expectedPassed: true,
            expectedBaseCalled: true,
        });
    });

    for (const isNull of [undefined, null]) {
        describe(`${nameof<TargetFilter>('isNull')} = ${isNull}`, () => {
            for (const value of [undefined, null]) {
                runTest(`value is ${value}`, {
                    value,
                    filter: { isNull },
                    expectedPassed: true,
                    expectedBaseCalled: false,
                });
            }
        });
    }

    for (const isNull of [true, false]) {
        describe(`${nameof<TargetFilter>('isNull')} = ${isNull}`, () => {
            for (const value of [undefined, null]) {
                runTest(`value is ${value}`, {
                    value,
                    filter: { isNull },
                    expectedPassed: isNull,
                    expectedBaseCalled: false,
                });
            }

            runTest('value is defined', {
                value: 'omg',
                filter: { isNull },
                expectedPassed: !isNull,
                expectedBaseCalled: true,
            });
        });
    }

    interface NullableTestOptions extends TestOptions<Nullish<string>, TargetFilterInterface> {
        expectedBaseCalled: boolean;
    }

    function runTest(testCondition: string, options: NullableTestOptions) {
        it(`${nameof<TargetFilter>('passes')}() should be ${options.expectedPassed} if ${testCondition}`, () => {
            const target = Object.assign(new TargetFilter(), options.filter ?? {});

            const actual = target.passes(options.value);

            expect(actual).toBe(options.expectedPassed);
            expect(target.baseCalls).toEqual(options.expectedBaseCalled ? [options.value] : []);
        });
    }
});
