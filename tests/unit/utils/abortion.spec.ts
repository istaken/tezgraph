import { AbortController, AbortSignal } from 'abort-controller';
import { instance, mock, verify } from 'ts-mockito';

import { AbortError, addAbortListener, isAbortError, throwIfAborted } from '../../../src/utils/abortion';
import { expectToThrow, TestAbortError } from '../mocks';

describe('Abortion utils', () => {
    describe(isAbortError.name, () => {
        it.each([
            [true, 'test AbortError', new TestAbortError()],
            [true, 'our own AbortError', new AbortError()],
            [false, 'other Error', new Error()],
            [false, 'other type', 'foo'],
        ])('should return %s if %s', (expected: boolean, _desc: string, error: unknown) => {
            expect(isAbortError(error)).toBe(expected);
        });
    });

    interface Listener {
        action(): void;
    }

    describe(addAbortListener.name, () => {
        let controller: AbortController;
        let listener: Listener;

        beforeEach(() => {
            controller = new AbortController();
            listener = mock<Listener>();
        });

        it('should register listener', () => {
            addAbortListener(controller.signal, () => instance(listener).action());
            verify(listener.action()).never();

            controller.abort();
            verify(listener.action()).once();
        });

        it('should allow unsubscribe', () => {
            const unsubscribe = addAbortListener(controller.signal, () => instance(listener).action());
            unsubscribe();

            controller.abort();
            verify(listener.action()).never();
        });
    });

    describe(throwIfAborted.name, () => {
        it('should throw if aborted', () => {
            const signal = { aborted: true } as AbortSignal;

            expectToThrow(() => throwIfAborted(signal));
        });

        it('should pass if not aborted', () => {
            const signal = { aborted: false } as AbortSignal;

            expect(() => throwIfAborted(signal)).not.toThrow();
        });
    });
});
