import { ComponentHealthState } from '../../../../src/utils/health/component-health-state';
import { HealthCheckResult, HealthStatus } from '../../../../src/utils/health/health-check';
import { TestClock } from '../../mocks/test-clock';

class FooHealthState extends ComponentHealthState {
    name = 'Foo';
}

describe(ComponentHealthState.name, () => {
    let target: ComponentHealthState;
    let clock: TestClock;

    beforeEach(() => {
        clock = new TestClock();
        target = new FooHealthState(clock);
    });

    it('should initially return healthy', () => {
        const result = target.checkHealth();

        expect(result.status).toBe(HealthStatus.Degraded);
        expect(typeof result.data).toBe('string');
        expect(result.evaluatedOn).toBeUndefined();
    });

    it('should return result which was set', () => {
        target.set(HealthStatus.Degraded, { value: 123 });
        const result = target.checkHealth();

        expect(result).toEqual<HealthCheckResult>({
            status: HealthStatus.Degraded,
            data: { value: 123 },
            evaluatedOn: clock.nowDate,
        });
    });
});
