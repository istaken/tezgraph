import { instance, mock, verify, when } from 'ts-mockito';

import { ApolloHealthCheck } from '../../../../src/utils/health/apollo-health-check';
import { HealthProvider, HealthReport } from '../../../../src/utils/health/health-provider';

describe(ApolloHealthCheck.name, () => {
    let target: ApolloHealthCheck;
    let healthProvider: HealthProvider;

    beforeEach(() => {
        healthProvider = mock(HealthProvider);
        target = new ApolloHealthCheck(instance(healthProvider));
    });

    afterEach(() => {
        verify(healthProvider.generateHealthReport()).once();
    });

    it(`should call health provider for IsHealthy and not throw error`, async () => {
        setupHealthReport(true);

        await target.checkIsHealthy();
    });

    it(`should call health provider for IsHealthy and throw error`, async () => {
        setupHealthReport(false);

        const act = async () => target.checkIsHealthy();

        await expect(act()).rejects.toThrow();
    });

    function setupHealthReport(isHealthy: boolean) {
        when(healthProvider.generateHealthReport()).thenResolve({ isHealthy } as HealthReport);
    }
});
