import http from 'http';
import { Gauge } from 'prom-client';
import { spy, verify } from 'ts-mockito';

import { MetricsContainer } from '../../../../../src/utils/metrics/metrics-container';
import { HttpServerMetricsUpdater } from '../../../../../src/utils/metrics/updaters/http-server-metrics-updater';
import { expectToThrowAsync } from '../../../mocks';

describe(HttpServerMetricsUpdater.name, () => {
    let target: HttpServerMetricsUpdater;
    let httpServer: http.Server;
    let httpServerConnectionCountGauge: Gauge<string>;

    beforeEach(() => {
        const metrics = new MetricsContainer();
        httpServer = {} as http.Server;
        target = new HttpServerMetricsUpdater(metrics, httpServer);

        httpServerConnectionCountGauge = spy(metrics.httpServerConnectionCount);
    });

    it('should report connection count', async () => {
        httpServer.getConnections = (cb) => cb(null, 123);

        await target.update();

        verify(httpServerConnectionCountGauge.set(123));
    });

    it('should report error if unable to get connection count', async () => {
        const testError = new Error('oops');
        httpServer.getConnections = (cb) => cb(testError, 123);

        const error = await expectToThrowAsync(async () => target.update());

        expect(error).toBe(testError);
    });
});
