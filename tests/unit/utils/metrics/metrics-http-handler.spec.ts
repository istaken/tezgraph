import express from 'express';
import { Registry } from 'prom-client';
import { instance, mock, verify, when } from 'ts-mockito';

import { HttpHeader } from '../../../../src/utils/http-constants';
import { HttpHandler } from '../../../../src/utils/http-handler';
import { MetricsCollector } from '../../../../src/utils/metrics/metrics-collector';
import { MetricsHttpHandler } from '../../../../src/utils/metrics/metrics-http-handler';

describe(MetricsHttpHandler.name, () => {
    let target: HttpHandler;
    let prometheusRegistry: Registry;
    let response: express.Response;
    let metricsCollector: MetricsCollector;

    beforeEach(() => {
        prometheusRegistry = mock(Registry);
        metricsCollector = mock(MetricsCollector);
        target = new MetricsHttpHandler(instance(prometheusRegistry), instance(metricsCollector));

        response = mock<express.Response>();
    });

    it('should send metrics to HTTP response', async () => {
        when(metricsCollector.collectMetrics()).thenResolve('lol omg');
        when(prometheusRegistry.contentType).thenReturn('funny/memes');

        await target.handle(null!, instance(response));

        verify(response.setHeader(HttpHeader.ContentType, 'funny/memes'));
        verify(response.send('lol omg'));
    });
});
