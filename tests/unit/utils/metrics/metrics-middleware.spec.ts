import { Counter } from 'prom-client';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { NextFn, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../../../src/bootstrap/resolver-context';
import {
    GraphQLErrorCounterLabel,
    GraphQLQueryMethodLabel,
    MetricsContainer,
    QueryTypeFieldTotalLabel,
} from '../../../../src/utils/metrics/metrics-container';
import { MetricsMiddleware, ParentType, SERVICE_NAME } from '../../../../src/utils/metrics/metrics-middleware';
import { QueryMetricsUtils } from '../../../../src/utils/metrics/query-metrics-utils';
import { ResolveInfoParser } from '../../../../src/utils/query/resolve-info-parser';
import { expectToThrowAsync } from '../../mocks';
describe(MetricsMiddleware.name, () => {
    let queryMetricsUtils: QueryMetricsUtils;
    let target: MetricsMiddleware;
    let graphQLQueryMethodCounter: Counter<GraphQLQueryMethodLabel>;
    let graphQLErrorCounter: Counter<GraphQLErrorCounterLabel>;
    let queryTypeFieldTotal: Counter<QueryTypeFieldTotalLabel>;

    let metrics: MetricsContainer;
    let resolveInfoParser: ResolveInfoParser;

    let resolverData: ResolverData<ResolverContext>;

    let next: NextFn;

    const act = async () => target.use(resolverData, next);

    beforeEach(() => {
        graphQLQueryMethodCounter = mock(Counter);
        graphQLErrorCounter = mock(Counter);
        queryTypeFieldTotal = mock(Counter);
        resolveInfoParser = mock(ResolveInfoParser);

        metrics = {
            graphQLQueryMethodCounter: instance(graphQLQueryMethodCounter),
            graphQLErrorCounter: instance(graphQLErrorCounter),
            queryTypeFieldTotal: instance(queryTypeFieldTotal),
        } as MetricsContainer;

        queryMetricsUtils = new QueryMetricsUtils(metrics, resolveInfoParser);
        target = new MetricsMiddleware(metrics, queryMetricsUtils, instance(resolveInfoParser));

        resolverData = {
            info: {
                fieldName: 'transactions',
                parentType: { name: 'Explosion' },
            },
            args: {},
        } as ResolverData<ResolverContext>;

        when(resolveInfoParser.parse(resolverData.info)).thenReturn({
            alias: 'account',
            args: {
                address: 'tz1QAqFKkRGkHvjLiKByTZKZUvvupN1vPxeZ',
            },
            fieldsByTypeName: {
                Account: {
                    address: {
                        alias: 'address',
                        args: {},
                        fieldsByTypeName: {},
                        name: 'address',
                    },
                },
            },
            name: 'account',
        });
        next = async () => Promise.resolve('resultData');
    });

    it('should not increment query counter if not query', async () => {
        const result = await act();

        expect(result).toBe('resultData');
        verify(graphQLQueryMethodCounter.inc(anything())).never();
        verify(graphQLErrorCounter.inc(anything())).never();
        verify(graphQLQueryMethodCounter.inc(anything())).never();
    });

    it.each([ParentType.Query, ParentType.Mutation])('should increment query counter if %s', async (type) => {
        resolverData.info.parentType.name = type;

        const result = await act();

        expect(result).toBe('resultData');
        verify(resolveInfoParser.parse(anything())).once();
        verify(graphQLQueryMethodCounter.inc(anything())).once();
        verify(graphQLQueryMethodCounter.inc(deepEqual({ method: 'transactions', service: SERVICE_NAME }))).once();
        verify(graphQLErrorCounter.inc(anything())).never();
    });

    it('should increment error counter if next throws', async () => {
        const nextError = new Error('oops');
        next = async () => Promise.reject(nextError);

        const error = await expectToThrowAsync(act);

        expect(error).toBe(nextError);
        verify(graphQLErrorCounter.inc(anything())).once();
        verify(
            graphQLErrorCounter.inc(deepEqual({ type: 'Explosion', field: 'transactions', service: SERVICE_NAME })),
        ).once();
    });
});
