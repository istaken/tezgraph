import { Registry } from 'prom-client';
import { instance, mock, verify, when } from 'ts-mockito';

import { MetricsCollector, MetricsUpdater } from '../../../../src/utils/metrics/metrics-collector';
import { TestClock } from '../../mocks/test-clock';

describe(MetricsCollector.name, () => {
    let target: MetricsCollector;
    let prometheusRegistry: Registry;
    let updater1: MetricsUpdater;
    let updater2: MetricsUpdater;
    let clock: TestClock;

    beforeEach(() => {
        prometheusRegistry = mock(Registry);
        updater1 = mock<MetricsUpdater>();
        updater2 = mock<MetricsUpdater>();
        clock = new TestClock();
        target = new MetricsCollector(instance(prometheusRegistry), [instance(updater1), instance(updater2)], clock);

        when(prometheusRegistry.metrics()).thenResolve('metricsStr1', 'metricsStr2');
    });

    it.each([
        ['initially', 0, 'metricsStr1', 1],
        ['without update if updated recently', 999, 'metricsStr2', 1],
        ['and update them before', 1_000, 'metricsStr2', 1],
    ])('should collect metrics %s', async (_desc, delayMillis, expectedMetrics, expectedUpdateCount) => {
        if (delayMillis !== 0) {
            await target.collectMetrics();
            clock.tick(delayMillis);
        }

        const metrics = await target.collectMetrics();

        expect(metrics).toBe(expectedMetrics);
        verify(updater1.update()).times(expectedUpdateCount);
        verify(updater2.update()).times(expectedUpdateCount);
    });
});
