import { AbortController, AbortSignal } from 'abort-controller';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { AbortControllerFactory } from '../../../../src/utils/app-control/abort-controller-factory';
import { AsyncIterableProcessor } from '../../../../src/utils/async-iterable/async-iterable-processor';
import { AsyncIterableWorker, ProcessingStatus } from '../../../../src/utils/async-iterable/async-iterable-worker';
import { nameof } from '../../../../src/utils/reflection';
import { expectToThrow, expectToThrowAsync, PromiseSource } from '../../mocks';

describe(AsyncIterableWorker.name, () => {
    let target: AsyncIterableWorker;
    let processor: AsyncIterableProcessor;
    let globalAbortSignal: AbortSignal;

    let linkedAbortController: AbortController;
    let processorPromise: PromiseSource<void>;

    beforeEach(() => {
        processor = mock<AsyncIterableProcessor>();
        globalAbortSignal = 'global-abort' as unknown as AbortSignal;
        const abortControllerFactory = mock(AbortControllerFactory);
        target = new AsyncIterableWorker(
            'Foo',
            instance(processor),
            globalAbortSignal,
            instance(abortControllerFactory),
        );

        processorPromise = new PromiseSource();
        when(processor.processItems(anything())).thenReturn(processorPromise.promise);

        linkedAbortController = new AbortController();
        when(abortControllerFactory.createLinked(globalAbortSignal)).thenReturn(linkedAbortController);
    });

    describe(nameof<AsyncIterableWorker>('name'), () => {
        it('should be correct', () => {
            expect(target.name).toBe('Foo');
        });
    });

    describe(nameof<AsyncIterableWorker>('status'), () => {
        it.each([ProcessingStatus.Stopped])('should be %s by default', (expected) => {
            expect(target.status).toBe(expected);
        });
    });

    describe(nameof<AsyncIterableWorker>('start'), () => {
        it('should start processing items', () => {
            target.start();

            verify(processor.processItems(linkedAbortController.signal)).once();
            expect(target.status).toBe(ProcessingStatus.Running);
        });

        it('should throw if processing already started', () => {
            target.start();

            const error = expectToThrow(() => target.start());

            expect(error.message).toInclude('already running');
        });
    });

    describe(nameof<AsyncIterableWorker>('stop'), () => {
        it('should abort processing and wait for it to end', async () => {
            target.start();

            const promise = target.stop();

            expect(linkedAbortController.signal.aborted).toBeTrue();
            expect(target.status).toBe(ProcessingStatus.Stopping);

            processorPromise.resolve();
            await promise;
            expect(target.status).toBe(ProcessingStatus.Stopped);
        });

        it('should throw if no iterable.return on processor', async () => {
            const error = await expectToThrowAsync(async () => target.stop());

            expect(error.message).toInclude('NOT running');
        });
    });
});
