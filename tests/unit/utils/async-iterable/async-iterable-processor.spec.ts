import { AbortSignal } from 'abort-controller';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { AbortError } from '../../../../src/utils/abortion';
import { AsyncIterableProvider, ItemProcessor } from '../../../../src/utils/async-iterable';
import { AsyncIterableProcessor } from '../../../../src/utils/async-iterable/async-iterable-processor';
import { toAsyncIterable } from '../../../../src/utils/collections';
import { LogLevel } from '../../../../src/utils/logging';
import { TestLogger } from '../../mocks';
import { TestClock } from '../../mocks/test-clock';

describe(AsyncIterableProcessor.name, () => {
    let target: AsyncIterableProcessor<number>;
    let iterableProvider: AsyncIterableProvider<number>;
    let itemProcessor: ItemProcessor<number>;
    let abortSignalMock: AbortSignal;
    let abortSignal: AbortSignal;
    let clock: TestClock;
    let logger: TestLogger;

    beforeEach(() => {
        iterableProvider = mock<AsyncIterableProvider<number>>();
        itemProcessor = mock<ItemProcessor<number>>();
        const itemInfoProvider = { getInfo: (x: number) => `info-${x}` };
        abortSignalMock = mock(AbortSignal);
        abortSignal = instance(abortSignalMock);
        clock = new TestClock();
        logger = new TestLogger();
        target = new AsyncIterableProcessor(
            instance(iterableProvider),
            instance(itemProcessor),
            itemInfoProvider,
            clock,
            logger,
        );

        when(iterableProvider.iterate(abortSignal))
            .thenReturn(toAsyncIterable([1, 2]))
            .thenReturn(mockAbortedIterable([3]));
        when(abortSignalMock.aborted).thenReturn(false).thenReturn(false).thenReturn(true);
    });

    it('should expose empty defaults', () => {
        expect(target.lastItemInfo).toBeUndefined();
        expect(target.lastError).toBeUndefined();
        expect(target.lastSuccessTime).toEqual(new Date(0));
    });

    it('should continuously process items', async () => {
        await target.processItems(abortSignal);

        verify(itemProcessor.processItem(anything())).times(3);
        verify(itemProcessor.processItem(1)).calledBefore(itemProcessor.processItem(2));
        verify(itemProcessor.processItem(2)).calledBefore(itemProcessor.processItem(3));

        expect(target.lastItemInfo).toBe('info-3');
        expect(target.lastSuccessTime).toBe(clock.nowDate);
        expect(target.lastError).toBeUndefined();

        logger.verifyLoggedCount(3);
        logger.logged(0).verify(LogLevel.Information).verifyMessageIncludesAll('Start');
        logger.logged(1).verify(LogLevel.Error);
        logger.logged(2).verify(LogLevel.Information).verifyMessageIncludesAll('aborted');
    });

    it('should keep processing items if some item processing failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(1)).thenThrow(error);

        await target.processItems(abortSignal);

        verify(itemProcessor.processItem(2)).once();
        expect(target.lastError).toBeUndefined();

        logger.verifyLoggedCount(4);
        logger.logged(1).verify(LogLevel.Error, { item: 'info-1', error });
    });

    it('should expose error if last item failed', async () => {
        const error = new Error('WTF');
        when(itemProcessor.processItem(3)).thenThrow(error);

        await target.processItems(abortSignal);

        expect(target.lastError).toBe(error);
    });
});

async function* mockAbortedIterable<T>(array: readonly T[]): AsyncIterableIterator<T> {
    await Promise.resolve();
    yield* array;
    throw new AbortError();
}
