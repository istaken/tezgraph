import { externalTriggers } from '../../../../src/utils';
import { testUniqueTriggers } from './pubsub-trigger-tests';

describe('externalTriggers', () => {
    testUniqueTriggers(externalTriggers);
});
