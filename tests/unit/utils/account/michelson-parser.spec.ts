import { mock } from 'ts-mockito';

import { Micheline } from '../../../../src/entity/scalars';
import { BigmapDataSource } from '../../../../src/modules/queries-graphql/datasources/bigmap-datasource';
import MichelsonParser from '../../../../src/modules/queries-graphql/repositories/michelson-parser';
import { Logger } from '../../../../src/utils/logging';

describe('MichelsonParser', () => {
    const logger = mock<Logger>();
    const bigmapDataSource = mock<BigmapDataSource>();
    const michelsonParser: MichelsonParser = new MichelsonParser(logger, bigmapDataSource);

    describe('convertMichelsonJsonToCanonicalForm', () => {
        it('return a canonical michelson json string as a valid michelson object', () => {
            const michelsonJSON: Micheline = {
                args: [
                    {
                        args: [
                            {
                                args: [
                                    {
                                        args: [
                                            {
                                                args: [
                                                    {
                                                        int: '100000000000000000',
                                                    },
                                                    {
                                                        int: '20523',
                                                    },
                                                ],
                                                prim: 'Pair',
                                            },
                                            {
                                                string: 'tz1QKmewVc3nMi2MHBqeeowPHtffr3YyLxms',
                                            },
                                        ],
                                        prim: 'Pair',
                                    },
                                ],
                                prim: 'Left',
                            },
                        ],
                        prim: 'Left',
                    },
                ],
                prim: 'Right',
            };

            expect(michelsonParser.convertMichelsonJsonToCanonicalForm(michelsonJSON)).toBe(
                '{"prim":"Right","args":[{"prim":"Left","args":[{"prim":"Left","args":[{"prim":"Pair","args":[{"prim":"Pair","args":[{"int":"100000000000000000"},{"int":"20523"}]},{"string":"tz1QKmewVc3nMi2MHBqeeowPHtffr3YyLxms"}]}]}]}]}',
            );
        });
    });

    describe('convertMichelsonJsonToMichelsonString', () => {
        it('return a michelson json string as a valid michelson object', () => {
            const michelsonJSON: Micheline = [
                { prim: 'DROP' },
                { prim: 'NIL', args: [{ prim: 'operation' }] },
                { prim: 'PUSH', args: [{ prim: 'key_hash' }, { string: 'tz1gR18MNcRqTANRzpbxUvZUvCxnQzSPtg5n' }] },
                { prim: 'IMPLICIT_ACCOUNT' },
                { prim: 'PUSH', args: [{ prim: 'mutez' }, { int: '312900000' }] },
                { prim: 'UNIT' },
                { prim: 'TRANSFER_TOKENS' },
                { prim: 'CONS' },
            ];

            expect(michelsonParser.convertMichelsonJsonToMichelsonString(michelsonJSON)).toBe(
                '{DROP; NIL operation; PUSH key_hash "tz1gR18MNcRqTANRzpbxUvZUvCxnQzSPtg5n"; IMPLICIT_ACCOUNT; PUSH mutez 312900000; UNIT; TRANSFER_TOKENS; CONS}',
            );
        });
    });
});
