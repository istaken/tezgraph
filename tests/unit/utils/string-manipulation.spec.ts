import {
    humanReadable,
    isWhiteSpace,
    joinQuoted,
    removeRequiredPrefix,
    removeRequiredSuffix,
    replaceAll,
} from '../../../src/utils/string-manipulation';
import { expectToThrow } from '../mocks';

describe('String manipulation utils', () => {
    describe(removeRequiredPrefix.name, () => {
        it('should remove prefix correctly', () => {
            const result = removeRequiredPrefix('Hello world', 'Hello ');

            expect(result).toBe('world');
        });

        it(`should throw if input doesn't start with the prefix`, () => {
            const error = expectToThrow(() => removeRequiredPrefix('Hello world', 'not prefix'));

            expect(error.message).toIncludeMultiple([`'Hello world'`, `'not prefix'`]);
        });
    });

    describe(removeRequiredSuffix.name, () => {
        it('should remove suffix correctly', () => {
            const result = removeRequiredSuffix('Hello world', ' world');

            expect(result).toBe('Hello');
        });

        it(`should throw if input doesn't end with the suffix`, () => {
            const error = expectToThrow(() => removeRequiredSuffix('Hello world', 'not suffix'));

            expect(error.message).toIncludeMultiple([`'Hello world'`, `'not suffix'`]);
        });
    });

    describe(isWhiteSpace.name, () => {
        it.each([
            ['undefined', true, undefined],
            ['null', true, null],
            ['empty', true, ''],
            ['spaces', true, '  '],
            ['tabs', true, '\t'],
            ['text', false, 'abc'],
            ['text with white-spaces', false, 'a b\tc'],
        ])('should take %s and return %s', (_desc, expected, input) => {
            expect(isWhiteSpace(input)).toBe(expected);
        });
    });

    describe(joinQuoted.name, () => {
        it('should quote and join strings', () => {
            const str = joinQuoted(['a', '', 'b']);

            expect(str).toBe(`'a', '', 'b'`);
        });
    });

    describe(humanReadable.name, () => {
        it.each([
            ['BigMapDiff', 'big map diff'],
            ['balanceUpdate', 'balance update'],
            ['internal_operation_result', 'internal operation result'],
        ])('should convert "%s" correctly', (input, expected) => {
            const str = humanReadable(input);

            expect(str).toBe(expected);
        });
    });

    describe(replaceAll.name, () => {
        it('should replace all occurrences', () => {
            const str = replaceAll('Hou, Hou, Hou! Merry Christmas!', 'Hou', 'Fuck');

            expect(str).toBe('Fuck, Fuck, Fuck! Merry Christmas!');
        });
    });
});
