import { AbortController } from 'abort-controller';

import { AbortControllerFactory } from '../../../../src/utils/app-control/abort-controller-factory';

describe(AbortControllerFactory.name, () => {
    const target = new AbortControllerFactory();

    it.each([true, false])('should abort if parent is already aborted %s', (parentAborted) => {
        const parentController = new AbortController();
        if (parentAborted) {
            parentController.abort();
        }

        const linkedController = target.createLinked(parentController.signal);

        expect(linkedController.signal.aborted).toBe(parentAborted);
    });

    it('should abort if parent gets aborted %s', () => {
        const parentController = new AbortController();
        const linkedController = target.createLinked(parentController.signal);

        parentController.abort();

        expect(linkedController.signal.aborted).toBeTrue();
    });
});
