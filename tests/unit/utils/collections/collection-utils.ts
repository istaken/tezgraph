import { withIndex } from '../../../../src/utils/collections/collection-utils';

describe(withIndex.name, () => {
    it('should iterate items with their index', () => {
        const results = withIndex(['a', 'b', 'c']);

        expect(results).toEqual([
            ['a', 0],
            ['b', 1],
            ['c', 2],
        ]);
    });
});
