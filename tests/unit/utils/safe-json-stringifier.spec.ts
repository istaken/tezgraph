import { safeJsonStringify } from '../../../src/utils/safe-json-stringifier';

describe(safeJsonStringify.name, () => {
    const primitiveTestCases = [
        {
            desc: 'undefined',
            expected: 'null',
        },
        {
            desc: 'null',
            input: null,
            expected: 'null',
        },
        {
            desc: 'number',
            input: 123,
            expected: '123',
        },
        {
            desc: 'string',
            input: 'omg',
            expected: '"omg"',
        },
        {
            desc: 'big int',
            input: BigInt(123),
            expected: '"123"',
        },
    ];

    const objectTestCases = primitiveTestCases.map((t) => ({
        desc: `object with ${t.desc} property`,
        input: { prop: t.input },
        expected: `{"prop":${t.expected}}`,
    }));

    const obj = { value: 123, circle: null as any };
    obj.circle = obj;
    const circularReferenceTestCase = {
        desc: 'circular object',
        input: obj,
        expected: '{"value":123,"circle":"(circular reference)"}',
    };

    const testCases = [...primitiveTestCases, ...objectTestCases, circularReferenceTestCase];

    for (const { desc, input, expected } of testCases) {
        it(`should stringify ${desc} correctly`, () => {
            const json = safeJsonStringify(input);

            expect(json).toBe(expected);
        });
    }

    it(`should correctly convert to string if value is Error`, () => {
        const error = new Error('wtf');

        const str = safeJsonStringify(error);

        expect(str).toStartWith('"Error: wtf');
        expect(str).toEndWith('"');
        expect(str).toIncludeMultiple(['at', '<anonymous>']);
    });
});
