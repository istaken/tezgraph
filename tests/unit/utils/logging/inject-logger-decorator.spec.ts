import { instance, mock, when } from 'ts-mockito';
import { container, singleton } from 'tsyringe';

import { injectLogger, Logger, LoggerFactory } from '../../../../src/utils/logging';

@singleton()
class Foo {
    constructor(@injectLogger(Foo) readonly logger: Logger) {}
}

describe(injectLogger.name, () => {
    it('should inject correct logger', () => {
        const factory = mock(LoggerFactory);
        const logger = {} as Logger;
        container.registerInstance(LoggerFactory, instance(factory));
        when(factory.getLogger(Foo)).thenReturn(logger);

        const foo = container.resolve(Foo);

        expect(foo.logger).toBe(logger);
    });
});
