import { LoggerFactory, RootLogger } from '../../../../src/utils/logging';
import { MetricsContainer } from '../../../../src/utils/metrics/metrics-container';
import { nameof } from '../../../../src/utils/reflection';
import { Clock } from '../../../../src/utils/time/clock';

class MyService {}

describe(LoggerFactory.name, () => {
    let target: LoggerFactory;
    let rootLogger: RootLogger;
    let clock: Clock;
    let metrics: MetricsContainer;

    beforeEach(() => {
        rootLogger = {} as RootLogger;
        clock = {} as Clock;
        metrics = new MetricsContainer();
        target = new LoggerFactory(rootLogger, clock, metrics);
    });

    const categories = [
        { type: 'string', value: 'FooBar', expected: 'FooBar' },
        { type: 'constructor', value: MyService, expected: 'MyService' },
    ];

    categories.forEach((category) => {
        it(`${nameof<LoggerFactory>('getLogger')}(${category.type}) should create logger correctly`, () => {
            const logger: any = target.getLogger(category.value);

            expect(logger.category).toBe(category.expected);
            expect(logger.rootLogger).toBe(rootLogger);
            expect(logger.clock).toBe(clock);
        });
    });
});
