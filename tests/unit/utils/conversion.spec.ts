import { create, deepFreeze, errorObjToString, errorToString } from '../../../src/utils/conversion';
import { expectToThrow } from '../mocks';

class Foo {
    readonly firstName!: string;
    readonly lastName!: string;
}

describe('Conversion utils', () => {
    describe(errorToString.name, () => {
        const testCases = [
            { desc: 'error', error: new Error('Oops'), expected: 'Oops' },
            { desc: 'string', error: 'OMG', expected: 'OMG' },
            { desc: 'number', error: 123, expected: '123' },
            { desc: 'object literal', error: { meme: 'lol' }, expected: '{"meme":"lol"}' },
            { desc: 'empty string', error: '', expected: '' },
            { desc: 'undefined', error: undefined, expected: 'undefined' },
            { desc: 'null', error: null, expected: 'null' },
        ];

        testCases.forEach(({ desc, error, expected }) => {
            it(`should correctly convert to string if value is ${desc} and onlyMessage flag`, () => {
                const str = errorToString(error, { onlyMessage: true });

                expect(str).toBe(expected);
            });
        });

        it(`should correctly convert to string if value is Error`, () => {
            const error = new Error('wtf');

            const str = errorToString(error);

            expect(str).toStartWith('Error: wtf');
            expect(str).toIncludeMultiple(['at', '<anonymous>']);
        });
    });

    describe(errorObjToString.name, () => {
        it('should include error stack trace', () => {
            const error = foo();

            const str = errorObjToString(error);

            expect(str).toStartWith('Error: omg');
            expect(str).toIncludeMultiple(['at', foo.name, '<anonymous>']);
        });

        function foo(): Error {
            try {
                throw new Error('omg');
            } catch (error: any) {
                return error;
            }
        }

        it('should return toString() if no stack', () => {
            const error = { toString: () => 'wtf' } as Error;

            const str = errorObjToString(error);

            expect(str).toBe('wtf');
        });
    });

    describe(create.name, () => {
        it('should create object and assign properties even if read-only', () => {
            const foo = create(Foo, { firstName: 'James' }, { lastName: 'Bond' });

            expect(foo).toBeInstanceOf(Foo);
            expect(foo).toEqual<Foo>({
                firstName: 'James',
                lastName: 'Bond',
            });
        });
    });

    describe(deepFreeze.name, () => {
        it('should make object read-only', () => {
            const obj = {
                foo: {
                    bar: 123,
                },
                meme: 'omg',
                lambda: () => 'Hello lambda',
                func(): string {
                    return 'Hello func';
                },
            };

            const result = deepFreeze(obj);

            expect(result).toBe(obj);
            expectToThrow(() => {
                obj.meme = 'omg';
            });
            expectToThrow(() => {
                obj.foo.bar = 456;
            });
        });
    });
});
