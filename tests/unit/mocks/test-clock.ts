import { Clock } from '../../../src/utils/time/clock';

export class TestClock implements Clock {
    nowDate: Date;

    constructor(nowDate?: Date) {
        this.nowDate = nowDate ?? getRandomDate();
    }

    getNowDate(): Date {
        return this.nowDate;
    }

    getNowTimestamp(): number {
        return this.nowDate.getTime();
    }

    tick(millis?: number) {
        this.nowDate = new Date(this.nowDate.getTime() + (millis ?? Math.random() * 1_000_000));
    }
}

export function getRandomDate(): Date {
    return new Date(Math.random() * 10_000_000_000_000);
}
