export function expectToThrow(act: () => any): Error {
    try {
        act();
    } catch (error) {
        return verifyReceived(error);
    }
    return onNothingThrown();
}

export async function expectToThrowAsync(act: () => Promise<any>): Promise<Error> {
    try {
        await act();
    } catch (error) {
        return verifyReceived(error);
    }
    onNothingThrown();
}

function verifyReceived(error: any): Error {
    expect(error).toBeInstanceOf(Error);
    return error;
}

function onNothingThrown(): never {
    throw new Error(`Expected given function to throw Error but nothing was thrown.`);
}
