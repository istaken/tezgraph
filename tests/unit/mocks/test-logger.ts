/* eslint-disable jest/no-standalone-expect */
import { LogData, Logger, LogLevel } from '../../../src/utils/logging';

export class LoggedEntry {
    constructor(
        readonly level: LogLevel,
        readonly message: string,
        readonly data: Readonly<Record<string, any>> | undefined,
    ) {}

    verify(expectedLevel: LogLevel, expectedData?: LogData): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        if (expectedData) {
            expect(this.data).toEqual(expectedData);
        }
        expect(this.level).toBe(expectedLevel);
        return this;
    }

    verifyData(expectedData: LogData | undefined): LoggedEntry {
        this.verifyMessagePlaceholdersInData();
        expect(this.data).toEqual(expectedData);
        return this;
    }

    verifyMessageIncludesAll(...expectedSubstrings: string[]): LoggedEntry {
        expectedSubstrings.forEach((s) => expect(this.message).toContain(s));
        return this;
    }

    verifyMessagePlaceholdersInData(): void {
        const messageKeys = [...(this.message as any).matchAll(/\{[^\\}]+\}/gu)]
            .map((m) => m.toString() as string)
            .map((s) => s.substr(1, s.length - 2))
            .sort();
        const dataKeys = Object.keys(this.data ?? {}).sort();

        expect(dataKeys).toEqual(expect.arrayContaining(messageKeys));
    }
}

export class TestLogger extends Logger {
    private readonly loggedEntries: LoggedEntry[] = [];

    constructor() {
        super(null!, null!, null!, null!);
    }

    isEnabled(_level: LogLevel): boolean {
        return true;
    }

    log(level: LogLevel, message: string, data?: LogData): void {
        const entry = new LoggedEntry(level, message, data);
        entry.verifyMessagePlaceholdersInData();
        this.loggedEntries.push(entry);
    }

    logged(index: number): LoggedEntry {
        expect(this.loggedEntries.length).toBeGreaterThanOrEqual(index + 1);
        return this.loggedEntries[index]!;
    }

    loggedSingle(level?: LogLevel): LoggedEntry {
        const entries = this.loggedEntries.filter((e) => !level || e.level === level);
        expect(entries).toHaveLength(1);
        return entries[0]!;
    }

    verifyLoggedCount(expectedCount: number): void {
        // Assert on "level" to give more useful error message.
        expect(this.loggedEntries.map((e) => e.level)).toHaveLength(expectedCount);
    }

    verifyNothingLogged() {
        // Assert on "level" to give more useful error message.
        expect(this.loggedEntries.map((e) => e.level)).toEqual([]);
    }

    verifyNotLogged(unexpectedLevel: LogLevel, ...otherUnexpectedLevels: LogLevel[]) {
        for (const level of [unexpectedLevel, ...otherUnexpectedLevels]) {
            expect(this.loggedEntries.map((e) => e.level)).not.toContain(level);
        }
    }
}
