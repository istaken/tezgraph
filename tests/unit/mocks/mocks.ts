import { range } from 'lodash';

export class PromiseSource<T> {
    readonly promise: Promise<T>;
    resolve: (value: T) => void = undefined!;
    reject: (reason?: any) => void = undefined!;

    constructor() {
        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    }
}

export class TestAbortError extends Error {
    readonly name = 'AbortError';
}

type ItemGenerator<T> = (index: number) => T;

export function generateRange<T>(count: 2, generator: ItemGenerator<T>): [T, T];
export function generateRange<T>(count: 3, generator: ItemGenerator<T>): [T, T, T];
export function generateRange<T>(count: 4, generator: ItemGenerator<T>): [T, T, T, T];
export function generateRange<T>(count: 5, generator: ItemGenerator<T>): [T, T, T, T, T];
export function generateRange<T>(count: number, generator: ItemGenerator<T>): T[] {
    return range(count).map(generator);
}
