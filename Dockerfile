FROM node:17.8 AS build-env
WORKDIR /app/prod_node_modules
ADD package*.json /app/prod_node_modules/
RUN npm set-script prepare ""
RUN npm ci --only=production
ADD ./prisma /app/prod_node_modules/prisma/
RUN npx prisma generate

WORKDIR /app
ADD package*.json /app/
RUN npm set-script prepare ""
RUN npm ci
ADD ./prisma /app/prisma/
RUN npx prisma generate
ADD . /app/
RUN npm run build

FROM node:17.8-alpine
COPY --from=build-env /app/dist /app
COPY --from=build-env /app/prod_node_modules/node_modules /app/node_modules

WORKDIR /app

ARG GIT_TAG
ARG GIT_SHA
ARG APP_URL
ARG APP_NAME

ENV ENABLE_DEBUG=false
ENV TZ=UTC

# App Version Env Variables
ENV GIT_TAG=$GIT_TAG
ENV GIT_SHA=$GIT_SHA
ENV APP_NAME=$APP_NAME
ENV APP_URL=$APP_URL

WORKDIR /app
EXPOSE 3000
CMD ["node", "index.js"]
