### Description

What was changed and why?

### Additional Notes/Related Links

### Merge Request Checklist

-   [ ] This MR has integration tests for changes that affect the GraphQL schema
-   [ ] This MR covers all failure cases and returns meaningful error messages
-   [ ] This MR updates all affected documentation as needed
-   [ ] If this MR affects deployment, this MR provides details on the how and what changes need to be made.
-   [ ] The terms used in MR are consistent with the Queries and Subscriptions (fields, filters, etc.)
-   [ ] The data types used in MR are consistent with the Queries and Subscriptions (fields, filters, etc.)
-   [ ] This MR adds to or updates the existing /metrics and /health pages as needed
