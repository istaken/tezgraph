import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    BigMapAlloc,
    BigMapCopy,
    BigMapDiff,
    BigMapDiffAction,
    BigMapRemove,
    BigMapUpdate,
} from '../../../entity/subscriptions';
import { typed } from '../../../utils/reflection';
import { primitives } from './primitives';
import { RpcCollectionConverter } from './rpc-collection-converter';

@singleton()
export class BigMapDiffConverter extends RpcCollectionConverter<rpc.ContractBigMapDiffItem, BigMapDiff> {
    convert(rpcDiff: rpc.ContractBigMapDiffItem): BigMapDiff {
        primitives.object.guard(rpcDiff);
        const action = primitives.enum.convertToNonNullable(BigMapDiffAction, rpcDiff.action);

        switch (action) {
            case BigMapDiffAction.alloc:
                return typed<BigMapAlloc>({
                    graphQLTypeName: BigMapAlloc.name,
                    action,
                    big_map: primitives.bigInt.convertToNonNullable(rpcDiff.big_map),
                    key_type: primitives.michelson.convertToNonNullable(rpcDiff.key_type),
                    value_type: primitives.michelson.convertToNonNullable(rpcDiff.value_type),
                });
            case BigMapDiffAction.copy:
                return typed<BigMapCopy>({
                    graphQLTypeName: BigMapCopy.name,
                    action,
                    source_big_map: primitives.bigInt.convertToNonNullable(rpcDiff.source_big_map),
                    destination_big_map: primitives.bigInt.convertToNonNullable(rpcDiff.destination_big_map),
                });
            case BigMapDiffAction.remove:
                return typed<BigMapRemove>({
                    graphQLTypeName: BigMapRemove.name,
                    action,
                    big_map: primitives.bigInt.convertToNonNullable(rpcDiff.big_map),
                });
            case BigMapDiffAction.update:
                return typed<BigMapUpdate>({
                    graphQLTypeName: BigMapUpdate.name,
                    action,
                    big_map: primitives.bigInt.convertToNonNullable(rpcDiff.big_map),
                    key_hash: primitives.string.convertToNonNullable(rpcDiff.key_hash),
                    key: primitives.michelson.convertToNonNullable(rpcDiff.key),
                    value: primitives.michelson.convertNullable(rpcDiff.value),
                });
        }
    }
}
