import * as rpc from '@taquito/rpc';

import { PrimitiveConverter } from './primitive-converter';

const strValidationRegex = /^\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z$/u;

export class DateConverter extends PrimitiveConverter<rpc.TimeStampMixed, Date> {
    protected readonly typeDescription = 'date';

    protected convertInternal(rpcValue: unknown): Date {
        if (rpcValue instanceof Date) {
            return rpcValue;
        }
        if (typeof rpcValue !== 'string' || !strValidationRegex.test(rpcValue)) {
            throw new Error(
                `The value must be a Date or an ISO date string according to regex /${strValidationRegex.toString()}/.`,
            );
        }
        return new Date(rpcValue);
    }
}
