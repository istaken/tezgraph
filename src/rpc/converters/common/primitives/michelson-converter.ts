import * as rpc from '@taquito/rpc';

import { Micheline } from '../../../../entity/scalars';
import { isObjectLiteral } from '../../../../utils/reflection';
import { PrimitiveConverter } from './primitive-converter';

export class MichelsonConverter extends PrimitiveConverter<rpc.MichelsonV1Expression, Micheline> {
    protected readonly typeDescription = 'michelson';

    protected convertInternal(rpcValue: unknown): Micheline {
        if (!isObjectLiteral(rpcValue) && !Array.isArray(rpcValue)) {
            throw new Error('The value must be an object or an array to be a valid Michelson.');
        }
        return rpcValue;
    }
}
