import { isNotNullish, Nullish } from '../../../../utils/reflection';
import { safeJsonStringify } from '../../../../utils/safe-json-stringifier';

type Enum<TKey extends string, TEnum extends string> = Readonly<Record<TKey, TEnum>>;

export class EnumConverter {
    convert<TKey extends string, TEnum extends string>(enumType: Enum<TKey, TEnum>, rpcValue: TKey): TEnum {
        return this.convertToNonNullable(enumType, rpcValue);
    }

    convertNullable<TKey extends string, TEnum extends string>(
        enumType: Enum<TKey, TEnum>,
        rpcValue: Nullish<TKey>,
    ): TEnum | null {
        return isNotNullish(rpcValue) ? this.convert(enumType, rpcValue) : null;
    }

    /** Overload useful when common interface (e.g. Taquito) has nullable property but we know it must be non-nullable. */
    convertToNonNullable<TKey extends string, TEnum extends string>(
        enumType: Enum<TKey, TEnum>,
        rpcValue: Nullish<TKey>,
    ): TEnum {
        const value = typeof rpcValue === 'string' ? enumType[rpcValue] : null;
        if (!value) {
            throw new Error(
                `The value ${safeJsonStringify(rpcValue)} from RPC is not a supported enum value.` +
                    ` Supported enum values: ${Object.keys(enumType).join()}.`,
            );
        }
        return value;
    }
}
