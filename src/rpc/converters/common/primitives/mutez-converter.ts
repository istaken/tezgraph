import { BigIntConverter } from './big-int-converter';

export class MutezConverter extends BigIntConverter {
    protected readonly typeDescription: string = 'mutez';

    protected convertInternal(rpcValue: unknown): bigint {
        const value = super.convertInternal(rpcValue);

        if (value < BigInt('0')) {
            throw new Error('The value must positive or zero.');
        }
        return value;
    }
}
