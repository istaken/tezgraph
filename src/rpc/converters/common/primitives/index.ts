import { ArrayConverter } from './array-converter';
import { BigIntConverter } from './big-int-converter';
import { BooleanConverter } from './boolean-converter';
import { DateConverter } from './date-converter';
import { EnumConverter } from './enum-converter';
import { IntConverter } from './int-converter';
import { MichelsonConverter } from './michelson-converter';
import { MutezConverter } from './mutez-converter';
import { ObjectConverter } from './object-converter';
import { StringConverter } from './string-converter';

export const primitives = {
    array: new ArrayConverter(),
    bigInt: new BigIntConverter(),
    boolean: new BooleanConverter(),
    date: new DateConverter(),
    enum: new EnumConverter(),
    int: new IntConverter(),
    michelson: new MichelsonConverter(),
    mutez: new MutezConverter(),
    object: new ObjectConverter(),
    string: new StringConverter(),
} as const;
