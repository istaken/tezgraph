import { errorToString } from '../../../../utils/conversion';
import { safeJsonStringify } from '../../../../utils/safe-json-stringifier';
import { RpcConverter } from '../rpc-converter';

export abstract class PrimitiveConverter<TRpc, TTarget> extends RpcConverter<TRpc, TTarget> {
    protected abstract readonly typeDescription: string;

    convert(rpcValue: TRpc): TTarget {
        try {
            return this.convertInternal(rpcValue);
        } catch (error: unknown) {
            throw new Error(
                `Failed conversion to ${this.typeDescription} from RPC value. See stack trace to find out respective property.` +
                    ` RPC value: ${safeJsonStringify(rpcValue)}. Error: ${errorToString(error)}`,
            );
        }
    }

    protected abstract convertInternal(rpcValue: unknown): TTarget;
}
