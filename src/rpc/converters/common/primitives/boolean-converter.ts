import { PrimitiveConverter } from './primitive-converter';

export class BooleanConverter extends PrimitiveConverter<boolean, boolean> {
    protected readonly typeDescription = 'boolean';

    protected convertInternal(value: unknown): boolean {
        if (typeof value !== 'boolean') {
            throw new Error('The value must be a boolean.');
        }
        return value;
    }
}
