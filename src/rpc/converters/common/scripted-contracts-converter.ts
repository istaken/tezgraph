import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { ScriptedContracts } from '../../../entity/subscriptions';
import { primitives } from './primitives';
import { RpcConverter } from './rpc-converter';

@singleton()
export class ScriptedContractsConverter extends RpcConverter<rpc.ScriptedContracts, ScriptedContracts> {
    convert(rpcScript: rpc.ScriptedContracts): ScriptedContracts {
        primitives.object.guard(rpcScript);
        return {
            code: primitives.michelson.convert(rpcScript.code),
            storage: primitives.michelson.convert(rpcScript.storage),
        };
    }
}
