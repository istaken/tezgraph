import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
} from '../../../entity/subscriptions';
import { primitives } from './primitives';
import { RpcCollectionConverter } from './rpc-collection-converter';

export type RpcBalanceUpdate = rpc.OperationMetadataBalanceUpdates | rpc.OperationBalanceUpdatesItem;

@singleton()
export class BalanceUpdateConverter extends RpcCollectionConverter<RpcBalanceUpdate, BalanceUpdate> {
    convert(rpcUpdate: RpcBalanceUpdate): BalanceUpdate {
        primitives.object.guard(rpcUpdate);
        return {
            kind: primitives.enum.convert(BalanceUpdateKind, rpcUpdate.kind),
            change: primitives.bigInt.convert(rpcUpdate.change),
            origin: primitives.enum.convertToNonNullable(BalanceUpdateOrigin, rpcUpdate.origin),
            contract: primitives.string.convertNullable(rpcUpdate.contract),
            category: fixCategory(primitives.enum.convertNullable(OriginalBalanceUpdateCategory, rpcUpdate.category)),
            delegate: primitives.string.convertNullable(rpcUpdate.delegate),
            cycle: primitives.int.convertNullable(rpcUpdate.cycle),
        };
    }
}

enum OriginalBalanceUpdateCategory {
    'baking rewards' = 'baking rewards',
    rewards = 'rewards',
    fees = 'fees',
    deposits = 'deposits',
    legacy_rewards = 'legacy_rewards',
    legacy_fees = 'legacy_fees',
    legacy_deposits = 'legacy_deposits',
    'block fees' = 'block fees',
    'nonce revelation rewards' = 'nonce revelation rewards',
    'double signing evidence rewards' = 'double signing evidence rewards',
    'endorsing rewards' = 'endorsing rewards',
    'baking bonuses' = 'baking bonuses',
    'storage fees' = 'storage fees',
    punishments = 'punishments',
    'lost endorsing rewards' = 'lost endorsing rewards',
    subsidy = 'subsidy',
    burned = 'burned',
    commitment = 'commitment',
    bootstrap = 'bootstrap',
    invoice = 'invoice',
    minted = 'minted',
}

function fixCategory(original: OriginalBalanceUpdateCategory | null): BalanceUpdateCategory | null {
    switch (original) {
        case null:
            return null;
        case OriginalBalanceUpdateCategory['baking rewards']:
            return BalanceUpdateCategory.baking_rewards;
        case OriginalBalanceUpdateCategory['block fees']:
            return BalanceUpdateCategory.block_fees;
        case OriginalBalanceUpdateCategory['nonce revelation rewards']:
            return BalanceUpdateCategory.nonce_revelation_rewards;
        case OriginalBalanceUpdateCategory['double signing evidence rewards']:
            return BalanceUpdateCategory.double_signing_evidence_rewards;
        case OriginalBalanceUpdateCategory['endorsing rewards']:
            return BalanceUpdateCategory.endorsing_rewards;
        case OriginalBalanceUpdateCategory['baking bonuses']:
            return BalanceUpdateCategory.baking_bonuses;
        case OriginalBalanceUpdateCategory['storage fees']:
            return BalanceUpdateCategory.storage_fees;
        case OriginalBalanceUpdateCategory['lost endorsing rewards']:
            return BalanceUpdateCategory.lost_endorsing_rewards;
        default:
            return BalanceUpdateCategory[original];
    }
}
