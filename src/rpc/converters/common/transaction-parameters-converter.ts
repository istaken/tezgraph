import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { TransactionParameters } from '../../../entity/common/transaction';
import { primitives } from './primitives';
import { RpcConverter } from './rpc-converter';

@singleton()
export class TransactionParametersConverter extends RpcConverter<
    rpc.TransactionOperationParameter,
    TransactionParameters
> {
    convert(rpcParameters: rpc.TransactionOperationParameter): TransactionParameters {
        primitives.object.guard(rpcParameters);
        return {
            entrypoint: primitives.string.convert(rpcParameters.entrypoint),
            value: primitives.michelson.convert(rpcParameters.value),
        };
    }
}
