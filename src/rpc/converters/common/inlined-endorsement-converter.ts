import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { InlinedEndorsement, InlinedEndorsementKind } from '../../../entity/subscriptions';
import { primitives } from './primitives';

@singleton()
export class InlinedEndorsementConverter {
    convert(rpcEndorsement: rpc.InlinedEndorsement): InlinedEndorsement {
        primitives.object.guard(rpcEndorsement);
        return {
            branch: primitives.string.convert(rpcEndorsement.branch),
            operations: primitives.object.convert(rpcEndorsement.operations, (rpcOperations) => ({
                kind: primitives.enum.convert(InlinedEndorsementKind, rpcOperations.kind),
                level: primitives.int.convert(rpcOperations.level),
                slot: primitives.int.convertNullable(rpcOperations.slot),
                round: primitives.int.convertNullable(rpcOperations.round),
                block_payload_hash: primitives.string.convertNullable(rpcOperations.block_payload_hash),
            })),
            signature: primitives.string.convertNullable(rpcEndorsement.signature),
        };
    }
}
