import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    LazyBigMapAlloc,
    LazyBigMapCopy,
    LazyBigMapDiff,
    LazyBigMapRemove,
    LazyBigMapStorageDiff,
    LazyBigMapUpdate,
    LazyBigMapUpdateItem,
    LazySaplingStateAlloc,
    LazySaplingStateCopy,
    LazySaplingStateDiff,
    LazySaplingStateDiffUpdates,
    LazySaplingStateRemove,
    LazySaplingStateStorageDiff,
    LazySaplingStateUpdate,
    LazyStorageDiff,
    LazyStorageDiffAction,
    LazyStorageDiffKind,
    SaplingCommitmentAndCiphertext,
} from '../../../entity/subscriptions';
import { typed } from '../../../utils';
import { primitives } from './primitives';
import { RpcCollectionConverter } from './rpc-collection-converter';

@singleton()
export class LazyStorageDiffConverter extends RpcCollectionConverter<rpc.LazyStorageDiff, LazyStorageDiff> {
    convert(rpcDiff: rpc.LazyStorageDiff): LazyStorageDiff {
        primitives.object.guard(rpcDiff);
        primitives.enum.convert(LazyStorageDiffKind, rpcDiff.kind);
        const id = primitives.bigInt.convert(rpcDiff.id);

        switch (rpcDiff.kind) {
            case 'big_map':
                return typed<LazyBigMapStorageDiff>({
                    graphQLTypeName: LazyBigMapStorageDiff.name,
                    kind: LazyStorageDiffKind.big_map,
                    id,
                    big_map_diff: convertBigMapDiff(rpcDiff.diff),
                });
            case 'sapling_state':
                return typed<LazySaplingStateStorageDiff>({
                    graphQLTypeName: LazySaplingStateStorageDiff.name,
                    kind: LazyStorageDiffKind.sapling_state,
                    id,
                    sapling_state_diff: convertSaplingStateDiff(rpcDiff.diff),
                });
        }
    }
}

function convertBigMapDiff(rpcDiff: rpc.LazyStorageDiffBigMapItems): LazyBigMapDiff {
    primitives.object.guard(rpcDiff);
    const action = primitives.enum.convert(LazyStorageDiffAction, rpcDiff.action);

    switch (action) {
        case LazyStorageDiffAction.alloc:
            return typed<LazyBigMapAlloc>({
                graphQLTypeName: LazyBigMapAlloc.name,
                action,
                key_type: primitives.michelson.convertToNonNullable(rpcDiff.key_type),
                value_type: primitives.michelson.convertToNonNullable(rpcDiff.value_type),
                updates: convertBigMapDiffUpdates(rpcDiff.updates),
            });
        case LazyStorageDiffAction.copy:
            return typed<LazyBigMapCopy>({
                graphQLTypeName: LazyBigMapCopy.name,
                action,
                source: primitives.bigInt.convertToNonNullable(rpcDiff.source),
                updates: convertBigMapDiffUpdates(rpcDiff.updates),
            });
        case LazyStorageDiffAction.remove:
            return typed<LazyBigMapRemove>({
                graphQLTypeName: LazyBigMapRemove.name,
                action,
            });
        case LazyStorageDiffAction.update:
            return typed<LazyBigMapUpdate>({
                graphQLTypeName: LazyBigMapUpdate.name,
                action,
                updates: convertBigMapDiffUpdates(rpcDiff.updates),
            });
    }
}

function convertBigMapDiffUpdates(rpcUpdates: rpc.LazyStorageDiffUpdatesBigMap[] | undefined): LazyBigMapUpdateItem[] {
    return primitives.array.convertToNonNullable(rpcUpdates, (u) =>
        primitives.object.convert(u, (rpcUpdate) =>
            typed<LazyBigMapUpdateItem>({
                key: primitives.michelson.convert(rpcUpdate.key),
                key_hash: primitives.string.convert(rpcUpdate.key_hash),
                value: primitives.michelson.convertNullable(rpcUpdate.value),
            }),
        ),
    );
}

function convertSaplingStateDiff(rpcDiff: rpc.LazyStorageDiffSaplingStateItems): LazySaplingStateDiff {
    primitives.object.guard(rpcDiff);
    const action = primitives.enum.convert(LazyStorageDiffAction, rpcDiff.action);

    switch (action) {
        case LazyStorageDiffAction.alloc:
            return typed<LazySaplingStateAlloc>({
                graphQLTypeName: LazySaplingStateAlloc.name,
                action,
                memo_size: primitives.int.convertToNonNullable(rpcDiff.memo_size),
                updates: convertSaplingStateUpdates(rpcDiff.updates),
            });
        case LazyStorageDiffAction.copy:
            return typed<LazySaplingStateCopy>({
                graphQLTypeName: LazySaplingStateCopy.name,
                action,
                source: primitives.bigInt.convertToNonNullable(rpcDiff.source),
                updates: convertSaplingStateUpdates(rpcDiff.updates),
            });
        case LazyStorageDiffAction.remove:
            return typed<LazySaplingStateRemove>({
                graphQLTypeName: LazySaplingStateRemove.name,
                action,
            });
        case LazyStorageDiffAction.update:
            return typed<LazySaplingStateUpdate>({
                graphQLTypeName: LazySaplingStateUpdate.name,
                action,
                updates: convertSaplingStateUpdates(rpcDiff.updates),
            });
    }
}

function convertSaplingStateUpdates(
    nullishRpcUpdates: rpc.LazyStorageDiffUpdatesSaplingState | undefined,
): LazySaplingStateDiffUpdates {
    const rpcUpdates = primitives.object.guardNonNullable(nullishRpcUpdates);
    return {
        commitments_and_ciphertexts: primitives.array.convert(rpcUpdates.commitments_and_ciphertexts, (cac) => {
            primitives.array.guard(cac);
            return typed<SaplingCommitmentAndCiphertext>({
                commitment: primitives.string.convert(cac[0]),
                ciphertext: primitives.object.convert(cac[1], (rpcCiphertext) => ({
                    cv: primitives.string.convert(rpcCiphertext.cv),
                    epk: primitives.string.convert(rpcCiphertext.epk),
                    payload_enc: primitives.string.convert(rpcCiphertext.payload_enc),
                    nonce_enc: primitives.string.convert(rpcCiphertext.nonce_enc),
                    payload_out: primitives.string.convert(rpcCiphertext.payload_out),
                    nonce_out: primitives.string.convert(rpcCiphertext.nonce_out),
                })),
            });
        }),
        nullifiers: primitives.array.convert(rpcUpdates.nullifiers, (n) => primitives.string.convert(n)),
    };
}
