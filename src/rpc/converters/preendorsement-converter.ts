import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, PreendorsementNotification } from '../../entity/subscriptions';
import { isNotNullish, isNullish } from '../../utils';
import { BalanceUpdateConverter, primitives } from './common';
import { RpcOperation } from './common/operation-metadata-converter';
import { BaseOperationProperties } from './operation-converter';

export type RpcPreendorsement = (rpc.OperationContentsPreEndorsement | rpc.OperationContentsAndResultPreEndorsement) &
    RpcOperation<rpc.OperationContentsAndResultMetadataPreEndorsement>;

@singleton()
export class PreendorsementConverter {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {}

    convert(rpcOperation: RpcPreendorsement, baseProperties: BaseOperationProperties): PreendorsementNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: PreendorsementNotification.name,
            kind: OperationKind.preendorsement,
            slot: primitives.int.convert(rpcOperation.slot),
            level: primitives.int.convert(rpcOperation.level),
            round: primitives.int.convert(rpcOperation.round),
            block_payload_hash: primitives.string.convert(rpcOperation.block_payload_hash),
            metadata: !isNullish(rpcOperation.metadata)
                ? primitives.object.convert(rpcOperation.metadata, (rpcMetadata) => ({
                      balance_updates: isNotNullish(rpcMetadata.balance_updates)
                          ? this.balanceUpdateConverter.convertArray(rpcMetadata.balance_updates)
                          : [],
                      delegate: primitives.string.convert(rpcMetadata.delegate),
                      preendorsement_power: primitives.int.convert(rpcMetadata.preendorsement_power),
                  }))
                : null,
        };
    }
}
