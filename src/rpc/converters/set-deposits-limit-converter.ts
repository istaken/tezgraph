import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    OperationKind,
    SetDepositsLimitNotification,
    SetDepositsLimitNotificationMetadata,
} from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';
import { SetDepositsLimitResultConverter } from './operation-results/set-deposits-limit-result-converter';

export type RpcSetDepositsLimit =
    | rpc.OperationContentsSetDepositsLimit
    | rpc.OperationContentsAndResultSetDepositsLimit;

@singleton()
export class SetDepositsLimitConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: SetDepositsLimitResultConverter,
    ) {}

    convert(rpcOperation: RpcSetDepositsLimit, baseProperties: BaseOperationProperties): SetDepositsLimitNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            SetDepositsLimitNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: SetDepositsLimitNotification.name,
            kind: OperationKind.set_deposits_limit,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            limit: primitives.mutez.convertNullable(rpcOperation.limit),
            metadata,
        };
    }
}
