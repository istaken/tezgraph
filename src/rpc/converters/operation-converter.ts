import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    BlockNotification,
    OperationGroup,
    OperationKind,
    OperationNotification,
    OperationOrigin,
} from '../../entity/subscriptions';
import { ActivateAccountConverter } from './activate-account-converter';
import { BallotConverter } from './ballot-converter';
import { primitives } from './common';
import { DelegationConverter } from './delegation-converter';
import { DoubleBakingEvidenceConverter } from './double-baking-evidence-converter';
import { DoubleEndorsementEvidenceConverter } from './double-endorsement-evidence-converter';
import { DoublePreendorsementEvidenceConverter } from './double-preendorsement-evidence-converter';
import { EndorsementConverter } from './endorsement-converter';
import { EndorsementWithSlotConverter } from './endorsement-with-slot-converter';
import { OriginationConverter } from './origination-converter';
import { PreendorsementConverter } from './preendorsement-converter';
import { ProposalsConverter } from './proposals-converter';
import { RegisterGlobalConstantConverter } from './register-global-constant-converter';
import { RevealConverter } from './reveal-converter';
import { SeedNonceRevelationConverter } from './seed-nonce-revelation-converter';
import { SetDepositsLimitConverter } from './set-deposits-limit-converter';
import { TransactionConverter } from './transaction-converter';

export type RpcOperation = rpc.OperationContents | rpc.OperationContentsAndResult;

export interface BaseOperationProperties {
    readonly operation_group: OperationGroup;
    readonly block: BlockNotification | null;
    readonly origin: OperationOrigin;
}

/** Converts an operation from Tezos RPC format to a GraphQL object of concrete type. */
@singleton()
export class OperationConverter {
    // eslint-disable-next-line max-params
    constructor(
        private readonly activateAccountConverter: ActivateAccountConverter,
        private readonly ballotConverter: BallotConverter,
        private readonly delegationConverter: DelegationConverter,
        private readonly doubleBakingEvidenceConverter: DoubleBakingEvidenceConverter,
        private readonly doubleEndorsementEvidenceConverter: DoubleEndorsementEvidenceConverter,
        private readonly doublePreendorsementEvidenceConverter: DoublePreendorsementEvidenceConverter,
        private readonly endorsementConverter: EndorsementConverter,
        private readonly endorsementWithSlotConverter: EndorsementWithSlotConverter,
        private readonly originationConverter: OriginationConverter,
        private readonly preendorsementConverter: PreendorsementConverter,
        private readonly proposalsConverter: ProposalsConverter,
        private readonly registerGlobalConstantConverter: RegisterGlobalConstantConverter,
        private readonly revealConverter: RevealConverter,
        private readonly seedNonceRevelationConverter: SeedNonceRevelationConverter,
        private readonly setDepositsLimitConverter: SetDepositsLimitConverter,
        private readonly transactionConverter: TransactionConverter,
    ) {}

    convert(rpcOperation: RpcOperation, baseProperties: BaseOperationProperties): OperationNotification | null {
        primitives.object.guard(rpcOperation);
        primitives.enum.convert(RpcOperationKind, rpcOperation.kind);

        switch (rpcOperation.kind) {
            case rpc.OpKind.ACTIVATION:
                return this.activateAccountConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.BALLOT:
                return this.ballotConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DELEGATION:
                return this.delegationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DOUBLE_BAKING_EVIDENCE:
                return this.doubleBakingEvidenceConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DOUBLE_ENDORSEMENT_EVIDENCE:
                return this.doubleEndorsementEvidenceConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.DOUBLE_PREENDORSEMENT_EVIDENCE:
                return this.doublePreendorsementEvidenceConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.ENDORSEMENT:
                return this.endorsementConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.ENDORSEMENT_WITH_SLOT:
                return this.endorsementWithSlotConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.ORIGINATION:
                return this.originationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.PREENDORSEMENT:
                return this.preendorsementConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.PROPOSALS:
                return this.proposalsConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.REGISTER_GLOBAL_CONSTANT:
                return this.registerGlobalConstantConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.REVEAL:
                return this.revealConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.SEED_NONCE_REVELATION:
                return this.seedNonceRevelationConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.SET_DEPOSITS_LIMIT:
                return this.setDepositsLimitConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.TRANSACTION:
                return this.transactionConverter.convert(rpcOperation, baseProperties);
            case rpc.OpKind.FAILING_NOOP:
                return null;
        }
    }
}

const RpcOperationKind = {
    ...OperationKind,
    failing_noop: 'failing_noop',
} as const;
