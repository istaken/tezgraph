import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DoubleEndorsementEvidenceNotification, OperationKind } from '../../entity/subscriptions';
import { InlinedEndorsementConverter, OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcDoubleEndorsement =
    | rpc.OperationContentsDoubleEndorsement
    | rpc.OperationContentsAndResultDoubleEndorsement;

@singleton()
export class DoubleEndorsementEvidenceConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly inlinedEndorsementConverter: InlinedEndorsementConverter,
    ) {}

    convert(
        rpcOperation: RpcDoubleEndorsement,
        baseProperties: BaseOperationProperties,
    ): DoubleEndorsementEvidenceNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: DoubleEndorsementEvidenceNotification.name,
            kind: OperationKind.double_endorsement_evidence,
            op1: this.inlinedEndorsementConverter.convert(rpcOperation.op1),
            op2: this.inlinedEndorsementConverter.convert(rpcOperation.op2),
            slot: primitives.int.convertToNonNullable(rpcOperation.slot),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }
}
