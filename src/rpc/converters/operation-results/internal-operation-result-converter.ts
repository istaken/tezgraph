import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    InternalDelegationResult,
    InternalOperationKind,
    InternalOperationResult,
    InternalOriginationResult,
    InternalRegisterGlobalConstantResult,
    InternalRevealResult,
    InternalSetDepositsLimitResult,
    InternalTransactionResult,
} from '../../../entity/subscriptions';
import { typed } from '../../../utils/reflection';
import { primitives } from '../common/primitives';
import { RpcCollectionConverter } from '../common/rpc-collection-converter';
import { ScriptedContractsConverter } from '../common/scripted-contracts-converter';
import { TransactionParametersConverter } from '../common/transaction-parameters-converter';
import { DelegationResultConverter } from './delegation-result-converter';
import { OriginationResultConverter } from './origination-result-converter';
import { RegisterGlobalConstantResultConverter } from './register-global-constant-result-converter';
import { RevealResultConverter } from './reveal-result-converter';
import { SetDepositsLimitResultConverter } from './set-deposits-limit-result-converter';
import { TransactionResultConverter } from './transaction-result-converter';

@singleton()
export class InternalOperationResultConverter extends RpcCollectionConverter<
    rpc.InternalOperationResult,
    InternalOperationResult
> {
    // eslint-disable-next-line max-params
    constructor(
        private readonly delegationResultConverter: DelegationResultConverter,
        private readonly originationResultConverter: OriginationResultConverter,
        private readonly registerGlobalConstantResultConverter: RegisterGlobalConstantResultConverter,
        private readonly revealResultConverter: RevealResultConverter,
        private readonly setDepositsLimitResultConverter: SetDepositsLimitResultConverter,
        private readonly transactionResultConverter: TransactionResultConverter,
        private readonly transactionParametersConverter: TransactionParametersConverter,
        private readonly scriptConverter: ScriptedContractsConverter,
    ) {
        super();
    }

    convert(rpcResult: rpc.InternalOperationResult): InternalOperationResult {
        primitives.object.guard(rpcResult);
        const kind = primitives.enum.convert(InternalOperationKind, rpcResult.kind);

        const commonProperties = {
            source: primitives.string.convert(rpcResult.source),
            nonce: primitives.int.convert(rpcResult.nonce),
        };
        switch (kind) {
            case InternalOperationKind.delegation:
                return typed<InternalDelegationResult>({
                    graphQLTypeName: InternalDelegationResult.name,
                    ...commonProperties,
                    kind,
                    delegate: primitives.string.convertNullable(rpcResult.delegate),
                    result: this.delegationResultConverter.convert(rpcResult.result),
                });
            case InternalOperationKind.origination:
                return typed<InternalOriginationResult>({
                    graphQLTypeName: InternalOriginationResult.name,
                    ...commonProperties,
                    kind,
                    balance: primitives.mutez.convertToNonNullable(rpcResult.balance),
                    delegate: primitives.string.convertNullable(rpcResult.delegate),
                    script: this.scriptConverter.convertToNonNullable(rpcResult.script),
                    result: this.originationResultConverter.convert(rpcResult.result),
                });
            case InternalOperationKind.register_global_constant:
                return typed<InternalRegisterGlobalConstantResult>({
                    graphQLTypeName: InternalRegisterGlobalConstantResult.name,
                    ...commonProperties,
                    kind,
                    value: primitives.michelson.convertToNonNullable(rpcResult.value),
                    result: this.registerGlobalConstantResultConverter.convert(rpcResult.result),
                });
            case InternalOperationKind.reveal:
                return typed<InternalRevealResult>({
                    graphQLTypeName: InternalRevealResult.name,
                    ...commonProperties,
                    kind,
                    public_key: primitives.string.convertToNonNullable(rpcResult.public_key),
                    result: this.revealResultConverter.convert(rpcResult.result),
                });
            case InternalOperationKind.set_deposits_limit:
                return typed<InternalSetDepositsLimitResult>({
                    graphQLTypeName: InternalSetDepositsLimitResult.name,
                    ...commonProperties,
                    kind,
                    limit: primitives.mutez.convertNullable(rpcResult.limit),
                    result: this.setDepositsLimitResultConverter.convert(rpcResult.result),
                });
            case InternalOperationKind.transaction:
                return typed<InternalTransactionResult>({
                    graphQLTypeName: InternalTransactionResult.name,
                    ...commonProperties,
                    kind,
                    amount: primitives.mutez.convertToNonNullable(rpcResult.amount),
                    destination: primitives.string.convertToNonNullable(rpcResult.destination),
                    parameters: this.transactionParametersConverter.convertNullable(rpcResult.parameters),
                    result: this.transactionResultConverter.convert(rpcResult.result),
                });
        }
    }
}
