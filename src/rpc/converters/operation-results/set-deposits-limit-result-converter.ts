import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { SetDepositsLimitNotificationResult } from '../../../entity/subscriptions';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class SetDepositsLimitResultConverter {
    convert(rpcResult: rpc.OperationResultSetDepositsLimit): SetDepositsLimitNotificationResult {
        return {
            ...getBaseOperationResultProperties(rpcResult),
            graphQLTypeName: SetDepositsLimitNotificationResult.name,
        };
    }
}
