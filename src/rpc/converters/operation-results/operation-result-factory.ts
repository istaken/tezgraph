import * as rpc from '@taquito/rpc';

import { OperationResult, OperationResultStatus } from '../../../entity/common/operation';
import { primitives } from '../common';

interface RpcOperationResultBase {
    status: rpc.OperationResultStatusEnum;
    consumed_gas?: string;
    consumed_milligas?: string;
    errors?: rpc.TezosGenericOperationError[];
}

export function getBaseOperationResultProperties(
    rpcResult: RpcOperationResultBase,
): Omit<OperationResult, 'graphQLTypeName'> {
    primitives.object.guard(rpcResult);
    return {
        status: primitives.enum.convert(OperationResultStatus, rpcResult.status),
        consumed_gas: primitives.bigInt.convertNullable(rpcResult.consumed_gas),
        consumed_milligas: primitives.bigInt.convertNullable(rpcResult.consumed_milligas),
        errors: primitives.array.convertNullable(rpcResult.errors, (e) => primitives.object.guard(e)),
    };
}
