import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { RevealNotificationResult } from '../../../entity/subscriptions';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class RevealResultConverter {
    convert(rpcResult: rpc.OperationResultReveal): RevealNotificationResult {
        return {
            ...getBaseOperationResultProperties(rpcResult),
            graphQLTypeName: RevealNotificationResult.name,
        };
    }
}
