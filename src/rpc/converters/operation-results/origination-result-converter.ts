import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OriginationNotificationResult } from '../../../entity/subscriptions/operations/origination/origination-notification';
import { BalanceUpdateConverter, BigMapDiffConverter, LazyStorageDiffConverter, primitives } from '../common';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class OriginationResultConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
        private readonly lazyStorageDiffConverter: LazyStorageDiffConverter,
    ) {}

    convert(rpcResult: rpc.OperationResultOrigination): OriginationNotificationResult {
        return {
            ...getBaseOperationResultProperties(rpcResult),
            balance_updates: this.balanceUpdateConverter.convertNullableArray(rpcResult.balance_updates),
            big_map_diff: this.bigMapDiffConverter.convertNullableArray(rpcResult.big_map_diff),
            lazy_storage_diff: this.lazyStorageDiffConverter.convertNullableArray(rpcResult.lazy_storage_diff),
            originated_contracts: primitives.array.convertNullable(rpcResult.originated_contracts, (c) =>
                primitives.string.convert(c),
            ),
            storage_size: primitives.bigInt.convertNullable(rpcResult.storage_size),
            paid_storage_size_diff: primitives.bigInt.convertNullable(rpcResult.paid_storage_size_diff),
            graphQLTypeName: OriginationNotificationResult.name,
        };
    }
}
