import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationNotificationResult } from '../../../entity/subscriptions/operations/delegation/delegation-notification';
import { getBaseOperationResultProperties } from './operation-result-factory';

@singleton()
export class DelegationResultConverter {
    convert(rpcResult: rpc.OperationResultDelegation): DelegationNotificationResult {
        return {
            ...getBaseOperationResultProperties(rpcResult),
            graphQLTypeName: DelegationNotificationResult.name,
        };
    }
}
