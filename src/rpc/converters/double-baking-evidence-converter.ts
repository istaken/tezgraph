import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    DoubleBakingEvidenceBlockHeader,
    DoubleBakingEvidenceNotification,
    OperationKind,
} from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcDoubleBakingEvidence = rpc.OperationContentsDoubleBaking | rpc.OperationContentsAndResultDoubleBaking;

@singleton()
export class DoubleBakingEvidenceConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(
        rpcOperation: RpcDoubleBakingEvidence,
        baseProperties: BaseOperationProperties,
    ): DoubleBakingEvidenceNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: DoubleBakingEvidenceNotification.name,
            kind: OperationKind.double_baking_evidence,
            bh1: this.convertHeader(rpcOperation.bh1),
            bh2: this.convertHeader(rpcOperation.bh2),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }

    private convertHeader(rpcHeader: rpc.BlockFullHeader): DoubleBakingEvidenceBlockHeader {
        primitives.object.guard(rpcHeader);
        return {
            level: primitives.int.convert(rpcHeader.level),
            proto: primitives.int.convert(rpcHeader.proto),
            predecessor: primitives.string.convert(rpcHeader.predecessor),
            timestamp: primitives.date.convert(rpcHeader.timestamp),
            validation_pass: primitives.int.convert(rpcHeader.validation_pass),
            operations_hash: primitives.string.convertNullable(rpcHeader.operations_hash),
            fitness: primitives.array.convert(rpcHeader.fitness, (f) => primitives.string.convert(f)),
            context: primitives.string.convert(rpcHeader.context),
            priority: primitives.int.convert(rpcHeader.priority),
            proof_of_work_nonce: primitives.string.convert(rpcHeader.proof_of_work_nonce),
            seed_nonce_hash: primitives.string.convertNullable(rpcHeader.seed_nonce_hash),
            signature: primitives.string.convert(rpcHeader.signature),
        };
    }
}
