import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BlockNotification, OperationNotification, OperationOrigin } from '../../entity/subscriptions';
import { RpcMempoolOperationGroup } from '../../modules/tezos-monitor/mempool/rpc-mempool-operation-group';
import { Clock, deepFreeze, injectLogger, Logger, withIndex } from '../../utils';
import { primitives } from './common';
import { BaseOperationProperties, OperationConverter } from './operation-converter';

type RpcOperationGroup = rpc.OperationEntry | RpcMempoolOperationGroup;

/** Converts an operation group (either from block or mempool) from Tezos RPC format to a GraphQL object. */
@singleton()
export class OperationGroupConverter {
    constructor(
        private readonly operationConverter: OperationConverter,
        private readonly clock: Clock,
        @injectLogger(OperationGroupConverter) private readonly logger: Logger,
    ) {}

    convert(rpcGroup: rpc.OperationEntry, block: BlockNotification): readonly OperationNotification[];
    convert(rpcGroup: RpcMempoolOperationGroup): readonly OperationNotification[];
    convert(rpcGroup: RpcOperationGroup, block?: BlockNotification): readonly OperationNotification[] {
        const baseProperties = this.createBaseOperationProperties(rpcGroup, block);
        const operations = Array.from(this.convertOperations(rpcGroup, baseProperties));

        if (!operations.length) {
            throw new Error(
                `Failed to convert all operations in the group with signature '${rpcGroup.signature ?? ''}'` +
                    ` from ${block ? `block with hash '${block.hash}'` : 'mempool'}'. See previous errors.`,
            );
        }

        // Don't freeze when deserializing a block b/c the block is referenced -> would be frozen too -> can't add these operations.
        this.logger.logDebug('Converted operation group with {signature} and {operationCount}.', {
            signature: rpcGroup.signature,
            operationCount: operations.length,
        });
        return block ? operations : deepFreeze(operations);
    }

    private createBaseOperationProperties(
        rpcGroup: RpcOperationGroup,
        block: BlockNotification | undefined,
    ): BaseOperationProperties {
        primitives.object.guard(rpcGroup);
        return {
            operation_group: {
                protocol: primitives.string.convert(rpcGroup.protocol),
                chain_id: block ? primitives.string.convertToNonNullable(rpcGroup.chain_id) : null,
                hash: block ? primitives.string.convertToNonNullable(rpcGroup.hash) : null,
                branch: primitives.string.convert(rpcGroup.branch),
                signature: primitives.string.convertNullable(rpcGroup.signature),
                received_from_tezos_on: block?.received_from_tezos_on ?? this.clock.getNowDate(),
            },
            block: block ?? null,
            origin: block ? OperationOrigin.block : OperationOrigin.mempool,
        };
    }

    private *convertOperations(
        rpcGroup: RpcOperationGroup,
        baseProperties: BaseOperationProperties,
    ): Iterable<OperationNotification> {
        for (const [rpcOperation, index] of withIndex(primitives.array.guard(rpcGroup.contents))) {
            try {
                const operation = this.operationConverter.convert(rpcOperation, baseProperties);
                if (operation) {
                    this.logger.logDebug('Converted operation with {index} and {kind} in group with {signature}.', {
                        index,
                        kind: operation.kind,
                        signature: rpcGroup.signature,
                    });
                    yield operation;
                } else {
                    this.logger.logDebug('Skipped operation with {index} in group with {signature}.', {
                        index,
                        signature: rpcGroup.signature,
                        rpcOperation,
                    });
                }
            } catch (error: unknown) {
                this.logger.logError(
                    'Failed to convert operation with {index} in the group with {signature} from RPC data.' +
                        ' It will be skipped. {error}.',
                    {
                        index,
                        signature: rpcGroup.signature,
                        rpcOperation,
                        error,
                    },
                );
            }
        }
    }
}
