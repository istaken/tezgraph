import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import {
    DoublePreendorsementEvidenceNotification,
    InlinedPreendorsement,
    InlinedPreendorsementKind,
    OperationKind,
} from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcDoublePreendorsementEvidence =
    | rpc.OperationContentsDoublePreEndorsement
    | rpc.OperationContentsAndResultDoublePreEndorsement;

@singleton()
export class DoublePreendorsementEvidenceConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(
        rpcOperation: RpcDoublePreendorsementEvidence,
        baseProperties: BaseOperationProperties,
    ): DoublePreendorsementEvidenceNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: DoublePreendorsementEvidenceNotification.name,
            kind: OperationKind.double_preendorsement_evidence,
            op1: this.convertInlinedPreendorsement(rpcOperation.op1),
            op2: this.convertInlinedPreendorsement(rpcOperation.op2),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }

    private convertInlinedPreendorsement(rpcInlinedPreendorsement: rpc.InlinedPreEndorsement): InlinedPreendorsement {
        primitives.object.guard(rpcInlinedPreendorsement);
        return {
            branch: primitives.string.convert(rpcInlinedPreendorsement.branch),
            operations: primitives.object.convert(rpcInlinedPreendorsement.operations, (rpcOperations) => ({
                block_payload_hash: primitives.string.convert(rpcOperations.block_payload_hash),
                kind: primitives.enum.convert(InlinedPreendorsementKind, rpcOperations.kind),
                level: primitives.int.convert(rpcOperations.level),
                round: primitives.int.convert(rpcOperations.round),
                slot: primitives.int.convert(rpcOperations.slot),
            })),
            signature: primitives.string.convertNullable(rpcInlinedPreendorsement.signature),
        };
    }
}
