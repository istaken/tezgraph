import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { OperationKind, TransactionNotification, TransactionNotificationMetadata } from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives, TransactionParametersConverter } from './common';
import { BaseOperationProperties } from './operation-converter';
import { TransactionResultConverter } from './operation-results/transaction-result-converter';

export type RpcTransaction = rpc.OperationContentsTransaction | rpc.OperationContentsAndResultTransaction;

@singleton()
export class TransactionConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: TransactionResultConverter,
        private readonly parametersConverter: TransactionParametersConverter,
    ) {}

    convert(rpcOperation: RpcTransaction, baseProperties: BaseOperationProperties): TransactionNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            TransactionNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: TransactionNotification.name,
            kind: OperationKind.transaction,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            amount: primitives.mutez.convert(rpcOperation.amount),
            destination: primitives.string.convert(rpcOperation.destination),
            parameters: this.parametersConverter.convertNullable(rpcOperation.parameters),
            metadata,
        };
    }
}
