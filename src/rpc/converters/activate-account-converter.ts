import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { ActivateAccountNotification, OperationKind } from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcActivateAccount = rpc.OperationContentsActivateAccount | rpc.OperationContentsAndResultActivateAccount;

@singleton()
export class ActivateAccountConverter {
    constructor(private readonly metadataConverter: OperationMetadataConverter) {}

    convert(rpcOperation: RpcActivateAccount, baseProperties: BaseOperationProperties): ActivateAccountNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: ActivateAccountNotification.name,
            kind: OperationKind.activate_account,
            pkh: primitives.string.convert(rpcOperation.pkh),
            secret: primitives.string.convert(rpcOperation.secret),
            metadata: this.metadataConverter.convertSimple(rpcOperation),
        };
    }
}
