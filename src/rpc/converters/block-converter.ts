import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BlockNotification, OperationNotification } from '../../entity/subscriptions';
import { deepFreeze, injectLogger, Logger } from '../../utils';
import { primitives } from './common';
import { OperationGroupConverter } from './operation-group-converter';

/** Converts a block from Tezos RPC to a GraphQL object. */
@singleton()
export class BlockConverter {
    constructor(
        private readonly operationGroupConverter: OperationGroupConverter,
        @injectLogger(BlockConverter) private readonly logger: Logger,
    ) {}

    convert(rpcBlock: rpc.BlockResponse, receivedFromTezosOn: Date): BlockNotification {
        primitives.object.guard(rpcBlock);
        this.logger.logDebug('Converting block {hash}.', { hash: rpcBlock.hash });

        const operations: OperationNotification[] = [];
        const block = this.convertBlockItself(rpcBlock, receivedFromTezosOn, operations);

        const rpcGroups = primitives.array.convert(rpcBlock.operations, (g) => primitives.array.guard(g)).flat();
        for (const rpcGroup of rpcGroups) {
            try {
                const groupOperations = this.operationGroupConverter.convert(rpcGroup, block);
                operations.push(...groupOperations);
            } catch (error: unknown) {
                this.logger.logError(
                    'Failed to convert an operation group in the block with {hash} from RPC data. It will be skipped. {error}.',
                    {
                        hash: block.hash,
                        rpcGroup,
                        error,
                    },
                );
            }
        }

        if (!block.operations.length) {
            throw new Error(
                `Failed to convert all operation groups in the block with hash '${block.hash}'. See previous errors.`,
            );
        }

        this.logger.logDebug('Converted block with {hash}, {level} and {operationCount}.', {
            hash: block.hash,
            level: block.header.level,
            operationCount: block.operations.length,
        });
        return deepFreeze(block);
    }

    private convertBlockItself(
        rpcBlock: rpc.BlockResponse,
        receivedFromTezosOn: Date,
        operations: readonly OperationNotification[],
    ): BlockNotification {
        primitives.object.guard(rpcBlock);
        return {
            hash: primitives.string.convert(rpcBlock.hash),
            protocol: primitives.string.convert(rpcBlock.protocol),
            chain_id: primitives.string.convert(rpcBlock.chain_id),
            header: primitives.object.convert(rpcBlock.header, (rpcHeader) => ({
                level: primitives.int.convert(rpcHeader.level),
                proto: primitives.int.convert(rpcHeader.proto),
                predecessor: primitives.string.convert(rpcHeader.predecessor),
                timestamp: primitives.date.convert(rpcHeader.timestamp),
                signature: primitives.string.convert(rpcHeader.signature),
                validation_pass: primitives.int.convert(rpcHeader.validation_pass),
                operations_hash: primitives.string.convert(rpcHeader.operations_hash),
                fitness: primitives.array.convert(rpcHeader.fitness, (f) => primitives.string.convert(f)),
                context: primitives.string.convert(rpcHeader.context),
                priority: primitives.int.convertNullable(rpcHeader.priority),
                proof_of_work_nonce: primitives.string.convert(rpcHeader.proof_of_work_nonce),
                seed_nonce_hash: primitives.string.convertNullable(rpcHeader.seed_nonce_hash),
                liquidity_baking_escape_vote: primitives.boolean.convertToNonNullable(
                    typeof rpcHeader.liquidity_baking_escape_vote === 'boolean'
                        ? rpcHeader.liquidity_baking_escape_vote
                        : rpcHeader.liquidity_baking_escape_vote === 'off', // TODO: This is just a placeholder implementation, needs proper handling by the AV team, also does not handle the pass case.
                ),
                payload_hash: primitives.string.convertNullable(rpcHeader.payload_hash),
                payload_round: primitives.int.convertNullable(rpcHeader.payload_round),
            })),
            operations,
            received_from_tezos_on: receivedFromTezosOn,
        };
    }
}
