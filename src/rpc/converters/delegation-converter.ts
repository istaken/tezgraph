import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { DelegationNotification, DelegationNotificationMetadata, OperationKind } from '../../entity/subscriptions';
import { OperationMetadataConverter, primitives } from './common';
import { BaseOperationProperties } from './operation-converter';
import { DelegationResultConverter } from './operation-results/delegation-result-converter';

export type RpcDelegation = rpc.OperationContentsDelegation | rpc.OperationContentsAndResultDelegation;

@singleton()
export class DelegationConverter {
    constructor(
        private readonly metadataConverter: OperationMetadataConverter,
        private readonly resultConverter: DelegationResultConverter,
    ) {}

    convert(rpcOperation: RpcDelegation, baseProperties: BaseOperationProperties): DelegationNotification {
        primitives.object.guard(rpcOperation);
        const metadata = this.metadataConverter.convert(
            rpcOperation,
            this.resultConverter,
            DelegationNotificationMetadata.name,
        );
        return {
            ...baseProperties,
            graphQLTypeName: DelegationNotification.name,
            kind: OperationKind.delegation,
            source: primitives.string.convert(rpcOperation.source),
            fee: primitives.mutez.convert(rpcOperation.fee),
            counter: primitives.bigInt.convert(rpcOperation.counter),
            gas_limit: primitives.bigInt.convert(rpcOperation.gas_limit),
            storage_limit: primitives.bigInt.convert(rpcOperation.storage_limit),
            delegate: primitives.string.convertNullable(rpcOperation.delegate),
            metadata,
        };
    }
}
