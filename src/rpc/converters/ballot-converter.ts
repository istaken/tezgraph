import * as rpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { BallotNotification, BallotVote, OperationKind } from '../../entity/subscriptions';
import { primitives } from './common';
import { BaseOperationProperties } from './operation-converter';

export type RpcBallot = rpc.OperationContentsBallot | rpc.OperationContentsAndResultBallot;

@singleton()
export class BallotConverter {
    convert(rpcOperation: RpcBallot, baseProperties: BaseOperationProperties): BallotNotification {
        primitives.object.guard(rpcOperation);
        return {
            ...baseProperties,
            graphQLTypeName: BallotNotification.name,
            kind: OperationKind.ballot,
            source: primitives.string.convert(rpcOperation.source),
            period: primitives.int.convert(rpcOperation.period),
            proposal: primitives.string.convert(rpcOperation.proposal),
            ballot: primitives.enum.convert(BallotVote, rpcOperation.ballot),
        };
    }
}
