/* eslint-disable capitalized-comments */
/* eslint-disable multiline-comment-style */
/* istanbul ignore file */

import { Field, ObjectType } from 'type-graphql';

import { Delegation, DelegationMetadata, DelegationResult } from '../common/delegation';
import { MoneyOperation } from '../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../common/operation';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperationRecord } from './money-operation-record';
import { OperationRecord } from './operation-record';

@ObjectType({ implements: [OperationResult, DelegationResult] })
export class DelegationResultRecord extends DelegationResult implements OperationResult {}

@ObjectType({ implements: [OperationMetadata, DelegationMetadata] })
export class DelegationRecordMetadata extends DelegationMetadata implements OperationMetadata {
    @Field(() => DelegationResultRecord)
    readonly operation_result!: DelegationResultRecord;
}

@ObjectType({ implements: [Operation, OperationRecord, MoneyOperation, MoneyOperationRecord, Delegation] })
export class DelegationRecord extends MoneyOperationRecord implements Delegation {
    @Field(() => DelegationRecordMetadata, { nullable: true })
    readonly metadata!: DelegationRecordMetadata | null;

    readonly kind!: OperationKind.delegation;
    readonly delegate_address?: string | null;
    readonly delegate_id?: bigint | null;
}

export interface DelegationRecordData extends DelegationRecord {
    readonly cursor: string;
}
