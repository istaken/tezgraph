import { ObjectType } from 'type-graphql';

import { Operation } from '../common/operation';
import { OperationRecord } from './operation-record';

@ObjectType({
    implements: [Operation, OperationRecord],
})
export class BallotRecord extends OperationRecord {}
