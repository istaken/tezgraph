import { Field, ObjectType } from 'type-graphql';

import { RelayConnection, RelayEdge } from '../relay';
import { Micheline, scalars } from '../scalars';

@ObjectType()
export class BigmapKey {
    @Field(() => String)
    key_hash!: string;

    @Field(() => scalars.Micheline)
    key!: Micheline;

    id!: bigint;
}

@ObjectType()
export class BigmapKeyRecord extends BigmapKey {}

export interface BigmapKeyRecordData extends BigmapKeyRecord {
    cursor: string;
}

@ObjectType()
export class BigmapKeyRecordEdge extends RelayEdge(BigmapKeyRecord) {}

@ObjectType()
export class BigmapKeyRecordConnection extends RelayConnection(BigmapKeyRecordEdge) {}
