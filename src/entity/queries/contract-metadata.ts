/* istanbul ignore file */
import { MichelineTzip16Expression } from '@taquito/tzip16';
import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { scalars } from '../scalars';

export class BigmapRecordWithMetadataValue {
    bytes?: string | undefined;
}

@ObjectType()
export class ContractMetadataErrorValues {
    @Field(() => Int, { nullable: true })
    int?: number | null;

    @Field(() => String, { nullable: true })
    string?: string | null;

    @Field(() => String, { nullable: true })
    bytes?: string | null;
}

@ObjectType()
export class MichelsonStorageViewTypeAnnotation {
    @Field(() => String, { nullable: true })
    name?: string | null;

    @Field(() => String, { nullable: true })
    description?: string | null;
}

@ObjectType()
export class MichelsonStorageViewType {
    @Field(() => String, {
        nullable: true,
        description: 'A string representing the version of Michelson that the view is meant to work with.',
    })
    version?: string | null;

    @Field(() => scalars.Micheline, {
        nullable: true,
        description: 'Michelson type parameters required by the view code.',
    })
    parameter?: MichelineTzip16Expression | null;

    @Field(() => scalars.Micheline, { description: 'The type of the result of the view.' })
    returnType!: MichelineTzip16Expression;

    @Field(() => scalars.Micheline, {
        description: 'The Michelson code expression implementing the view.',
        nullable: true,
    })
    canonical_code?: MichelineTzip16Expression | null;

    @Field(() => MichelsonStorageViewTypeAnnotation, {
        nullable: true,
        description: 'A list of objects documenting the annotation name and description.',
    })
    annotations?: MichelsonStorageViewTypeAnnotation[] | null;

    code!: MichelineTzip16Expression;
}

export enum RestApiQueryTypeMethodKind {
    get = 'GET',
    post = 'POST',
    put = 'PUT',
}

registerEnumType(RestApiQueryTypeMethodKind, {
    name: 'RestApiQueryTypeMethodKind',
    description: 'The HTTP method used for the view.',
});

@ObjectType()
export class RestApiQueryType {
    @Field(() => String, { description: 'A string giving the location of the URI of the full specification.' })
    specificationUri!: string;

    @Field(() => String, { nullable: true, description: 'The recommended server to use.' })
    baseUri?: string | null;

    @Field(() => String, { description: 'The API path within the specification that implements the view.' })
    path!: string;

    @Field(() => RestApiQueryTypeMethodKind, { nullable: true, description: 'The HTTP method used for the view.' })
    method?: 'GET' | 'POST' | 'PUT' | null;
}

@ObjectType()
export class ViewImplementation {
    @Field(() => MichelsonStorageViewType, {
        nullable: true,
        description: 'The view details for the Michelson function type view.',
    })
    michelsonStorageView?: MichelsonStorageViewType | null;

    @Field(() => RestApiQueryType, { nullable: true, description: 'The view details for rest-api query type view.' })
    restApiQuery?: RestApiQueryType | null;
}

@ObjectType()
export class ViewDefinition {
    @Field(() => String, { nullable: true, description: 'The name of the view.' })
    name?: string | null;

    @Field(() => String, { nullable: true })
    description?: string | null;

    @Field(() => [ViewImplementation], { nullable: true, description: 'A list of usable views.' })
    implementations?: ViewImplementation[] | null;

    @Field(() => String, {
        nullable: true,
        description: 'A boolean advertising whether a view should be considered a pure function.',
    })
    pure?: boolean | null;
}

@ObjectType()
export class ContractMetadataError {
    @Field(() => ContractMetadataErrorValues, { nullable: true })
    error?: ContractMetadataErrorValues | null;

    @Field(() => String, { nullable: true })
    view?: string | null;

    @Field(() => [String], { nullable: true })
    languages?: string[] | null;

    @Field(() => ContractMetadataErrorValues, { nullable: true })
    expansion?: ContractMetadataErrorValues | null;
}

@ObjectType()
export class ContractMetadataSource {
    @Field(() => [String], { nullable: true })
    tools?: string[] | null;

    @Field(() => String, { nullable: true })
    location?: string | null;
}

@ObjectType()
export class ContractMetadataLicense {
    @Field(() => String, { nullable: true })
    name?: string | null;

    @Field(() => String, { nullable: true })
    details?: string | null;
}

@ObjectType()
export class ContractMetadata {
    @Field(() => String, { nullable: true })
    name?: string | null;

    @Field(() => String, { nullable: true })
    description?: string | null;

    @Field(() => String, { nullable: true })
    version?: string | null;

    @Field(() => ContractMetadataLicense, { nullable: true })
    license?: ContractMetadataLicense | null;

    @Field(() => [String], { nullable: true })
    authors?: string[] | null;

    @Field(() => String, { nullable: true })
    homepage?: string | null;

    @Field(() => ContractMetadataSource, { nullable: true })
    source?: ContractMetadataSource | null;

    @Field(() => [String], { nullable: true })
    interfaces?: string[] | null;

    @Field(() => [ContractMetadataError], { nullable: true })
    errors?: ContractMetadataError[] | null;

    @Field(() => scalars.JSONObject, {
        nullable: true,
        description: 'The raw contract metadata as a free-form object.',
    })
    raw?: object | null;

    @Field(() => [ViewDefinition], {
        nullable: true,
        description: 'The views provided by the contract metadata.',
    })
    views?: ViewDefinition[] | null;
}
