import { Prisma } from '@prisma/client';
import { Field, ObjectType } from 'type-graphql';

import { RelayConnection, RelayEdge } from '../relay';
import { scalars } from '../scalars';

@ObjectType()
export class Bigmap {
    @Field(() => String, { nullable: true, description: 'The annotation of bigmap in contract' })
    annots?: string | null;

    @Field(() => scalars.Micheline)
    key_type!: Prisma.JsonValue;

    @Field(() => scalars.Micheline, { nullable: true })
    value_type!: Prisma.JsonValue;

    block_hash_id!: number;
    id!: bigint;
    block_hash!: string;
    i!: bigint;
    operation_id?: bigint | null;
    sender_id?: bigint | null;
    receiver_id?: bigint | null;
}

@ObjectType()
export class BigmapRecord extends Bigmap {}

export interface AccountIdAndAddress {
    address: string;
    address_id: bigint;
}

export interface BigmapRecordData extends BigmapRecord {
    cursor: string;
}

@ObjectType()
export class BigmapRecordEdge extends RelayEdge(BigmapRecord) {}

@ObjectType()
export class BigmapRecordConnection extends RelayConnection(BigmapRecordEdge) {}
