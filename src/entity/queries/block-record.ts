import { Field, InputType, ObjectType } from 'type-graphql';

import { DateRangeFilter } from '../../modules/queries-graphql/repositories/date-range.utils';
import { IntRangeFilter } from '../../modules/queries-graphql/resolvers/types/range-filters';
import { RelayConnection, RelayEdge } from '../relay';
import { scalars } from '../scalars';

@ObjectType()
export class Block {
    level!: number;
    hash?: string;
    timestamp?: Date | null;
}

@ObjectType()
export class BlockRecord extends Block {}

@InputType()
export class BlockFilter {
    @Field(() => DateRangeFilter, { nullable: true })
    timestamp?: DateRangeFilter;

    @Field(() => IntRangeFilter, { nullable: true })
    level?: IntRangeFilter;

    @Field(() => [scalars.BlockHash], { nullable: true })
    hashes?: string[];
}

@ObjectType()
export class BlockRecordEdge extends RelayEdge(BlockRecord) {}

@ObjectType()
export class BlockRecordConnection extends RelayConnection(BlockRecordEdge) {}
