import { Field, Int, InterfaceType, ObjectType } from 'type-graphql';

import { Operation, OperationResultStatus } from '../common/operation';
import { RelayConnection, RelayEdge } from '../relay';
import { scalars } from '../scalars';
import { BlockRecord } from './block-record';

@InterfaceType({
    implements: [Operation],
    resolveType: (operation: OperationRecord) => operation.graphQLTypeName,
})
export abstract class OperationRecord extends Operation {
    @Field(() => scalars.OperationHash, { description: 'The hash of the operation.' })
    readonly hash!: string;

    @Field(() => Int, { description: 'The position of the operation in the operation batch.' })
    readonly batch_position!: number;

    @Field(() => Int, { description: 'The internal position of the internal operation in the operation.' })
    readonly internal!: number;

    @Field(() => BlockRecord, { description: 'The block containing this operation in the chain.' })
    readonly block!: BlockRecord;

    /**
     * The unique identifier of an account for the sender of the operation.
     * An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.
     */
    readonly senderAddress?: string | null;

    readonly senderAddressID?: bigint | null;

    /**
     * The unique identifier of an account for the receiver of the operation.
     * An Implicit account starts with the letters tz followed by 1, 2 or 3 and the hash of the public key.
     */
    readonly receiverAddress?: string | null;

    readonly receiverAddressID?: bigint | null;

    readonly autoid!: bigint;

    readonly graphQLTypeName!: string;
}

export interface OperationRecordData extends OperationRecord {
    readonly cursor: string;
}

@ObjectType()
export class OperationRecordEdge extends RelayEdge(OperationRecord) {}

@ObjectType()
export class OperationRecordConnection extends RelayConnection(OperationRecordEdge) {}

// https://gitlab.com/nomadic-labs/tezos-indexer/-/blob/master/src/db-schema/chain.sql#L576
export const MAP_OPERATION_STATUS_TO_RESULT_STATUS = {
    0: OperationResultStatus.applied,
    1: OperationResultStatus.backtracked,
    2: OperationResultStatus.failed,
    3: OperationResultStatus.skipped,
} as const;

export type OperationRecordStatus = 0 | 1 | 2 | 3;
