/* istanbul ignore file */

import { Field, InputType, ObjectType } from 'type-graphql';

import { RelayConnection, RelayEdge } from '../relay';
import { scalars } from '../scalars';

@ObjectType()
export class Account {
    @Field(() => scalars.Address)
    address!: string;

    address_id!: bigint;
}

@ObjectType()
export class AccountRecord extends Account {}

@InputType()
export class AccountFilter {
    @Field(() => [scalars.Address], {
        nullable: true,
        description: `The public key hash of implicit or originated accounts to be listed.`,
    })
    addresses?: string[];

    @Field(() => [String], {
        nullable: true,
        description: `Filters accounts based on the starting characters in their address.`,
    })
    address_prefixes?: string[];

    @Field(() => Boolean, {
        nullable: true,
        description:
            `Filters accounts based on the type of their address,` +
            `if true: returns "Originated Accounts" (also known as "Smart Contracts"), ` +
            `if false: returns "Implicit Accounts", if not provided: does not apply this filter.`,
    })
    is_contract?: boolean;

    address_ids?: bigint[];
}

@ObjectType()
export class AccountRecordEdge extends RelayEdge(AccountRecord) {}

@ObjectType()
export class AccountRecordConnection extends RelayConnection(AccountRecordEdge) {}
