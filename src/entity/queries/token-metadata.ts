import { Field, Int, ObjectType } from 'type-graphql';

import { scalars } from '../scalars';

@ObjectType()
export class TokenMetadata {
    @Field(() => Int, {
        nullable: false,
        description:
            'An integer which defines the position of the decimal point in token balances for display purposes.',
    })
    decimals?: number;

    @Field(() => String, { nullable: true, description: 'A UTF-8 string giving a “display name” to the token.' })
    name?: string | null;

    @Field(() => String, {
        nullable: true,
        description: 'A UTF-8 string for the short identifier of the token (e.g. XTZ, EUR).',
    })
    symbol?: string | null;

    @Field(() => Int, { nullable: true, description: 'An id assigned by the FA2 token contract.' })
    token_id?: number | null;

    @Field(() => scalars.JSONObject, { nullable: true, description: 'The raw token metadata as a free-form object.' })
    raw?: object | null;
}
