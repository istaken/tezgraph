import { Field, InterfaceType } from 'type-graphql';

import { MoneyOperation } from '../common/money-operation';
import { Operation } from '../common/operation';
import { scalars } from '../scalars';
import { OperationRecord } from './operation-record';

@InterfaceType({
    implements: [Operation, OperationRecord, MoneyOperation],
})
export abstract class MoneyOperationRecord extends OperationRecord implements MoneyOperation {
    @Field(() => scalars.Mutez, { description: 'The cost of an operation in mutez.' })
    readonly fee!: bigint;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly counter?: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly gas_limit?: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly storage_limit?: bigint | null;
}
