/* istanbul ignore file */

import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../common/operation';
import { Reveal, RevealMetadata, RevealResult } from '../common/reveal';
import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperationRecord } from './money-operation-record';
import { OperationRecord } from './operation-record';

@ObjectType({ implements: [OperationResult, RevealResult] })
export class RevealResultRecord extends RevealResult implements OperationResult {}

@ObjectType({ implements: [OperationMetadata, RevealMetadata] })
export class RevealRecordMetadata extends RevealMetadata implements OperationMetadata {
    @Field(() => RevealResultRecord)
    readonly operation_result!: RevealResultRecord;
}

@ObjectType({ implements: [Operation, OperationRecord, MoneyOperation, MoneyOperationRecord, Reveal] })
export class RevealRecord extends MoneyOperationRecord implements Reveal {
    @Field(() => scalars.PublicKey)
    readonly public_key!: string;

    @Field(() => RevealRecordMetadata, { nullable: true })
    readonly metadata!: RevealRecordMetadata | null;

    readonly kind!: OperationKind.reveal;
}

export interface RevealRecordData extends RevealRecord {
    readonly cursor: string;
}
