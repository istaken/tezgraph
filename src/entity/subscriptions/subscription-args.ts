import { ArgsType, Field, Int } from 'type-graphql';

import { EnvConfig } from '../../utils/configuration/env-config';
import { EnvConfigProvider } from '../../utils/configuration/env-config-provider';
import { DefaultConstructor, Nullish } from '../../utils/reflection';
import { Filter } from './common-filters';

export interface ReplayFromBlockArgs {
    readonly replayFromBlockLevel: Nullish<number>;
}

export interface FilteringArgs<TItem> {
    readonly filter: Nullish<Filter<TItem>>;
}

const envConfig = new EnvConfig(new EnvConfigProvider(process.env));
@ArgsType()
export abstract class AbstractSubscriptionArgs implements ReplayFromBlockArgs {
    @Field(() => Int, {
        nullable: true,
        description:
            'Specifies a historical block level to start to retrieve operations. It seamlessly switches to new operations.' +
            ` The value can go only ${envConfig.replayFromBlockLevelMaxFromHead} levels back from the level of the head block.`,
    })
    readonly replayFromBlockLevel: Nullish<number>;
}

export function createSubscriptionFilterArgsClass<TItem, TFilter extends Filter<TItem>>(
    filterType: DefaultConstructor<TFilter>,
): DefaultConstructor<FilteringArgs<TItem> & ReplayFromBlockArgs> {
    @ArgsType()
    class SubscriptionFilterArgsClass extends AbstractSubscriptionArgs {
        @Field(() => filterType, { nullable: true })
        readonly filter: Nullish<Filter<TItem>>;
    }
    return SubscriptionFilterArgsClass;
}
