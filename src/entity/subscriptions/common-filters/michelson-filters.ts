import { isEqual } from 'lodash';
import { InputType } from 'type-graphql';

import { Micheline, scalars } from '../../scalars';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createEqualityFilterClassForProperty } from './equality-filter';
import { makeNullable } from './nullable-filter';

/** Applies deep equality. */
@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.Micheline.name) })
export class MichelsonFilter extends createEqualityFilterClassForProperty<Micheline, Micheline>(
    scalars.Micheline,
    isEqual,
) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.Micheline.name, { nullable: true }) })
export class NullableMichelsonFilter extends makeNullable(MichelsonFilter) {}
