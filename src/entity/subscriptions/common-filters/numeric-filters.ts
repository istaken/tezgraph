import { InputType } from 'type-graphql';

import { scalars } from '../../scalars';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createComparableFilterClass } from './comparable-filter';

@InputType({ description: graphQLDescriptions.getComparableFilter(scalars.Mutez.name) })
export class MutezFilter extends createComparableFilterClass<BigInt>(scalars.Mutez) {}
