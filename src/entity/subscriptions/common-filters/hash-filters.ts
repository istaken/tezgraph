import { InputType } from 'type-graphql';

import { scalars } from '../../scalars';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createArrayFilterClass } from './array-filter';
import { createEqualityFilterClass } from './equality-filter';
import { makeNullable } from './nullable-filter';

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.ProtocolHash.name) })
export class ProtocolHashFilter extends createEqualityFilterClass<string>(scalars.ProtocolHash) {}

@InputType({ description: graphQLDescriptions.getArrayFilter(scalars.ProtocolHash.name) })
export class ProtocolHashArrayFilter extends createArrayFilterClass<string>(scalars.ProtocolHash) {}

@InputType({ description: graphQLDescriptions.getArrayFilter(scalars.ProtocolHash.name, { nullable: true }) })
export class NullableProtocolHashArrayFilter extends makeNullable(ProtocolHashArrayFilter) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.BlockHash.name) })
export class BlockHashFilter extends createEqualityFilterClass<string>(scalars.BlockHash) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.OperationHash.name) })
export class OperationHashFilter extends createEqualityFilterClass<string>(scalars.OperationHash) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(scalars.OperationHash.name, { nullable: true }) })
export class NullableOperationHashFilter extends makeNullable(OperationHashFilter) {}
