import { Field, InputType } from 'type-graphql';
import { ReturnTypeFuncValue } from 'type-graphql/dist/decorators/types';

import { DefaultConstructor, isNullish, NonNullishValue, Nullish } from '../../../utils/reflection';
import { graphQLDescriptions } from '../graphql-descriptions';
import { Filter } from './filter';

export interface EqualityFilter<TFilterScalar, TProperty = TFilterScalar> extends Filter<TProperty> {
    equalTo: Nullish<TFilterScalar>;
    notEqualTo: Nullish<TFilterScalar>;
    in: Nullish<TFilterScalar[]>;
    notIn: Nullish<TFilterScalar[]>;
}

export function createEqualityFilterClass<TScalar extends NonNullishValue>(
    scalarType: ReturnTypeFuncValue,
): DefaultConstructor<EqualityFilter<TScalar, TScalar>> {
    return createEqualityFilterClassForProperty(
        scalarType,
        (filterValue, propertyValue) => filterValue === propertyValue,
    );
}

export type EqualsFunction<TFilterScalar, TProperty> = (
    filterValue: TFilterScalar,
    propertyValue: TProperty,
) => boolean;

export function createEqualityFilterClassForProperty<TFilterScalar extends NonNullishValue, TProperty>(
    filterScalarType: ReturnTypeFuncValue,
    equals: EqualsFunction<TFilterScalar, TProperty>,
): DefaultConstructor<EqualityFilter<TFilterScalar, TProperty>> {
    @InputType({ isAbstract: true })
    class EqualityFilterClass implements Filter<TProperty> {
        @Field(() => filterScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('equals to'),
        })
        equalTo: Nullish<TFilterScalar>;

        @Field(() => filterScalarType, {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('does NOT equal to'),
        })
        notEqualTo: Nullish<TFilterScalar>;

        @Field(() => [filterScalarType], {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is contained in', { filterPlural: true }),
        })
        in: Nullish<TFilterScalar[]>;

        @Field(() => [filterScalarType], {
            nullable: true,
            description: graphQLDescriptions.getFilterProperty('is NOT contained in', { filterPlural: true }),
        })
        notIn: Nullish<TFilterScalar[]>;

        passes(value: TProperty): boolean {
            // Using isNullish() instead of ! operator b/c filters can be empty string/zero/false.
            return (
                (isNullish(this.equalTo) || equals(this.equalTo, value)) &&
                (isNullish(this.notEqualTo) || !equals(this.notEqualTo, value)) &&
                (isNullish(this.in) || this.in.some((f) => equals(f, value))) &&
                (isNullish(this.notIn) || this.notIn.every((f) => !equals(f, value)))
            );
        }
    }
    return EqualityFilterClass;
}
