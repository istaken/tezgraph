import { InputType } from 'type-graphql';

import { getEnumTypeName } from '../../../utils/reflection';
import { OperationResultStatus } from '../../common/operation';
import { graphQLDescriptions } from '../graphql-descriptions';
import { createEqualityFilterClass } from './equality-filter';
import { makeNullable } from './nullable-filter';

@InputType({ description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ OperationResultStatus })) })
export class OperationResultStatusFilter extends createEqualityFilterClass<OperationResultStatus>(
    OperationResultStatus,
) {}

@InputType({
    description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ OperationResultStatus }), { nullable: true }),
})
export class NullableOperationResultStatusFilter extends makeNullable(OperationResultStatusFilter) {}
