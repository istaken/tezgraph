import { LazyBigMapStorageDiff } from './lazy-big-map-diff';
import { LazySaplingStateStorageDiff } from './lazy-sapling-state-diff';
import { AbstractLazyStorageDiff } from './lazy-storage-diff';

export type LazyStorageDiff = LazyBigMapStorageDiff | LazySaplingStateStorageDiff;
export const LazyStorageDiff = AbstractLazyStorageDiff; // eslint-disable-line @typescript-eslint/no-redeclare
