import { isEqual } from 'lodash';
import { ArgsType, Field, InputType } from 'type-graphql';

import { getEnumTypeName, Nullish } from '../../../../utils/reflection';
import { Micheline, scalars } from '../../../scalars';
import {
    AddressFilter,
    createEqualityFilterClass,
    createEqualityFilterClassForProperty,
    Filter,
    makeNullable,
    NullableAddressFilter,
} from '../../common-filters';
import { createArrayFilterClassForProperty } from '../../common-filters/array-filter';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { createSubscriptionFilterArgsClass } from '../../subscription-args';
import { BigMapDiffAction } from '../big-map-diff/big-map-diff';
import { LazyBigMapUpdateItem } from './lazy-big-map-diff';
import { LazyStorageDiffAction, LazyStorageDiffKind } from './lazy-storage-diff';
import { LazyStorageDiffNotification } from './lazy-storage-diff-notification';
import { LazyStorageDiff } from './lazy-storage-diff-union';

@InputType({ description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ LazyStorageDiffKind })) })
export class LazyStorageDiffKindFilter extends createEqualityFilterClass<LazyStorageDiffKind>(LazyStorageDiffKind) {}

@InputType({ description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ LazyStorageDiffAction })) })
export class LazyStorageDiffActionFilter extends createEqualityFilterClass<LazyStorageDiffAction>(BigMapDiffAction) {}

@InputType({
    description: `Provides filters for filtering lazy storage diffs to certain big maps or sapling states based on their global IDs.`,
})
export class LazyStorageIdFilter extends createEqualityFilterClassForProperty(scalars.BigNumber, lazyStorageIdEquals) {}

export function lazyStorageIdEquals(filterValue: bigint, storageDiff: LazyStorageDiff): boolean {
    if (
        storageDiff.kind === LazyStorageDiffKind.big_map &&
        storageDiff.big_map_diff.action === LazyStorageDiffAction.copy
    ) {
        return storageDiff.id === filterValue || storageDiff.big_map_diff.source === filterValue;
    }
    return storageDiff.id === filterValue;
}

@InputType({ isAbstract: true })
class LazyStorageBigMapKeyFilter extends createArrayFilterClassForProperty(scalars.Micheline, bigMapUpdateIncludeKey) {}

export function bigMapUpdateIncludeKey(
    bigMapUpdates: readonly LazyBigMapUpdateItem[],
    filterValue: Micheline,
): boolean {
    return bigMapUpdates.some((d) => isEqual(d.key, filterValue)); // Deep equality.
}

@InputType({ description: `Provides filters for big map key if the diff has it.` })
export class NullableLazyStorageBigMapKeyFilter extends makeNullable(LazyStorageBigMapKeyFilter) {}

@InputType()
export class LazyStorageDiffFilter implements Filter<LazyStorageDiffNotification> {
    @Field(() => LazyStorageDiffKindFilter, { nullable: true })
    readonly kind: Nullish<LazyStorageDiffKindFilter>;

    @Field(() => LazyStorageIdFilter, { nullable: true })
    readonly id: Nullish<LazyStorageIdFilter>;

    @Field(() => LazyStorageDiffActionFilter, { nullable: true })
    readonly action: Nullish<LazyStorageDiffActionFilter>;

    @Field(() => NullableLazyStorageBigMapKeyFilter, { nullable: true })
    readonly big_map_key: Nullish<NullableLazyStorageBigMapKeyFilter>;

    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly destination: Nullish<NullableAddressFilter>;

    passes(notification: LazyStorageDiffNotification): boolean {
        return (
            (!this.kind || this.kind.passes(notification.lazy_storage_diff.kind)) &&
            (!this.id || this.id.passes(notification.lazy_storage_diff)) &&
            (!this.action || this.action.passes(getAction(notification.lazy_storage_diff))) &&
            (!this.big_map_key || this.big_map_key.passes(getBigMapUpdates(notification.lazy_storage_diff))) &&
            (!this.source || this.source.passes(notification.parent.source)) &&
            (!this.destination || this.destination.passes(notification.parent.destination))
        );
    }
}

function getAction(storageDiff: LazyStorageDiff): LazyStorageDiffAction {
    switch (storageDiff.kind) {
        case LazyStorageDiffKind.big_map:
            return storageDiff.big_map_diff.action;
        case LazyStorageDiffKind.sapling_state:
            return storageDiff.sapling_state_diff.action;
    }
}

export function getBigMapUpdates(storageDiff: LazyStorageDiff): readonly LazyBigMapUpdateItem[] | undefined {
    return storageDiff.kind === LazyStorageDiffKind.big_map ? storageDiff.big_map_diff.updates : undefined;
}

@ArgsType()
export class LazyStorageDiffArgs extends createSubscriptionFilterArgsClass(LazyStorageDiffFilter) {}
