import { Field, InterfaceType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../../utils/reflection';
import { removeRequiredPrefix } from '../../../../utils/string-manipulation';
import { scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';

export enum LazyStorageDiffKind {
    big_map = 'big_map',
    sapling_state = 'sapling_state',
}

registerEnumType(LazyStorageDiffKind, {
    name: getEnumTypeName({ LazyStorageDiffKind }),
    description: 'The kind of lazy storage diff.',
});

@InterfaceType(removeRequiredPrefix(AbstractLazyStorageDiff.name, 'Abstract'), {
    description: getGraphQLDescription(),
    resolveType: (d: AbstractLazyStorageDiff) => d.graphQLTypeName,
})
export abstract class AbstractLazyStorageDiff {
    @Field(() => LazyStorageDiffKind)
    abstract readonly kind: LazyStorageDiffKind;

    @Field(() => scalars.BigNumber)
    readonly id!: bigint;

    readonly graphQLTypeName!: string;
}

export enum LazyStorageDiffAction {
    alloc = 'alloc',
    copy = 'copy',
    remove = 'remove',
    update = 'update',
}

registerEnumType(LazyStorageDiffAction, {
    name: getEnumTypeName({ LazyStorageDiffAction }),
    description: 'The action of lazy storage diff of particular kind.',
});

export function getGraphQLDescription(kind?: LazyStorageDiffKind, action?: LazyStorageDiffAction | 'diff'): string {
    return graphQLDescriptions.getRpc([
        'lazy_storage_diff',
        ...(kind ? [`[@kind = ${kind}]`] : []),
        ...(action ? ['diff'] : []),
        ...(action && action !== 'diff' ? [`[@action = ${action}]`] : []),
    ]);
}
