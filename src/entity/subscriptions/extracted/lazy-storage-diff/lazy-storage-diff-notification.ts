import { Field, ObjectType } from 'type-graphql';

import { ContractRelatedNotification } from '../../operations/contract-related-notification';
import { LazyStorageDiff } from './lazy-storage-diff-union';

@ObjectType({ description: 'Notification about a diff in a storage.' })
export class LazyStorageDiffNotification extends ContractRelatedNotification {
    @Field(() => LazyStorageDiff)
    readonly lazy_storage_diff!: LazyStorageDiff;
}
