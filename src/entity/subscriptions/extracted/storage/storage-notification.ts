import { Field, ObjectType } from 'type-graphql';

import { Micheline, scalars } from '../../../scalars';
import { ContractRelatedNotification } from '../../operations/contract-related-notification';

@ObjectType({ description: 'Notification about a new storage.' })
export class StorageNotification extends ContractRelatedNotification {
    @Field(() => scalars.Micheline)
    readonly storage!: Micheline;
}
