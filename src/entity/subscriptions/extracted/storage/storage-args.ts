import { ArgsType, Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter, NullableAddressFilter } from '../../common-filters';
import { createSubscriptionFilterArgsClass } from '../../subscription-args';
import { StorageNotification } from './storage-notification';

@InputType()
export class StorageFilter implements Filter<StorageNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly destination: Nullish<NullableAddressFilter>;

    passes(notification: StorageNotification): boolean {
        return (
            (!this.source || this.source.passes(notification.parent.source)) &&
            (!this.destination || this.destination.passes(notification.parent.destination))
        );
    }
}

@ArgsType()
export class StorageArgs extends createSubscriptionFilterArgsClass(StorageFilter) {}
