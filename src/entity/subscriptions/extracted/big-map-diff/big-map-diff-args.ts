import { ArgsType, Field, InputType } from 'type-graphql';

import { getEnumTypeName, Nullish } from '../../../../utils/reflection';
import { scalars } from '../../../scalars';
import {
    AddressFilter,
    createEqualityFilterClass,
    createEqualityFilterClassForProperty,
    Filter,
    MichelsonFilter,
    NullableAddressFilter,
    NullableMichelsonFilter,
} from '../../common-filters';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { createSubscriptionFilterArgsClass } from '../../subscription-args';
import { BigMapDiff, BigMapDiffAction } from './big-map-diff';
import { BigMapDiffNotification } from './big-map-diff-notification';

@InputType({
    description: `Provides filters for filtering big map diffs to certain big maps based on their global IDs.`,
})
export class BigMapDiffBigMapFilter extends createEqualityFilterClassForProperty(scalars.BigNumber, bigMapEquals) {}

export function bigMapEquals(filterValue: bigint, diff: BigMapDiff): boolean {
    switch (diff.action) {
        case BigMapDiffAction.alloc:
        case BigMapDiffAction.remove:
        case BigMapDiffAction.update:
            return diff.big_map === filterValue;

        case BigMapDiffAction.copy:
            return diff.source_big_map === filterValue || diff.destination_big_map === filterValue;
    }
}

@InputType({ description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ BigMapDiffAction })) })
export class BigMapDiffActionFilter extends createEqualityFilterClass<BigMapDiffAction>(BigMapDiffAction) {}

@InputType()
export class BigMapDiffFilter implements Filter<BigMapDiffNotification> {
    @Field(() => BigMapDiffBigMapFilter, { nullable: true })
    readonly big_map: Nullish<BigMapDiffBigMapFilter>;

    @Field(() => MichelsonFilter, { nullable: true })
    readonly key: Nullish<NullableMichelsonFilter>;

    @Field(() => BigMapDiffActionFilter, { nullable: true })
    readonly action: Nullish<BigMapDiffActionFilter>;

    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly destination: Nullish<NullableAddressFilter>;

    passes(notification: BigMapDiffNotification): boolean {
        const diff = notification.big_map_diff;
        return (
            (!this.big_map || this.big_map.passes(diff)) &&
            (!this.key || this.key.passes(diff.key)) &&
            (!this.action || this.action.passes(diff.action)) &&
            (!this.source || this.source.passes(notification.parent.source)) &&
            (!this.destination || this.destination.passes(notification.parent.destination))
        );
    }
}

@ArgsType()
export class BigMapDiffArgs extends createSubscriptionFilterArgsClass(BigMapDiffFilter) {}
