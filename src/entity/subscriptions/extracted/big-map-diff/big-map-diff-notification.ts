import { Field, ObjectType } from 'type-graphql';

import { ContractRelatedNotification } from '../../operations/contract-related-notification';
import { BigMapDiff } from './big-map-diff';

@ObjectType({ description: 'Notification about a diff in a bigmap.' })
export class BigMapDiffNotification extends ContractRelatedNotification {
    @Field(() => BigMapDiff)
    readonly big_map_diff!: BigMapDiff;
}
