import { createUnionType, Field, ObjectType } from 'type-graphql';

import { ActivateAccountNotification } from '../../operations/activate-account/activate-account-notification';
import { DelegationNotification } from '../../operations/delegation/delegation-notification';
import { DoubleBakingEvidenceNotification } from '../../operations/double-baking-evidence/double-baking-evidence-notification';
import {
    DoubleEndorsementEvidenceNotification,
    EndorsementNotification,
    EndorsementWithSlotNotification,
} from '../../operations/endorsements/endorsement-notification';
import {
    InternalOriginationResult,
    OriginationNotification,
} from '../../operations/origination/origination-notification';
import { RegisterGlobalConstantNotification } from '../../operations/register-global-constant/register-global-constant-notification';
import { RevealNotification } from '../../operations/reveal/reveal-notification';
import { SeedNonceRevelationNotification } from '../../operations/seed-nonce-revelation/seed-nonce-revelation-notification';
import {
    InternalTransactionResult,
    TransactionNotification,
} from '../../operations/transaction/transaction-notification';
import { BalanceUpdate } from './balance-update';
import { OperationNotificationWithBalanceUpdates } from './operation-notification-with-balance-updates';

const BalanceUpdateParent = createUnionType({
    name: 'BalanceUpdateParent',
    types: () =>
        [
            InternalTransactionResult,
            InternalOriginationResult,
            ActivateAccountNotification,
            DelegationNotification,
            DoubleBakingEvidenceNotification,
            DoubleEndorsementEvidenceNotification,
            EndorsementNotification,
            EndorsementWithSlotNotification,
            OriginationNotification,
            RegisterGlobalConstantNotification,
            RevealNotification,
            SeedNonceRevelationNotification,
            TransactionNotification,
        ] as const,
    resolveType: (value) => value.graphQLTypeName,
});
// eslint-disable-next-line @typescript-eslint/no-redeclare
export type BalanceUpdateParent = typeof BalanceUpdateParent;

@ObjectType({ description: 'Notification about a balance update.' })
export class BalanceUpdateNotification {
    @Field(() => OperationNotificationWithBalanceUpdates, {
        description: 'Associated operation that contains this change.',
    })
    readonly operation!: OperationNotificationWithBalanceUpdates;

    @Field(() => BalanceUpdateParent, {
        description:
            'Parent entity that contains this change.' +
            ` It is either \`${TransactionNotification.name}\` if the change was done directly in its result.` +
            ` Or it is \`InternalOperationResult\` if the change was done internally.`,
    })
    readonly parent!: BalanceUpdateParent;

    @Field(() => BalanceUpdate)
    readonly balance_update!: BalanceUpdate;
}
