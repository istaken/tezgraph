import { ArgsType, Field, InputType } from 'type-graphql';

import { getEnumTypeName, Nullish } from '../../../../utils/reflection';
import { createEqualityFilterClass, Filter, makeNullable, NullableAddressFilter } from '../../common-filters';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { createSubscriptionFilterArgsClass } from '../../subscription-args';
import { BalanceUpdateCategory, BalanceUpdateKind } from './balance-update';
import { BalanceUpdateNotification } from './balance-update-notification';

@InputType({ description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ BalanceUpdateKind })) })
export class BalanceUpdateKindFilter extends createEqualityFilterClass<BalanceUpdateKind>(BalanceUpdateKind) {}

@InputType({
    description: graphQLDescriptions.getEqualityFilter(getEnumTypeName({ BalanceUpdateCategory }), { nullable: true }),
})
export class NullableBalanceUpdateCategoryFilter extends makeNullable(
    createEqualityFilterClass<BalanceUpdateCategory>(BalanceUpdateCategory),
) {}

@InputType()
export class BalanceUpdateFilter implements Filter<BalanceUpdateNotification> {
    @Field(() => BalanceUpdateKindFilter, { nullable: true })
    readonly kind: Nullish<BalanceUpdateKindFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly contract: Nullish<NullableAddressFilter>;

    @Field(() => NullableBalanceUpdateCategoryFilter, { nullable: true })
    readonly category: Nullish<NullableBalanceUpdateCategoryFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    passes(notification: BalanceUpdateNotification): boolean {
        const update = notification.balance_update;
        return (
            (!this.kind || this.kind.passes(update.kind)) &&
            (!this.contract || this.contract.passes(update.contract)) &&
            (!this.category || this.category.passes(update.category)) &&
            (!this.delegate || this.delegate.passes(update.delegate))
        );
    }
}

@ArgsType()
export class BalanceUpdateArgs extends createSubscriptionFilterArgsClass(BalanceUpdateFilter) {}
