import { Field, ObjectType } from 'type-graphql';

import { Delegation, DelegationMetadata, DelegationResult } from '../../../common/delegation';
import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationResult, InternalOperationKind } from '../operation-result';

@ObjectType({
    implements: [OperationResult, DelegationResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.delegation),
})
export class DelegationNotificationResult extends OperationResult implements DelegationResult {}

@ObjectType({
    implements: InternalOperationResult,
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.delegation),
})
export class InternalDelegationResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind = InternalOperationKind.delegation;

    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | null;

    @Field(() => DelegationNotificationResult)
    readonly result!: DelegationNotificationResult;
}

@ObjectType({
    implements: [OperationMetadata, DelegationMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.delegation),
})
export class DelegationNotificationMetadata
    extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults
    implements DelegationMetadata
{
    @Field(() => DelegationNotificationResult)
    readonly operation_result!: DelegationNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
        Delegation,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.delegation),
})
export class DelegationNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => scalars.Address, { nullable: true })
    readonly delegate!: string | null;

    @Field(() => DelegationNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: DelegationNotificationMetadata | null;

    readonly kind = OperationKind.delegation;
}
