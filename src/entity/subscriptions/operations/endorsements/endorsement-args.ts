import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { Filter, NullableAddressFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import {
    AbstractEndorsementNotification,
    EndorsementNotification,
    EndorsementWithSlotNotification,
} from './endorsement-notification';

@InputType({ isAbstract: true })
abstract class AbstractEndorsementSpecificFilter<TEndorsement extends AbstractEndorsementNotification>
    implements Filter<TEndorsement>
{
    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    passes(operation: TEndorsement): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata?.delegate);
    }
}

@InputType({ isAbstract: true })
export class EndorsementSpecificFilter extends AbstractEndorsementSpecificFilter<EndorsementNotification> {}

@InputType({ isAbstract: true })
export class EndorsementWithSlotSpecificFilter extends AbstractEndorsementSpecificFilter<EndorsementWithSlotNotification> {}

export const EndorsementFilter = createOperationFilterClass(EndorsementSpecificFilter);
export const EndorsementArgs = createOperationArgsClass(EndorsementFilter);

export const EndorsementWithSlotFilter = createOperationFilterClass(EndorsementWithSlotSpecificFilter);
export const EndorsementWithSlotArgs = createOperationArgsClass(EndorsementWithSlotFilter);
