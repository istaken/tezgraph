import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter, MutezFilter, NullableOperationResultStatusFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { TransactionNotification } from './transaction-notification';

@InputType({ isAbstract: true })
export class TransactionSpecificFilter implements Filter<TransactionNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => AddressFilter, { nullable: true })
    readonly destination: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    @Field(() => MutezFilter, { nullable: true })
    readonly amount: Nullish<MutezFilter>;

    passes(operation: TransactionNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.destination || this.destination.passes(operation.destination)) &&
            (!this.status || this.status.passes(operation.metadata?.operation_result.status)) &&
            (!this.amount || this.amount.passes(operation.amount))
        );
    }
}

export const TransactionFilter = createOperationFilterClass(TransactionSpecificFilter);
export const TransactionArgs = createOperationArgsClass(TransactionFilter);
