import { Field, Int, ObjectType } from 'type-graphql';

import { Operation } from '../../../common/operation';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { SimpleOperationMetadata } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.seed_nonce_revelation),
})
export class SeedNonceRevelationNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => Int)
    readonly level!: number;

    @Field(() => String)
    readonly nonce!: string;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | null;

    readonly kind = OperationKind.seed_nonce_revelation;
}
