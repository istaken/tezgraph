import { InputType } from 'type-graphql';

import { Filter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { SeedNonceRevelationNotification } from './seed-nonce-revelation-notification';

@InputType({ isAbstract: true })
export class SeedNonceRevelationSpecificFilter implements Filter<SeedNonceRevelationNotification> {
    passes(): boolean {
        return true;
    }
}

export const SeedNonceRevelationFilter = createOperationFilterClass(SeedNonceRevelationSpecificFilter);
export const SeedNonceRevelationArgs = createOperationArgsClass(SeedNonceRevelationFilter);
