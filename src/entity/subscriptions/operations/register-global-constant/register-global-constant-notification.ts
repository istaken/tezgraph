import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import { Micheline, scalars } from '../../../scalars';
import { BalanceUpdate } from '../../extracted/balance-update';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationResult, InternalOperationKind } from '../operation-result';

@ObjectType({
    implements: [OperationResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.register_global_constant),
})
export class RegisterGlobalConstantNotificationResult extends OperationResult {
    @Field(() => [BalanceUpdate], { nullable: true })
    readonly balance_updates!: readonly BalanceUpdate[] | null;

    @Field(() => scalars.Address, { nullable: true })
    readonly global_address!: string | null;

    @Field(() => scalars.BigNumber, { nullable: true })
    readonly storage_size!: bigint | null;
}

@ObjectType({
    implements: InternalOperationResult,
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.register_global_constant),
})
export class InternalRegisterGlobalConstantResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind!: InternalOperationKind.register_global_constant;

    @Field(() => [scalars.Micheline], {
        description: 'The value of the constant.',
    })
    readonly value!: Micheline;

    @Field(() => RegisterGlobalConstantNotificationResult)
    readonly result!: RegisterGlobalConstantNotificationResult;
}

@ObjectType({
    implements: [OperationMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.register_global_constant),
})
export class RegisterGlobalConstantNotificationMetadata extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults {
    @Field(() => RegisterGlobalConstantNotificationResult)
    readonly operation_result!: RegisterGlobalConstantNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.register_global_constant),
})
export class RegisterGlobalConstantNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => [scalars.Micheline], {
        description: 'The value of the constant.',
    })
    readonly value!: Micheline;

    @Field(() => RegisterGlobalConstantNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: RegisterGlobalConstantNotificationMetadata | null;

    readonly kind = OperationKind.register_global_constant;
}
