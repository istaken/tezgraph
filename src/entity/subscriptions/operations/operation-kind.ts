import { registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../utils';

export enum OperationKind {
    activate_account = 'activate_account',
    ballot = 'ballot',
    delegation = 'delegation',
    double_baking_evidence = 'double_baking_evidence',
    double_endorsement_evidence = 'double_endorsement_evidence',
    double_preendorsement_evidence = 'double_preendorsement_evidence',
    endorsement = 'endorsement',
    endorsement_with_slot = 'endorsement_with_slot',
    origination = 'origination',
    preendorsement = 'preendorsement',
    proposals = 'proposals',
    register_global_constant = 'register_global_constant',
    reveal = 'reveal',
    seed_nonce_revelation = 'seed_nonce_revelation',
    set_deposits_limit = 'set_deposits_limit',
    transaction = 'transaction',
}

registerEnumType(OperationKind, {
    name: getEnumTypeName({ OperationKind }),
    description: 'The kind of an operation.',
});
