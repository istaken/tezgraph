import { ActivateAccountNotification } from './activate-account/activate-account-notification';
import { BallotNotification } from './ballot/ballot-notification';
import { DelegationNotification, InternalDelegationResult } from './delegation/delegation-notification';
import { DoubleBakingEvidenceNotification } from './double-baking-evidence/double-baking-evidence-notification';
import {
    DoubleEndorsementEvidenceNotification,
    EndorsementNotification,
    EndorsementWithSlotNotification,
} from './endorsements/endorsement-notification';
import { OperationNotification as AbstractOperationNotification } from './operation-notification';
import { InternalOperationResult as AbstractInternalOperationResult } from './operation-result';
import { InternalOriginationResult, OriginationNotification } from './origination/origination-notification';
import { DoublePreendorsementEvidenceNotification } from './preendorsements';
import { PreendorsementNotification } from './preendorsements/preendorsement-notification';
import { ProposalsNotification } from './proposals/proposals-notification';
import {
    InternalRegisterGlobalConstantResult,
    RegisterGlobalConstantNotification,
} from './register-global-constant/register-global-constant-notification';
import { InternalRevealResult, RevealNotification } from './reveal/reveal-notification';
import { SeedNonceRevelationNotification } from './seed-nonce-revelation/seed-nonce-revelation-notification';
import {
    InternalSetDepositsLimitResult,
    SetDepositsLimitNotification,
} from './set-deposits-limit/set-deposits-limit-notification';
import { InternalTransactionResult, TransactionNotification } from './transaction/transaction-notification';

export const InternalOperationResult = AbstractInternalOperationResult;
export type InternalOperationResult =  // eslint-disable-line @typescript-eslint/no-redeclare
    | InternalDelegationResult
    | InternalOriginationResult
    | InternalRegisterGlobalConstantResult
    | InternalRevealResult
    | InternalSetDepositsLimitResult
    | InternalTransactionResult;

export const OperationNotification = AbstractOperationNotification;
export type OperationNotification =  // eslint-disable-line @typescript-eslint/no-redeclare
    | ActivateAccountNotification
    | BallotNotification
    | DelegationNotification
    | DoubleBakingEvidenceNotification
    | DoubleEndorsementEvidenceNotification
    | DoublePreendorsementEvidenceNotification
    | EndorsementNotification
    | EndorsementWithSlotNotification
    | OriginationNotification
    | PreendorsementNotification
    | ProposalsNotification
    | RegisterGlobalConstantNotification
    | RevealNotification
    | SeedNonceRevelationNotification
    | SetDepositsLimitNotification
    | TransactionNotification;
