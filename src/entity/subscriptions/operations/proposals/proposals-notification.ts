import { Field, Int, ObjectType } from 'type-graphql';

import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { OperationNotification } from '../operation-notification';

@ObjectType({
    implements: [Operation, OperationNotification],
    description: graphQLDescriptions.getRpcOperation(OperationKind.proposals),
})
export class ProposalsNotification extends OperationNotification {
    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly period!: number;

    @Field(() => [scalars.ProtocolHash])
    readonly proposals!: readonly string[];

    readonly kind = OperationKind.proposals;
}
