import { Field, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../../common/money-operation';
import { Operation, OperationMetadata, OperationResult } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { MoneyOperationNotification } from '../money-operation-notification';
import { OperationKind } from '../operation-kind';
import { AbstractOperationMetadataWithBalanceUpdatesAndInternalResults } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';
import { InternalOperationKind, InternalOperationResult } from '../operation-result';

@ObjectType({
    implements: [OperationResult],
    description: graphQLDescriptions.getRpcOperationResult(OperationKind.set_deposits_limit),
})
export class SetDepositsLimitNotificationResult extends OperationResult {}

@ObjectType({
    implements: InternalOperationResult,
    description: graphQLDescriptions.getRpcInternalOperationResult(InternalOperationKind.set_deposits_limit),
})
export class InternalSetDepositsLimitResult extends InternalOperationResult {
    @Field(() => InternalOperationKind)
    readonly kind!: InternalOperationKind.set_deposits_limit;

    @Field(() => scalars.Mutez, { nullable: true })
    readonly limit!: bigint | null;

    @Field(() => SetDepositsLimitNotificationResult)
    readonly result!: SetDepositsLimitNotificationResult;
}

@ObjectType({
    implements: [OperationMetadata],
    description: graphQLDescriptions.getRpcOperationMetadata(OperationKind.set_deposits_limit),
})
export class SetDepositsLimitNotificationMetadata extends AbstractOperationMetadataWithBalanceUpdatesAndInternalResults {
    @Field(() => SetDepositsLimitNotificationResult)
    readonly operation_result!: SetDepositsLimitNotificationResult;

    readonly graphQLTypeName!: string;
}

@ObjectType({
    implements: [
        Operation,
        OperationNotification,
        OperationNotificationWithBalanceUpdates,
        MoneyOperation,
        MoneyOperationNotification,
    ],
    description: graphQLDescriptions.getRpcOperation(OperationKind.set_deposits_limit),
})
export class SetDepositsLimitNotification
    extends MoneyOperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => scalars.Mutez, { nullable: true })
    readonly limit!: bigint | null;

    @Field(() => SetDepositsLimitNotificationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SetDepositsLimitNotificationMetadata | null;

    readonly kind = OperationKind.set_deposits_limit;
}
