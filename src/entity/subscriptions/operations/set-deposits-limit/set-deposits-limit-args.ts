import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, Filter, NullableOperationResultStatusFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { SetDepositsLimitNotification } from './set-deposits-limit-notification';

@InputType({ isAbstract: true })
export class SetDepositsLimitSpecificFilter implements Filter<SetDepositsLimitNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: SetDepositsLimitNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.status || this.status.passes(operation.metadata?.operation_result.status))
        );
    }
}

export const SetDepositsLimitFilter = createOperationFilterClass(SetDepositsLimitSpecificFilter);
export const SetDepositsLimitArgs = createOperationArgsClass(SetDepositsLimitFilter);
