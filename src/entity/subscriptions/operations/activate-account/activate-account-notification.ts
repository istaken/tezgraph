import { Field, ObjectType } from 'type-graphql';

import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { SimpleOperationMetadata } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.activate_account),
})
export class ActivateAccountNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => scalars.Address)
    readonly pkh!: string;

    @Field(() => String)
    readonly secret!: string;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | null;

    readonly kind = OperationKind.activate_account;
}
