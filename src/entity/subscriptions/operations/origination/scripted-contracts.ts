import { Field, ObjectType } from 'type-graphql';

import { Micheline, scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';

@ObjectType({
    description: `The code and the storage of a contract. ${graphQLDescriptions.getRpc(['$scripted.contracts'])}`,
})
export class ScriptedContracts {
    @Field(() => [scalars.Micheline], {
        description: 'The code of the contract.',
    })
    readonly code!: Micheline;

    @Field(() => scalars.Micheline, {
        description: 'The current storage value in Michelson format.',
    })
    readonly storage!: Micheline;
}
