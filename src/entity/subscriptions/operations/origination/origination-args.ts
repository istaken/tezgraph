import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import {
    AddressFilter,
    Filter,
    NullableAddressArrayFilter,
    NullableAddressFilter,
    NullableOperationResultStatusFilter,
} from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { OriginationNotification } from './origination-notification';

@InputType({ isAbstract: true })
export class OriginationSpecificFilter implements Filter<OriginationNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly originated_contract: Nullish<NullableAddressArrayFilter>;

    @Field(() => NullableOperationResultStatusFilter, { nullable: true })
    readonly status: Nullish<NullableOperationResultStatusFilter>;

    passes(operation: OriginationNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.delegate || this.delegate.passes(operation.delegate)) &&
            (!this.originated_contract ||
                this.originated_contract.passes(operation.metadata?.operation_result.originated_contracts)) &&
            (!this.status || this.status.passes(operation.metadata?.operation_result.status))
        );
    }
}

export const OriginationFilter = createOperationFilterClass(OriginationSpecificFilter);
export const OriginationArgs = createOperationArgsClass(OriginationFilter);
