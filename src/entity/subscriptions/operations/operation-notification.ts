import { Field, InterfaceType, ObjectType, registerEnumType } from 'type-graphql';

import { DefaultConstructor, getEnumTypeName } from '../../../utils/reflection';
import { Operation } from '../../common/operation';
import { scalars } from '../../scalars';
import { BlockNotification } from '../block/block-notification';
import { graphQLDescriptions } from '../graphql-descriptions';
import { OperationKind } from './operation-kind';

@ObjectType({
    description: `${graphQLDescriptions.getRpc(['$operation'], {
        trailingPeriod: false,
    })} but its \`contents\` is the parent object.`,
})
export class OperationGroup {
    @Field(() => scalars.ProtocolHash)
    readonly protocol!: string;

    @Field(() => scalars.ChainId, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly chain_id!: string | null;

    @Field(() => scalars.OperationHash, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly hash!: string | null;

    @Field(() => scalars.BlockHash)
    readonly branch!: string;

    @Field(() => scalars.Signature, { nullable: true })
    readonly signature!: string | null;

    readonly received_from_tezos_on!: Date;
}

export enum OperationOrigin {
    block = 'block',
    mempool = 'mempool',
}

registerEnumType(OperationOrigin, {
    name: getEnumTypeName({ OperationOrigin }),
    description: 'Indicates if the operation is just in *mempool* or already baked into a *block*.',
});

@InterfaceType({
    implements: [Operation],
    resolveType: (operation: OperationNotification): string => operation.graphQLTypeName,
    description: graphQLDescriptions.getRpc([graphQLDescriptions.rpcOperation]),
})
export abstract class OperationNotification extends Operation {
    @Field(() => OperationKind, {
        description: graphQLDescriptions.getKindProperty({
            parentName: OperationNotification.name,
            exampleChild: { kind: OperationKind.transaction, typeName: 'TransactionNotification' },
        }),
    })
    abstract readonly kind: OperationKind;

    @Field()
    readonly operation_group!: OperationGroup;

    @Field(() => BlockNotification, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly block!: BlockNotification | null;

    @Field(() => OperationOrigin)
    readonly origin!: OperationOrigin;
}

export function getOperationKind<TOperation extends OperationNotification>(
    operationType: DefaultConstructor<TOperation>,
): OperationKind {
    return new operationType().kind;
}
