import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { Filter, NullableAddressArrayFilter } from '../../common-filters';
import { getDelegates } from '../double-baking-evidence/double-baking-evidence-args';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { DoublePreendorsementEvidenceNotification } from './double-preendorsement-evidence-notification';

@InputType({ isAbstract: true })
export class DoublePreendorsementEvidenceSpecificFilter implements Filter<DoublePreendorsementEvidenceNotification> {
    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressArrayFilter>;

    passes(operation: DoublePreendorsementEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(getDelegates(operation));
    }
}

export const DoublePreendorsementEvidenceFilter = createOperationFilterClass(
    DoublePreendorsementEvidenceSpecificFilter,
);
export const DoublePreendorsementEvidenceArgs = createOperationArgsClass(DoublePreendorsementEvidenceFilter);
