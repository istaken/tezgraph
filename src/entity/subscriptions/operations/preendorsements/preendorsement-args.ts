import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { Filter, NullableAddressFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { PreendorsementNotification } from './preendorsement-notification';

@InputType({ isAbstract: true })
export class PreendorsementSpecificFilter implements Filter<PreendorsementNotification> {
    @Field(() => NullableAddressFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressFilter>;

    passes(operation: PreendorsementNotification): boolean {
        return !this.delegate || this.delegate.passes(operation.metadata?.delegate);
    }
}

export const PreendorsementFilter = createOperationFilterClass(PreendorsementSpecificFilter);
export const PreendorsementArgs = createOperationArgsClass(PreendorsementFilter);
