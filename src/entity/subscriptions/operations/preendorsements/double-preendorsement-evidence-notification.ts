import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../../utils';
import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { SimpleOperationMetadata } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

export enum InlinedPreendorsementKind {
    preendorsement = 'preendorsement',
}

registerEnumType(InlinedPreendorsementKind, {
    name: getEnumTypeName({ InlinedPreendorsementKind }),
    description: 'Inlined preendorsement kind.',
});

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.preendorsement.contents']) })
export class InlinedPreendorsementContents {
    @Field(() => InlinedPreendorsementKind)
    readonly kind!: InlinedPreendorsementKind;

    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly slot!: number;

    @Field(() => Int)
    readonly round!: number;

    @Field(() => scalars.PayloadHash)
    readonly block_payload_hash!: string;
}

@ObjectType({ description: graphQLDescriptions.getRpc(['$inlined.preendorsement']) })
export class InlinedPreendorsement {
    @Field(() => scalars.BlockHash)
    readonly branch!: string;

    @Field(() => InlinedPreendorsementContents)
    readonly operations!: InlinedPreendorsementContents;

    @Field(() => scalars.Signature, { nullable: true })
    readonly signature!: string | null;
}

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.preendorsement),
})
export class DoublePreendorsementEvidenceNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => InlinedPreendorsement)
    readonly op1!: InlinedPreendorsement;

    @Field(() => InlinedPreendorsement)
    readonly op2!: InlinedPreendorsement;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | null;

    readonly kind = OperationKind.double_preendorsement_evidence;
}
