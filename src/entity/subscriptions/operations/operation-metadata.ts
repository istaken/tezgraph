import { Field, ObjectType } from 'type-graphql';

import { BalanceUpdate } from '../extracted/balance-update/balance-update';
import { graphQLDescriptions } from '../graphql-descriptions';
import { InternalOperationResult } from './operation-notification-union';

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadataWithBalanceUpdates {
    @Field(() => [BalanceUpdate])
    readonly balance_updates!: readonly BalanceUpdate[];
}

@ObjectType({ isAbstract: true })
export abstract class AbstractOperationMetadataWithBalanceUpdatesAndInternalResults extends AbstractOperationMetadataWithBalanceUpdates {
    @Field(() => [InternalOperationResult], { nullable: true })
    readonly internal_operation_results!: readonly InternalOperationResult[] | null;
}

@ObjectType({
    description:
        `${graphQLDescriptions.getRpc([graphQLDescriptions.rpcOperation, 'metadata'], { trailingPeriod: false })}` +
        ' of simple operations where there are only `balance_updates`.',
})
export class SimpleOperationMetadata extends AbstractOperationMetadataWithBalanceUpdates {}
