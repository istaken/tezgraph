import { Field, InterfaceType, ObjectType } from 'type-graphql';

import { MoneyOperation } from '../../common/money-operation';
import { OperationResult } from '../../common/operation';
import { scalars } from '../../scalars';
import { BalanceUpdate } from '../extracted/balance-update/balance-update';
import { BigMapDiff } from '../extracted/big-map-diff/big-map-diff';
import { LazyStorageDiff } from '../extracted/lazy-storage-diff/lazy-storage-diff-union';
import { OperationNotification } from './operation-notification';

@ObjectType({ isAbstract: true })
export abstract class MoneyOperationResult extends OperationResult {
    @Field(() => [BalanceUpdate], { nullable: true })
    readonly balance_updates!: readonly BalanceUpdate[] | null;

    @Field(() => [scalars.Address], { nullable: true })
    readonly originated_contracts!: readonly string[] | null;

    @Field(() => scalars.BigNumber, { nullable: true })
    readonly storage_size!: bigint | null;

    @Field(() => scalars.BigNumber, { nullable: true })
    readonly paid_storage_size_diff!: bigint | null;

    @Field(() => [BigMapDiff], { nullable: true })
    readonly big_map_diff!: readonly BigMapDiff[] | null;

    @Field(() => [LazyStorageDiff], { nullable: true })
    readonly lazy_storage_diff!: readonly LazyStorageDiff[] | null;
}

@InterfaceType({
    implements: [OperationNotification, MoneyOperation],
    description: `Corresponds to an \`${OperationNotification.name}\` that deals with money.`,
    resolveType: (operation: MoneyOperationNotification): string => operation.graphQLTypeName,
})
export abstract class MoneyOperationNotification extends OperationNotification implements MoneyOperation {
    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => scalars.Mutez)
    readonly fee!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly counter!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly gas_limit!: bigint;

    @Field(() => scalars.PositiveBigNumber)
    readonly storage_limit!: bigint;
}
