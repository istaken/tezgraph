import { Field, Int, ObjectType } from 'type-graphql';

import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { OperationNotificationWithBalanceUpdates } from '../../extracted/balance-update/operation-notification-with-balance-updates';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { SimpleOperationMetadata } from '../operation-metadata';
import { OperationNotification } from '../operation-notification';

@ObjectType({ description: graphQLDescriptions.getRpc(['$block_header.alpha.full_header']) })
export class DoubleBakingEvidenceBlockHeader {
    @Field(() => Int)
    readonly level!: number;

    @Field(() => Int)
    readonly proto!: number;

    @Field(() => scalars.BlockHash)
    readonly predecessor!: string;

    @Field(() => Date)
    readonly timestamp!: Date;

    @Field(() => Int)
    readonly validation_pass!: number;

    @Field(() => scalars.OperationHash, { nullable: true })
    readonly operations_hash!: string | null;

    @Field(() => [String])
    readonly fitness!: readonly string[];

    @Field(() => scalars.ContextHash)
    readonly context!: string;

    @Field(() => Int)
    readonly priority!: number;

    @Field(() => String, { nullable: true })
    readonly proof_of_work_nonce!: string | null;

    @Field(() => scalars.NonceHash, { nullable: true })
    readonly seed_nonce_hash!: string | null;

    @Field(() => scalars.Signature)
    readonly signature!: string;
}

@ObjectType({
    implements: [Operation, OperationNotification, OperationNotificationWithBalanceUpdates],
    description: graphQLDescriptions.getRpcOperation(OperationKind.double_baking_evidence),
})
export class DoubleBakingEvidenceNotification
    extends OperationNotification
    implements OperationNotificationWithBalanceUpdates
{
    @Field(() => DoubleBakingEvidenceBlockHeader)
    readonly bh1!: DoubleBakingEvidenceBlockHeader;

    @Field(() => DoubleBakingEvidenceBlockHeader)
    readonly bh2!: DoubleBakingEvidenceBlockHeader;

    @Field(() => SimpleOperationMetadata, {
        nullable: true,
        description: graphQLDescriptions.nullIfMempool,
    })
    readonly metadata!: SimpleOperationMetadata | null;

    readonly kind = OperationKind.double_baking_evidence;
}
