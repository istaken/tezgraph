import { Field, InputType } from 'type-graphql';

import { isNotNullish, Nullish } from '../../../../utils/reflection';
import { Filter, NullableAddressArrayFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { AbstractOperationMetadataWithBalanceUpdates } from '../operation-metadata';
import { DoubleBakingEvidenceNotification } from './double-baking-evidence-notification';

@InputType({ isAbstract: true })
export class DoubleBakingEvidenceSpecificFilter implements Filter<DoubleBakingEvidenceNotification> {
    @Field(() => NullableAddressArrayFilter, { nullable: true })
    readonly delegate: Nullish<NullableAddressArrayFilter>;

    passes(operation: DoubleBakingEvidenceNotification): boolean {
        return !this.delegate || this.delegate.passes(getDelegates(operation));
    }
}

export const DoubleBakingEvidenceFilter = createOperationFilterClass(DoubleBakingEvidenceSpecificFilter);
export const DoubleBakingEvidenceArgs = createOperationArgsClass(DoubleBakingEvidenceFilter);

export function getDelegates(options: {
    metadata: AbstractOperationMetadataWithBalanceUpdates | null;
}): string[] | undefined {
    return options.metadata?.balance_updates.map((u) => u.delegate).filter(isNotNullish);
}
