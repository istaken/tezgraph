import { Field, Int, ObjectType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../../utils/reflection';
import { Operation } from '../../../common/operation';
import { scalars } from '../../../scalars';
import { graphQLDescriptions } from '../../graphql-descriptions';
import { OperationKind } from '../operation-kind';
import { OperationNotification } from '../operation-notification';

export enum BallotVote {
    nay = 'nay',
    pass = 'pass',
    yay = 'yay',
}

registerEnumType(BallotVote, {
    name: getEnumTypeName({ BallotVote }),
    description: 'The ballot vote.',
});

@ObjectType({
    implements: [Operation, OperationNotification],
    description: graphQLDescriptions.getRpcOperation(OperationKind.ballot),
})
export class BallotNotification extends OperationNotification {
    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly period!: number;

    @Field(() => scalars.ProtocolHash)
    readonly proposal!: string;

    @Field(() => BallotVote)
    readonly ballot!: BallotVote;

    readonly kind = OperationKind.ballot;
}
