import { Field, InputType } from 'type-graphql';

import { Nullish } from '../../../../utils/reflection';
import { AddressFilter, createEqualityFilterClass, Filter, ProtocolHashFilter } from '../../common-filters';
import { createOperationArgsClass, createOperationFilterClass } from '../operation-args';
import { BallotNotification, BallotVote } from './ballot-notification';

@InputType()
export class BallotVoteFilter extends createEqualityFilterClass<BallotVote>(BallotVote) {}

@InputType({ isAbstract: true })
export class BallotSpecificFilter implements Filter<BallotNotification> {
    @Field(() => AddressFilter, { nullable: true })
    readonly source: Nullish<AddressFilter>;

    @Field(() => ProtocolHashFilter, { nullable: true })
    readonly proposal: Nullish<ProtocolHashFilter>;

    @Field(() => BallotVoteFilter, { nullable: true })
    readonly ballot: Nullish<BallotVoteFilter>;

    passes(operation: BallotNotification): boolean {
        return (
            (!this.source || this.source.passes(operation.source)) &&
            (!this.proposal || this.proposal.passes(operation.proposal)) &&
            (!this.ballot || this.ballot.passes(operation.ballot))
        );
    }
}

export const BallotFilter = createOperationFilterClass(BallotSpecificFilter);
export const BallotArgs = createOperationArgsClass(BallotFilter);
