import { Field, Int, InterfaceType, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../../utils/reflection';
import { OperationResult } from '../../common/operation';
import { scalars } from '../../scalars';
import { graphQLDescriptions } from '../graphql-descriptions';

export enum InternalOperationKind {
    delegation = 'delegation',
    origination = 'origination',
    register_global_constant = 'register_global_constant',
    reveal = 'reveal',
    set_deposits_limit = 'set_deposits_limit',
    transaction = 'transaction',
}

registerEnumType(InternalOperationKind, {
    name: getEnumTypeName({ InternalOperationKind }),
    description: 'The kind of an internal operation result.',
});

@InterfaceType({
    description: graphQLDescriptions.getRpcInternalOperationResult(),
    resolveType: (r: InternalOperationResult) => r.graphQLTypeName,
})
export abstract class InternalOperationResult {
    @Field(() => InternalOperationKind, {
        description: graphQLDescriptions.getKindProperty({
            parentName: InternalOperationResult.name,
            exampleChild: { kind: InternalOperationKind.transaction, typeName: 'InternalTransactionResult' },
        }),
    })
    abstract readonly kind: InternalOperationKind;

    @Field(() => scalars.Address)
    readonly source!: string;

    @Field(() => Int)
    readonly nonce!: number;

    @Field(() => OperationResult)
    readonly result!: OperationResult;

    readonly graphQLTypeName!: string;
}
