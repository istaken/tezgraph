import { ArgsType, Field, InputType } from 'type-graphql';

import { DefaultConstructor, nameof, Nullish } from '../../../utils/reflection';
import { removeRequiredSuffix } from '../../../utils/string-manipulation';
import { BlockHashFilter, Filter, NullableOperationHashFilter, ProtocolHashFilter } from '../common-filters';
import { createSubscriptionFilterArgsClass, FilteringArgs, ReplayFromBlockArgs } from '../subscription-args';
import { OperationNotification } from './operation-notification-union';

export interface OperationFilter<TOperation extends OperationNotification> extends Filter<TOperation> {
    readonly hash: Nullish<NullableOperationHashFilter>;
    readonly protocol: Nullish<ProtocolHashFilter>;
    readonly branch: Nullish<BlockHashFilter>;
    readonly and: Nullish<readonly OperationFilter<TOperation>[]>;
    readonly or: Nullish<readonly OperationFilter<TOperation>[]>;
}

export function createOperationFilterClass<TOperation extends OperationNotification>(
    specificFilterType: DefaultConstructor<Filter<TOperation>>,
): DefaultConstructor<OperationFilter<TOperation>> {
    const operationName = removeRequiredSuffix(specificFilterType.name, 'SpecificFilter');

    @InputType(`${operationName}Filter`, {
        description: `Provides filters for filtering *${operationName}* operations.`,
    })
    class OperationFilterClass extends specificFilterType {
        @Field(() => NullableOperationHashFilter, { nullable: true })
        readonly hash: Nullish<NullableOperationHashFilter>;

        @Field(() => ProtocolHashFilter, { nullable: true })
        readonly protocol: Nullish<ProtocolHashFilter>;

        @Field(() => BlockHashFilter, { nullable: true })
        readonly branch: Nullish<BlockHashFilter>;

        @Field(() => [OperationFilterClass], {
            description: getFilterArrayDescription({ mustPass: 'all filters' }),
            nullable: true,
        })
        readonly and: Nullish<OperationFilterClass[]>;

        @Field(() => [OperationFilterClass], {
            description: getFilterArrayDescription({ mustPass: 'at least one filter' }),
            nullable: true,
        })
        readonly or: Nullish<OperationFilterClass[]>;

        passes(operation: TOperation): boolean {
            return (
                super.passes(operation) &&
                (!this.hash || this.hash.passes(operation.operation_group.hash)) &&
                (!this.protocol || this.protocol.passes(operation.operation_group.protocol)) &&
                (!this.branch || this.branch.passes(operation.operation_group.branch)) &&
                (!this.and || this.and.length === 0 || this.and.every((f) => f.passes(operation))) &&
                (!this.or || this.or.length === 0 || this.or.some((f) => f.passes(operation)))
            );
        }
    }
    return OperationFilterClass;
}

function getFilterArrayDescription(options: { mustPass: string }): string {
    return (
        `The recursive array of filters of the same type. It can be used to specify advanced composite conditions.` +
        ` If specified (non-empty array) then ${options.mustPass} in the array must pass (in addition to other filter properties).`
    );
}

export interface OperationArgs<TOperation extends OperationNotification>
    extends FilteringArgs<TOperation>,
        ReplayFromBlockArgs {
    readonly includeMempool: boolean;
}

export function createOperationArgsClass<TOperation extends OperationNotification>(
    filterType: DefaultConstructor<OperationFilter<TOperation>>,
): DefaultConstructor<OperationArgs<TOperation>> {
    @ArgsType()
    class OperationArgsClass extends createSubscriptionFilterArgsClass(filterType) {
        @Field(() => Boolean, {
            description:
                `If combined with \`${nameof<ReplayFromBlockArgs>('replayFromBlockLevel')}\`` +
                ' then while returning historical operations, no mempool operations are returned.' +
                ' Once switched to new operations then also mempool operations are included.',
        })
        readonly includeMempool: boolean = false;
    }
    return OperationArgsClass;
}
