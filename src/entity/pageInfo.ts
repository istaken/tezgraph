/* istanbul ignore file */

import { Field, ObjectType } from 'type-graphql';

import { scalars } from './scalars';

@ObjectType()
export class PageInfo {
    @Field({ description: 'The hash of the operation.' })
    has_next_page!: boolean;

    @Field({ description: 'The position of the operation in the operation batch.' })
    has_previous_page!: boolean;

    @Field(() => scalars.Cursor, { description: 'The hash of the operation.', nullable: true })
    start_cursor?: string | null;

    @Field(() => scalars.Cursor, {
        description: 'The position of the operation in the operation batch.',
        nullable: true,
    })
    end_cursor?: string | null;
}
