import { GraphQLScalarType } from 'graphql';
import { GraphQLJSONObject } from 'graphql-type-json';
import { GraphQLISODateTime } from 'type-graphql';

import {
    createBase58HashScalar,
    createScalar,
    parseBigInt,
    parseDecimalJS,
    parsePositiveBigInt,
    parseDateRangeTimestamp,
} from './scalars-helper';

export const scalars = {
    Address: createBase58HashScalar({
        name: 'Address',
        description:
            'Tezos address. Represented as public key hash (Base58Check-encoded) prefixed with tz1, tz2, tz3 or KT1',
        supportedPrefixes: [
            { prefix: 'tz1', length: 36 },
            { prefix: 'KT1', length: 36 },
            { prefix: 'tz2', length: 36 },
            { prefix: 'tz3', length: 36 },
        ],
    }),
    BigNumber: createScalar({
        name: 'BigNumber',
        description: 'Arbitrary precision integer number represented as string in JSON.',
        parseValue: parseBigInt,
    }),
    PositiveBigNumber: createScalar({
        name: 'PositiveBigNumber',
        description: 'Arbitrary precision positive integer number represented as string in JSON.',
        parseValue: parsePositiveBigInt,
    }),
    Mutez: createScalar({
        name: 'Mutez',
        description: 'Micro tez. It uses same rules as PositiveBigNumber. 1 tez = 1,000,000 micro tez.',
        parseValue: parsePositiveBigInt,
    }),
    OperationHash: createBase58HashScalar({
        name: 'OperationHash',
        description:
            'Operation identifier (Base58Check-encoded) prefixed with o.Operation identifier (Base58Check-encoded) prefixed with o.',
        supportedPrefixes: [{ prefix: 'o', length: 51 }],
    }),
    BlockHash: createBase58HashScalar({
        name: 'BlockHash',
        description: 'Block identifier (Base58Check-encoded) prefixed with B.',
        supportedPrefixes: [{ prefix: 'B', length: 51 }],
    }),
    ProtocolHash: createBase58HashScalar({
        name: 'ProtocolHash',
        description: 'Protocol identifier (Base58Check-encoded) prefixed with P.',
        supportedPrefixes: [{ prefix: 'P', length: 51 }],
    }),
    ContextHash: createBase58HashScalar({
        name: 'ContextHash',
        description: 'ContextHash identifier (Base58Check-encoded) prefixed with Co.',
        supportedPrefixes: [{ prefix: 'Co', length: 52 }],
    }),
    OperationsHash: createBase58HashScalar({
        name: 'OperationsHash',
        description: 'OperationsHash identifier (Base58Check-encoded) prefixed with LLo.',
        supportedPrefixes: [{ prefix: 'LLo', length: 53 }],
    }),
    PayloadHash: createScalar({
        name: 'PayloadHash',
        description: 'Payload hash (Base58Check-encoded) prefixed with vh.',
        parseValue: (s: string) => s,
    }),
    ChainId: createBase58HashScalar({
        name: 'ChainId',
        description: 'Chain identifier (Base58Check-encoded) prefixed with Net.',
        supportedPrefixes: [{ prefix: 'Net', length: 15 }],
    }),
    Signature: createBase58HashScalar({
        name: 'Signature',
        description: 'Generic signature (Base58Check-encoded) prefixed with sig.',
        supportedPrefixes: [{ prefix: 'sig', length: 96 }],
    }),
    PublicKey: createBase58HashScalar({
        name: 'PublicKey',
        description: 'Public key (Base58Check-encoded) prefixed with edpk, sppk or p2pk.',
        supportedPrefixes: [
            { prefix: 'edpk', length: 54 },
            { prefix: 'p2pk', length: 55 },
            { prefix: 'sppk', length: 55 },
        ],
    }),
    NonceHash: createBase58HashScalar({
        name: 'NonceHash',
        description: 'Nonce hash (Base58Check-encoded).',
    }),
    DateTime: GraphQLISODateTime,
    JSONObject: GraphQLJSONObject,
    Micheline: new GraphQLScalarType({
        name: 'Micheline',
        description: 'Raw Micheline value represented as nested objects and arrays.',
    }),
    Cursor: createScalar({
        name: 'Cursor',
        description:
            'A string that is used to paginate query results. This value can be used as the `before` or `after` query arguments.',
        parseValue: (s: string) => s,
    }),
    Decimal: createScalar({
        name: 'Decimal',
        description: 'GraphQL Scalar representing the Prisma.Decimal type, based on Decimal.js library.',
        parseValue: parseDecimalJS,
    }),
    DateString: createScalar({
        name: 'DateString',
        description: 'A date in string format.',
        parseValue: parseDateRangeTimestamp,
    }),
};

interface MichelineBase {
    readonly int?: string;
    readonly string?: string;
    readonly bytes?: string;
}
interface MichelineExtended {
    readonly prim: string;
    readonly args?: readonly Micheline[];
    readonly annots?: readonly string[];
}

export type Micheline = MichelineBase | MichelineExtended | readonly Micheline[];
