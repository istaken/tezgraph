import { Field, InterfaceType } from 'type-graphql';

import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperation } from './money-operation';
import { Operation, OperationMetadata, OperationResult } from './operation';

@InterfaceType({ implements: [OperationResult] })
export class OriginationResult extends OperationResult {}

@InterfaceType({
    implements: [OperationMetadata],
    resolveType: (metadata: OriginationMetadata): string => metadata.graphQLTypeName,
})
export class OriginationMetadata extends OperationMetadata {
    @Field(() => OriginationResult)
    readonly operation_result!: OriginationResult;
}

@InterfaceType({
    implements: [Operation, MoneyOperation],
})
export class Origination extends MoneyOperation implements Operation {
    @Field(() => scalars.Mutez, {
        nullable: true,
        description: 'The initial balance of the originated contract.',
    })
    readonly balance!: bigint | null;

    @Field(() => OriginationMetadata, { nullable: true })
    readonly metadata!: OriginationMetadata | null;

    readonly kind = OperationKind.origination;
}
