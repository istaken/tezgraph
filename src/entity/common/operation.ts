import { InterfaceType, Field, registerEnumType } from 'type-graphql';

import { getEnumTypeName } from '../../utils';
import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';

export enum OperationResultStatus {
    applied = 'applied',
    failed = 'failed',
    skipped = 'skipped',
    backtracked = 'backtracked',
}

registerEnumType(OperationResultStatus, {
    name: getEnumTypeName({ OperationResultStatus }),
    description: 'Operation result status.',
});

@InterfaceType({
    resolveType: (result: OperationResult) => result.graphQLTypeName,
})
export abstract class OperationResult {
    @Field(() => OperationResultStatus)
    readonly status!: OperationResultStatus;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly consumed_gas!: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly consumed_milligas!: bigint | null;

    @Field(() => [scalars.JSONObject], { nullable: true })
    readonly errors!: readonly object[] | null;

    readonly graphQLTypeName!: string;
}

@InterfaceType({
    resolveType: (operation: Operation): string => operation.graphQLTypeName,
})
export class OperationMetadata {
    @Field(() => OperationResult)
    readonly operation_result!: OperationResult;

    readonly graphQLTypeName!: string;
}

@InterfaceType({
    resolveType: (operation: Operation): string => operation.graphQLTypeName,
})
export abstract class Operation {
    @Field(() => OperationKind, { description: 'The kind of operation.' })
    readonly kind!: OperationKind;

    readonly graphQLTypeName!: string;
}
