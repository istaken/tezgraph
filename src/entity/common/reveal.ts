import { Field, InterfaceType } from 'type-graphql';

import { scalars } from '../scalars';
import { OperationKind } from '../subscriptions/operations/operation-kind';
import { MoneyOperation } from './money-operation';
import { Operation, OperationMetadata, OperationResult } from './operation';

@InterfaceType({ implements: [OperationResult] })
export class RevealResult extends OperationResult {}

@InterfaceType({
    implements: [OperationMetadata],
    resolveType: (metadata: RevealMetadata) => metadata.graphQLTypeName,
})
export class RevealMetadata extends OperationMetadata {
    @Field(() => RevealResult)
    readonly operation_result!: RevealResult;
}

@InterfaceType({
    implements: [Operation, MoneyOperation],
})
export class Reveal extends MoneyOperation implements Operation {
    @Field(() => scalars.PublicKey)
    readonly public_key!: string;

    @Field(() => RevealMetadata, { nullable: true })
    readonly metadata!: RevealMetadata | null;

    readonly kind = OperationKind.reveal;
}
