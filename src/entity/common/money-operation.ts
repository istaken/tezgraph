import { InterfaceType, Field } from 'type-graphql';

import { scalars } from '../scalars';
import { Operation } from './operation';

@InterfaceType({
    implements: [Operation],
    description: `Corresponds to an \`${Operation.name}\` that deals with money.`,
    resolveType: (operation: MoneyOperation): string => operation.graphQLTypeName,
})
export abstract class MoneyOperation extends Operation {
    @Field(() => scalars.Mutez)
    readonly fee!: bigint;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly counter?: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly gas_limit?: bigint | null;

    @Field(() => scalars.PositiveBigNumber, { nullable: true })
    readonly storage_limit?: bigint | null;
}
