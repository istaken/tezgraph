/* eslint-disable no-console */
import 'reflect-metadata';
// We need to load .env files correctly before prisma kicks in because it overwrites it itself.
// eslint-disable-next-line import/order
import './utils/configuration/env-config-initialization';

import path from 'path';
import { emitSchemaDefinitionFile } from 'type-graphql';

import { ApolloServerFactory } from './bootstrap/apollo-server-factory';
import { createDIContainer } from './dependency-injection-container';

async function main(): Promise<void> {
    const container = createDIContainer({
        ...process.env,
        MODULES: 'QueriesGraphQL,SubscriptionsGraphQL',
    });
    const apolloServerFactory = container.resolve(ApolloServerFactory);
    const { schema } = await apolloServerFactory.create();

    const schemaPath = path.resolve(__dirname, '../', 'generated-schema.graphql');
    await emitSchemaDefinitionFile(schemaPath, schema);

    console.log(`Schema generated successfully into '${schemaPath}'`);
}

main().catch(console.error);
