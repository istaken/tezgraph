import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { singleton } from 'tsyringe';

import { EnvConfig } from '../utils/configuration/env-config';
import { injectLogger, Logger } from '../utils/logging';
import { AllProperties, isNotNullish } from '../utils/reflection';
import { UuidGenerator } from '../utils/uuid-generator';
import { ResolverContext } from './resolver-context';

@singleton()
export class GraphQLErrorHandler {
    constructor(
        private readonly uuidGenerator: UuidGenerator,
        private readonly envConfig: EnvConfig,
        @injectLogger(GraphQLErrorHandler) private readonly logger: Logger,
    ) {}

    handle(error: GraphQLError, context: ResolverContext | undefined): GraphQLFormattedError {
        const requestId = context?.requestId;
        const errorId = this.uuidGenerator.generate();
        const message = `An error has occurred. Please provide error ID '${errorId}' if you contact our help desk.`;
        this.logger.logError('An error has occurred with {errorId} for {requestId}. Actual {error}', {
            errorId,
            error,
            requestId,
        });

        const censoredError = this.censorSensitiveErrorDetails(error.message);

        return {
            ...getSignificantProperties(error),
            message: `${message} Error: '${censoredError}'`,
            extensions: {
                error: `${censoredError}`,
                requestId: `${requestId ?? 'n/a'}`,
                errorId: `${errorId}`,
            },
        };
    }

    censorSensitiveErrorDetails(errorMsg: string): string {
        const ec = this.envConfig;
        let censoredErrorMsg = errorMsg.slice();
        const sensitiveEnvs = [
            ec.host,
            ec.apiKey,
            process.env.DATABASE_CONNECTION_STRING,
            process.env.REDIS_CONNECTION_STRING,
        ];

        sensitiveEnvs.forEach((env) => {
            if (isNotNullish(env)) {
                const envString = env.toString();
                censoredErrorMsg = censoredErrorMsg.replaceAll(envString, envString.replace(/./gu, '*'));
            }
        });
        return censoredErrorMsg;
    }
}

function getSignificantProperties(error: GraphQLError): AllProperties<GraphQLFormattedError> {
    return {
        message: error.message,
        locations: error.locations,
        path: error.path,
        extensions: error.extensions,
    };
}
