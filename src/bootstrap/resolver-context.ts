import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { ExecutionParams } from 'subscriptions-transport-ws';
import { DependencyContainer } from 'tsyringe';

/** Enhanced context used for processing requests. */
export interface ResolverContext extends Readonly<Partial<ExpressContext>> {
    /** Container so that we can resolve dependencies also in JavaScript decorators. */
    readonly container: DependencyContainer;

    /** UUID identifying the request so that logged entries can be paired. */
    readonly requestId: string;

    connection?: ExecutionParams;
}
