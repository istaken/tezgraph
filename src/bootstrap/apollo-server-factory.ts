import { ApolloServerPluginLandingPageGraphQLPlayground } from 'apollo-server-core';
import { GraphQLServerOptions } from 'apollo-server-core/src/graphqlOptions';
import { formatApolloErrors } from 'apollo-server-errors';
import { ApolloServer, ApolloServerExpressConfig } from 'apollo-server-express';
import { Request, Response } from 'express';
import { ExecutionResult, GraphQLError, GraphQLFormattedError, GraphQLSchema } from 'graphql';
import depthLimit from 'graphql-depth-limit';
import { createComplexityLimitRule } from 'graphql-validation-complexity';
import { performance } from 'perf_hooks';
import { ExecutionParams } from 'subscriptions-transport-ws';
import { DependencyContainer, inject, injectAll, singleton } from 'tsyringe';
import { buildSchema, NonEmptyArray } from 'type-graphql';

import { scalars } from '../entity/scalars';
import {
    LazyBigMapAlloc,
    LazyBigMapCopy,
    LazyBigMapRemove,
    LazyBigMapUpdate,
    LazySaplingStateAlloc,
    LazySaplingStateCopy,
    LazySaplingStateRemove,
    LazySaplingStateUpdate,
} from '../entity/subscriptions';
import { EnvConfig } from '../utils/configuration/env-config';
import { diContainerDIToken } from '../utils/dependency-injection';
import { injectLogger, Logger } from '../utils/logging';
import { QueryLoggerPlugin } from '../utils/logging/query-logger-plugin';
import { UuidGenerator } from '../utils/uuid-generator';
import { GraphQLAuthChecker } from './graphql-auth-checker';
import { GraphQLDependencyContainer } from './graphql-dependency-container';
import { GraphQLErrorHandler } from './graphql-error-handler';
import { graphQLMiddlewaresDIToken, Middleware } from './graphql-middleware-type';
import { graphQLResolversDIToken, GraphQLResolverType } from './graphql-resolver-type';
import { ResolverContext } from './resolver-context';

type ErrorFormatter = (error: GraphQLError) => GraphQLFormattedError;
type CreateErrorFormatter = (context: ResolverContext | undefined) => ErrorFormatter;

export interface CustomApolloServerConfig extends ApolloServerExpressConfig {
    createErrorFormatter: CreateErrorFormatter;
    schema: GraphQLSchema;
}

export class CustomApolloServer extends ApolloServer {
    readonly schema: GraphQLSchema;
    private readonly createErrorFormatter: CreateErrorFormatter;

    constructor(config: CustomApolloServerConfig) {
        super(config);
        this.schema = config.schema;
        this.createErrorFormatter = config.createErrorFormatter;
    }

    async createGraphQLServerOptions(req: Request, res: Response): Promise<GraphQLServerOptions> {
        const options = await super.createGraphQLServerOptions(req, res);
        return {
            ...options,
            formatError: this.createErrorFormatter(options.context as ResolverContext),
        };
    }
}

@singleton()
export class ApolloServerFactory {
    // eslint-disable-next-line max-params
    constructor(
        private readonly envConfig: EnvConfig,
        @injectAll(graphQLResolversDIToken) private readonly resolvers: NonEmptyArray<GraphQLResolverType>,
        @injectAll(graphQLMiddlewaresDIToken) private readonly middlewares: Middleware[],
        @inject(diContainerDIToken) private readonly container: DependencyContainer,
        private readonly errorHandler: GraphQLErrorHandler,
        private readonly authChecker: GraphQLAuthChecker,
        private readonly uuidGenerator: UuidGenerator,
        @injectLogger(ApolloServerFactory) private readonly logger: Logger,
    ) {}

    async create(): Promise<CustomApolloServer> {
        return new CustomApolloServer({
            schema: await buildSchema({
                resolvers: this.resolvers,
                validate: false,
                container: new GraphQLDependencyContainer(this.container),
                // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
                authChecker: (c) => this.authChecker.check(c.context),
                globalMiddlewares: this.middlewares,
                scalarsMap: [
                    {
                        type: Date,
                        scalar: scalars.DateTime,
                    },
                ],
                orphanedTypes: [
                    LazyBigMapAlloc,
                    LazyBigMapCopy,
                    LazyBigMapRemove,
                    LazyBigMapUpdate,
                    LazySaplingStateAlloc,
                    LazySaplingStateCopy,
                    LazySaplingStateRemove,
                    LazySaplingStateUpdate,
                ],
            }),
            context: (original): ResolverContext => {
                const context = {
                    ...original,
                    requestId: this.uuidGenerator.generate(),
                    startTime: performance.now(),
                    container: this.container,
                };
                return context;
            },
            // CustomApolloServer.createGraphQLServerOptions() also sets formatError but it's not called always -> we keep it here too.
            formatError: this.createErrorFormatter(undefined),
            createErrorFormatter: this.createErrorFormatter.bind(this),
            validationRules: [
                ...(this.envConfig.graphQLDepthLimit > 0 ? [depthLimit(this.envConfig.graphQLDepthLimit)] : []),
                ...(this.envConfig.graphQLComplexityLimit > 0
                    ? [createComplexityLimitRule(this.envConfig.graphQLComplexityLimit)]
                    : []),
            ],
            debug: this.envConfig.enableDebug, // E.g. shows error stack traces.
            introspection: true, // Needed for the playground.
            plugins: [
                ApolloServerPluginLandingPageGraphQLPlayground, // We want in also on prod b/c it's a documentation.
                new QueryLoggerPlugin(this.logger),
            ],
        });
    }

    registerErrorFormatter(connection: ExecutionParams): void {
        connection.formatError = this.createErrorFormatter(undefined);
        connection.formatResponse = (value: ExecutionResult): ExecutionResult => {
            if (value.errors && value.errors.length > 0) {
                value.errors = formatApolloErrors(value.errors, {
                    formatter: this.createErrorFormatter(undefined),
                    debug: this.envConfig.enableDebug,
                });
            }
            return value;
        };
    }

    private createErrorFormatter(context: ResolverContext | undefined): ErrorFormatter {
        return (error): GraphQLFormattedError => this.errorHandler.handle(error, context);
    }
}
