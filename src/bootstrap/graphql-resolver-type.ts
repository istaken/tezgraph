import { InjectionToken } from 'tsyringe';

import { Constructor } from '../utils/reflection';

export type GraphQLResolverType = Constructor;

export const graphQLResolversDIToken: InjectionToken<GraphQLResolverType> = 'GraphQLResolvers';
