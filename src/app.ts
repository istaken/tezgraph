/* eslint-disable max-params */
import 'reflect-metadata';
import express from 'express';
import { execute, subscribe } from 'graphql';
import http from 'http';
import { performance } from 'perf_hooks';
import { ExecutionParams, SubscriptionServer } from 'subscriptions-transport-ws';
import { DependencyContainer, inject, injectAll, singleton } from 'tsyringe';

import { ApolloServerFactory, CustomApolloServer } from './bootstrap/apollo-server-factory';
import { BackgroundWorker, backgroundWorkersDIToken } from './bootstrap/background-worker';
import { expressAppDIToken } from './bootstrap/express-app-factory';
import { ResolverContext } from './bootstrap/resolver-context';
import { EnvConfig } from './utils/configuration/env-config';
import { diContainerDIToken } from './utils/dependency-injection';
import { ApolloHealthCheck } from './utils/health/apollo-health-check';
import { injectLogger, Logger } from './utils/logging';
import { UuidGenerator } from './utils/uuid-generator';

@singleton()
export class App {
    apolloServer: CustomApolloServer | undefined;
    private subscriptionServer?: SubscriptionServer;

    constructor(
        private readonly envConfig: EnvConfig,
        readonly httpServer: http.Server,
        @inject(expressAppDIToken) private readonly expressApp: express.Express,
        private readonly apolloServerFactory: ApolloServerFactory,
        @injectAll(backgroundWorkersDIToken) private readonly backgroundWorkers: readonly BackgroundWorker[],
        private readonly apolloHealthCheck: ApolloHealthCheck,
        @injectLogger(App) private readonly logger: Logger,
        private readonly uuidGenerator: UuidGenerator,
        @inject(diContainerDIToken) private readonly container: DependencyContainer,
    ) {}

    async start(): Promise<void> {
        if (!this.apolloServer) {
            await this.initialize();
        }

        await Promise.all(
            this.backgroundWorkers.map(async (worker) => {
                this.logWorker('Starting', worker);
                await worker.start();
                this.logWorker('Started', worker);
            }),
        );

        this.logger.logInformation('Server starting at {url}.', {
            url: `http://${this.envConfig.host}:${this.envConfig.port}`,
        });
        await new Promise<void>((resolve) => {
            this.httpServer.listen(this.envConfig.port, this.envConfig.host, resolve);
        });
        this.logger.logInformation('Server started.');
    }

    async stop(): Promise<void> {
        if (!this.apolloServer) {
            throw new Error('App is not initialized.');
        }

        await Promise.all(
            this.backgroundWorkers.map(async (worker) => {
                if (worker.stop) {
                    this.logWorker('Stopping', worker);
                    await worker.stop();
                    this.logWorker('Stopped', worker);
                }
            }),
        );

        this.logger.logInformation('Server stopping.');
        await this.apolloServer.stop();
        this.subscriptionServer?.close();
        await new Promise((resolve) => {
            this.httpServer.close(resolve);
        });

        this.logger.logInformation('Server stopped.');
        this.logger.close();
    }

    async initialize(): Promise<void> {
        this.apolloServer = await this.apolloServerFactory.create();
        await this.apolloServer.start();

        this.apolloServer.applyMiddleware({
            app: this.expressApp,
            path: '/graphql',
            onHealthCheck: async () => this.apolloHealthCheck.checkIsHealthy(),
        });

        this.subscriptionServer = SubscriptionServer.create(
            {
                schema: this.apolloServer.schema,
                execute,
                subscribe,
                onConnect: (_connectionParams: unknown, _webSocket: unknown, original: object): ResolverContext => {
                    const context = {
                        ...original,
                        requestId: this.uuidGenerator.generate(),
                        startTime: performance.now(),
                        container: this.container,
                    };
                    return context;
                },
                onOperation: (_: unknown, params: ExecutionParams) => {
                    this.apolloServerFactory.registerErrorFormatter(params);
                    return params;
                },
                ...(this.envConfig.subscriptionKeepAliveMillis > 0
                    ? { keepAlive: this.envConfig.subscriptionKeepAliveMillis }
                    : undefined),
            },
            {
                server: this.httpServer,
                path: this.apolloServer.graphqlPath,
            },
        );
    }

    private logWorker(verb: string, worker: BackgroundWorker): void {
        this.logger.logInformation(`${verb} {worker}.`, { worker: worker.name });
    }
}
