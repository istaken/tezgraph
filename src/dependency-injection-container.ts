import { RpcClient } from '@taquito/rpc';
import { AbortController, AbortSignal } from 'abort-controller';
import http from 'http';
import NodeCache from 'node-cache';
import { Registry } from 'prom-client';
import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { backgroundWorkersDIToken } from './bootstrap/background-worker';
import { expressAppDIToken, ExpressAppFactory } from './bootstrap/express-app-factory';
import { graphQLMiddlewaresDIToken } from './bootstrap/graphql-middleware-type';
import { graphQLResolversDIToken } from './bootstrap/graphql-resolver-type';
import { memoryPubSubModule } from './modules/memory-pubsub/memory-pubsub-module';
import { CompositeModule, CompositeModuleName, compositeModulesDIToken, modulesDIToken } from './modules/module';
import { ModulesInitializer } from './modules/modules-initializer';
import { queriesGraphQLModule } from './modules/queries-graphql/queries-graphql-module';
import { redisPubSubModule } from './modules/redis-pubsub/redis-pubsub-module';
import { subscriptionsSubscriberModule } from './modules/subscriptions-subscriber/subscriptions-subscriber-module';
import { tezosMonitorModule } from './modules/tezos-monitor/tezos-monitor-module';
import { VersionGraphQLResolver } from './utils/app-version/version-graphql-resolver';
import { VersionMetricsWorker } from './utils/app-version/version-metrics-worker';
import { EnvConfig } from './utils/configuration/env-config';
import { processDIToken, processEnvDIToken } from './utils/configuration/env-config-provider';
import { diContainerDIToken, registerSingleton } from './utils/dependency-injection';
import { HealthCheckHttpHandler, HealthStatusHttpHandler } from './utils/health/health-http-handler';
import { appUrlPaths, httpHandlersDIToken } from './utils/http-handler';
import { createRootLogger, LoggerFactory } from './utils/logging';
import { QueryLogger } from './utils/logging/query-logger-middleware';
import { metricsUpdatersDIToken } from './utils/metrics/metrics-collector';
import { MetricsContainer } from './utils/metrics/metrics-container';
import { MetricsHttpHandler } from './utils/metrics/metrics-http-handler';
import { MetricsMiddleware } from './utils/metrics/metrics-middleware';
import { MetricsWorker } from './utils/metrics/metrics-worker';
import { AppUptimeMetricsUpdater } from './utils/metrics/updaters/app-uptime-metrics-updater';
import { HealthMetricsUpdater } from './utils/metrics/updaters/health-metrics-updater';
import { HttpServerMetricsUpdater } from './utils/metrics/updaters/http-server-metrics-updater';
import { Clock } from './utils/time/clock';

export function createDIContainer(processEnv: NodeJS.ProcessEnv): DependencyContainer {
    const diContainer = globalDIContainer.createChildContainer();

    diContainer.registerInstance(processDIToken, process);
    diContainer.registerInstance(processEnvDIToken, processEnv);
    diContainer.registerInstance(diContainerDIToken, diContainer);
    diContainer.registerInstance(NodeCache, new NodeCache({ checkperiod: 1 }));
    registerSingleton(
        diContainer,
        LoggerFactory,
        (c) => new LoggerFactory(createRootLogger(c.resolve(EnvConfig)), c.resolve(Clock), c.resolve(MetricsContainer)),
    );
    registerSingleton(diContainer, RpcClient, (c) => new RpcClient(c.resolve(EnvConfig).tezosNodeUrl));

    registerAbortion(diContainer);
    registerVersionGraphQL(diContainer);
    registerMetrics(diContainer);
    registerHttpServer(diContainer);
    registerModules(diContainer);
    registerQueryLogger(diContainer);

    diContainer.resolve(ModulesInitializer).initializeConfiguredModules();
    return diContainer;
}

function registerAbortion(diContainer: DependencyContainer): void {
    const abortController = new AbortController();
    diContainer.registerInstance(AbortController, abortController);
    diContainer.registerInstance(AbortSignal, abortController.signal);
}

function registerVersionGraphQL(diContainer: DependencyContainer): void {
    diContainer.registerInstance(graphQLResolversDIToken, VersionGraphQLResolver);
    diContainer.register(backgroundWorkersDIToken, { useToken: VersionMetricsWorker });
}

function registerMetrics(diContainer: DependencyContainer): void {
    diContainer.register(backgroundWorkersDIToken, { useToken: MetricsWorker });
    diContainer.registerInstance(graphQLMiddlewaresDIToken, MetricsMiddleware);
    diContainer.registerInstance(Registry, new Registry());

    diContainer.register(metricsUpdatersDIToken, { useToken: AppUptimeMetricsUpdater });
    diContainer.register(metricsUpdatersDIToken, { useToken: HealthMetricsUpdater });
    diContainer.register(metricsUpdatersDIToken, { useToken: HttpServerMetricsUpdater });
}

function registerHttpServer(diContainer: DependencyContainer): void {
    registerSingleton(diContainer, expressAppDIToken, (c) => c.resolve(ExpressAppFactory).create());
    registerSingleton(diContainer, http.Server, (c) => http.createServer(c.resolve(expressAppDIToken)));

    diContainer.register(httpHandlersDIToken, { useToken: HealthStatusHttpHandler });
    diContainer.register(httpHandlersDIToken, { useToken: HealthCheckHttpHandler });
    diContainer.register(httpHandlersDIToken, { useToken: MetricsHttpHandler });

    diContainer.registerInstance(httpHandlersDIToken, {
        urlPath: appUrlPaths.root,
        handle: (_request, response) => response.redirect(appUrlPaths.graphQL),
    });
}

function registerModules(diContainer: DependencyContainer): void {
    diContainer.registerInstance(modulesDIToken, queriesGraphQLModule);
    diContainer.registerInstance(modulesDIToken, subscriptionsSubscriberModule);
    diContainer.registerInstance(modulesDIToken, tezosMonitorModule);
    diContainer.registerInstance(modulesDIToken, memoryPubSubModule);
    diContainer.registerInstance(modulesDIToken, redisPubSubModule);

    diContainer.registerInstance(
        compositeModulesDIToken,
        CompositeModule.create(CompositeModuleName.SubscriptionsGraphQL, [
            subscriptionsSubscriberModule,
            memoryPubSubModule,
            tezosMonitorModule,
        ]),
    );
}

function registerQueryLogger(diContainer: DependencyContainer): void {
    diContainer.registerInstance(graphQLMiddlewaresDIToken, QueryLogger);
}
