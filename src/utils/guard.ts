import { isNullish, Nullish } from './reflection';
import { isWhiteSpace } from './string-manipulation';

const seeStack = 'See the error stack for more details about the value.';

export const guard = {
    notNullish<T>(value: Nullish<T>): T {
        if (isNullish(value)) {
            throw new Error(`Value can't be nullish. ${seeStack}`);
        }
        return value;
    },

    notWhiteSpace(value: Nullish<string>): string {
        if (isWhiteSpace(value)) {
            throw new Error(`The string can't be nullish nor white-space. ${seeStack}`);
        }
        return value;
    },
};
