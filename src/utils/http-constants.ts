export enum HttpStatusCode {
    OK = 200,
    InternalServerError = 500,
    ServiceUnavailable = 503,
}

export enum HttpHeader {
    ContentType = 'Content-Type',
}

export enum ContentType {
    Json = 'application/json',
}
