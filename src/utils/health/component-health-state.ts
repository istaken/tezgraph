import { inject } from 'tsyringe';

import { Clock } from '../time/clock';
import { HealthCheck, HealthCheckResult, HealthStatus } from './health-check';

/** Statically stores a health of some component. The component is specified in the inherited class. */
export abstract class ComponentHealthState implements HealthCheck {
    private result: HealthCheckResult = {
        status: HealthStatus.Degraded,
        data: `No health state available because the component hasn't been executed yet.`,
    };

    abstract readonly name: string;

    constructor(@inject(Clock) private readonly clock: Clock) {}

    set(status: HealthStatus, data: unknown): void {
        const evaluatedOn = this.clock.getNowDate();
        this.result = { status, data, evaluatedOn };
    }

    checkHealth(): HealthCheckResult {
        return this.result;
    }
}
