import { injectAll, singleton } from 'tsyringe';

import { Version } from '../app-version/version';
import { VersionProvider } from '../app-version/version-provider';
import { Clock } from '../time/clock';
import { HealthCheck, HealthCheckResult, healthChecksDIToken, HealthStatus, isHealthy } from './health-check';

export interface HealthReport {
    readonly isHealthy: boolean;
    readonly version: Version;
    readonly evaluatedOn: Date;
    readonly results: Readonly<Record<string, HealthCheckResult>>;
}

@singleton()
export class HealthProvider {
    constructor(
        @injectAll(healthChecksDIToken) private readonly checks: readonly HealthCheck[],
        private readonly versionProvider: VersionProvider,
        private readonly clock: Clock,
    ) {}

    async generateHealthReport(): Promise<HealthReport> {
        const results = await Promise.all(
            this.checks.map(async (check) => {
                const result = await this.executeCheck(check);
                return { name: check.name, result };
            }),
        );

        const aggregatedIsHealthy = results.every((r) => isHealthy(r.result));
        return {
            isHealthy: aggregatedIsHealthy,
            version: this.versionProvider.version,
            evaluatedOn: this.clock.getNowDate(),
            results: Object.fromEntries(
                results.sort((r1, r2) => r1.name.localeCompare(r2.name)).map((r) => [r.name, r.result]),
            ),
        };
    }

    private async executeCheck(check: HealthCheck): Promise<HealthCheckResult> {
        try {
            return await check.checkHealth();
        } catch (error: unknown) {
            return {
                status: HealthStatus.Unhealthy,
                data: { error },
                evaluatedOn: this.clock.getNowDate(),
            };
        }
    }
}
