import { DependencyContainer, InjectionToken } from 'tsyringe';

import { EnvConfig } from '../configuration/env-config';
import { Clock } from '../time/clock';
import { HealthCheck, HealthCheckResult } from './health-check';

/** Decorator which caches health state for configured time. */
export class CachedHealthCheck implements HealthCheck {
    private cachedResult: Required<HealthCheckResult> | undefined;

    constructor(
        private readonly innerCheck: HealthCheck,
        private readonly envConfig: EnvConfig,
        private readonly clock: Clock,
    ) {}

    get name(): string {
        return this.innerCheck.name;
    }

    async checkHealth(): Promise<HealthCheckResult> {
        if (!this.cachedResult || this.isExpired(this.cachedResult.evaluatedOn)) {
            const result = await this.innerCheck.checkHealth();
            this.cachedResult = {
                ...result,
                evaluatedOn: this.clock.getNowDate(),
            };
        }
        return this.cachedResult;
    }

    private isExpired(evaluatedOn: Date): boolean {
        const expiresOn = evaluatedOn.getTime() + 1_000 * this.envConfig.healthStatusCacheTtlSeconds;
        return expiresOn < this.clock.getNowDate().getTime();
    }
}

export function createCached(
    container: DependencyContainer,
    healthCheckDIToken: InjectionToken<HealthCheck>,
): HealthCheck {
    return new CachedHealthCheck(
        container.resolve(healthCheckDIToken),
        container.resolve(EnvConfig),
        container.resolve(Clock),
    );
}
