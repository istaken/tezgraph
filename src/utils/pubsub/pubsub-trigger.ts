/** Strongly-typed PubSub trigger. */
export class PubSubTrigger<TPayload> {
    constructor(
        readonly name: string,
        readonly dummyPayload?: TPayload, // TS compiler distinguishes between triggers only if there are incompatible properties.
    ) {}
}
