import { AbortSignal } from 'abort-controller';
import { PubSubEngine } from 'graphql-subscriptions';

import { AbortError, addAbortListener } from '../abortion';
import { isNullish } from '../reflection';
import { PubSubTrigger } from './pubsub-trigger';

export class AbortablePubSubIterable<TPayload> implements AsyncIterableIterator<TPayload> {
    private readonly resolvePromiseQueue: ((value: IteratorResult<TPayload>) => void)[] = [];
    private readonly payloadQueue: TPayload[] = [];
    private subscribedStatePromise?: Promise<SubscribedState>;
    private running = true;

    constructor(
        private readonly pubSub: PubSubEngine,
        private readonly triggers: readonly PubSubTrigger<TPayload>[],
        private readonly abortSignal: AbortSignal,
    ) {}

    async next(): Promise<IteratorResult<TPayload>> {
        if (!this.subscribedStatePromise) {
            await (this.subscribedStatePromise = this.subscribeAll());
        }
        return this.dequeue();
    }

    async return(): Promise<IteratorResult<TPayload>> {
        await this.emptyQueue();
        return { value: undefined, done: true };
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async throw(error: unknown): Promise<any> {
        await this.emptyQueue();
        return Promise.reject(error);
    }

    [Symbol.asyncIterator](): AsyncIterableIterator<TPayload> {
        return this;
    }

    private async enqueue(payload: TPayload): Promise<void> {
        await this.subscribedStatePromise;

        const resolve = this.resolvePromiseQueue.shift();
        if (resolve) {
            resolve(this.running ? this.createYieldResult(payload) : this.createReturnResult());
        } else {
            this.payloadQueue.push(payload);
        }
    }

    private async dequeue(): Promise<IteratorResult<TPayload>> {
        return new Promise((resolve, reject) => {
            if (this.abortSignal.aborted) {
                reject(new AbortError());
                return;
            }
            const unlinkAbortion = addAbortListener(this.abortSignal, () => reject(new AbortError()));

            if (!this.running) {
                unlinkAbortion();
                resolve(this.createReturnResult());
                return;
            }

            const payload = this.payloadQueue.shift();
            if (isNullish(payload)) {
                this.resolvePromiseQueue.push((result) => {
                    unlinkAbortion();
                    resolve(result);
                });
            } else {
                unlinkAbortion();
                resolve(this.createYieldResult(payload));
            }
        });
    }

    private async emptyQueue(): Promise<void> {
        if (this.running) {
            this.running = false;
            this.resolvePromiseQueue.forEach((resolve) => resolve(this.createReturnResult()));
            this.resolvePromiseQueue.length = 0;
            this.payloadQueue.length = 0;
            const subscribedState = await this.subscribedStatePromise;
            if (subscribedState) {
                subscribedState.unlinkAbortion();
                this.unsubscribeAll(subscribedState.subscriptionIds);
            }
        }
    }

    private async subscribeAll(): Promise<SubscribedState> {
        const subscriptionIds = await Promise.all(
            this.triggers.map(async (t) => this.pubSub.subscribe(t.name, this.enqueue.bind(this), {})),
        );

        if (this.abortSignal.aborted) {
            this.unsubscribeAll(subscriptionIds);
            throw new AbortError();
        }
        const unlinkAbortion = addAbortListener(this.abortSignal, () => this.unsubscribeAll(subscriptionIds));
        return { subscriptionIds, unlinkAbortion };
    }

    private unsubscribeAll(subscriptionIds: readonly number[]): void {
        for (const subscriptionId of subscriptionIds) {
            this.pubSub.unsubscribe(subscriptionId);
        }
    }

    private createYieldResult(payload: TPayload): IteratorYieldResult<TPayload> {
        return { value: payload, done: false };
    }

    private createReturnResult(): IteratorReturnResult<undefined> {
        return { value: undefined, done: true };
    }
}

interface SubscribedState {
    readonly subscriptionIds: readonly number[];
    unlinkAbortion(): void;
}
