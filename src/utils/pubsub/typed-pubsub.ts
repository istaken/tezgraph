import { AbortSignal } from 'abort-controller';
import { PubSubEngine } from 'graphql-subscriptions';

import { Logger } from '../logging';
import { AbortablePubSubIterable } from './abortable-pubsub-iterable';
import { PubSubTrigger } from './pubsub-trigger';

/** Base class for strongly-typed PubSub. */
export abstract class TypedPubSub {
    constructor(
        private readonly pubSub: PubSubEngine,
        private readonly logger: Logger,
        private readonly globalAbortSignal: AbortSignal,
    ) {}

    async publish<TPayload>(trigger: PubSubTrigger<TPayload>, payload: TPayload): Promise<void> {
        this.logger.logDebug('Publishing a payload to {trigger}.', { trigger: trigger.name });
        await this.pubSub.publish(trigger.name, payload);
    }

    subscribe<TPayload>(trigger: PubSubTrigger<TPayload>, onMessage: (p: TPayload) => void): void {
        void this.pubSub.subscribe(trigger.name, onMessage, {});
    }

    iterate<TPayload>(
        triggers: readonly PubSubTrigger<TPayload>[],
        abortSignal?: AbortSignal,
    ): AsyncIterableIterator<TPayload> {
        return new AbortablePubSubIterable(this.pubSub, triggers, abortSignal ?? this.globalAbortSignal);
    }
}
