import { errorObjToString } from './conversion';

export function safeJsonStringify(value: unknown, options?: { indent?: string | number }): string {
    const alreadyStringified = new Set<object>();

    return JSON.stringify(
        value,
        (_propertyName, propertyValue: unknown): unknown => {
            switch (typeof propertyValue) {
                case 'undefined':
                    return null;

                case 'bigint':
                    return propertyValue.toString();

                case 'object':
                    if (propertyValue === null || propertyValue instanceof Date) {
                        return propertyValue;
                    } else if (propertyValue instanceof Error) {
                        return errorObjToString(propertyValue);
                    } else if (alreadyStringified.has(propertyValue)) {
                        return '(circular reference)';
                    }
                    alreadyStringified.add(propertyValue);
                    return propertyValue;

                default:
                    return propertyValue;
            }
        },
        options?.indent,
    );
}
