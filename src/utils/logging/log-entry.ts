export enum LogLevel {
    Critical = 'critical',
    Error = 'error',
    Warning = 'warning',
    Information = 'information',
    Debug = 'debug',
}

export type LogData = Readonly<Record<string, unknown>>;

export interface LogEntry {
    readonly timestamp: Date;
    readonly category: string;
    readonly level: LogLevel;
    readonly message: string;
    readonly data: LogData | undefined;
}
