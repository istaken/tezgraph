/* eslint-disable @typescript-eslint/require-await */
/* eslint-disable prefer-named-capture-group */
/* eslint-disable require-unicode-regexp */
import { ApolloServerPlugin, GraphQLRequestContext, GraphQLRequestListener } from 'apollo-server-plugin-base';
import { performance } from 'perf_hooks';

import { injectLogger, Logger } from '../logging';
import { safeJsonStringify } from '../safe-json-stringifier';

export class QueryLoggerPlugin implements ApolloServerPlugin, GraphQLRequestListener {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    [key: string]: import('apollo-server-types').AnyFunction | any;
    constructor(@injectLogger(QueryLoggerPlugin) private readonly logger: Logger) {}

    async requestDidStart(context: GraphQLRequestContext): Promise<GraphQLRequestListener> {
        if (context.request.operationName && context.request.operationName !== 'IntrospectionQuery') {
            const requestId = context.context.requestId as string;
            const query = context.request.query
                ? context.request.query.replace(/(\r\n|\n|\r)/gm, '').replace(/\s\s+/g, ' ')
                : undefined;
            const vars = context.request.variables;
            this.logger.logInformation(
                `${context.request.operationName} request received - {requestId}, {query}, {vars}`,
                {
                    requestId,
                    query,
                    vars,
                },
            );
        }
        return this;
    }

    async willSendResponse(context: GraphQLRequestContext): Promise<void> {
        if (context.request.operationName && context.request.operationName !== 'IntrospectionQuery') {
            const duration = performance.now() - context.context.startTime;
            const size = safeJsonStringify(context.response).length;
            const requestId = context.context.requestId as string;
            this.logger.logInformation(
                `${context.request.operationName} request resolved - {requestId}, {duration}, {size}`,
                {
                    duration,
                    size,
                    requestId,
                },
            );
        }
    }
}
