import { Path } from 'graphql/jsutils/Path';
import { performance } from 'perf_hooks';
import { singleton } from 'tsyringe';
import { MiddlewareInterface, ResolverData } from 'type-graphql';

import { ResolverContext } from '../../bootstrap/resolver-context';
import { injectLogger, Logger } from '../logging';
import { ResolveInfoParser } from '../query/resolve-info-parser';

@singleton()
export class QueryLogger implements MiddlewareInterface<ResolverContext> {
    constructor(
        private readonly resolveInfoParser: ResolveInfoParser,
        @injectLogger(QueryLogger) private readonly logger: Logger,
    ) {}

    async use(resolverData: ResolverData<ResolverContext>, next: () => Promise<unknown>): Promise<unknown> {
        try {
            const startTime = this.logQueryStart(resolverData);
            const res = await next();
            this.logQueryEnd(resolverData, startTime, res);
            return res;
        } catch (error: unknown) {
            this.logError(resolverData.info.fieldName, resolverData.context.requestId, error);
            throw error;
        }
    }

    getQueryPrevPath(path: Path): string {
        return path.typename && path.typename !== '' ? `${path.typename}.` : '';
    }

    formatQueryLoggerNotation(opType: string): string {
        return opType.replace(/Connection/u, '');
    }

    logStartDebug(opType: string, requestId: string): void {
        const formattedOpType = this.formatQueryLoggerNotation(opType);

        if (!this.resolveInfoParser.isIgnoredOpType(formattedOpType)) {
            this.logger.logDebug(`${formattedOpType} request received - {requestId}`, { requestId });
        }
    }

    logEndDebug(opType: string, requestId: string, duration?: number, res?: unknown): void {
        const formattedOpType = this.formatQueryLoggerNotation(opType);

        if (opType && !this.resolveInfoParser.isIgnoredOpType(formattedOpType)) {
            this.logger.logDebug(`${formattedOpType} request resolved - {requestId}, {duration}, {res}`, {
                requestId,
                duration,
                res,
            });
        }
    }

    logError(operation: string, requestId: string, error: unknown): void {
        this.logger.logError(`${operation} Query failed - with {error} for {requestId}`, { error, requestId });
    }

    logQueryStart(resolverContext: ResolverData<ResolverContext>): number {
        const requestId = resolverContext.context.requestId;
        const path = resolverContext.info.path;
        const prevPath = this.getQueryPrevPath(path);
        this.logStartDebug(`${prevPath}${path.key}`, requestId);
        return performance.now();
    }

    logQueryEnd(resolverContext: ResolverData<ResolverContext>, startTime: number, res: unknown): void {
        const requestId = resolverContext.context.requestId;
        const path = resolverContext.info.path;
        const prevPath = this.getQueryPrevPath(path);
        this.logEndDebug(`${prevPath}${path.key}`, requestId, performance.now() - startTime, res);
    }
}
