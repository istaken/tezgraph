import { AbortSignal } from 'abort-controller';

const abortErrorName = 'AbortError' as const;

export function isAbortError(error: unknown): boolean {
    return error instanceof Error && error.name === abortErrorName;
}

const abortEvent = 'abort';

export function addAbortListener(signal: AbortSignal, listener: () => void): () => void {
    signal.addEventListener(abortEvent, listener);
    return (): void => signal.removeEventListener(abortEvent, listener);
}

export function throwIfAborted(signal: AbortSignal): void {
    if (signal.aborted) {
        throw new AbortError();
    }
}

export class AbortError extends Error {
    readonly name = abortErrorName;

    constructor() {
        super('The operation has been aborted explicitly.');
    }
}
