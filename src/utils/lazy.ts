/** Helper for lazy initialization of a singleton value. */
export class Lazy<T> {
    private innerValue?: T;
    private factory?: () => T;

    constructor(factory: () => T) {
        this.factory = factory;
    }

    get value(): T {
        if (this.factory) {
            this.innerValue = this.factory();
            this.factory = undefined; // Allows garbage collection.
        }
        return this.innerValue!; // eslint-disable-line @typescript-eslint/no-non-null-assertion
    }
}
