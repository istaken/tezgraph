import { MichelCodecPacker, TezosToolkit } from '@taquito/taquito';
import { Tzip12Module } from '@taquito/tzip12';
import {
    Handler,
    HttpHandler,
    IpfsHttpHandler,
    MetadataProvider,
    TezosStorageHandler,
    Tzip16Module,
} from '@taquito/tzip16';
import { InjectionToken, singleton } from 'tsyringe';

import { EnvConfig } from '../configuration/env-config';

export const tezosToolkitDIToken: InjectionToken<TezosToolkit> = 'tezosToolkit';

@singleton()
export class TezosToolkitFactory {
    constructor(private readonly envConfig: EnvConfig) {}

    create(): TezosToolkit {
        const ipfsGateway = this.envConfig.ipfsGateway.replace(/\/$/u, '');
        const customHandler = new Map<string, Handler>([
            ['ipfs', new IpfsHttpHandler(ipfsGateway)],
            ['http', new HttpHandler()],
            ['https', new HttpHandler()],
            ['tezos-storage', new TezosStorageHandler()],
        ]);
        const customMetadataProvider = new MetadataProvider(customHandler);
        const tezosNode = this.envConfig.tezosNodeUrl;
        const tezosToolkit = new TezosToolkit(tezosNode);
        tezosToolkit.addExtension(new Tzip16Module(customMetadataProvider));
        tezosToolkit.addExtension(new Tzip12Module(customMetadataProvider));
        tezosToolkit.setPackerProvider(new MichelCodecPacker());
        return tezosToolkit;
    }
}
