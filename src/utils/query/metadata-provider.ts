import { DEFAULT_HANDLERS, MetadataProvider } from '@taquito/tzip16';
import { InjectionToken } from 'tsyringe';

const metadataProvider = new MetadataProvider(DEFAULT_HANDLERS);

const metadataProviderDiToken: InjectionToken<MetadataProvider> = 'metadataProvider';

export { metadataProvider, metadataProviderDiToken };
