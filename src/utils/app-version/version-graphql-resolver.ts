import { singleton } from 'tsyringe';
import { Query, Resolver } from 'type-graphql';

import { Version } from './version';
import { VersionProvider } from './version-provider';

@Resolver()
@singleton()
export class VersionGraphQLResolver {
    constructor(private readonly versionProvider: VersionProvider) {}

    @Query(() => Version)
    version(): Version {
        return this.versionProvider.version;
    }
}
