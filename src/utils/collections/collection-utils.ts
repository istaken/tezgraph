export function* withIndex<T>(iterable: Iterable<T>): Iterable<[T, number]> {
    let index = 0;
    for (const item of iterable) {
        yield [item, index++];
    }
}
