export function asIterableIterator<T>(iterator: AsyncIterator<T>): AsyncIterableIterator<T> {
    if (isIterable(iterator)) {
        return iterator;
    }

    return {
        next: iterator.next.bind(iterator),
        return: iterator.return?.bind(iterator),
        throw: iterator.throw?.bind(iterator),

        [Symbol.asyncIterator](): AsyncIterableIterator<T> {
            return this;
        },
    };
}

function isIterable<T>(iterator: AsyncIterator<T>): iterator is AsyncIterableIterator<T> {
    return Symbol.asyncIterator in iterator;
}

export async function iterableToArray<T>(iterable: AsyncIterableIterator<T>): Promise<T[]> {
    const result = [];
    for await (const item of iterable) {
        result.push(item);
    }
    return result;
}

// eslint-disable-next-line @typescript-eslint/require-await
export async function* toAsyncIterable<T>(array: readonly T[]): AsyncIterableIterator<T> {
    yield* array;
}

export async function* filterIterable<T>(
    iterable: AsyncIterableIterator<T>,
    predicate: (value: T) => boolean,
): AsyncIterableIterator<T> {
    for await (const item of iterable) {
        if (predicate(item)) {
            yield item;
        }
    }
}

export async function* take<T>(iterable: AsyncIterableIterator<T>, maxCount: number): AsyncIterableIterator<T> {
    if (maxCount > 0) {
        let remainingCount = maxCount;
        for await (const item of iterable) {
            yield item;
            if (--remainingCount === 0) {
                break;
            }
        }
    }
}
