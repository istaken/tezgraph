import { singleton } from 'tsyringe';

/** Abstraction for easier testing. */
@singleton()
export class Clock {
    getNowDate(): Date {
        return new Date();
    }

    getNowTimestamp(): number {
        return Date.now();
    }
}
