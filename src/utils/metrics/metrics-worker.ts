import { Registry } from 'prom-client';
import { inject, singleton } from 'tsyringe';

import { BackgroundWorker } from '../../bootstrap/background-worker';
import { MetricsContainer, MetricsRecord } from './metrics-container';

/** Registers metrics in Prometheus registry on app startup. */
@singleton()
export class MetricsWorker implements BackgroundWorker {
    name = 'Metrics';

    constructor(
        @inject(MetricsContainer) private readonly metrics: MetricsRecord,
        private readonly prometheusRegistry: Registry,
    ) {}

    start(): void {
        for (const metric of Object.values(this.metrics)) {
            this.prometheusRegistry.registerMetric(metric);
        }
    }

    stop(): void {
        this.prometheusRegistry.clear();
    }
}
