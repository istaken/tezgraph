import { ResolveTree } from 'graphql-parse-resolve-info';
import { singleton } from 'tsyringe';

import { ResolveInfoParser } from '../query/resolve-info-parser';
import { MetricsContainer } from './metrics-container';

@singleton()
export class QueryMetricsUtils {
    constructor(private readonly metrics: MetricsContainer, private readonly resolveInfoParser: ResolveInfoParser) {}

    sendQueryTypeFieldCount(opType: string | undefined, field: string, caller: 'external' | 'internal'): void {
        if (opType && !this.resolveInfoParser.isIgnoredOpType(opType)) {
            this.metrics.queryTypeFieldTotal.inc({
                type: `${opType ? opType : ''}`,
                field: `${field.toLowerCase()}`,
                caller,
            });
        }
    }

    sendQuerySourceType(method: string, type: string, status: 'succeeded' | 'failed'): void {
        this.metrics.queryExternalCallSourceTypeTotal.inc({
            method,
            type,
            status,
        });
    }

    countQueryFields(parsedResolveInfoFragment: ResolveTree, caller: 'external' | 'internal'): void {
        const keyFields = Object.keys(parsedResolveInfoFragment.fieldsByTypeName);

        keyFields.forEach((field, index) => {
            if (index !== 0 && keyFields[index] !== undefined && keyFields[0] !== 'Operation') {
                this.sendQueryTypeFieldCount(keyFields[0], keyFields[index] as string, caller);
            }
            const nestedFields = parsedResolveInfoFragment.fieldsByTypeName[field];
            if (nestedFields === undefined) {
                return;
            }

            for (const [key, value] of Object.entries(nestedFields)) {
                this.sendQueryTypeFieldCount(keyFields[index], key, caller);
                if (Object.keys(value.fieldsByTypeName).length > 0) {
                    this.countQueryFields(value, caller);
                }
            }
        });
    }
}
