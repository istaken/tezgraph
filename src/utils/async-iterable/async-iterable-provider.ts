import { AbortSignal } from 'abort-controller';

export interface AsyncIterableProvider<TItem> {
    iterate(abortSignal: AbortSignal): AsyncIterableIterator<TItem>;
}
