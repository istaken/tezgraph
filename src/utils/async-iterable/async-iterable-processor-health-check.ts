import { EnvConfig } from '../configuration/env-config';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../health/health-check';
import { Logger } from '../logging';
import { isNotNullish } from '../reflection';
import { Clock } from '../time/clock';
import { AsyncIterableProcessor } from './async-iterable-processor';
import { AsyncIterableWorker, ProcessingStatus } from './async-iterable-worker';

export const messages = {
    notRunning: 'The worker is NOT running - it was not started or it was stopped. Trying to start the processing.',
    noSuccessForLongTime:
        'Last successfully processed item is older than max configured age.' +
        ' The processing is most likely stuck, trying to restart it.',
    errorRecently: 'Recently an error occurred.',
    everythingWorks: 'Everything works perfectly.',
} as const;

/** Reports health of processing of given IterableProcessor. */
export class AsyncIterableProcessorHealthCheck implements HealthCheck {
    constructor(
        readonly name: string,
        private readonly processor: Readonly<AsyncIterableProcessor>,
        private readonly worker: AsyncIterableWorker,
        private readonly utilsEnvConfig: EnvConfig,
        private readonly clock: Clock,
        private readonly logger: Logger,
    ) {}

    checkHealth(): HealthCheckResult {
        switch (this.worker.status) {
            case ProcessingStatus.Stopped:
                this.logger.logError(messages.notRunning);
                this.worker.start();
                return this.createNotRunningResult();
            case ProcessingStatus.Stopping:
                return this.createNotRunningResult();
            case ProcessingStatus.Running:
                break;
        }

        const successAgeMillis = this.clock.getNowDate().getTime() - this.processor.lastSuccessTime.getTime();
        if (successAgeMillis > Math.min(...Object.values(this.utilsEnvConfig.tezosProcessingMaxAgeMillis))) {
            this.logger.logError(messages.noSuccessForLongTime);
            void this.restartWorker();
            return this.createNoSuccessForLongTimeResult(successAgeMillis);
        }

        if (isNotNullish(this.processor.lastError)) {
            return this.createRecentErrorResult();
        }

        return this.createHealthyResult();
    }

    private createNotRunningResult(): HealthCheckResult {
        return {
            status: HealthStatus.Unhealthy,
            data: messages.notRunning,
        };
    }

    private createNoSuccessForLongTimeResult(successAgeMillis: number): HealthCheckResult {
        const maxAgeMillisConfig = this.utilsEnvConfig.tezosProcessingMaxAgeMillis;
        return {
            status: successAgeMillis > maxAgeMillisConfig.unhealthy ? HealthStatus.Unhealthy : HealthStatus.Degraded,
            data: {
                reason: messages.noSuccessForLongTime,
                lastItem: this.processor.lastItemInfo,
                lastError: this.processor.lastError,
                lastSuccessTime: this.processor.lastSuccessTime,
                successAgeMillis,
                maxAgeMillisConfig,
            },
        };
    }

    private createRecentErrorResult(): HealthCheckResult {
        return {
            status: HealthStatus.Degraded,
            data: {
                reason: messages.errorRecently,
                failedItem: this.processor.lastItemInfo,
                error: this.processor.lastError,
                lastSuccessTime: this.processor.lastSuccessTime,
            },
        };
    }

    private createHealthyResult(): HealthCheckResult {
        return {
            status: HealthStatus.Healthy,
            data: {
                reason: messages.everythingWorks,
                lastItem: this.processor.lastItemInfo,
                lastItemTime: this.processor.lastSuccessTime,
            },
        };
    }

    private async restartWorker(): Promise<void> {
        await this.worker.stop();
        this.worker.start();
    }
}
