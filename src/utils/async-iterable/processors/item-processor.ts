import { AsyncOrSync } from 'ts-essentials';

export interface ItemProcessor<TItem> {
    processItem(item: TItem): AsyncOrSync<void>;
}
