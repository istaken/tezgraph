declare module 'graphql-depth-limit' {
    import { ValidationContext } from 'graphql';

    export default function (depthLimit: number): (context: ValidationContext) => unknown;
}
