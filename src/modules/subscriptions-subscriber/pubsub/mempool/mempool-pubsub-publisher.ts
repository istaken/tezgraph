import { singleton } from 'tsyringe';

import { MempoolOperationGroup } from '../../../../entity/subscriptions';
import { ItemProcessor } from '../../../../utils';
import { SubscriberPubSub, subscriberTriggers } from '../subscriber-pub-sub';

/** Publishes GraphQL mempool operations groups to local subscriber PubSub. */
@singleton()
export class MempoolPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(private readonly subscriberPubSub: SubscriberPubSub) {}

    async processItem(operationGroup: MempoolOperationGroup): Promise<void> {
        /*
         * No need to extract nested entities e.g. BigMapDiff b/c they are present in operation results which mempool operations do not have yet.
         * The order of operations within a block is important e.g. a transaction cannot precede a reveal operation to an empty address.
         */
        for (const operation of operationGroup) {
            const trigger = subscriberTriggers.mempoolOperations[operation.kind];
            await this.subscriberPubSub.publish(trigger, operation);
        }
    }
}
