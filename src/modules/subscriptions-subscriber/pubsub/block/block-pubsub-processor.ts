import { injectAll, InjectionToken, singleton } from 'tsyringe';

import { BlockNotification } from '../../../../entity/subscriptions';
import { ItemProcessor } from '../../../../utils';
import { SubscriberPubSub } from '../subscriber-pub-sub';

export interface PubSubPublisher {
    publish(block: BlockNotification, subscriberPubSub: SubscriberPubSub): Promise<void>;
}

export const pubSubPublishersDIToken: InjectionToken<PubSubPublisher> = 'PubSubPublishers';

/** Executes all publishers for publishing GraphQL notifications from a block to local subscriber PubSub. */
@singleton()
export class BlockPubSubProcessor implements ItemProcessor<BlockNotification> {
    constructor(
        private readonly subscriberPubSub: SubscriberPubSub,
        @injectAll(pubSubPublishersDIToken) private readonly pubSubPublishers: readonly PubSubPublisher[],
    ) {}

    async processItem(block: BlockNotification): Promise<void> {
        await Promise.all(this.pubSubPublishers.map(async (p) => p.publish(block, this.subscriberPubSub)));
    }
}
