import { BlockNotification } from '../../../../entity/subscriptions';
import { PubSubTrigger } from '../../../../utils';
import { FromBlockExtractor } from '../../from-block-extractors/extractor';
import { SubscriberPubSub } from '../subscriber-pub-sub';
import { PubSubPublisher } from './block-pubsub-processor';

/** Publishes GraphQL entity extracted from a block e.g. big map diffs or block itself. */
export class ExtractedItemsPubSubPublisher<TItem> implements PubSubPublisher {
    constructor(
        private readonly extractor: FromBlockExtractor<TItem>,
        private readonly trigger: PubSubTrigger<TItem>,
    ) {}

    async publish(block: BlockNotification, subscriberPubSub: SubscriberPubSub): Promise<void> {
        for (const item of this.extractor.extract(block)) {
            // It's a memory PubSub so await-s are insignificant -> no parallelism is needed.
            await subscriberPubSub.publish(this.trigger, item);
        }
    }
}
