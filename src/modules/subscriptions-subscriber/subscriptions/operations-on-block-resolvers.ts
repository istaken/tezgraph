/* istanbul ignore file */
import {
    ActivateAccountFilter,
    ActivateAccountNotification,
    BallotFilter,
    BallotNotification,
    DelegationFilter,
    DelegationNotification,
    DoubleBakingEvidenceFilter,
    DoubleBakingEvidenceNotification,
    DoubleEndorsementEvidenceFilter,
    DoubleEndorsementEvidenceNotification,
    DoublePreendorsementEvidenceFilter,
    DoublePreendorsementEvidenceNotification,
    EndorsementFilter,
    EndorsementNotification,
    EndorsementWithSlotFilter,
    EndorsementWithSlotNotification,
    OperationKind,
    OriginationFilter,
    OriginationNotification,
    PreendorsementFilter,
    PreendorsementNotification,
    ProposalsFilter,
    ProposalsNotification,
    RegisterGlobalConstantFilter,
    RegisterGlobalConstantNotification,
    RevealFilter,
    RevealNotification,
    SeedNonceRevelationFilter,
    SeedNonceRevelationNotification,
    SetDepositsLimitFilter,
    SetDepositsLimitNotification,
    TransactionFilter,
    TransactionNotification,
} from '../../../entity/subscriptions';
import { DefaultConstructor } from '../../../utils';
import { createOperationsOnBlockResolverClass } from './operations-on-block-resolver-factory';

const create = createOperationsOnBlockResolverClass;

// Enforces all operations to be handled.
const resolversPerKind: Record<OperationKind, DefaultConstructor> = {
    activate_account: create(ActivateAccountNotification, ActivateAccountFilter),
    ballot: create(BallotNotification, BallotFilter),
    delegation: create(DelegationNotification, DelegationFilter),
    double_baking_evidence: create(DoubleBakingEvidenceNotification, DoubleBakingEvidenceFilter),
    double_endorsement_evidence: create(DoubleEndorsementEvidenceNotification, DoubleEndorsementEvidenceFilter),
    double_preendorsement_evidence: create(
        DoublePreendorsementEvidenceNotification,
        DoublePreendorsementEvidenceFilter,
    ),
    endorsement: create(EndorsementNotification, EndorsementFilter),
    endorsement_with_slot: create(EndorsementWithSlotNotification, EndorsementWithSlotFilter),
    origination: create(OriginationNotification, OriginationFilter),
    preendorsement: create(PreendorsementNotification, PreendorsementFilter),
    proposals: create(ProposalsNotification, ProposalsFilter),
    register_global_constant: create(RegisterGlobalConstantNotification, RegisterGlobalConstantFilter),
    reveal: create(RevealNotification, RevealFilter),
    seed_nonce_revelation: create(SeedNonceRevelationNotification, SeedNonceRevelationFilter),
    set_deposits_limit: create(SetDepositsLimitNotification, SetDepositsLimitFilter),
    transaction: create(TransactionNotification, TransactionFilter),
};

/** GraphQL resolvers for all types of operations on a block GraphQL object. */
export const operationsOnBlockResolvers: readonly DefaultConstructor[] = Object.values(resolversPerKind);
