import { singleton } from 'tsyringe';

import { injectLogger, Logger } from '../../../../utils/logging';
import { SubscriptionOptions } from '../subscription';
import { SubscriptionEventsListener } from './listening-subscription';

@singleton()
export class LoggingSubscriptionListener<TItem, TArgs> implements SubscriptionEventsListener<TItem, TArgs> {
    constructor(@injectLogger(LoggingSubscriptionListener) private readonly logger: Logger) {}

    onConnectionOpen(options: SubscriptionOptions<TItem, TArgs>): void {
        this.logger.logInformation('Opening {subscription} for {itemType} for {requestId} with {args}.', {
            subscription: options.subscriptionName,
            itemType: options.itemType.name,
            requestId: options.context.requestId,
            args: options.args,
            rawQuery: options.context.connection?.query,
            rawQueryVariables: options.context.connection?.variables,
        });
    }

    onConnectionClose(options: SubscriptionOptions<TItem, TArgs>): void {
        this.logger.logInformation('Closing {subscription} for {requestId}.', {
            subscription: options.subscriptionName,
            requestId: options.context.requestId,
        });
    }
}
