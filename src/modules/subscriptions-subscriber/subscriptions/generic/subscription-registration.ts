import { DependencyContainer } from 'tsyringe';

import { graphQLResolversDIToken } from '../../../../bootstrap/graphql-resolver-type';
import { FilteringArgs, FilterOptimizer, ReplayFromBlockArgs } from '../../../../entity/subscriptions';
import { getDIToken, registerSingleton } from '../../../../utils/dependency-injection';
import { LoggerFactory } from '../../../../utils/logging';
import { MetricsContainer } from '../../../../utils/metrics/metrics-container';
import { DefaultConstructor } from '../../../../utils/reflection';
import { Clock } from '../../../../utils/time/clock';
import { FromBlockExtractor } from '../../from-block-extractors/extractor';
import { ReplayBlockProvider } from '../../helpers/replay-block-provider';
import { Subscription } from '../subscription';
import { FilteringSubscription } from './filtering-subscription';
import { createGraphQLResolverClass } from './graphql-resolver-factory';
import { ListeningSubscription, SubscriptionEventsListener } from './listening-subscription';
import { LoggingSubscriptionListener } from './logging-subscription-listener';
import { ItemMetricsData, MetricsSubscriptionListener } from './metrics-subscription-listener';
import { ReplayFromBlockSubscription } from './replay-from-block-subscription';

export interface SubscriptionRegistrationOptions<TItem, TArgs> {
    subscriptionName: string;
    description: string;
    itemType: DefaultConstructor<TItem>;
    argsType: DefaultConstructor<TArgs>;
    resolveItemExtractor(): FromBlockExtractor<TItem>;
    resolveRootSubscription(): Subscription<TItem, TArgs>;
    getItemMetrics(i: TItem): ItemMetricsData;
}

export function registerSubscription<TItem, TArgs extends FilteringArgs<TItem> & ReplayFromBlockArgs>(
    diContainer: DependencyContainer,
    options: SubscriptionRegistrationOptions<TItem, TArgs>,
): void {
    const subscriptionDIToken = getDIToken<Subscription<TItem, TArgs>>(options.subscriptionName);
    diContainer.registerInstance(
        graphQLResolversDIToken,
        createGraphQLResolverClass({ subscriptionDIToken, ...options }),
    );

    registerSingleton(diContainer, subscriptionDIToken, () => {
        const loggingListener: SubscriptionEventsListener<TItem, TArgs> =
            diContainer.resolve(LoggingSubscriptionListener);
        const metricsListener = new MetricsSubscriptionListener<TItem, TArgs>(
            diContainer.resolve(MetricsContainer),
            diContainer.resolve(Clock),
            (i) => options.getItemMetrics(i),
        );
        return new ListeningSubscription(
            [loggingListener, metricsListener],
            new FilteringSubscription(
                diContainer.resolve(FilterOptimizer),
                new ReplayFromBlockSubscription(
                    diContainer.resolve(ReplayBlockProvider),
                    diContainer.resolve(LoggerFactory).getLogger(ReplayFromBlockSubscription),
                    options.resolveItemExtractor(),
                    options.resolveRootSubscription(),
                ),
            ),
        );
    });
}
