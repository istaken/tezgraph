import { PubSubTrigger } from '../../../../utils';
import { SubscriberPubSub } from '../../pubsub/subscriber-pub-sub';
import { Subscription, SubscriptionOptions } from '../subscription';

/** A subscription to data from pub sub associated to given triggers. */
export class PubSubSubscription<TItem> implements Subscription<TItem, unknown> {
    constructor(
        private readonly pubSub: SubscriberPubSub,
        private readonly triggers: readonly PubSubTrigger<TItem>[],
    ) {}

    subscribe(_options: SubscriptionOptions<TItem, unknown>): AsyncIterableIterator<TItem> {
        return this.pubSub.iterate(this.triggers);
    }
}
