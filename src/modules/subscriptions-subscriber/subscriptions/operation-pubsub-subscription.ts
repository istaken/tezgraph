import { singleton } from 'tsyringe';

import { getOperationKind, OperationArgs, OperationNotification } from '../../../entity/subscriptions';
import { SubscriberPubSub, subscriberTriggers } from '../pubsub/subscriber-pub-sub';
import { Subscription, SubscriptionOptions } from './subscription';

export type OperationSubscription = Subscription<OperationNotification, OperationArgs<OperationNotification>>;
export type OperationSubscriptionOptions = SubscriptionOptions<
    OperationNotification,
    OperationArgs<OperationNotification>
>;

/** A subscription to operations of particular kind from pub sub with/without mempool. */
@singleton()
export class PubSubOperationSubscription implements OperationSubscription {
    constructor(private readonly pubSub: SubscriberPubSub) {}

    subscribe(options: OperationSubscriptionOptions): AsyncIterableIterator<OperationNotification> {
        const kind = getOperationKind(options.itemType);
        const triggers = [
            subscriberTriggers.blockOperations[kind],
            ...(options.args.includeMempool ? [subscriberTriggers.mempoolOperations[kind]] : []),
        ];

        return this.pubSub.iterate(triggers);
    }
}
