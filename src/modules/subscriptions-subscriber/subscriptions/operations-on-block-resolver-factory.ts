/* istanbul ignore file */
import { camelCase } from 'lodash';
import { Arg, FieldResolver, Resolver, Root } from 'type-graphql';

import {
    BlockNotification,
    getOperationKind,
    OperationFilter,
    OperationNotification,
} from '../../../entity/subscriptions';
import { DefaultConstructor, Nullish } from '../../../utils/reflection';
import { removeRequiredSuffix } from '../../../utils/string-manipulation';

/** Creates a GraphQL resolver class for the given type of operations on a block GraphQL object. */
export function createOperationsOnBlockResolverClass<
    TOperation extends OperationNotification,
    TFilter extends OperationFilter<TOperation>,
>(operationType: DefaultConstructor<TOperation>, filterType: DefaultConstructor<TFilter>): DefaultConstructor {
    const operationName = removeRequiredSuffix(operationType.name, 'Notification');
    const operationKind = getOperationKind(operationType);
    const fieldName = `${camelCase(operationName)}${operationName.endsWith('s') ? '' : 's'}`;

    @Resolver(() => BlockNotification)
    class BlockOperationsResolverClass {
        @FieldResolver(() => [operationType])
        [fieldName](
            @Root() block: BlockNotification,
            @Arg('filter', () => filterType, { nullable: true }) filter: Nullish<TFilter>, // eslint-disable-line @typescript-eslint/indent
        ): OperationNotification[] {
            return block.operations.filter(
                (o) => o.kind === operationKind && (!filter || filter.passes(o as TOperation)),
            );
        }
    }
    return BlockOperationsResolverClass;
}
