import { UserInputError } from 'apollo-server-express';
import { singleton } from 'tsyringe';

import { BlockNotification, OperationArgs, OperationNotification } from '../../../entity/subscriptions';
import { BlockConverter } from '../../../rpc/converters/block-converter';
import { TezosRpcClient } from '../../../rpc/tezos-rpc-client';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { injectLogger, Logger } from '../../../utils/logging';
import { nameof } from '../../../utils/reflection';
import { Clock } from '../../../utils/time/clock';
import { HeadBlockProvider } from './head-block-provider';

@singleton()
export class ReplayBlockProvider {
    constructor(
        private readonly rpcClient: TezosRpcClient,
        private readonly blockConverter: BlockConverter,
        private readonly headBlockProvider: HeadBlockProvider,
        private readonly config: EnvConfig,
        private readonly clock: Clock,
        @injectLogger(ReplayBlockProvider) private readonly logger: Logger,
    ) {}

    async *iterateFrom(fromLevel: number, requestId: string): AsyncIterableIterator<BlockNotification> {
        let headLevel = await this.getHeadLevel();
        if (headLevel - fromLevel > this.config.replayFromBlockLevelMaxFromHead) {
            throw new UserInputError(
                `Argument ${nameof<OperationArgs<OperationNotification>>('replayFromBlockLevel')}` +
                    ` can go only ${this.config.replayFromBlockLevelMaxFromHead} levels to the past` +
                    ` but specified #${fromLevel} is further from head #${headLevel}.`,
            );
        }

        // Block order is important -> loop instead of concurrent promises.
        let level = fromLevel;
        while (level <= headLevel) {
            try {
                const receivedFromTezosOn = this.clock.getNowDate();
                const rpcBlock = await this.rpcClient.getBlock(level);
                const block = this.blockConverter.convert(rpcBlock, receivedFromTezosOn);
                yield block;
            } catch (error: unknown) {
                this.logger.logError(
                    'Failed retrieving historical block with {blockLevel} for {requestId}. Skipping it. {error}',
                    {
                        blockLevel: level,
                        error,
                        requestId,
                    },
                );
            }

            headLevel = await this.getHeadLevel(); // New block could have been baked in the meantime.
            level++;
        }
    }

    private async getHeadLevel(): Promise<number> {
        const header = await this.headBlockProvider.getHeader();
        return header.level;
    }
}
