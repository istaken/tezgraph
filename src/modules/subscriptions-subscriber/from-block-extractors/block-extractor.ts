import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions';
import { FromBlockExtractor } from './extractor';

/** Just passes blocks themselves. */
@singleton()
export class BlockExtractor implements FromBlockExtractor<BlockNotification> {
    *extract(block: BlockNotification): Iterable<BlockNotification> {
        yield block;
    }
}
