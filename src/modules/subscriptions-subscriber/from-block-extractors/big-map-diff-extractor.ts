import { singleton } from 'tsyringe';

import {
    BigMapDiff,
    BigMapDiffNotification,
    ContractResultParent,
    InternalOperationKind,
    InternalOperationResult,
    MoneyOperationNotification,
    OperationKind,
    OperationNotification,
} from '../../../entity/subscriptions';
import { Nullish } from '../../../utils/reflection';
import { FromOperationExtractor } from './extractor';

/** Extracts all big map diffs from an operation of a block and converts them to notifications. */
@singleton()
export class BigMapDiffExtractor extends FromOperationExtractor<BigMapDiffNotification> {
    *extractFromOperation(operation: OperationNotification): Iterable<BigMapDiffNotification> {
        switch (operation.kind) {
            case OperationKind.delegation:
            case OperationKind.reveal:
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            case OperationKind.origination:
            case OperationKind.transaction:
                yield* iterate(operation.metadata?.operation_result.big_map_diff, operation, operation);
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            default:
                break;
        }
    }
}

function* fromInternalResults(
    internalResults: Nullish<readonly InternalOperationResult[]>,
    operation: MoneyOperationNotification,
): Iterable<BigMapDiffNotification> {
    for (const internalResult of internalResults ?? []) {
        switch (internalResult.kind) {
            case InternalOperationKind.origination:
            case InternalOperationKind.transaction:
                yield* iterate(internalResult.result.big_map_diff, operation, internalResult);
                break;
            default:
                break;
        }
    }
}

function* iterate(
    big_map_diffs: Nullish<readonly BigMapDiff[]>,
    operation: MoneyOperationNotification,
    parent: ContractResultParent,
): Iterable<BigMapDiffNotification> {
    for (const big_map_diff of big_map_diffs ?? []) {
        yield { big_map_diff, operation, parent };
    }
}
