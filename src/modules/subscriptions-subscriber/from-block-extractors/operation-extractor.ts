import { BlockNotification, OperationKind, OperationNotification } from '../../../entity/subscriptions';
import { FromBlockExtractor } from './extractor';

/** Extracts operations of particular kind from a block. */
export class OperationExtractor implements FromBlockExtractor<OperationNotification> {
    constructor(private readonly kind: OperationKind) {}

    *extract(block: BlockNotification): Iterable<OperationNotification> {
        for (const operation of block.operations) {
            if (this.kind === operation.kind) {
                yield operation;
            }
        }
    }
}
