import { singleton } from 'tsyringe';

import {
    ContractResultParent,
    InternalOperationKind,
    InternalOperationResult,
    LazyStorageDiff,
    LazyStorageDiffNotification,
    MoneyOperationNotification,
    OperationKind,
    OperationNotification,
} from '../../../entity/subscriptions';
import { Nullish } from '../../../utils/reflection';
import { FromOperationExtractor } from './extractor';

@singleton()
export class LazyStorageDiffExtractor extends FromOperationExtractor<LazyStorageDiffNotification> {
    *extractFromOperation(operation: OperationNotification): Iterable<LazyStorageDiffNotification> {
        switch (operation.kind) {
            case OperationKind.delegation:
            case OperationKind.reveal:
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            case OperationKind.origination:
            case OperationKind.transaction:
                yield* iterate(operation.metadata?.operation_result.lazy_storage_diff, operation, operation);
                yield* fromInternalResults(operation.metadata?.internal_operation_results, operation);
                break;
            default:
                break;
        }
    }
}

function* fromInternalResults(
    internalResults: Nullish<readonly InternalOperationResult[]>,
    operation: MoneyOperationNotification,
): Iterable<LazyStorageDiffNotification> {
    for (const internalResult of internalResults ?? []) {
        switch (internalResult.kind) {
            case InternalOperationKind.origination:
            case InternalOperationKind.transaction:
                yield* iterate(internalResult.result.lazy_storage_diff, operation, internalResult);
                break;
            default:
                break;
        }
    }
}

function* iterate(
    lazy_storage_diffs: Nullish<readonly LazyStorageDiff[]>,
    operation: MoneyOperationNotification,
    parent: ContractResultParent,
): Iterable<LazyStorageDiffNotification> {
    for (const lazy_storage_diff of lazy_storage_diffs ?? []) {
        yield { lazy_storage_diff, operation, parent };
    }
}
