export function serialize(value: unknown): string {
    return JSON.stringify(value, serializePropertyToJson);
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function deserialize(jsonStr: string | Buffer): any {
    if (typeof jsonStr !== 'string') {
        throw new Error(`Buffer is not implemented for deserialize.`);
    }
    return JSON.parse(jsonStr, deserializePropertyFromJson);
}

const prefixes = {
    bigint: 'tezgraph:bigint:',
    date: 'tezgraph:date:',
};

function serializePropertyToJson(this: Record<string, unknown> | undefined, name: string, value: unknown): unknown {
    // Date value is already as a string so we try to get actual Date object from parent object.
    const realValue = this && name && name in this ? this[name] : value;

    switch (typeof realValue) {
        case 'bigint':
            return `${prefixes.bigint}${realValue}`;
        case 'object':
            return realValue instanceof Date ? `${prefixes.date}${realValue.toISOString()}` : realValue;
        default:
            return realValue;
    }
}

function deserializePropertyFromJson(_name: string, json: unknown): unknown {
    if (typeof json === 'string' && json) {
        if (json.startsWith(prefixes.bigint)) {
            return BigInt(json.substr(prefixes.bigint.length));
        }
        if (json.startsWith(prefixes.date)) {
            return new Date(json.substr(prefixes.date.length));
        }
    }
    return json;
}
