import { AbortSignal } from 'abort-controller';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';
import { DependencyContainer } from 'tsyringe';

import { externalPubSubDIToken } from '../../utils';
import { addAbortListener } from '../../utils/abortion';
import { LoggerFactory } from '../../utils/logging';
import { Module, ModuleName } from '../module';
import { onlyOnePubSubModuleEnabledDependency, RequireOneOfModulesDependency } from '../module-helpers';
import { RedisPubSubEnvConfig } from './redis-pubsub-env-config';
import { deserialize, serialize } from './redis-pubsub-serializer';

export const redisPubSubModule: Module = {
    name: ModuleName.RedisPubSub,

    dependencies: [
        new RequireOneOfModulesDependency([ModuleName.TezosMonitor, ModuleName.SubscriptionsSubscriber], {
            reason: 'there is some producer or subscriber of notifications',
        }),
        onlyOnePubSubModuleEnabledDependency,
    ],

    initialize(diContainer: DependencyContainer): void {
        diContainer.register(externalPubSubDIToken, {
            useFactory: (c) => {
                const envConfig = c.resolve(RedisPubSubEnvConfig);
                const logger = c.resolve(LoggerFactory).getLogger(Redis);
                const abortSignal = c.resolve(AbortSignal);

                function createRedis(description: string): Redis.Redis {
                    const redis = new Redis(envConfig.redisConnectionString);
                    addAbortListener(abortSignal, () => {
                        redis.disconnect();
                        logger.logInformation(`${description} connection was aborted.`);
                    });
                    return redis;
                }

                return new RedisPubSub({
                    // Needs differences instances to write and read.
                    publisher: createRedis('Publisher'),
                    subscriber: createRedis('Subscriber'),
                    serializer: serialize,
                    deserializer: deserialize,
                });
            },
        });
    },
};
