import { DependencyContainer } from 'tsyringe';

import { TezosRpcHealthWatcher } from '../rpc/tezos-rpc-health-watcher';
import { Names } from '../utils/configuration/env-config';
import { createCached } from '../utils/health/cached-health-check';
import { healthChecksDIToken } from '../utils/health/health-check';
import { joinQuoted } from '../utils/string-manipulation';
import { ModuleDependency, ModuleName } from './module';

export const modulesErrorPrefix = `Env variable '${Names.Modules}':`;

export class RequireOneOfModulesDependency implements ModuleDependency {
    private readonly reason: string;

    constructor(private readonly oneOfModuleNames: readonly ModuleName[], options: { reason: string }) {
        this.reason = options.reason;
    }

    validate(thisModuleName: ModuleName, enabledModuleNames: readonly ModuleName[]): void {
        if (!this.oneOfModuleNames.some((n) => enabledModuleNames.includes(n))) {
            throw new Error(
                `${modulesErrorPrefix} Module '${thisModuleName}' requires` +
                    ` one of modules ${joinQuoted(this.oneOfModuleNames)} to be enabled so that ${this.reason}` +
                    ` but enabled modules are ${joinQuoted(enabledModuleNames)}.`,
            );
        }
    }
}

const pubSubModuleNames = [ModuleName.MemoryPubSub, ModuleName.RedisPubSub] as const;

export class RequirePubSubDependency extends RequireOneOfModulesDependency {
    constructor(options: { reason: string }) {
        super(pubSubModuleNames, options);
    }
}

export const onlyOnePubSubModuleEnabledDependency: ModuleDependency = {
    validate(_thisModuleName: ModuleName, enabledModuleNames: readonly ModuleName[]): void {
        const enabledPubSubModules = pubSubModuleNames.filter((n) => enabledModuleNames.includes(n));
        if (enabledPubSubModules.length > 1) {
            throw new Error(
                `${modulesErrorPrefix} Only one PubSub module can be enabled at the time` +
                    ` but these are enabled: ${joinQuoted(enabledPubSubModules)}.`,
            );
        }
    },
};

export function registerTezosRpcHealthWatcher(container: DependencyContainer): void {
    const guardInjectionToken = 'TezosRpcHealthWatcherGuard';

    if (!container.isRegistered(guardInjectionToken)) {
        container.register(healthChecksDIToken, { useFactory: (c) => createCached(c, TezosRpcHealthWatcher) });
        container.registerInstance(guardInjectionToken, {});
    }
}
