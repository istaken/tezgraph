import { GraphQLError } from 'graphql';
import { injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { TransactionRecord } from '../../../entity/queries/transaction-record';
import { isNullish } from '../../../utils';

@injectable()
@Resolver(() => TransactionRecord)
export class TransactionRecordResolver {
    @FieldResolver(() => AccountRecord, {
        description: 'The Account that this transaction was sent to.',
    })
    destination(@Root() transaction: TransactionRecord): AccountRecord {
        if (isNullish(transaction.destination_address) || isNullish(transaction.destination_id)) {
            throw new GraphQLError(
                `The destination of a transaction is non-null, but for transaction ${transaction.hash} no destination was found.`,
            );
        }
        return { address: transaction.destination_address, address_id: transaction.destination_id };
    }
}
