/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Args, ArgsType, Field, FieldResolver, Info, Int, Query, Resolver, Root } from 'type-graphql';

import {
    Block,
    BlockRecordEdge,
    BlockFilter,
    BlockRecordConnection,
    BlockRecord,
} from '../../../entity/queries/block-record';
import { scalars } from '../../../entity/scalars';
import { isNotNullish } from '../../../utils';
import { AggregationInput, getAggregationInput } from '../helpers/aggregation';
import { checkPaginationArgs, PaginationArgs, wrapIntoConnection } from '../helpers/pagination';
import BlockRepository from '../repositories/block-repository';
import { BlockOrderByInput } from './types/block-order-by';

@ArgsType()
export class BlockArgs extends PaginationArgs {
    @Field({ nullable: true })
    filter?: BlockFilter;

    @Field({ nullable: true })
    order_by?: BlockOrderByInput;

    aggregationInput?: AggregationInput;
}

@injectable()
@Resolver(() => BlockRecord)
export class BlockResolver {
    constructor(private readonly blockRepository: BlockRepository) {}

    @Query(() => BlockRecordConnection)
    async blocks(
        @Args() args: BlockArgs,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BlockRecordConnection | undefined> {
        checkPaginationArgs(args);

        const { data, hasMorePages, aggregationResult } = await this.blockRepository.getBlocks({
            ...args,
            aggregationInput: getAggregationInput(info),
        });
        const blocks = data.map((block) => ({
            ...block,
            cursor: block.hash!,
        }));
        return wrapIntoConnection(
            BlockRecordConnection,
            BlockRecordEdge,
            blocks,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => scalars.BlockHash, { nullable: false, description: 'The hash of this block' })
    async hash(@Root() block: Block): Promise<string> {
        if (isNotNullish(block.hash)) {
            return block.hash;
        }
        const { hash } = await this.blockRepository.findByHashOrLevel(block);
        return hash;
    }

    @FieldResolver(() => Int)
    async level(@Root() block: Block): Promise<number> {
        if (isNotNullish(block.level)) {
            return block.level;
        }
        const { level } = await this.blockRepository.findByHashOrLevel(block);
        return level;
    }

    @FieldResolver(() => Date, {
        description: 'The timestamp for when the block pushed to the block chain.',
    })
    async timestamp(@Root() block: Block): Promise<Date> {
        if (isNotNullish(block.timestamp)) {
            return block.timestamp;
        }
        const { timestamp } = await this.blockRepository.findByHashOrLevel(block);
        return timestamp;
    }
}
