/* eslint-disable max-params */
/* eslint-disable @typescript-eslint/init-declarations */
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Resolver, Query, Args, ArgsType, Field, Info } from 'type-graphql';

import {
    AccountRecordConnection,
    AccountRecordEdge,
    AccountFilter,
    AccountRecord,
} from '../../../entity/queries/account-record';
import { AggregationInput, getAggregationInput } from '../helpers/aggregation';
import { checkPaginationArgs, PaginationArgs, wrapIntoConnection } from '../helpers/pagination';
import AddressRepository from '../repositories/address-repository';
import { AccountOrderByInput } from './types/account-order-by';

@ArgsType()
export class AccountArgs extends PaginationArgs {
    @Field({ nullable: true })
    filter?: AccountFilter;

    @Field({ nullable: true })
    order_by?: AccountOrderByInput;

    aggregationInput?: AggregationInput;
}

@injectable()
@Resolver(() => AccountRecord)
export class AccountResolver {
    constructor(private readonly addressRepository: AddressRepository) {}

    @Query(() => AccountRecordConnection)
    async accounts(
        @Args() args: AccountArgs,
        @Info() info: GraphQLResolveInfo,
    ): Promise<AccountRecordConnection | undefined> {
        checkPaginationArgs(args);

        const { data, hasMorePages, aggregationResult } = await this.addressRepository.getAddresses({
            ...args,
            aggregationInput: getAggregationInput(info),
        });
        const accounts = data.map((account) => ({
            ...account,
            cursor: account.address,
        }));
        return wrapIntoConnection(
            AccountRecordConnection,
            AccountRecordEdge,
            accounts,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }
}
