/* eslint-disable @typescript-eslint/init-declarations */
import { Prisma } from '@prisma/client';
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Args, FieldResolver, Info, Query, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { BigmapKeyRecordConnection, BigmapKeyRecordEdge } from '../../../entity/queries/bigmap-key-record';
import { Bigmap, BigmapRecord, BigmapRecordConnection, BigmapRecordEdge } from '../../../entity/queries/bigmap-record';
import { BlockRecord } from '../../../entity/queries/block-record';
import { OperationRecord } from '../../../entity/queries/operation-record';
import { Micheline, scalars } from '../../../entity/scalars';
import { isNotNullish, isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { getAggregationInput } from '../helpers/aggregation';
import { mapBigmapKeysToBigmapKeysData, mapBigmapToBigmapData } from '../helpers/bigmap-mappers';
import { OperationMapper } from '../helpers/operation-mapper';
import { wrapIntoConnection } from '../helpers/pagination';
import AddressRepository from '../repositories/address-repository';
import BigmapKeyRawRepository from '../repositories/bigmap-key-raw-repository';
import BigmapKeyRepository from '../repositories/bigmap-key-repository';
import BigmapRawRepository from '../repositories/bigmap-raw-repository';
import BigmapRepository from '../repositories/bigmap-repository';
import MichelsonParser from '../repositories/michelson-parser';
import OperationAlphaRawRepository from '../repositories/operation-alpha-raw-repository';
import OperationAlphaRepository from '../repositories/operation-alpha-repository';
import { BigmapKeySelectionInBigmap, BigmapSelection } from './types/bigmap-filters';

@injectable()
@Resolver(() => BigmapRecord)
export class BigmapResolver {
    // eslint-disable-next-line max-params
    constructor(
        private readonly addressRepository: AddressRepository,
        private readonly bigmapRepository: BigmapRepository,
        private readonly bigmapRawRepository: BigmapRawRepository,
        private readonly bigmapKeyRepository: BigmapKeyRepository,
        private readonly bigmapKeyRawRepository: BigmapKeyRawRepository,
        private readonly operationAlphaRepository: OperationAlphaRepository,
        private readonly operationAlphaRawRepository: OperationAlphaRawRepository,
        private readonly operationUtils: OperationMapper,
        private readonly michelsonParser: MichelsonParser,
        private readonly envConfig: EnvConfig,
    ) {}

    @Query(() => BigmapRecordConnection, { nullable: true })
    async bigmaps(
        @Args() bigmapArgs: BigmapSelection,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapRecordConnection | undefined> {
        const repository = this.envConfig.useRawRepo ? this.bigmapRawRepository : this.bigmapRepository;
        const { data, hasMorePages, aggregationResult } = await repository.findManyBigmaps({
            ...bigmapArgs,
            aggregationInput: getAggregationInput(info),
        });
        const bigmapObjects = data.map((bigmap) => mapBigmapToBigmapData(bigmap));
        return wrapIntoConnection(
            BigmapRecordConnection,
            BigmapRecordEdge,
            bigmapObjects,
            bigmapArgs,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => String, { description: 'A Micheline json string describing the type of keys for this Bigmap' })
    key_type_micheline_json(@Root() bigmap: Bigmap): string | null {
        return this.michelsonParser.convertMichelsonJsonToCanonicalForm(bigmap.key_type as Micheline);
    }

    @FieldResolver(() => String, { description: 'A Michelson string describing the type of keys for this Bigmap' })
    key_type_michelson(@Root() bigmap: Bigmap): string | null {
        return this.michelsonParser.convertMichelsonJsonToMichelsonString(bigmap.key_type as Micheline);
    }

    @FieldResolver(() => String, {
        description: 'A Micheline json string describing the type of values for this Bigmap',
    })
    value_type_micheline_json(@Root() bigmap: Bigmap): string | null {
        return this.michelsonParser.convertMichelsonJsonToCanonicalForm(bigmap.value_type as Micheline);
    }

    @FieldResolver(() => String, { description: 'A Michelson string describing the type of values for this Bigmap' })
    value_type_michelson(@Root() bigmap: Bigmap): string | null {
        return this.michelsonParser.convertMichelsonJsonToMichelsonString(bigmap.value_type as Micheline);
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        description: 'The object containing the contract defining this Bigmap',
    })
    async contract(@Root() bigmap: Bigmap): Promise<AccountRecord | null> {
        if (isNullish(bigmap.receiver_id)) {
            return null;
        }
        const { data: addresses } = await this.addressRepository.getAddresses({
            first: 1,
            filter: {
                address_ids: [bigmap.receiver_id],
            },
        });
        const address = addresses[0];
        if (isNullish(address)) {
            throw new Error(`Data inconsistency: the address with id ${bigmap.receiver_id} does not exist.`);
        }
        return address;
    }

    @FieldResolver(() => BlockRecord, { description: 'The block defining this bigmap' })
    block(@Root() bigmap: Bigmap): BlockRecord {
        const block: BlockRecord = { hash: bigmap.block_hash, level: bigmap.block_hash_id };
        return block;
    }

    @FieldResolver(() => scalars.BigNumber, { description: 'The id of this bigmap' })
    id(@Root() bigmap: Bigmap): bigint | null {
        return bigmap.id;
    }

    @FieldResolver(() => scalars.BigNumber, { description: 'The batch position of this bigmap' })
    batch_position(@Root() bigmap: Bigmap): bigint {
        return bigmap.i;
    }

    @FieldResolver(() => OperationRecord, { nullable: true, description: 'The operation that allocated this bigmap' })
    async operation(@Root() bigmap: Bigmap): Promise<OperationRecord | null> {
        if (isNullish(bigmap.operation_id)) {
            return null;
        }
        const repository = this.envConfig.useRawRepo ? this.operationAlphaRawRepository : this.operationAlphaRepository;
        const { data: operationAlphasData } = await repository.findManyOperations({
            filter: {
                autoid: bigmap.operation_id,
            },
            pagination: {
                first: 1,
            },
        });
        const operationAlpha = operationAlphasData[0];
        if (!operationAlpha) {
            return null;
        }
        const operation = this.operationUtils.mapOperationsResultToSpecificOperation(operationAlpha);
        return operation;
    }

    @FieldResolver(() => BigmapKeyRecordConnection, {
        description: 'The array containing the keys inside this bigmap',
    })
    async keys(
        @Root() bigmap: Bigmap,
        @Args() args: BigmapKeySelectionInBigmap,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapKeyRecordConnection> {
        const repository = this.envConfig.useRawRepo ? this.bigmapKeyRawRepository : this.bigmapKeyRepository;
        let binaryKeys: Prisma.JsonObject[] | undefined;
        if (isNotNullish(args.filter) && isNotNullish(args.filter.keys) && args.filter.keys.length > 0) {
            binaryKeys = await Promise.all(
                args.filter.keys.map(async (k) =>
                    this.michelsonParser.convertBigmapKeyMichelineToJsonWithBinaryFields(k, bigmap.id),
                ),
            );
        }

        const { data, hasMorePages, aggregationResult } = await repository.findManyBigmapKeys(
            {
                bigmap_id: bigmap.id,
            },
            {
                ...args,
                filter: {
                    keys: binaryKeys,
                },
                aggregationInput: getAggregationInput(info),
            },
        );
        const bigmapKeyObjects = data.map((bigmapKey) => mapBigmapKeysToBigmapKeysData(bigmapKey));
        return wrapIntoConnection(
            BigmapKeyRecordConnection,
            BigmapKeyRecordEdge,
            bigmapKeyObjects,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }
}
