/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/init-declarations */

import { Prisma } from '@prisma/client';
import { MichelsonV1Expression } from '@taquito/rpc';
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Args, FieldResolver, Info, Query, Resolver, Root } from 'type-graphql';

import {
    BigmapKey,
    BigmapKeyRecord,
    BigmapKeyRecordConnection,
    BigmapKeyRecordEdge,
} from '../../../entity/queries/bigmap-key-record';
import {
    BigmapValueRecordConnection,
    BigmapValueRecordEdge,
    BigmapValueRecord,
} from '../../../entity/queries/bigmap-value-record';
import { Micheline, scalars } from '../../../entity/scalars';
import { isNotNullish, isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { safeJsonStringify } from '../../../utils/safe-json-stringifier';
import { BigmapDataSource } from '../datasources/bigmap-datasource';
import { getAggregationInput } from '../helpers/aggregation';
import { mapBigmapValuesToBigmapValuesData, mapBigmapKeysToBigmapKeysData } from '../helpers/bigmap-mappers';
import { wrapIntoConnection } from '../helpers/pagination';
import { OrderByDirection } from '../helpers/types';
import BigmapKeyRawRepository from '../repositories/bigmap-key-raw-repository';
import BigmapKeyRepository, { BigmapKeyParentInfo } from '../repositories/bigmap-key-repository';
import BigmapValueRepository from '../repositories/bigmap-value-repository';
import MichelsonParser from '../repositories/michelson-parser';
import { BigmapValueOrderByField, BigmapValueSelectionInKey, BigmapKeySelection } from './types/bigmap-filters';

@injectable()
@Resolver(() => BigmapKeyRecord)
export class BigmapKeyResolver {
    constructor(
        private readonly michelsonParser: MichelsonParser,
        private readonly bigmapValueRepository: BigmapValueRepository,
        private readonly bigmapKeyRepository: BigmapKeyRepository,
        private readonly bigmapKeyRawRepository: BigmapKeyRawRepository,
        private readonly envConfig: EnvConfig,
        private readonly bigmapDataSource: BigmapDataSource,
    ) {}

    @Query(() => BigmapKeyRecordConnection, { nullable: true })
    async bigmap_keys(
        @Args() args: BigmapKeySelection,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapKeyRecordConnection> {
        const parentInfo: BigmapKeyParentInfo = { bigmap_id: BigInt(args.filter.bigmap_id) };
        const repository = this.envConfig.useRawRepo ? this.bigmapKeyRawRepository : this.bigmapKeyRepository;
        let binaryKeys: Prisma.JsonObject[] | undefined;
        if (isNotNullish(args.filter.keys) && args.filter.keys.length > 0) {
            binaryKeys = await Promise.all(
                args.filter.keys.map(async (k) =>
                    this.michelsonParser.convertBigmapKeyMichelineToJsonWithBinaryFields(k, args.filter.bigmap_id),
                ),
            );
        }
        const { data, hasMorePages, aggregationResult } = await repository.findManyBigmapKeys(parentInfo, {
            ...args,
            filter: {
                ...args.filter,
                keys: binaryKeys,
            },
            aggregationInput: getAggregationInput(info),
        });
        const bigmapKeyObjects = data.map((key) => mapBigmapKeysToBigmapKeysData(key));
        return wrapIntoConnection(
            BigmapKeyRecordConnection,
            BigmapKeyRecordEdge,
            bigmapKeyObjects,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => scalars.Micheline)
    async key(@Root() bigmapKey: BigmapKey): Promise<Micheline> {
        const bigmap = await this.bigmapDataSource.get(bigmapKey.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
            bigmapKey.key,
            bigmap.key_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => String)
    async key_micheline_json(@Root() bigmapKey: BigmapKey): Promise<string | null> {
        const bigmap = await this.bigmapDataSource.get(bigmapKey.id);
        const micheline = this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMicheline(
            bigmapKey.key,
            bigmap.key_type as MichelsonV1Expression,
        );
        return safeJsonStringify(micheline);
    }

    @FieldResolver(() => String)
    async key_michelson(@Root() bigmapKey: BigmapKey): Promise<string | null> {
        const bigmap = await this.bigmapDataSource.get(bigmapKey.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMichelson(
            bigmapKey.key,
            bigmap.key_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => BigmapValueRecordConnection, {
        description: 'The array containing the history of values for this bigmap key',
    })
    async values_history(
        @Root() bigmapKey: BigmapKey,
        @Args() args: BigmapValueSelectionInKey,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapValueRecordConnection> {
        const { bigmapValuesData, hasMorePages, aggregationResult } =
            await this.bigmapValueRepository.findManyBigmapValues(
                { key: bigmapKey.key as Prisma.JsonObject, bigmapId: bigmapKey.id },
                {
                    ...args,
                    aggregationInput: getAggregationInput(info),
                },
            );
        const bigmapValueObjects = bigmapValuesData.map((value) => mapBigmapValuesToBigmapValuesData(value));
        return wrapIntoConnection(
            BigmapValueRecordConnection,
            BigmapValueRecordEdge,
            bigmapValueObjects,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => BigmapValueRecord, { description: 'The object containing the current value for this key' })
    async current_value(@Root() bigmapKey: BigmapKey): Promise<BigmapValueRecord> {
        const { bigmapValuesData } = await this.bigmapValueRepository.findManyBigmapValues(
            { key: bigmapKey.key as Prisma.JsonObject, bigmapId: bigmapKey.id },
            { first: 1, order_by: { field: BigmapValueOrderByField.block_level, direction: OrderByDirection.desc } },
        );
        const data = bigmapValuesData[0];
        if (isNullish(data)) {
            throw new Error(
                // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                `The current value for a BigmapKey is expected to exist, but nothing was found for id: ${
                    bigmapKey.id
                } and key: ${safeJsonStringify(bigmapKey.key)}`,
            );
        }
        return {
            i: data.i,
            id: data.id!,
            key: data.key!,
            key_hash: bigmapKey.key_hash,
            kind: data.kind,
            operation_id: data.operation_id!,
            address_addressTobigmap_receiver_id: data.address_addressTobigmap_receiver_id,
            address_addressTobigmap_sender_id: data.address_addressTobigmap_sender_id,
            block_hash: data.block?.hash,
            value: data.value as Micheline | undefined | null,
            block_level: data.block_level,
        };
    }
}
