import { MichelineTzip16Expression } from '@taquito/tzip16';
import { injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { MichelsonStorageViewType } from '../../../entity/queries/contract-metadata';
import { isNullish } from '../../../utils';
import MichelsonParser from '../repositories/michelson-parser';

@injectable()
@Resolver(() => MichelsonStorageViewType)
export class ContractMetadataResolver {
    constructor(private readonly michelsonParser: MichelsonParser) {}

    @FieldResolver(() => String, { nullable: true })
    michelson_code(@Root() parameters: MichelsonStorageViewType): string | null {
        return this.michelsonParser.convertMichelsonJsonToMichelsonString(parameters.code);
    }

    @FieldResolver(() => String, { nullable: true })
    canonical_code(@Root() parameters: MichelsonStorageViewType): MichelineTzip16Expression | null {
        if (isNullish(parameters.code)) {
            return null;
        }
        return parameters.code;
    }
}
