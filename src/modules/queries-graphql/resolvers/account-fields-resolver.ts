/* eslint-disable max-params */
/* eslint-disable @typescript-eslint/init-declarations */
import { injectable } from 'tsyringe';
import { Resolver, Args, FieldResolver, Root, Ctx, ArgsType, Field, Int } from 'type-graphql';

import { ResolverContext } from '../../../bootstrap/resolver-context';
import { Account, AccountRecord } from '../../../entity/queries/account-record';
import { ContractMetadata } from '../../../entity/queries/contract-metadata';
import { TokenMetadata } from '../../../entity/queries/token-metadata';
import { ScriptedContracts } from '../../../entity/subscriptions';
import AddressRepository from '../repositories/address-repository';
import ContractMetadataRepository from '../repositories/contract-metadata-repository';
import ContractScriptRepository from '../repositories/contract-script-repository';
import RevealRepository from '../repositories/reveal-repository';
import TokenMetadataRepository from '../repositories/token-metadata-repository';

@ArgsType()
export class ContractMetadataArgs {
    @Field(() => [String], { nullable: true })
    view_names?: string[];
}

@ArgsType()
export class TokenMetadataArgs {
    @Field(() => Int, { nullable: true })
    token_id?: number;
}

@injectable()
@Resolver(() => AccountRecord)
export class AccountFieldsResolver {
    constructor(
        private readonly revealRepository: RevealRepository,
        private readonly addressRepository: AddressRepository,
        private readonly contractMetadataRepository: ContractMetadataRepository,
        private readonly tokenMetadataRepository: TokenMetadataRepository,
        private readonly contractScriptRepository: ContractScriptRepository,
    ) {}

    @FieldResolver(() => String, { nullable: true })
    async public_key(@Root() { address_id }: Account): Promise<string | undefined> {
        const reveal = await this.revealRepository.findLatestByAddressId(address_id);
        return reveal?.pk;
    }

    @FieldResolver(() => Date, {
        description:
            'The timestamp of the activate_account operation applied to the specified account address. The activate_account operation is an anonymous operation that bootstraps the account with tez purchased during Tezos ICO.',
        nullable: true,
    })
    async activated(@Root() { address }: Account, @Ctx() { requestId }: ResolverContext): Promise<Date | undefined> {
        return this.addressRepository.getActivated(address, requestId);
    }

    @FieldResolver(() => ContractMetadata, { nullable: true })
    async contract_metadata(
        @Root() address: Account,
        @Args() args: ContractMetadataArgs,
    ): Promise<ContractMetadata | null> {
        const contractMetadata = await this.contractMetadataRepository.findByAccount(address, args);
        return contractMetadata;
    }

    @FieldResolver(() => TokenMetadata, { nullable: true })
    async token_metadata(@Root() address: Account, @Args() args: TokenMetadataArgs): Promise<TokenMetadata | null> {
        return await this.tokenMetadataRepository.findTokenMetadataWithTokenId(address, args);
    }

    @FieldResolver(() => ScriptedContracts, { nullable: true })
    async script(@Root() address: Account): Promise<ScriptedContracts | null> {
        const contractScript = await this.contractScriptRepository.getContractSource(address.address_id);
        return contractScript;
    }
}
