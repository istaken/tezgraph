import { InputType, registerEnumType } from 'type-graphql';

import { createOrderByInput, SortFieldInfo } from '../../helpers/utils';

export enum BlockOrderByField {
    level = 'level',
}

registerEnumType(BlockOrderByField, {
    name: 'BlockOrderByField',
    description: 'Block fields that are used for sorting block records.',
});

@InputType()
export class BlockOrderByInput extends createOrderByInput(BlockOrderByField) {}

export const blockOrderByInfo: Record<string, SortFieldInfo<BlockOrderByField>> = {
    [BlockOrderByField.level]: {
        isUnique: true,
        prismaModelPath: 'level',
        rawModel: { tableAlias: 'b', columnName: 'level' },
    },
};
