import { InputType, registerEnumType } from 'type-graphql';

import { createOrderByInput, SortFieldInfo, UniquenessFieldDirection } from '../../helpers/utils';

export enum OperationsOrderByField {
    source = 'source',
    hash = 'hash',
    chronological_order = 'chronological_order',
}
registerEnumType(OperationsOrderByField, {
    name: 'OperationsOrderByField',
});

export enum OperationsOrderByFieldInternal {
    source = 'source',
    hash = 'hash',
    batch_position = 'batch_position',
    internal = 'internal',
    chronological_order = 'chronological_order',
}

export const operationAlphaOrderByInfo: Record<string, SortFieldInfo<OperationsOrderByFieldInternal>> = {
    [OperationsOrderByFieldInternal.chronological_order]: {
        isUnique: true,
        prismaModelPath: 'autoid',
        rawModel: { equivalentGroupName: 'OperationAlpha_AutoID' },
    },
    [OperationsOrderByFieldInternal.batch_position]: {
        isUnique: false,
        prismaModelPath: 'id',
        rawModel: { tableAlias: 'operation_alpha', columnName: 'id' },
        makeUniqueBy: [
            {
                field: OperationsOrderByFieldInternal.chronological_order,
                direction: UniquenessFieldDirection.sameAsMainSortOrder,
            },
        ],
    },
    [OperationsOrderByFieldInternal.internal]: {
        isUnique: true,
        prismaModelPath: 'internal',
        rawModel: { tableAlias: 'operation_alpha', columnName: 'internal' },
    },
    [OperationsOrderByFieldInternal.source]: {
        isUnique: false,
        prismaModelPath: 'sender_address',
        rawModel: {
            tableAlias: 'sender',
            columnName: 'address',
            dependsOnTableAliases: ['operation_sender_and_receiver'],
        },
        makeUniqueBy: [
            {
                field: OperationsOrderByFieldInternal.chronological_order,
                direction: UniquenessFieldDirection.sameAsMainSortOrder,
            },
        ],
    },
    [OperationsOrderByFieldInternal.hash]: {
        isUnique: false,
        prismaModelPath: 'operation_hash',
        rawModel: { tableAlias: 'operation', columnName: 'hash' },
        makeUniqueBy: [
            {
                field: OperationsOrderByFieldInternal.batch_position,
                direction: UniquenessFieldDirection.sameAsMainSortOrder,
            },
            {
                field: OperationsOrderByFieldInternal.internal,
                direction: UniquenessFieldDirection.sameAsMainSortOrder,
            },
        ],
    },
};

@InputType()
export class OperationsOrderByInput extends createOrderByInput(OperationsOrderByField) {}
