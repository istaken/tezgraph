import { InputType, Field, Int, registerEnumType } from 'type-graphql';

import { scalars } from '../../../../entity/scalars';
import { DateRangeFilter } from '../../repositories/date-range.utils';
import { OperationRecordKind } from './operation-record-kind';
import { OperationRecordStatus } from './operations-status';
import { DecimalRangeFilter, IntRangeFilter, MutezRangeFilter } from './range-filters';

@InputType()
export class OperationsFilterBase {
    @Field(() => scalars.OperationHash, { nullable: true })
    hash?: string;

    @Field(() => Int, { nullable: true })
    batch_position?: number;

    @Field(() => Int, { nullable: true })
    internal?: number;

    @Field(() => [OperationRecordKind], { nullable: true })
    kind?: OperationRecordKind[];

    @Field(() => Int, { nullable: true })
    level?: number;

    @Field(() => scalars.BlockHash, { nullable: true })
    block_hash?: string;

    @Field(() => scalars.PublicKey, { nullable: true })
    public_key?: string;

    @Field(() => String, { nullable: true })
    entrypoint?: string;

    @Field(() => DateRangeFilter, { nullable: true })
    timestamp?: DateRangeFilter;

    @Field(() => DecimalRangeFilter, { nullable: true })
    consumed_milligas?: DecimalRangeFilter;

    @Field(() => MutezRangeFilter, { nullable: true })
    amount?: MutezRangeFilter;

    @Field(() => MutezRangeFilter, { nullable: true })
    fee?: MutezRangeFilter;

    @Field(() => OperationRecordStatus, { nullable: true })
    status?: OperationRecordStatus;

    @Field(() => IntRangeFilter, { nullable: true })
    level_range?: IntRangeFilter;

    autoid?: bigint;
}

@InputType()
export class OperationsFilter extends OperationsFilterBase {
    @Field(() => [scalars.Address], { nullable: true, description: 'The sources of the operations.' })
    sources?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The delegates of the endorsement operations.' })
    endorsement_delegates?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The delegates of the delegation operations.' })
    delegation_delegates?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The originated contracts of the operations.' })
    originated_contracts?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The destinations of the operations.' })
    destinations?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The receivers of the operations.' })
    receivers?: string[];

    @Field(() => [scalars.Address], { nullable: true, description: 'The senders of the operations.' })
    senders?: string[];
}

@InputType()
export class OperationsFilterInAccount extends OperationsFilterBase {
    @Field(() => AccountToOperationRelationshipType, { nullable: false })
    relationship_type!: AccountToOperationRelationshipType;
}

export enum AccountToOperationRelationshipType {
    source = 'source',
    destination = 'destination',
    sender = 'sender',
    receiver = 'receiver',
    originated_contract = 'originated_contract',
    endorsement_delegate = 'endorsement_delegate',
    delegation_delegate = 'delegation_delegate',
}
registerEnumType(AccountToOperationRelationshipType, {
    name: 'AccountToOperationRelationshipType',
});
