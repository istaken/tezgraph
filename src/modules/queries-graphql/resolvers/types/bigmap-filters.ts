/* eslint-disable @typescript-eslint/no-extraneous-class */
/* eslint-disable capitalized-comments */
import { ArgsType, Field, InputType, registerEnumType } from 'type-graphql';

import { Micheline, scalars } from '../../../../entity/scalars';
import { AggregationInput } from '../../helpers/aggregation';
import { PaginationArgs } from '../../helpers/pagination';
import { createOrderByInput, SortFieldInfo, UniquenessFieldDirection } from '../../helpers/utils';

export enum BigmapValueOrderByField {
    block_level = 'block_level',
}
registerEnumType(BigmapValueOrderByField, {
    name: 'BigmapValueOrderByField',
});

export enum BigmapValueOrderByFieldInternal {
    i = 'i',
    block_level = 'block_level',
}

export const bigmapValueOrderByInfo: Record<string, SortFieldInfo<BigmapValueOrderByFieldInternal>> = {
    [BigmapValueOrderByFieldInternal.i]: {
        isUnique: true,
        prismaModelPath: 'i',
        rawModel: { tableAlias: '', columnName: 'i' },
    },
    [BigmapValueOrderByFieldInternal.block_level]: {
        isUnique: false,
        prismaModelPath: 'block_level',
        rawModel: { tableAlias: '', columnName: 'block_level' },
        makeUniqueBy: [
            { field: BigmapValueOrderByFieldInternal.i, direction: UniquenessFieldDirection.sameAsMainSortOrder },
        ],
    },
};

@InputType()
export class BigmapValueOrderByInput extends createOrderByInput(BigmapValueOrderByField) {}

@InputType()
@ArgsType()
export class BigmapValueSelectionInKey extends PaginationArgs {
    @Field({ nullable: true })
    order_by?: BigmapValueOrderByInput;

    aggregationInput?: AggregationInput;
}

@InputType()
export class BigmapValueFilter {
    @Field(() => scalars.Micheline, { nullable: true, description: 'The keys of values to be included' })
    key?: Micheline;

    @Field(() => scalars.BigNumber, { nullable: false, description: 'The id of the bigmap.' })
    bigmap_id!: bigint;
}

@InputType()
@ArgsType()
export class BigmapValueSelection extends PaginationArgs {
    @Field({ nullable: true })
    filter?: BigmapValueFilter;

    @Field({ nullable: true })
    order_by?: BigmapValueOrderByInput;
}

@InputType()
@ArgsType()
export class BigmapValueSelectionInOperation extends PaginationArgs {
    @Field({ nullable: true })
    order_by?: BigmapValueOrderByInput;
}

@InputType()
export class BigmapKeyFilterInBigmap {
    @Field(() => [scalars.Micheline], {
        nullable: true,
        description: 'The keys of values to be included, in micheline',
    })
    keys?: Micheline[];
}

@InputType()
export class BigmapKeyFilter {
    @Field(() => [scalars.Micheline], {
        nullable: true,
        description: 'The keys of values to be included, in micheline',
    })
    keys?: Micheline[];

    @Field(() => scalars.BigNumber, { nullable: false, description: 'The id of the bigmap.' })
    bigmap_id!: bigint;
}

export enum BigmapKeyOrderByField {
    key = 'key',
}
registerEnumType(BigmapKeyOrderByField, {
    name: 'BigmapKeyOrderByField',
});

export enum BigmapKeyOrderByFieldInternal {
    key = 'key',
}

export const bigmapKeyOrderByInfo: Record<string, SortFieldInfo<BigmapKeyOrderByFieldInternal>> = {
    [BigmapKeyOrderByFieldInternal.key]: {
        isUnique: true,
        prismaModelPath: 'key',
        rawModel: { tableAlias: 'bigmap', columnName: 'key' },
    },
};

@InputType()
export class BigmapKeyOrderByInput extends createOrderByInput(BigmapKeyOrderByField) {}

@InputType()
@ArgsType()
export class BigmapKeySelectionInBigmap extends PaginationArgs {
    @Field({ nullable: true })
    filter?: BigmapKeyFilterInBigmap;

    @Field({ nullable: true })
    order_by?: BigmapKeyOrderByInput;

    aggregationInput?: AggregationInput;
}

@InputType()
@ArgsType()
export class BigmapKeySelection extends PaginationArgs {
    @Field({ nullable: true })
    filter!: BigmapKeyFilter;

    @Field({ nullable: true })
    order_by?: BigmapKeyOrderByInput;
}

@InputType()
export class BigmapFilter {
    @Field(() => [String], { nullable: true, description: 'The annotations of bigmaps to be fetched' })
    annots?: string[];

    @Field(() => [scalars.Address], {
        nullable: true,
        description: 'Filter conditions on contracts defining the bigmap',
    })
    contract?: string[];

    @Field(() => [scalars.BigNumber], {
        nullable: true,
        description: 'Id of bigmaps to include',
    })
    ids?: bigint[];
}

export enum BigmapOrderByField {
    id = 'id',
    annots = 'annots',
}
registerEnumType(BigmapOrderByField, {
    name: 'BigmapOrderByField',
});

export const bigmapOrderByInfo: Record<string, SortFieldInfo<BigmapOrderByField>> = {
    [BigmapOrderByField.id]: {
        isUnique: true,
        prismaModelPath: 'id',
        rawModel: { tableAlias: 'bigmap', columnName: 'id' },
    },
    [BigmapOrderByField.annots]: {
        isUnique: false,
        makeUniqueBy: [{ field: BigmapOrderByField.id, direction: UniquenessFieldDirection.sameAsMainSortOrder }],
        prismaModelPath: 'annots',
        rawModel: { tableAlias: 'bigmap', columnName: 'annots' },
    },
};

@InputType()
export class BigmapOrderByInput extends createOrderByInput(BigmapOrderByField) {}

@InputType()
@ArgsType()
export class BigmapSelection extends PaginationArgs {
    @Field({ nullable: true })
    filter?: BigmapFilter;

    @Field({ nullable: true })
    order_by?: BigmapOrderByInput;

    aggregationInput?: AggregationInput;
}

@InputType()
@ArgsType()
export class BigmapSelectionInAccount extends PaginationArgs {
    @Field({ nullable: true })
    order_by?: BigmapOrderByInput;
}
