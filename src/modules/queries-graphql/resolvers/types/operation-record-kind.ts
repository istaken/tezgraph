import { registerEnumType } from 'type-graphql';

export enum OperationRecordKind {
    endorsement = 0,
    seed_nonce_revelation = 1,
    double_endorsement_evidence = 2,
    double_baking_evidence = 3,
    activate_account = 4,
    proposals = 5,
    ballot = 6,
    reveal = 7,
    transaction = 8,
    origination = 9,
    delegation = 10,
    endorsement_with_slot = 11,
}

registerEnumType(OperationRecordKind, { name: 'OperationRecordKind' });
