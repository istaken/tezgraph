import { registerEnumType } from 'type-graphql';

export enum OperationRecordStatus {
    applied = 0,
    backtracked = 1,
    failed = 2,
    skipped = 3,
}

registerEnumType(OperationRecordStatus, { name: 'OperationRecordStatus' });
