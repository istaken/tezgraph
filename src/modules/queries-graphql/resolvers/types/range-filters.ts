import { Prisma } from '@prisma/client';
import { Field, InputType, Int } from 'type-graphql';

import { scalars } from '../../../../entity/scalars';

@InputType()
export class DecimalRangeFilter {
    @Field(() => scalars.Decimal, { nullable: true })
    lte?: Prisma.Decimal;

    @Field(() => scalars.Decimal, { nullable: true })
    gte?: Prisma.Decimal;
}

@InputType()
export class IntRangeFilter {
    @Field(() => Int, { nullable: true })
    lte?: number;

    @Field(() => Int, { nullable: true })
    gte?: number;
}

@InputType()
export class MutezRangeFilter {
    @Field(() => scalars.Mutez, { nullable: true })
    lte?: bigint;

    @Field(() => scalars.Mutez, { nullable: true })
    gte?: bigint;
}
