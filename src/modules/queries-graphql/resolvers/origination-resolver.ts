import { injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { OriginationRecord } from '../../../entity/queries/origination-record';
import { scalars } from '../../../entity/scalars';
import { isNullish } from '../../../utils';
import AddressRepository from '../repositories/address-repository';
import BalanceUpdateRepository from '../repositories/balance-update-repository';
import OperationAlphaRepository from '../repositories/operation-alpha-repository';

@injectable()
@Resolver(() => OriginationRecord)
export class OriginationRecordResolver {
    constructor(
        private readonly addressRepository: AddressRepository,
        private readonly balanceUpdateRepository: BalanceUpdateRepository,
        private readonly operationAlphaRepository: OperationAlphaRepository,
    ) {}

    @FieldResolver(() => AccountRecord, { nullable: true, description: 'The script of the origination contract.' })
    async contract(@Root() origination: OriginationRecord): Promise<AccountRecord | null> {
        if (isNullish(origination.contract_address)) {
            return null;
        }
        const { data } = await this.addressRepository.getAddresses({
            first: 1,
            filter: { addresses: [origination.contract_address] },
        });
        return data[0] ?? null;
    }

    @FieldResolver(() => scalars.Mutez, { nullable: true, description: 'The script of the origination contract.' })
    async burned(@Root() origination: OriginationRecord): Promise<bigint | null> {
        if (isNullish(origination.senderAddressID)) {
            return null;
        }

        if (!(await this.operationAlphaRepository.isOperationSentBy(origination.autoid, origination.senderAddressID))) {
            return null;
        }

        const sum = await this.balanceUpdateRepository.getSumByOperationId(origination.autoid);
        if (sum === null) {
            return null;
        }
        return -sum;
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        description: 'The delegate of the operation.',
    })
    delegate(@Root() origination: OriginationRecord): AccountRecord | null {
        if (isNullish(origination.delegate_address) || isNullish(origination.delegate_id)) {
            return null;
        }
        return { address: origination.delegate_address, address_id: origination.delegate_id };
    }
}
