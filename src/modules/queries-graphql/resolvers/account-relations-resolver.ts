/* eslint-disable max-params */
/* eslint-disable @typescript-eslint/init-declarations */
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Resolver, Args, FieldResolver, Root, ArgsType, Field, Info } from 'type-graphql';

import { Account, AccountRecord } from '../../../entity/queries/account-record';
import { BigmapRecordConnection, BigmapRecordEdge } from '../../../entity/queries/bigmap-record';
import { OperationRecordConnection, OperationRecordEdge } from '../../../entity/queries/operation-record';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { injectLogger, Logger } from '../../../utils/logging';
import { getAggregationInput } from '../helpers/aggregation';
import { mapBigmapToBigmapData } from '../helpers/bigmap-mappers';
import { OperationMapper } from '../helpers/operation-mapper';
import { checkPaginationArgs, PaginationArgs, wrapIntoConnection } from '../helpers/pagination';
import BigmapRawRepository from '../repositories/bigmap-raw-repository';
import BigmapRepository from '../repositories/bigmap-repository';
import OperationAlphaRawRepository from '../repositories/operation-alpha-raw-repository';
import OperationAlphaRepository from '../repositories/operation-alpha-repository';
import { BigmapSelection, BigmapSelectionInAccount } from './types/bigmap-filters';
import {
    OperationsFilterInAccount,
    OperationsFilter,
    AccountToOperationRelationshipType,
} from './types/operations-filter';
import { OperationsOrderByInput } from './types/operations-order-by';

@ArgsType()
export class OperationsArgsInAccount extends PaginationArgs {
    @Field({ nullable: false })
    filter!: OperationsFilterInAccount;

    @Field({ nullable: true })
    order_by?: OperationsOrderByInput;
}

@injectable()
@Resolver(() => AccountRecord)
export class AccountRelationsResolver {
    constructor(
        private readonly bigmapRepository: BigmapRepository,
        private readonly bigmapRawRepository: BigmapRawRepository,
        private readonly operationAlphaRepository: OperationAlphaRepository,
        @injectLogger(AccountRelationsResolver) private readonly logger: Logger,
        private readonly operationUtils: OperationMapper,
        private readonly operationAlphaRawRepository: OperationAlphaRawRepository,
        private readonly envConfig: EnvConfig,
    ) {}

    @FieldResolver(() => BigmapRecordConnection, { nullable: true })
    async bigmaps(
        @Root() { address }: Account,
        @Args() { first, last, before, after, order_by }: BigmapSelectionInAccount,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapRecordConnection> {
        checkPaginationArgs({ first, last, before, after });
        const args: BigmapSelection = {
            after,
            before,
            first,
            last,
            filter: {
                contract: [address],
            },
            order_by,
            aggregationInput: getAggregationInput(info),
        };
        const repository = this.envConfig.useRawRepo ? this.bigmapRawRepository : this.bigmapRepository;
        const { data, hasMorePages, aggregationResult } = await repository.findManyBigmaps(args);
        const bigmapObjects = data.map((bigmap) => mapBigmapToBigmapData(bigmap));
        return wrapIntoConnection(
            BigmapRecordConnection,
            BigmapRecordEdge,
            bigmapObjects,
            { after, before, first, last },
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => OperationRecordConnection, { nullable: true })
    async operations(
        @Root() { address }: Account,
        @Args() { filter, order_by: orderBy, ...pagination }: OperationsArgsInAccount,
        @Info() info: GraphQLResolveInfo,
    ): Promise<OperationRecordConnection | undefined> {
        checkPaginationArgs(pagination);

        const processedFilter: OperationsFilter = {
            originated_contracts:
                filter.relationship_type === AccountToOperationRelationshipType.originated_contract
                    ? [address]
                    : undefined,
            delegation_delegates:
                filter.relationship_type === AccountToOperationRelationshipType.delegation_delegate
                    ? [address]
                    : undefined,
            endorsement_delegates:
                filter.relationship_type === AccountToOperationRelationshipType.endorsement_delegate
                    ? [address]
                    : undefined,
            receivers: filter.relationship_type === AccountToOperationRelationshipType.receiver ? [address] : undefined,
            senders: filter.relationship_type === AccountToOperationRelationshipType.sender ? [address] : undefined,
            ...filter,
            destinations:
                filter.relationship_type === AccountToOperationRelationshipType.destination ? [address] : undefined,
            sources: filter.relationship_type === AccountToOperationRelationshipType.source ? [address] : undefined,
            ...filter,
        };

        const repository = this.envConfig.useRawRepo ? this.operationAlphaRawRepository : this.operationAlphaRepository;
        const {
            data: operationAlphasData,
            hasMorePages,
            aggregationResult,
        } = await repository.findManyOperations({
            filter: processedFilter,
            orderBy,
            pagination,
            aggregationInput: getAggregationInput(info),
        });
        if (operationAlphasData.length === 0) {
            this.logger.logInformation('No OperationAlpha rows found for {filter}.', { filter });
            return;
        }

        const operations = operationAlphasData.map((data) =>
            this.operationUtils.mapOperationsResultToSpecificOperation(data),
        );

        return wrapIntoConnection(
            OperationRecordConnection,
            OperationRecordEdge,
            operations,
            pagination,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }
}
