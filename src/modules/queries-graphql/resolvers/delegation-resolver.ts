import { inject, injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { DelegationRecord } from '../../../entity/queries/delegation-record';
import { scalars } from '../../../entity/scalars';
import { isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import BlockRepository from '../repositories/block-repository';
import ContractBalanceRepository from '../repositories/contract-balance-repository';
import TaquitoRepository from '../repositories/taquito-repository';

@injectable()
@Resolver(() => DelegationRecord)
export class DelegationRecordResolver {
    constructor(
        private readonly blockRepository: BlockRepository,
        private readonly envConfig: EnvConfig,
        private readonly contractBalanceRepository: ContractBalanceRepository,
        @inject(TaquitoRepository) private readonly taquitoRepository: TaquitoRepository,
    ) {}

    @FieldResolver(() => scalars.Mutez, { nullable: true, description: 'The amount delegated in this operation.' })
    async amount(@Root() delegation: DelegationRecord): Promise<bigint | null> {
        if (this.envConfig.resolveContractBalanceFromDB) {
            if (isNullish(delegation.senderAddressID)) {
                return null;
            }

            const balance = await this.contractBalanceRepository.findBalanceByAddressAndBlockHash(
                delegation.senderAddressID,
                delegation.block.level,
            );
            if (isNullish(balance)) {
                return null;
            }
            return balance.balance;
        }

        if (isNullish(delegation.senderAddress)) {
            return null;
        }

        /*
         * TODO: This can happen in a different tik from other calls to block's
         * find methods and can result in multiple calls to the database
         */
        const blockHash =
            delegation.block.hash ?? (await this.blockRepository.findByHashOrLevel(delegation.block)).hash;

        return await this.taquitoRepository.getDelegationAmountFromTaquito({
            block_hash: blockHash,
            source_address: delegation.senderAddress,
        });
    }

    @FieldResolver(() => AccountRecord, {
        nullable: true,
        description: 'An Implicit account to which an account has delegated their baking and endorsement rights.',
    })
    delegate(@Root() delegation: DelegationRecord): AccountRecord | null {
        if (isNullish(delegation.delegate_address) || isNullish(delegation.delegate_id)) {
            return null;
        }
        return { address: delegation.delegate_address, address_id: delegation.delegate_id };
    }
}
