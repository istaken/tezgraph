import { injectable } from 'tsyringe';
import { FieldResolver, Resolver, Root } from 'type-graphql';

import { TransactionParameters } from '../../../entity/common/transaction';
import MichelsonParser from '../repositories/michelson-parser';

@injectable()
@Resolver(() => TransactionParameters)
export class TransactionParametersResolver {
    constructor(private readonly michelsonParser: MichelsonParser) {}

    @FieldResolver(() => String, { nullable: true })
    canonical_value(@Root() parameters: TransactionParameters): string | null {
        return this.michelsonParser.convertMichelsonJsonToCanonicalForm(parameters.value);
    }

    @FieldResolver(() => String, { nullable: true })
    michelson_value(@Root() parameters: TransactionParameters): string | null {
        return this.michelsonParser.convertMichelsonJsonToMichelsonString(parameters.value);
    }
}
