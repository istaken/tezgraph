/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { MichelsonV1Expression } from '@taquito/rpc';
import { GraphQLResolveInfo } from 'graphql';
import { injectable } from 'tsyringe';
import { Args, FieldResolver, Info, Query, Resolver, Root } from 'type-graphql';

import { AccountRecord } from '../../../entity/queries/account-record';
import { BigmapRecord } from '../../../entity/queries/bigmap-record';
import {
    BigmapValue,
    BigmapValueRecordConnection,
    BigmapValueRecordEdge,
    BigmapValueRecord,
} from '../../../entity/queries/bigmap-value-record';
import { Block, BlockRecord } from '../../../entity/queries/block-record';
import { OperationRecord } from '../../../entity/queries/operation-record';
import { Micheline, scalars } from '../../../entity/scalars';
import { isNotNullish, isNullish } from '../../../utils';
import { EnvConfig } from '../../../utils/configuration/env-config';
import { safeJsonStringify } from '../../../utils/safe-json-stringifier';
import { BigmapDataSource } from '../datasources/bigmap-datasource';
import { getAggregationInput } from '../helpers/aggregation';
import { mapBigmapToBigmapData, mapBigmapValuesToBigmapValuesData } from '../helpers/bigmap-mappers';
import { OperationMapper } from '../helpers/operation-mapper';
import { wrapIntoConnection } from '../helpers/pagination';
import BigmapRepository from '../repositories/bigmap-repository';
import BigmapValueRepository, { BigmapValueFilter } from '../repositories/bigmap-value-repository';
import BlockRepository from '../repositories/block-repository';
import MichelsonParser from '../repositories/michelson-parser';
import OperationAlphaRawRepository from '../repositories/operation-alpha-raw-repository';
import OperationAlphaRepository from '../repositories/operation-alpha-repository';
import { BigmapValueSelection } from './types/bigmap-filters';

@injectable()
@Resolver(() => BigmapValueRecord)
export class BigmapValueResolver {
    // eslint-disable-next-line max-params
    constructor(
        private readonly bigmapValueRepository: BigmapValueRepository,
        private readonly bigmapRepository: BigmapRepository,
        private readonly michelsonParser: MichelsonParser,
        private readonly operationAlphaRepository: OperationAlphaRepository,
        private readonly blockRepository: BlockRepository,
        private readonly operationUtils: OperationMapper,
        private readonly operationAlphaRawRepository: OperationAlphaRawRepository,
        private readonly envConfig: EnvConfig,
        private readonly bigmapDataSource: BigmapDataSource,
    ) {}

    @Query(() => BigmapValueRecordConnection, { nullable: true })
    async bigmap_values(
        @Args() args: BigmapValueSelection,
        @Info() info: GraphQLResolveInfo,
    ): Promise<BigmapValueRecordConnection> {
        let key: any = null;
        if (isNotNullish(args.filter) && isNotNullish(args.filter.key)) {
            if (isNullish(args.filter.bigmap_id)) {
                throw new Error('Filtering BigmapValues by key can only be done if bigmap_id is also filtered');
            }
            key = await this.michelsonParser.convertBigmapKeyMichelineToJsonWithBinaryFields(
                args.filter.key,
                args.filter.bigmap_id,
            );
        }

        const filter: BigmapValueFilter = {
            bigmapId: args.filter?.bigmap_id,
            key,
        };
        const { bigmapValuesData, hasMorePages, aggregationResult } =
            await this.bigmapValueRepository.findManyBigmapValues(filter, {
                order_by: args.order_by,
                after: args.after,
                before: args.before,
                first: args.first,
                last: args.last,
                aggregationInput: getAggregationInput(info),
            });
        const bigmapValueObjects = bigmapValuesData.map((value) => mapBigmapValuesToBigmapValuesData(value));
        return wrapIntoConnection(
            BigmapValueRecordConnection,
            BigmapValueRecordEdge,
            bigmapValueObjects,
            args,
            hasMorePages,
            aggregationResult.totalCount,
        );
    }

    @FieldResolver(() => scalars.Micheline, { description: 'The Key of this bigmap value' })
    async key(@Root() bigmapValue: BigmapValue): Promise<Micheline | null> {
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
            bigmapValue.key as Micheline,
            bigmap.key_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => String, { description: 'The Key of this bigmap value, in Micheline json' })
    async key_micheline_json(@Root() bigmapValue: BigmapValue): Promise<string | null> {
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        const micheline = this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMicheline(
            bigmapValue.key as Micheline,
            bigmap.key_type as MichelsonV1Expression,
        );
        return safeJsonStringify(micheline);
    }

    @FieldResolver(() => String, { description: 'The Key of this bigmap value, in Michelson' })
    async key_michelson(@Root() bigmapValue: BigmapValue): Promise<string | null> {
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMichelson(
            bigmapValue.key as Micheline,
            bigmap.key_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => scalars.Micheline, {
        nullable: true,
        description: 'The value of this bigmap value, in json',
    })
    async value(@Root() bigmapValue: BigmapValue): Promise<Micheline | null> {
        if (isNullish(bigmapValue.value)) {
            return null;
        }
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToJSObjectWithDecodedFields(
            bigmapValue.value,
            bigmap.value_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => scalars.Micheline, {
        nullable: true,
        description: 'The value of this bigmap value, in json',
    })
    async value_micheline_json(@Root() bigmapValue: BigmapValue): Promise<string | null> {
        if (isNullish(bigmapValue.value)) {
            return null;
        }
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        const micheline = this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMicheline(
            bigmapValue.value,
            bigmap.value_type as MichelsonV1Expression,
        );
        return safeJsonStringify(micheline);
    }

    @FieldResolver(() => String, { nullable: true, description: 'The value of this bigmap value, in Michelson' })
    async value_michelson(@Root() bigmapValue: BigmapValue): Promise<string | null> {
        if (isNullish(bigmapValue.value)) {
            return null;
        }
        const bigmap = await this.bigmapDataSource.get(bigmapValue.id);
        return this.michelsonParser.convertBigmapKeyOrValueFromJSObjectWithBinaryFieldsToMichelson(
            bigmapValue.value,
            bigmap.value_type as MichelsonV1Expression,
        );
    }

    @FieldResolver(() => AccountRecord, { description: 'The object containing the contract for this value' })
    contract(@Root() value: BigmapValue): AccountRecord | null {
        return value.address_addressTobigmap_receiver_id ?? null;
    }

    @FieldResolver(() => AccountRecord, { description: 'The object containing the source account for this value' })
    source(@Root() value: BigmapValue): AccountRecord | null {
        return value.address_addressTobigmap_sender_id ?? null;
    }

    @FieldResolver(() => BlockRecord, { description: 'The object containing the block defining this value' })
    async block(@Root() value: BigmapValue): Promise<BlockRecord> {
        if (value.block_hash) {
            const block: Block = { hash: value.block_hash, level: value.block_level };
            return block;
        }
        const block = await this.blockRepository.findByLevel(value.block_level);
        if (!block) {
            throw new Error(`Inconsistent data: block with level ${value.block_level} does not exist`);
        }
        return block;
    }

    @FieldResolver(() => scalars.BigNumber, { description: 'The batch position of the this value' })
    batch_position(@Root() value: BigmapValue): bigint {
        return value.i;
    }

    @FieldResolver(() => BigmapValueRecord, {
        nullable: true,
        description: 'The value preceding the current one, that was updated by the current value',
    })
    async previous(@Root() value: BigmapValue): Promise<BigmapValueRecord | null> {
        const prev = await this.bigmapValueRepository.findPreviousValue(value);
        if (!prev) {
            return null;
        }
        return {
            key_hash: value.key_hash,
            ...prev,
            value: prev.value as Micheline | null,
        };
    }

    @FieldResolver(() => OperationRecord, { description: 'The Operation that set this BigmapValue' })
    async operation(@Root() value: BigmapValue): Promise<OperationRecord | null> {
        const repository = this.envConfig.useRawRepo ? this.operationAlphaRawRepository : this.operationAlphaRepository;
        const { data: operationAlphasData } = await repository.findManyOperations({
            filter: {
                autoid: value.operation_id,
            },
            pagination: {
                first: 1,
            },
        });
        const operationAlpha = operationAlphasData[0];
        if (!operationAlpha) {
            return null;
        }
        const operation = this.operationUtils.mapOperationsResultToSpecificOperation(operationAlpha);
        return operation;
    }

    @FieldResolver(() => BigmapRecord, { description: 'The Bigmap that owns this BigmapValue' })
    async bigmap(@Root() value: BigmapValue): Promise<BigmapRecord> {
        const {
            data: [bigmap],
        } = await this.bigmapRepository.findManyBigmaps({
            first: 1,
            filter: {
                ids: [value.id],
            },
        });
        if (isNullish(bigmap)) {
            throw new Error(`Inconsistent data: Bigmap with id: ${value.id} does not exist.`);
        }
        return mapBigmapToBigmapData(bigmap);
    }
}
