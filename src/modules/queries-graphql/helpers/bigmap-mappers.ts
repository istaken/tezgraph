import { BigmapKeyRecordData, BigmapKeyRecord } from '../../../entity/queries/bigmap-key-record';
import { BigmapRecord, BigmapRecordData } from '../../../entity/queries/bigmap-record';
import { BigmapValueRecordData, BigmapValueRecord } from '../../../entity/queries/bigmap-value-record';
import { isNullish } from '../../../utils';
import { FindManyBigmapKeyResult } from '../repositories/bigmap-key-repository';
import { FindManyBigmapsResult } from '../repositories/bigmap-repository';
import { FindManyBigmapValueResult } from '../repositories/bigmap-value-repository';
import { packBigmapCursor, packBigmapValueCursor, packBigmapKeyCursor } from './bigmap-pagination';

export function mapBigmapToBigmapData(bigmap: FindManyBigmapsResult): BigmapRecordData {
    return Object.assign(new BigmapRecord(), {
        ...bigmap,
        cursor: packBigmapCursor(bigmap),
        batch_position: bigmap.i,
    });
}

export function mapBigmapKeysToBigmapKeysData(bigmapKey: FindManyBigmapKeyResult): BigmapKeyRecordData {
    return Object.assign(new BigmapKeyRecord(), {
        ...bigmapKey,
        cursor: packBigmapKeyCursor(bigmapKey),
    });
}

export function mapBigmapValuesToBigmapValuesData(bigmapValue: FindManyBigmapValueResult): BigmapValueRecordData {
    if (isNullish(bigmapValue.block?.hash)) {
        throw new Error('Unexpected null value for BigmapValue.block_hash.');
    }
    return Object.assign(new BigmapValueRecord(), {
        ...bigmapValue,
        cursor: packBigmapValueCursor(bigmapValue),
    });
}
