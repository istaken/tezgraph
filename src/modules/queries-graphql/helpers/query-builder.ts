/* eslint-disable max-statements */
/* eslint-disable @typescript-eslint/no-unsafe-argument */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Prisma } from '@prisma/client';
import { UserInputError } from 'apollo-server-errors';
import { clone } from 'lodash';

import { isNotNullish, isNullish, Nullish } from '../../../utils';
import { DataFetchOptions } from '../repositories/data-fetch-options';
import { DateRangeFilter } from '../repositories/date-range.utils';
import PaginationUtils from '../utils/PaginationUtils';
import { PaginationArgs } from './pagination';
import { qualifiedColumn } from './pagination-helper';
import { QueryMetadata, TableInfo, FromClause, JoinInfo } from './query-metadata';
import { ValueOrArray, Value, toArray, OrderByDirection } from './types';
import { DatabaseColumnInfo, EquivalencyGroupColumnInfo, OrderByItem, SortFieldInfo } from './utils';

/*
 * Facilitates creating queries that span multiple tables.
 * This class uses Metadata to add tables to from clause if those tables are referenced in other clauses.
 */
export class QueryInfo<TResult, TOrderByField extends string> {
    private fromClauseAliases: Set<string>;
    private where: string[];
    private selectColumns?: string[];
    private otherStatements: string[];
    private parameters: any[];
    private orderBy: { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[];
    private shouldReverseOrder: boolean;

    constructor(
        private readonly metadata: QueryMetadata<TOrderByField>,
        private readonly paginationUtils: PaginationUtils,
    ) {
        this.fromClauseAliases = new Set();
        Object.entries(metadata.tables).forEach(([alias, table]) => {
            if (table.isMain === true) {
                this.addTableToFromClause(alias);
            }
        });
        this.where = [];
        if (metadata.defaultWhereClauses !== undefined) {
            metadata.defaultWhereClauses.forEach((w) => {
                this.appendWhere(w.tableAliases, w.where);
            });
        }
        this.selectColumns = undefined;
        this.otherStatements = [];
        this.parameters = [];
        this.orderBy = [];
        this.shouldReverseOrder = false;
    }

    get values(): any[] {
        return [...this.parameters];
    }

    setSort(
        orderBy: { fieldSortInfo: SortFieldInfo<TOrderByField>; direction: OrderByDirection }[],
        shouldReverseOrder: boolean,
    ): void {
        this.orderBy = orderBy;
        this.shouldReverseOrder = shouldReverseOrder;
    }

    appendWhere(requiredTables: Nullish<ValueOrArray<string>>, statements: ValueOrArray<string>): void {
        this.addTableToFromClause(requiredTables);
        this.where.push(...toArray(statements));
    }

    appendOtherStatement(requiredTables: ValueOrArray<string> | null, statement: string): void {
        this.addTableToFromClause(requiredTables);
        this.otherStatements.push(statement);
    }

    /**
     * This is a tag function, to allow creating tagged template literals
     * for instance query'sql`columnA = ${value}` will add value to the list of parameters
     * And if it's the third parameter, returns `columnA = $3` which can be added to the
     * related part of the query by calling the right method
     */
    sql(strings: TemplateStringsArray | string[], ...parameters: any[]): string {
        const indexes = parameters.map((parameter) => {
            if (parameter instanceof Direct) {
                return parameter.value;
            }
            if (typeof parameter === 'bigint') {
                // This is due to a bug in Prisma: https://github.com/prisma/prisma/issues/8121
                return parameter.toString();
            }
            if (parameter instanceof Prisma.Decimal) {
                // This is due to another bug in Prisma raw queries
                return parameter.toString();
            }
            return `$${this.appendParameter(parameter) + 1}`;
        });
        return String.raw({ raw: strings }, ...indexes);
    }

    tokenized(tokenized: Tokenized): string {
        return this.sql(tokenized.strings, ...tokenized.parameters);
    }

    appendWhereValueOrArray<T>(
        requiredTables: Nullish<ValueOrArray<string>>,
        column: string,
        value: Nullish<ValueOrArray<T>>,
        expandArrayInQuery?: boolean, // Workaround for a bug in our stack (Postgres + Prisma) when passing bigint arrays
    ): void {
        if (isNullish(value)) {
            return;
        }
        const array = toArray(value);
        if (expandArrayInQuery === true) {
            let query = `${column} in (`;
            array.forEach((element, index) => {
                if (index !== 0) {
                    query += ', ';
                }
                // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                query += `${element}`;
            });
            query += ')';
            this.appendWhere(requiredTables, query);
        } else if (array.length === 1) {
            this.appendWhere(requiredTables, this.sql`${raw(column)} = ${array[0]}`);
        } else {
            this.appendWhere(requiredTables, this.sql`${raw(column)} = Any(${array})`);
        }
    }

    appendWhereRange(
        tables: ValueOrArray<string>,
        column: string,
        range: { lte?: Value | bigint | Prisma.Decimal; gte?: Value | bigint | Prisma.Decimal },
    ): void {
        const lte = range instanceof DateRangeFilter ? this.parseDate(range.lte as string) : range.lte;
        const gte = range instanceof DateRangeFilter ? this.parseDate(range.gte as string) : range.gte;

        if (isNotNullish(gte) && isNotNullish(lte)) {
            this.appendWhere(tables, this.sql`${raw(column)} between ${gte} and ${lte}`);
        } else if (isNotNullish(gte)) {
            this.appendWhere(tables, this.sql`${raw(column)} >= ${gte}`);
        } else if (isNotNullish(lte)) {
            this.appendWhere(tables, this.sql`${raw(column)} <= ${lte}`);
        }
    }

    parseDate(date: string): Date | null {
        if (isNullish(date) || date === '') {
            return null;
        }
        try {
            const d = new Date(date);
            if (d.toString() === 'Invalid Date') {
                throw new UserInputError(
                    `The value '${date}' is not a supported date and time format. The suggested format is Iso 8601.`,
                );
            }
            return d;
        } catch {
            throw new UserInputError(
                `The value '${date}' is not a supported date and time format. The suggested format is Iso 8601.`,
            );
        }
    }

    addTableToFromClause(tableAliases: Nullish<ValueOrArray<string>>): void {
        if (isNullish(tableAliases)) {
            return;
        }
        for (const fullAlias of toArray(tableAliases)) {
            const alias = fullAlias.startsWith('c.') ? fullAlias.substring(2) : fullAlias;
            if (this.fromClauseAliases.has(alias)) {
                continue;
            }
            const table = this.metadata.tables[alias];
            if (!table) {
                throw new Error(`Table information for alias ${alias} not found in Metadata`);
            }
            this.addTableToFromClause(table.dependsOn);
            this.fromClauseAliases.add(alias);
        }
    }

    hasTableAlias(tableAlias: string): boolean {
        const alias = tableAlias.startsWith('c.') ? tableAlias.substring(2) : tableAlias;
        return this.fromClauseAliases.has(alias);
    }

    addSelectColumns(tableAliases: Nullish<ValueOrArray<string>>, columns: string[]): void {
        this.addTableToFromClause(tableAliases);
        if (this.selectColumns === undefined) {
            this.selectColumns = [];
        }
        this.selectColumns.push(...columns);
    }

    build(): string {
        const selectColumns = this.selectColumns?.map((column) =>
            this.processColumnOrEquivalencyGroupName(column, true),
        );
        const orderBy = this.getOrderByClause();
        let query = `select ${selectColumns?.join(', ') ?? '*'}\nfrom `;

        const joinGroups: Map<string, (TableInfo & JoinInfo)[]> = new Map();

        for (const fullAlias of this.fromClauseAliases.values()) {
            const alias = fullAlias.startsWith('c.') ? fullAlias.substring(2) : fullAlias;
            const table = this.metadata.tables[alias];
            if (!table) {
                throw new Error(`Table information for alias ${alias} not found in Metadata`);
            }
            const fromClause = table as FromClause;
            if (isNotNullish(fromClause.fromClause)) {
                query += `${fromClause.fromClause}\n`;
            } else {
                const joinInfo = table as JoinInfo;
                let array = joinGroups.get(joinInfo.equivalentColumnGroupName);
                if (isNullish(array)) {
                    array = [];
                    joinGroups.set(joinInfo.equivalentColumnGroupName, array);
                }
                if (array.length === 0) {
                    query += `${joinInfo.tableName}\n`;
                } else {
                    const thisTableColumn = this.metadata.EquivalentColumnGroups?.find(
                        (x) => x.groupName === joinInfo.equivalentColumnGroupName,
                    )?.columns.find((x) => x.tableAlias === joinInfo.tableName);
                    if (isNullish(thisTableColumn)) {
                        throw new Error(
                            `The join info for ${joinInfo.equivalentColumnGroupName} and table ${joinInfo.tableName} was not found (Main Table)`,
                        );
                    }
                    const firstTableName = array[0]!.tableName;
                    const otherTableColumn = this.metadata.EquivalentColumnGroups?.find(
                        (x) => x.groupName === joinInfo.equivalentColumnGroupName,
                    )?.columns.find((x) => x.tableAlias === firstTableName);
                    if (isNullish(otherTableColumn)) {
                        throw new Error(
                            `The join info for ${joinInfo.equivalentColumnGroupName} and table ${firstTableName} was not found (Join Table)`,
                        );
                    }
                    /*
                     * TODO: Is doing FULL OUTER JOIN right? Earlier, as we always joined to one central table (operation_alpha in case of operations query)
                     * we could do a LEFT OUTER JOIN. But by having the possibility of queries without a central table, there might be cases that FULL OUTER JOIN is the only option.
                     * For instance, if we have a condition on consumed_milligas, we can technically join only tx, delegation, reveal, and origination.
                     * Each operation_alpha row can have a row in at most one of these tables. The current actual condition on consumed_milligas joins to operation_alpha,
                     * but that can change in a future optimization.
                     */
                    query += `FULL OUTER JOIN ${joinInfo.tableName} ON ${joinInfo.tableName}.${thisTableColumn.columnName} = ${firstTableName}.${otherTableColumn.columnName}\n`;
                }
                array.push(table as TableInfo & JoinInfo);
            }
        }

        if (this.where.length > 0) {
            query += `where ${this.where
                .map((condition) => this.processColumnOrEquivalencyGroupName(condition, false))
                .join('\nand ')}\n`;
        }

        const otherStatements = [orderBy, ...this.otherStatements];

        if (this.otherStatements.length > 0) {
            query += otherStatements.join('\n');
        }
        return query;
    }

    async run(
        orderBy: OrderByItem<TOrderByField>,
        pagination: PaginationArgs,
        options?: DataFetchOptions,
    ): Promise<{ data: TResult[]; hasMorePages: boolean }> {
        return this.paginationUtils.sortPaginateAndRunQuery<TResult, TOrderByField>({
            orderBy,
            pagination,
            queryInfo: this,
            metadata: this.metadata,
            options,
        });
    }

    clone<TNewResult>(): QueryInfo<TNewResult, TOrderByField> {
        const copy = new QueryInfo<TNewResult, TOrderByField>(this.metadata, this.paginationUtils);
        copy.fromClauseAliases = clone(this.fromClauseAliases);
        copy.where = clone(this.where);
        copy.selectColumns = clone(this.selectColumns);
        copy.otherStatements = clone(this.otherStatements);
        copy.parameters = clone(this.parameters);
        copy.orderBy = this.orderBy;
        copy.shouldReverseOrder = this.shouldReverseOrder;
        return copy;
    }

    processColumnOrEquivalencyGroupName(column: string, withAlias: boolean): string {
        const equivalencyGroup = this.metadata.EquivalentColumnGroups?.find((group) =>
            column.includes(group.groupName),
        );
        if (isNullish(equivalencyGroup)) {
            return column;
        }
        let firstTable = equivalencyGroup.columns.find((col) => this.hasTableAlias(col.tableAlias));
        if (isNullish(firstTable)) {
            firstTable = equivalencyGroup.columns[0]!;
            this.addTableToFromClause(firstTable.tableAlias);
        }
        if (column === equivalencyGroup.groupName) {
            if (withAlias) {
                return `${qualifiedColumn(firstTable)} as "${column}"`;
            }
            return qualifiedColumn(firstTable);
        }
        return column.replace(equivalencyGroup.groupName, qualifiedColumn(firstTable));
    }

    private getOrderByClause(): string {
        const orderByList: { column: string; order: 'asc' | 'desc' }[] = [];
        this.orderBy.forEach((field) => {
            const rawModel = field.fieldSortInfo.rawModel;
            const equivalenceGroup = rawModel as EquivalencyGroupColumnInfo;
            let fieldName: string | null = null;
            if (isNotNullish(equivalenceGroup.equivalentGroupName)) {
                fieldName = this.processColumnOrEquivalencyGroupName(equivalenceGroup.equivalentGroupName, false);
            } else {
                const databaseColumnInfo = field.fieldSortInfo.rawModel as DatabaseColumnInfo;
                if (isNotNullish(databaseColumnInfo.dependsOnTableAliases)) {
                    this.addTableToFromClause(databaseColumnInfo.dependsOnTableAliases);
                }
                this.addTableToFromClause(databaseColumnInfo.tableAlias);
                fieldName = qualifiedColumn(databaseColumnInfo);
            }
            orderByList.push({
                column: fieldName,
                order: (field.direction === OrderByDirection.desc) !== this.shouldReverseOrder ? 'desc' : 'asc',
            });
        });
        const orderByClause = orderByList.map((item) => `${item.column} ${item.order}`).join(', ');
        return `order by ${orderByClause}`;
    }

    private appendParameter(parameter: any): number {
        this.parameters.push(parameter);
        return this.parameters.length - 1;
    }
}

/*
 * Facilitates incrementally making tokenized queries that can be added to a query later
 */
export class Tokenized {
    strings: string[];
    parameters: any[];
    constructor() {
        this.strings = [];
        this.parameters = [];
    }

    append(strings: TemplateStringsArray, ...parameters: any[]): Tokenized {
        if (strings.length === 0) {
            return this;
        }
        if (this.strings.length === 0) {
            this.strings.push(...strings);
            this.parameters.push(...parameters);
        } else {
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            this.strings[this.strings.length - 1] = `${this.strings[this.strings.length - 1]} ${strings[0]}`;
            for (let i = 1; i < strings.length; i++) {
                this.strings.push(strings[i]!);
            }
            this.parameters.push(...parameters);
        }
        return this;
    }

    clone(): Tokenized {
        const c = new Tokenized();
        c.strings = clone(this.strings);
        c.parameters = clone(this.parameters);
        return c;
    }
}

/*
 * Used to pass text directly to the query.
 * For instance query.sql`${column} = ${value}` results in `$1 = $2`
 * But query.sql`${new Direct(column)} = ${value}` results in `columnName = $2`
 */
export class Direct {
    constructor(readonly value: string) {}
}

export function raw(text: string): Direct {
    return new Direct(text);
}
