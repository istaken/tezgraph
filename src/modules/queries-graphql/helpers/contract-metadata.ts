import { bigmap } from '@prisma/client';
import { bytes2Char } from '@taquito/tzip16';

import { BigmapRecordWithMetadataValue } from '../../../entity/queries/contract-metadata';
import { isNotNullish, isNullish } from '../../../utils';

export function findMetadataStorageString(bigmapMetadataArr: bigmap[]): string | null {
    const bigmapWithMetadataStorageString = bigmapMetadataArr.find(
        (obj) => isNotNullish(obj.key) && (obj.key as Record<string, unknown>).string === '',
    );

    if (!bigmapWithMetadataStorageString) {
        return null;
    }

    const contractMetadataStringBytes = (bigmapWithMetadataStorageString.value as BigmapRecordWithMetadataValue).bytes;

    if (isNullish(contractMetadataStringBytes)) {
        return null;
    }

    const metadataStorageString = bytes2Char(contractMetadataStringBytes);

    return metadataStorageString;
}

export function stringIsInteger(value: string): boolean {
    return /^\d+$/u.test(value);
}
