/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { UserInputError } from 'apollo-server-errors';
import { ArgsType, ClassType, Field, InputType, Int } from 'type-graphql';

import { PageInfo } from '../../../entity/pageInfo';
import { scalars } from '../../../entity/scalars';

@ArgsType()
@InputType()
export class PaginationArgs {
    @Field(() => scalars.Cursor, { nullable: true })
    before?: string | null;

    @Field(() => scalars.Cursor, { nullable: true })
    after?: string | null;

    @Field(() => Int, { nullable: true })
    first?: number | null;

    @Field(() => Int, { nullable: true })
    last?: number | null;
}

export function checkPaginationArgs(args: PaginationArgs): void {
    if (args.after && args.before) {
        throw new UserInputError(`In pagination, at most one of the fields: after and before should be provided.`);
    }

    if (args.first && args.last) {
        throw new UserInputError('In pagination, only one of the fields: first and last should be provided.');
    }

    if (args.after && args.last) {
        throw new UserInputError('In pagination, the fields after and last should not be used together.');
    }

    if (args.before && args.first) {
        throw new UserInputError('In pagination, the fields before and first should not be used together.');
    }
}

interface RelayEdge<TOperation> {
    cursor: string;
    node?: TOperation | null;
}

interface RelayConnection<TOperationEdge> {
    page_info: PageInfo;
    edges?: TOperationEdge[] | null;
    total_count: number | undefined;
}

export function wrapIntoConnection<TResult extends { cursor: string }>(
    ConnectionType: ClassType,
    EdgeType: ClassType,
    results: TResult[],
    pagination: PaginationArgs,
    hasMorePages: boolean,
    totalCount: number | undefined,
): RelayConnection<RelayEdge<TResult>> {
    const connectionData: RelayConnection<RelayEdge<TResult>> = {
        page_info: {
            start_cursor: results[0]?.cursor,
            end_cursor: results.slice(-1)[0]?.cursor,
            has_previous_page: pagination.first ? false : hasMorePages,
            has_next_page: pagination.last ? false : hasMorePages,
        },
        edges: results.map<RelayEdge<TResult>>((result) => {
            const edgeData: RelayEdge<TResult> = {
                cursor: result.cursor,
                node: result,
            };
            // eslint-disable-next-line @typescript-eslint/no-unsafe-return
            return Object.assign(new EdgeType(), edgeData);
        }),
        total_count: totalCount,
    };
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return Object.assign(new ConnectionType(), connectionData);
}
