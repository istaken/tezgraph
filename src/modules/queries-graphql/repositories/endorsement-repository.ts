/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { PrismaClient } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import {
    EndorsementRecord,
    EndorsementRecordData,
    EndorsementRecordMetadata,
} from '../../../entity/queries/endorsement-record';
import { OperationKind } from '../../../entity/subscriptions';
import { prismaClientDIToken } from '../database/prisma';
import { OperationMapper } from '../helpers/operation-mapper';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { FindManyOperationsResult } from './operation-alpha-repository';

export interface FindEndorsementByIdResult {
    operation_id: bigint;
    endorsement_address: string | null;
    endorsement_delegate_id: bigint | null;
    endorsement_slots: number[] | null;
}

@singleton()
export default class EndorsementRepository {
    constructor(
        @inject(delay(() => OperationMapper))
        private readonly operationUtils: OperationMapper,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
    ) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<FindEndorsementByIdResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const endorsements = await this.prisma.endorsement.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                slots: true,
                delegate_id: true,
                address: {
                    select: {
                        address: true,
                    },
                },
            },
        });
        return endorsements.map((endorsement) => ({
            ...endorsement,
            endorsement_address: endorsement.address?.address ?? null,
            endorsement_delegate_id: endorsement.delegate_id ?? null,
            endorsement_slots: endorsement.slots,
        }));
    }

    mapResultToEntity(result: FindManyOperationsResult): EndorsementRecordData {
        if (
            result.operation_kind !== OperationRecordKind.endorsement &&
            result.operation_kind !== OperationRecordKind.endorsement_with_slot
        ) {
            throw new Error(
                `Invalid endorsement.mapResultToEntity invocation for operation kind: ${result.operation_kind}`,
            );
        }

        return {
            kind: OperationKind.endorsement,
            graphQLTypeName: EndorsementRecord.name,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result),
            metadata: {
                delegate: {
                    address: result.endorsement_address!,
                    address_id: result.endorsement_delegate_id!,
                },
                slots: result.endorsement_slots,
                graphQLTypeName: EndorsementRecordMetadata.name,
            },
        };
    }
}
