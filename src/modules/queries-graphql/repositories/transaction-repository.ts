import { Prisma, PrismaClient } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import { TransactionMetadata, TransactionResult } from '../../../entity/common/transaction';
import { TransactionRecord, TransactionRecordData } from '../../../entity/queries/transaction-record';
import { Micheline } from '../../../entity/scalars';
import { OperationKind } from '../../../entity/subscriptions';
import { prismaClientDIToken } from '../database/prisma';
import { OperationMapper } from '../helpers/operation-mapper';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { FindManyOperationsResult } from './operation-alpha-repository';

export interface FindTransactionByIdResult {
    operation_id: bigint;
    transaction_entrypoint: string | null;
    transaction_amount: bigint | null;
    transaction_parameters: Prisma.JsonValue | null;
    transaction_destination_id: bigint;
    transaction_destination_address: string;
    consumed_milligas: Prisma.Decimal | null;
    fee: bigint;
    status: number;
    error_trace: Prisma.JsonValue | null;
    storage_size: Prisma.Decimal | null;
}

@singleton()
export default class TransactionRepository {
    constructor(
        @inject(delay(() => OperationMapper))
        private readonly operationUtils: OperationMapper,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
    ) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<FindTransactionByIdResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const transactions = await this.prisma.tx.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                entrypoint: true,
                amount: true,
                parameters: true,
                destination_id: true,
                address_addressTotx_destination_id: {
                    select: {
                        address: true,
                    },
                },
                consumed_milligas: true,
                fee: true,
                status: true,
                error_trace: true,
                storage_size: true,
            },
        });
        return transactions.map((transaction) => ({
            transaction_destination_id: transaction.destination_id,
            transaction_destination_address: transaction.address_addressTotx_destination_id.address,
            transaction_entrypoint: transaction.entrypoint,
            transaction_amount: transaction.amount,
            transaction_parameters: transaction.parameters,
            ...transaction,
            storage_size: transaction.storage_size ?? null,
        }));
    }

    mapResultToEntity(result: FindManyOperationsResult): TransactionRecordData {
        if (result.operation_kind !== OperationRecordKind.transaction) {
            throw new Error(
                `Invalid transaction.mapResultToEntity invocation for operation kind: ${result.operation_kind}`,
            );
        }

        return {
            kind: OperationKind.transaction,
            graphQLTypeName: TransactionRecord.name,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            metadata: this.operationUtils.mapResultToOperationMetadata(result, {
                metadataTypeName: TransactionMetadata.name,
                resultTypeName: TransactionResult.name,
            }),
            destination_address: result.transaction_destination_address,
            destination_id: result.transaction_destination_id,
            parameters:
                result.transaction_entrypoint !== null && result.transaction_parameters !== null
                    ? {
                          entrypoint: result.transaction_entrypoint,
                          value: result.transaction_parameters as Micheline,
                      }
                    : null,
            amount: result.transaction_amount,
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            fee: result.fee!,
            storage_size: result.storage_size ?? null,
        };
    }
}
