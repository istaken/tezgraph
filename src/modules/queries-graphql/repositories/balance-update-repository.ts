/* eslint-disable no-underscore-dangle */
import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';

@singleton()
export default class BalanceUpdateRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getSumByOperationIdContractAddressBalanceKind({
        operation_id,
        contract_address_id,
        balance_kind,
    }: {
        operation_id: bigint;
        contract_address_id: bigint;
        balance_kind?: number | null;
    }): Promise<bigint | null> {
        const aggregate = await this.prisma.balance_updates.aggregate({
            _sum: {
                diff: true,
            },
            where: {
                operation_id,
                contract_address_id,
                balance_kind: isNullish(balance_kind) ? undefined : balance_kind,
            },
        });
        return aggregate._sum.diff;
    }

    async getSumByOperationId(operationId: bigint): Promise<bigint | null> {
        const aggregate = await this.prisma.balance_updates.aggregate({
            _sum: {
                diff: true,
            },
            where: {
                operation_id: operationId,
            },
        });
        return aggregate._sum.diff;
    }
}
