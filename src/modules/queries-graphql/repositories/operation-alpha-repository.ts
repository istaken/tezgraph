/* eslint-disable @typescript-eslint/unbound-method */
import { Prisma, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';
import { AggregationInput, AggregationResult, getAggregations } from '../helpers/aggregation';
import { unpackOperationCursor } from '../helpers/operation-pagination';
import { PaginationArgs } from '../helpers/pagination';
import { sortPaginateAndRunQuery } from '../helpers/prisma-pagination';
import { OrderByDirection } from '../helpers/types';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { OperationsFilter } from '../resolvers/types/operations-filter';
import {
    OperationsOrderByInput,
    OperationsOrderByFieldInternal,
    operationAlphaOrderByInfo,
} from '../resolvers/types/operations-order-by';

export interface FindManyOperationsArgs {
    pagination: PaginationArgs;
    filter: OperationsFilter;
    orderBy?: OperationsOrderByInput;
    aggregationInput?: AggregationInput;
}

export interface FindManyOperationsResult {
    id: number;
    autoid: bigint;
    internal: number;
    operation_hash: string;
    block_level: number;
    block_timestamp: Date;
    block_hash: string;
    operation_kind: OperationRecordKind;
    sender_address: string | null;
    sender_id: bigint | null;
    receiver_address: string | null;
    receiver_id: bigint | null;
    manager_numbers_gas_limit: Prisma.Decimal | null;
    manager_numbers_counter: Prisma.Decimal | null;
    manager_numbers_storage_limit: Prisma.Decimal | null;
    endorsement_address: string | null;
    endorsement_delegate_id: bigint | null;
    endorsement_slots: number[] | null;
    delegation_address_pkh_address: string | null;
    delegation_address_pkh_id: bigint | null;
    reveal_public_key: string | null;
    origination_k_address: string | null;
    origination_delegate_address: string | null;
    origination_delegate_id: bigint | null;
    origination_balance: bigint | null;
    transaction_entrypoint: string | null;
    transaction_destination_id: bigint | null;
    transaction_destination_address: string | null;
    transaction_parameters: Prisma.JsonValue | null;
    transaction_amount: bigint | null;
    consumed_milligas: Prisma.Decimal | null;
    fee: bigint | null;
    status: number | null;
    error_trace: Prisma.JsonValue | null;
    storage_size: Prisma.Decimal | null;
}

@singleton()
export default class OperationAlphaRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async findManyOperations({ filter, orderBy, pagination, aggregationInput }: FindManyOperationsArgs): Promise<{
        data: FindManyOperationsResult[];
        hasMorePages: boolean;
        aggregationResult: AggregationResult;
    }> {
        const findManyOperationsArgs = this.getFindManyOperationsOptions({ filter });

        const resultPromise = this.sortPaginateAndRunOperationAlphaQuery({
            prismaFindManyArgs: findManyOperationsArgs,
            orderBy,
            pagination,
        });
        const aggregationPromise = getAggregations<
            Prisma.operation_alpha_infoCountArgs,
            Prisma.operation_alpha_infoWhereInput
        >({
            prismaCountFunction: this.prisma.operation_alpha_info.count,
            prismaFindManyArgs: { where: findManyOperationsArgs.where },
            aggregationInput,
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([resultPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    async isOperationSentBy(operationId: bigint, senderAddressId: bigint): Promise<boolean> {
        const operation_alpha = await this.prisma.operation_alpha.findUnique({
            where: {
                autoid: operationId,
            },
            select: {
                operation_sender_and_receiver: {
                    select: {
                        sender_id: true,
                    },
                },
            },
        });
        if (operation_alpha === null) {
            throw new Error(`An operation with id: ${operationId} does not exist.`);
        }
        return operation_alpha.operation_sender_and_receiver?.sender_id === senderAddressId;
    }

    private async sortPaginateAndRunOperationAlphaQuery<T extends Prisma.operation_alpha_infoFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.operation_alpha_infoFindManyArgs>;
        orderBy: OperationsOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: FindManyOperationsResult[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            FindManyOperationsResult,
            Prisma.operation_alpha_infoFindManyArgs,
            T,
            OperationsOrderByFieldInternal,
            Prisma.operation_alpha_infoGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? {
                field: OperationsOrderByFieldInternal.chronological_order,
                direction: OrderByDirection.desc,
            },
            sortInfo: operationAlphaOrderByInfo,
            pagination,
            maxPageSize: 100,
            // eslint-disable-next-line @typescript-eslint/unbound-method
            prismaFindManyFunction: this.prisma.operation_alpha_info.findMany,
            getCursorObject: async (cursor) => this.getFromCursor(cursor),
        });
    }

    private async getFromCursor(cursor: string): Promise<{
        autoid: bigint | undefined;
        hash_id: bigint | undefined;
        id: number | undefined;
        internal: number | undefined;
        block_level: number | undefined;
        block_timestamp: Date | undefined;
        sender_address: string | undefined;
        operation_hash: string | undefined;
    }> {
        const { operationHash, batchPosition, internal } = unpackOperationCursor(cursor);
        const c = await this.prisma.operation_alpha.findFirst({
            where: {
                operation: {
                    hash: operationHash,
                },
                id: batchPosition,
                internal,
            },
            select: {
                autoid: true,
                id: true,
                internal: true,
                hash_id: true,
                block: {
                    select: {
                        level: true,
                        timestamp: true,
                    },
                },
                operation_sender_and_receiver: {
                    select: {
                        address_addressTooperation_sender_and_receiver_sender_id: {
                            select: {
                                address: true,
                            },
                        },
                    },
                },
                operation: {
                    select: {
                        hash: true,
                    },
                },
            },
        });
        return {
            autoid: c?.autoid,
            hash_id: c?.hash_id,
            id: c?.id,
            internal: c?.internal,
            block_level: c?.block.level,
            block_timestamp: c?.block.timestamp,
            sender_address:
                c?.operation_sender_and_receiver?.address_addressTooperation_sender_and_receiver_sender_id.address,
            operation_hash: c?.operation.hash,
        };
    }

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    private getFindManyOperationsOptions({ filter }: { filter: OperationsFilter }) {
        return Prisma.validator<Prisma.operation_alpha_infoFindManyArgs>()({
            where: {
                operation_hash: { equals: filter.hash },
                block_timestamp: { ...filter.timestamp },
                block_level: { equals: filter.level },
                block_hash: filter.block_hash,
                internal: { equals: filter.internal },
                id: { equals: filter.batch_position },
                autoid: filter.autoid === undefined ? { equals: filter.autoid } : undefined,
                operation_kind: { in: filter.kind },
                sender_address: { in: filter.sources },
                status: { equals: filter.status },
                ...(filter.delegation_delegates && {
                    operation_kind: OperationRecordKind.delegation,
                    delegation_address_pkh_address: { in: filter.delegation_delegates },
                }),
                ...(filter.endorsement_delegates && {
                    operation_kind: OperationRecordKind.endorsement,
                    endorsement_address: { in: filter.endorsement_delegates },
                }),
                ...(filter.public_key && {
                    operation_kind: OperationRecordKind.reveal,
                    reveal_public_key: filter.public_key,
                }),
                ...(filter.originated_contracts && {
                    operation_kind: OperationRecordKind.origination,
                    origination_k_address: { in: filter.originated_contracts },
                }),
                ...(filter.entrypoint && {
                    operation_kind: OperationRecordKind.transaction,
                    transaction_entrypoint: filter.entrypoint,
                }),
                ...(filter.destinations && {
                    operation_kind: OperationRecordKind.transaction,
                    transaction_destination_address: { in: filter.destinations },
                }),
                ...(filter.consumed_milligas && {
                    consumed_milligas: filter.consumed_milligas,
                }),
                ...(filter.amount && {
                    OR: [
                        {
                            operation_kind: OperationRecordKind.transaction,
                            transaction_amount: filter.amount,
                        },
                        {
                            operation_kind: OperationRecordKind.delegation,
                            delegation_amount: filter.amount,
                        },
                    ],
                }),
                ...(filter.fee && {
                    fee: filter.fee,
                }),
            },
            select: {
                id: true,
                internal: true,
                autoid: true,
                operation_hash: true,
                block_timestamp: true,
                block_level: true,
                block_hash: true,
                operation_kind: true,
                sender_id: true,
                sender_address: true,
                storage_size: true,
                manager_numbers_gas_limit: true,
                manager_numbers_counter: true,
                manager_numbers_storage_limit: true,
                endorsement_address: true,
                endorsement_delegate_id: true,
                endorsement_slots: true,
                delegation_address_pkh_address: true,
                reveal_public_key: true,
                origination_k_address: true,
                origination_delegate_address: true,
                origination_balance: true,
                status: true,
                error_trace: true,
                transaction_entrypoint: true,
                transaction_destination_address: true,
                transaction_parameters: true,
                transaction_amount: true,
                consumed_milligas: true,
                fee: true,
            },
        });
    }
}
