/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/unbound-method */
import { Prisma, PrismaClient } from '@prisma/client';
import { merge } from 'lodash';
import { inject, singleton } from 'tsyringe';

import { Block, BlockFilter } from '../../../entity/queries/block-record';
import { isNotNullish, isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { PaginationArgs } from '../helpers/pagination';
import { sortPaginateAndRunQuery } from '../helpers/prisma-pagination';
import { OrderByDirection } from '../helpers/types';
import { BlockArgs } from '../resolvers/block-resolver';
import { BlockOrderByField, blockOrderByInfo, BlockOrderByInput } from '../resolvers/types/block-order-by';

export interface FindBlockResult {
    level: number;
    hash: string;
    timestamp: Date;
}

interface BlockModel {
    level: number;
    hash: string;
    timestamp: Date;
}

export interface FindBlockByIdResult {
    level: number;
    timestamp: Date;
    hash: string;
    hash_id: number;
}

@singleton()
export default class BlockRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getBlocks({
        filter,
        order_by: orderBy,
        aggregationInput,
        ...pagination
    }: BlockArgs): Promise<{ data: Block[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const findManyBlocksOptions = this.createFindManyBlockOptions({ filter });

        const dataPromise = this.sortPaginateAndRunBlockQuery({
            prismaFindManyArgs: findManyBlocksOptions,
            orderBy,
            pagination,
        });
        const aggregationPromise = getAggregations<Prisma.blockCountArgs, Prisma.blockWhereInput>({
            aggregationInput,
            prismaFindManyArgs: { where: findManyBlocksOptions.where },
            prismaCountFunction: this.prisma.block.count,
        });

        const [{ data: blocks, hasMorePages }, aggregationResult] = await Promise.all([
            dataPromise,
            aggregationPromise,
        ]);
        const data = blocks.map((blk) => ({
            timestamp: blk.timestamp,
            level: blk.level,
            hash: blk.hash,
        }));
        return { data, hasMorePages, aggregationResult };
    }

    async findByLevel(level: number): Promise<FindBlockResult | null> {
        const block = await this.prisma.block.findFirst({
            where: {
                level,
            },
            select: {
                hash: true,
                timestamp: true,
            },
        });
        if (!block) {
            return null;
        }
        return {
            hash: block.hash,
            level,
            timestamp: block.timestamp,
        };
    }

    async findByHash(hash: string): Promise<FindBlockResult | null> {
        const block = await this.prisma.block.findFirst({
            where: {
                hash,
            },
            select: {
                level: true,
                timestamp: true,
            },
        });
        if (!block) {
            return null;
        }
        return {
            hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }

    async getBlocksByHashIds(hashIds: number[]): Promise<FindBlockByIdResult[]> {
        const blocks = await this.prisma.block.findMany({
            where: {
                level: { in: hashIds },
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });
        return blocks.map((block) => ({
            level: block.level,
            hash: block.hash,
            hash_id: block.level,
            timestamp: block.timestamp,
        }));
    }

    async findByHashOrLevel({ hash, level }: Pick<Block, 'hash' | 'level'>): Promise<FindBlockResult> {
        if (isNotNullish(hash)) {
            const data = await this.findByHash(hash);
            if (isNullish(data)) {
                throw new Error(`The provided block hash ${hash} does not exist`);
            }
            return data;
        } else if (isNotNullish(level)) {
            const data = await this.findByLevel(level);
            if (isNullish(data)) {
                throw new Error(`The provided block level ${level} does not exist`);
            }
            return data;
        }
        throw new Error('When creating a block, at least one of hash and level should be provided');
    }

    async findLastBlock(): Promise<Block | null> {
        const block = await this.prisma.block.findFirst({
            orderBy: {
                level: 'desc',
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });
        if (isNullish(block)) {
            return null;
        }
        return {
            hash: block.hash,
            level: block.level,
            timestamp: block.timestamp,
        };
    }

    private async sortPaginateAndRunBlockQuery<T extends Prisma.blockFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.blockFindManyArgs>;
        orderBy: BlockOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: BlockModel[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            BlockModel,
            Prisma.blockFindManyArgs,
            T,
            BlockOrderByField,
            Prisma.blockGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? {
                field: BlockOrderByField.level,
                direction: OrderByDirection.desc,
            },
            sortInfo: blockOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.block.findMany,
            getCursorObject: async (cursor) => this.getFromCursor(cursor),
        });
    }

    private async getFromCursor(cursor: string): Promise<FindBlockResult> {
        const blockObject = await this.prisma.block.findFirst({
            where: {
                hash: cursor,
            },
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
        });
        if (isNullish(blockObject)) {
            throw new Error(`No block found for cursor ${cursor}`);
        }
        return {
            hash: blockObject.hash,
            level: blockObject.level,
            timestamp: blockObject.timestamp,
        };
    }

    private createFindManyBlockOptions({ filter }: { filter?: BlockFilter }): Prisma.blockFindManyArgs {
        let where: Prisma.blockWhereInput = {};
        if (filter?.hashes) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    hash: {
                        in: filter.hashes,
                    },
                }),
            );
        }

        if (isNotNullish(filter?.level?.gte) || isNotNullish(filter?.level?.lte)) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    level: filter?.level,
                }),
            );
        }

        if (isNotNullish(filter?.timestamp?.gte) || isNotNullish(filter?.timestamp?.lte)) {
            where = merge(
                where,
                Prisma.validator<Prisma.blockWhereInput>()({
                    timestamp: filter?.timestamp,
                }),
            );
        }
        return {
            select: {
                level: true,
                timestamp: true,
                hash: true,
            },
            where,
        };
    }
}
