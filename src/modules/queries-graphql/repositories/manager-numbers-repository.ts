import { Prisma, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

export interface ManagerNumbersResult {
    operation_id: bigint;
    gas_limit: Prisma.Decimal | null;
    counter: Prisma.Decimal | null;
    storage_limit: Prisma.Decimal | null;
}

@singleton()
export default class ManagerNumbersRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<ManagerNumbersResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const managerNumbers = await this.prisma.manager_numbers.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                gas_limit: true,
                counter: true,
                storage_limit: true,
            },
        });
        return managerNumbers;
    }
}
