import { Prisma, PrismaClient } from '.prisma/client';
import { inject, singleton } from 'tsyringe';

import { Micheline } from '../../../entity/scalars';
import { injectLogger, isNullish, Logger, Nullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';

export interface ContractSource {
    code: Micheline;
    storage: Micheline;
}

@singleton()
export default class ContractScriptRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @injectLogger(ContractScriptRepository) private readonly logger: Logger,
    ) {}

    async getContractSource(addressId: bigint): Promise<ContractSource | null> {
        const data = await this.prisma.contract_script.findUnique({
            where: {
                address_id: addressId,
            },
            select: {
                script: true,
            },
        });
        return this.mapContractScriptToContractSource(addressId, data?.script);
    }

    private mapContractScriptToContractSource(
        addressId: bigint,
        contractScript: Nullish<Prisma.JsonValue>,
    ): ContractSource | null {
        if (isNullish(contractScript) || typeof contractScript !== 'object' || Array.isArray(contractScript)) {
            this.logger.logWarning(
                'Getting contract_script for address {{addressId}} returned unexpected type of data.',
                { addressId, contractScript },
            );
            return null;
        }

        if (isNullish(contractScript.code) || isNullish(contractScript.storage)) {
            this.logger.logWarning(
                'Data of contract_script for address {{addressId}} has missing `code` or `storage` properties.',
                { addressId, contractScript },
            );
            return null;
        }

        return {
            code: contractScript.code as Micheline,
            storage: contractScript.storage as Micheline,
        };
    }
}
