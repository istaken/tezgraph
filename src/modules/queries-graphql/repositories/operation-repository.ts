import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

export interface OperationResult {
    hash_id: bigint;
    operation_hash: string;
}

@singleton()
export default class OperationRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}
    async getByHashIds(hashIds: (bigint | string)[]): Promise<OperationResult[]> {
        const bigInts = hashIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const operations = await this.prisma.operation.findMany({
            where: { hash_id: { in: bigInts } },
            select: {
                hash_id: true,
                hash: true,
            },
        });
        return operations.map((operation) => ({
            hash_id: operation.hash_id,
            operation_hash: operation.hash,
        }));
    }
}
