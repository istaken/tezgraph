/* eslint-disable @typescript-eslint/no-non-null-asserted-optional-chain */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable complexity */
/* eslint-disable max-statements */
/* eslint-disable max-params */
import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { isNotNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult } from '../helpers/aggregation';
import { QueryInfo } from '../helpers/query-builder';
import { OperationAlphaMetadata } from '../helpers/query-metadata';
import { getValueForQuery, OrderByDirection } from '../helpers/types';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { OperationsOrderByFieldInternal } from '../resolvers/types/operations-order-by';
import AggregationUtils from '../utils/AggregationUtils';
import PaginationUtils from '../utils/PaginationUtils';
import BlockRepository, { FindBlockByIdResult } from './block-repository';
import DelegationRepository, { FindDelegationByIdResult } from './delegation-repository';
import EndorsementRepository, { FindEndorsementByIdResult } from './endorsement-repository';
import ManagerNumbersRepository, { ManagerNumbersResult } from './manager-numbers-repository';
import { FindManyOperationsArgs, FindManyOperationsResult } from './operation-alpha-repository';
import OperationRepository, { OperationResult } from './operation-repository';
import OriginationRepository, { FindOriginationByIdResult } from './origination-repository';
import ReceiverRepository, { ReceiverResult } from './receiver-repository';
import RevealRepository, { FindRevealByIdResult } from './reveal-repository';
import SenderRepository, { SenderResult } from './sender-repository';
import TransactionRepository, { FindTransactionByIdResult } from './transaction-repository';

export interface FindOperationAlphaResult {
    hash_id: bigint;
    id: number;
    operation_kind: number;
    internal: number;
    autoid: bigint;
    block_hash_id: number;
    hash: string;
}

interface MainFilterResult {
    OperationAlpha_AutoID: bigint;
}

@singleton()
export default class OperationAlphaRawRepository {
    constructor(
        private readonly blockRepository: BlockRepository,
        private readonly managerNumbersRepository: ManagerNumbersRepository,
        private readonly operationRepository: OperationRepository,
        private readonly senderRepository: SenderRepository,
        private readonly receiverRepository: ReceiverRepository,
        private readonly delegationRepository: DelegationRepository,
        private readonly endorsementRepository: EndorsementRepository,
        private readonly revealRepository: RevealRepository,
        private readonly originationRepository: OriginationRepository,
        private readonly transactionRepository: TransactionRepository,
        private readonly aggregationUtils: AggregationUtils,
        private readonly paginationUtils: PaginationUtils,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
    ) {}

    async findManyOperations(
        args: FindManyOperationsArgs,
    ): Promise<{ data: FindManyOperationsResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        let query = new QueryInfo<FindOperationAlphaResult, OperationsOrderByFieldInternal>(
            OperationAlphaMetadata,
            this.paginationUtils,
        );
        query = await this.applyFilterToQuery(args, query);

        const dataPromise = this.getOperationRows({ args, query });
        const aggregationPromise = this.aggregationUtils.buildAndRunAggregationsQuery<OperationsOrderByFieldInternal>({
            args,
            query,
            requiredTables: [],
            requiredEquivalentColumnGroupNames: 'OperationAlpha_AutoID',
            columns: ['count(OperationAlpha_AutoID) as "totalCount"'],
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    async getOperationRows({
        args,
        query,
    }: {
        args: FindManyOperationsArgs;
        query: QueryInfo<unknown, OperationsOrderByFieldInternal>;
    }): Promise<{ data: FindManyOperationsResult[]; hasMorePages: boolean }> {
        const rowsQuery = query.clone<MainFilterResult>();
        rowsQuery.addSelectColumns([], ['OperationAlpha_AutoID']);

        const { data: mainData, hasMorePages } = await rowsQuery.run(
            args.orderBy ?? {
                field: OperationsOrderByFieldInternal.chronological_order,
                direction: OrderByDirection.desc,
            },
            args.pagination,
        );
        if (!mainData.length) {
            return { data: [], hasMorePages };
        }

        const allFields = await this.prisma.operation_alpha.findMany({
            select: {
                hash_id: true,
                id: true,
                operation_kind: true,
                internal: true,
                autoid: true,
                block_level: true,
                block: {
                    select: {
                        hash: true,
                    },
                },
            },
            where: {
                autoid: {
                    in: mainData.map((x) => x.OperationAlpha_AutoID),
                },
            },
        });

        const operationAlphas: FindOperationAlphaResult[] = mainData
            .map((x) => allFields.find((o) => o.autoid === BigInt(x.OperationAlpha_AutoID))!)
            .map((x) => ({
                hash_id: x.hash_id,
                id: x.id,
                operation_kind: x.operation_kind,
                internal: x.internal,
                autoid: x.autoid,
                block_hash_id: x.block_level,
                hash: x.block.hash,
            }));

        const data = await this.fetchAndSetRelatedData(operationAlphas);
        return { data, hasMorePages };
    }

    private async fetchAndSetRelatedData(data: FindOperationAlphaResult[]): Promise<FindManyOperationsResult[]> {
        const operationAlphas = data.map((operation) => ({
            id: operation.id,
            hash_id: BigInt(operation.hash_id),
            autoid: BigInt(operation.autoid),
            internal: operation.internal,
            block_hash_id: operation.block_hash_id,
            operation_kind: operation.operation_kind,
        }));

        const relatedIds = this.getRelatedIds(operationAlphas);
        const promises = this.fetchRelatedData(relatedIds);

        const [
            blocks,
            delegations,
            managerNumbers,
            operations,
            senders,
            receivers,
            endorsements,
            reveals,
            originations,
            transactions,
        ] = await Promise.all([
            promises.blocksPromise,
            promises.delegationsPromise,
            promises.managerNumbersPromise,
            promises.operationsPromise,
            promises.sendersPromise,
            promises.receiversPromise,
            promises.endorsementsPromise,
            promises.revealsPromise,
            promises.originationsPromise,
            promises.transactionsPromise,
        ]);

        const allRelatedData = operationAlphas.map((item) => ({
            operationAlpha: item,
            block: blocks.find((x) => x.hash_id === item.block_hash_id),
            managerNumber: managerNumbers.find((x) => x.operation_id === item.autoid),
            operation: operations.find((x) => x.hash_id === item.hash_id),
            sender: senders.find((x) => x.operation_id === item.autoid),
            receiver: receivers.find((x) => x.operation_id === item.autoid),
            delegation: delegations.find((x) => x.operation_id === item.autoid),
            endorsement: endorsements.find((x) => x.operation_id === item.autoid),
            reveal: reveals.find((x) => x.operation_id === item.autoid),
            origination: originations.find((x) => x.operation_id === item.autoid),
            transaction: transactions.find((x) => x.operation_id === item.autoid),
        }));

        const results: FindManyOperationsResult[] = allRelatedData.map((item) => ({
            ...item.operationAlpha,
            block_hash: item.block?.hash!,
            block_level: item.block?.level!,
            block_timestamp: item.block?.timestamp!,
            manager_numbers_counter: item.managerNumber?.counter ?? null,
            manager_numbers_gas_limit: item.managerNumber?.gas_limit ?? null,
            manager_numbers_storage_limit: item.managerNumber?.storage_limit ?? null,
            operation_hash: item.operation?.operation_hash!,
            sender_address: item.sender?.sender_address ?? null,
            sender_id: item.sender?.sender_id ?? null,
            receiver_address: item.receiver?.receiver_address ?? null,
            receiver_id: item.receiver?.receiver_id ?? null,

            endorsement_address: item.endorsement?.endorsement_address ?? null,
            endorsement_delegate_id: item.endorsement?.endorsement_delegate_id ?? null,
            endorsement_slots: item.endorsement?.endorsement_slots ?? null,

            consumed_milligas:
                item.delegation?.consumed_milligas ??
                item.reveal?.consumed_milligas ??
                item.origination?.consumed_milligas ??
                item.transaction?.consumed_milligas ??
                null,
            error_trace:
                item.delegation?.error_trace ??
                item.reveal?.error_trace ??
                item.origination?.error_trace ??
                item.transaction?.error_trace ??
                null,
            fee: item.delegation?.fee ?? item.reveal?.fee ?? item.origination?.fee ?? item.transaction?.fee ?? null,
            status:
                item.delegation?.status ??
                item.reveal?.status ??
                item.origination?.status ??
                item.transaction?.status ??
                null,

            origination_k_address: item.origination?.origination_k_address ?? null,
            origination_delegate_address: item.origination?.origination_delegate_address ?? null,
            origination_delegate_id: item.origination?.origination_delegate_id ?? null,
            delegation_address_pkh_address: item.delegation?.delegation_address_pkh_address ?? null,
            delegation_address_pkh_id: item.delegation?.delegation_address_pkh_id ?? null,
            reveal_public_key: item.reveal?.reveal_public_key ?? null,
            transaction_amount: item.transaction?.transaction_amount ?? null,
            transaction_destination_id: item.transaction?.transaction_destination_id ?? null,
            transaction_destination_address: item.transaction?.transaction_destination_address ?? null,
            transaction_entrypoint: item.transaction?.transaction_entrypoint ?? null,
            transaction_parameters: item.transaction?.transaction_parameters ?? null,
            origination_balance: item.origination?.origination_balance ?? null,
            storage_size: item.origination?.storage_size ?? item.transaction?.storage_size ?? null,
        }));
        return results;
    }

    private fetchRelatedData(relatedIds: {
        blocksIds: number[];
        delegationIds: bigint[];
        endorsementIds: bigint[];
        revealIds: bigint[];
        originationIds: bigint[];
        transactionIds: bigint[];
        operationIds: bigint[];
        hashIds: bigint[];
    }): {
        blocksPromise: Promise<FindBlockByIdResult[]>;
        delegationsPromise: Promise<FindDelegationByIdResult[]>;
        managerNumbersPromise: Promise<ManagerNumbersResult[]>;
        operationsPromise: Promise<OperationResult[]>;
        sendersPromise: Promise<SenderResult[]>;
        receiversPromise: Promise<ReceiverResult[]>;
        endorsementsPromise: Promise<FindEndorsementByIdResult[]>;
        revealsPromise: Promise<FindRevealByIdResult[]>;
        originationsPromise: Promise<FindOriginationByIdResult[]>;
        transactionsPromise: Promise<FindTransactionByIdResult[]>;
    } {
        const blocksPromise = this.blockRepository.getBlocksByHashIds(relatedIds.blocksIds);
        const delegationsPromise = relatedIds.delegationIds.length
            ? this.delegationRepository.getByOperationIds(relatedIds.delegationIds)
            : Promise.resolve([]);
        const endorsementsPromise = relatedIds.endorsementIds.length
            ? this.endorsementRepository.getByOperationIds(relatedIds.endorsementIds)
            : Promise.resolve([]);
        const revealsPromise = relatedIds.revealIds.length
            ? this.revealRepository.getByOperationIds(relatedIds.revealIds)
            : Promise.resolve([]);
        const originationsPromise = relatedIds.originationIds.length
            ? this.originationRepository.getByOperationIds(relatedIds.originationIds)
            : Promise.resolve([]);
        const transactionsPromise = relatedIds.transactionIds.length
            ? this.transactionRepository.getByOperationIds(relatedIds.transactionIds)
            : Promise.resolve([]);
        const managerNumbersPromise = this.managerNumbersRepository.getByOperationIds(relatedIds.operationIds);
        const sendersPromise = this.senderRepository.getByOperationIds(relatedIds.operationIds);
        const receiversPromise = this.receiverRepository.getByOperationIds(relatedIds.operationIds);
        const operationsPromise = this.operationRepository.getByHashIds(relatedIds.hashIds);
        return {
            blocksPromise,
            delegationsPromise,
            managerNumbersPromise,
            operationsPromise,
            sendersPromise,
            receiversPromise,
            endorsementsPromise,
            revealsPromise,
            originationsPromise,
            transactionsPromise,
        };
    }

    private getRelatedIds(
        results: {
            id: number;
            hash_id: bigint;
            autoid: bigint;
            internal: number;
            block_hash_id: number;
            operation_kind: number;
        }[],
    ): {
        blocksIds: number[];
        delegationIds: bigint[];
        endorsementIds: bigint[];
        revealIds: bigint[];
        originationIds: bigint[];
        transactionIds: bigint[];
        operationIds: bigint[];
        hashIds: bigint[];
    } {
        const blocksIds = [...new Set(results.map((operation) => operation.block_hash_id))];
        const delegationIds = results
            .filter((operation) => operation.operation_kind === OperationRecordKind.delegation)
            .map((operation) => operation.autoid);
        const endorsementIds = results
            .filter(
                (operation) =>
                    operation.operation_kind === OperationRecordKind.endorsement ||
                    operation.operation_kind === OperationRecordKind.endorsement_with_slot,
            )
            .map((operation) => operation.autoid);
        const revealIds = results
            .filter((operation) => operation.operation_kind === OperationRecordKind.reveal)
            .map((operation) => operation.autoid);
        const originationIds = results
            .filter((operation) => operation.operation_kind === OperationRecordKind.origination)
            .map((operation) => operation.autoid);
        const transactionIds = results
            .filter((operation) => operation.operation_kind === OperationRecordKind.transaction)
            .map((operation) => operation.autoid);
        const operationIds = results.map((operation) => operation.autoid);
        const hashIds = [...new Set(results.map((operation) => operation.hash_id))];
        return {
            blocksIds,
            delegationIds,
            endorsementIds,
            revealIds,
            originationIds,
            transactionIds,
            operationIds,
            hashIds,
        };
    }

    private async applyFilterToQuery(
        args: FindManyOperationsArgs,
        query: QueryInfo<FindOperationAlphaResult, OperationsOrderByFieldInternal>,
    ): Promise<QueryInfo<FindOperationAlphaResult, OperationsOrderByFieldInternal>> {
        if (isNotNullish(args.filter.amount)) {
            query.appendWhereRange(
                ['tx', 'contract_balance'],
                `CASE ` +
                    `WHEN operation_alpha.operation_kind = 8 THEN tx.amount ` +
                    `WHEN operation_alpha.operation_kind = 10 THEN contract_balance.balance ` +
                    'ELSE NULL::numeric ' +
                    `END`,
                args.filter.amount,
            );
        }
        if (isNotNullish(args.filter.autoid)) {
            query.appendWhere(null, query.sql`OperationAlpha_AutoID = ${args.filter.autoid}`);
        }
        if (isNotNullish(args.filter.batch_position)) {
            query.appendWhere('tx', query.sql`operation_alpha.id = ${args.filter.batch_position}`);
        }
        if (args.filter.block_hash) {
            query.appendWhere('block', query.sql`block.hash = ${args.filter.block_hash}`);
        }
        if (isNotNullish(args.filter.consumed_milligas)) {
            query.appendWhereRange(
                ['operation_alpha', 'tx', 'delegation', 'reveal', 'origination'],
                `CASE ` +
                    `WHEN operation_alpha.operation_kind = 8 THEN tx.consumed_milligas ` +
                    `WHEN operation_alpha.operation_kind = 10 THEN delegation.consumed_milligas ` +
                    `WHEN operation_alpha.operation_kind = 7 THEN reveal.consumed_milligas ` +
                    `WHEN operation_alpha.operation_kind = 9 THEN origination.consumed_milligas ` +
                    `ELSE NULL::numeric ` +
                    `END`,
                args.filter.consumed_milligas,
            );
        }
        if (isNotNullish(args.filter.originated_contracts)) {
            // TODO: Idea for optimization: fetch address_ids, and use them to query. Then we can eliminate join to orig_k_addr
            query.appendWhereValueOrArray(
                ['origination', 'orig_k_addr'],
                `orig_k_addr.address`,
                args.filter.originated_contracts,
            );
        }

        if (isNotNullish(args.filter.delegation_delegates)) {
            query.appendWhereValueOrArray(
                ['delegation', 'deleg_addr_pkh'],
                `deleg_addr_pkh.address`,
                args.filter.delegation_delegates,
            );
        }

        if (isNotNullish(args.filter.endorsement_delegates)) {
            query.appendWhereValueOrArray(
                ['endorsement', 'endors_addr'],
                `endors_addr.address`,
                args.filter.endorsement_delegates,
            );
        }

        if (args.filter.destinations) {
            const destinations = await this.prisma.address.findMany({
                select: {
                    address_id: true,
                },
                where: {
                    address: {
                        in: args.filter.destinations,
                    },
                },
            });
            const destinationIds = destinations.map((x) => x.address_id.toString());
            query.appendWhereValueOrArray(['tx'], `tx.destination_id`, destinationIds, true);
        }
        if (isNotNullish(args.filter.receivers)) {
            query.appendWhereValueOrArray(
                ['operation_sender_and_receiver', 'receiver'],
                `receiver.address`,
                args.filter.receivers,
            );
        }
        if (isNotNullish(args.filter.senders)) {
            query.appendWhereValueOrArray(
                ['operation_sender_and_receiver', 'sender'],
                `sender.address`,
                args.filter.senders,
            );
        }
        if (args.filter.entrypoint) {
            query.appendWhere(['tx'], query.sql`tx.entrypoint = ${args.filter.entrypoint}`);
        }
        if (isNotNullish(args.filter.fee)) {
            query.appendWhereRange(
                ['operation_alpha', 'tx', 'delegation', 'reveal', 'origination'],
                `CASE ` +
                    `WHEN operation_alpha.operation_kind = 8 THEN tx.fee ` +
                    `WHEN operation_alpha.operation_kind = 10 THEN delegation.fee ` +
                    `WHEN operation_alpha.operation_kind = 7 THEN reveal.fee ` +
                    `WHEN operation_alpha.operation_kind = 9 THEN origination.fee ` +
                    `ELSE NULL::bigint ` +
                    `END `,
                args.filter.fee,
            );
        }
        if (isNotNullish(args.filter.hash)) {
            query.appendWhere('operation', query.sql`operation.hash = ${args.filter.hash}`);
        }
        if (isNotNullish(args.filter.internal)) {
            query.appendWhere('operation_alpha', query.sql`operation_alpha.internal = ${args.filter.internal}`);
        }
        if (isNotNullish(args.filter.kind)) {
            query.appendWhere('operation_alpha', query.sql`operation_alpha.operation_kind = Any(${args.filter.kind})`);
        }
        if (isNotNullish(args.filter.level)) {
            query.appendWhere('block', query.sql`block.level = ${args.filter.level}`);
        }
        if (isNotNullish(args.filter.public_key)) {
            query.appendWhere('reveal', query.sql`reveal.pk = ${args.filter.public_key}`);
        }
        if (isNotNullish(args.filter.sources)) {
            query.appendWhere(
                ['operation_sender_and_receiver', 'sender'],
                query.sql`sender.address = Any(${args.filter.sources})`,
            );
        }
        if (isNotNullish(args.filter.status)) {
            query.appendWhere(
                ['operation_alpha', 'tx', 'delegation', 'reveal', 'origination'],
                `CASE ` +
                    `WHEN operation_alpha.operation_kind = 8 THEN tx.status ` +
                    `WHEN operation_alpha.operation_kind = 10 THEN delegation.status ` +
                    `WHEN operation_alpha.operation_kind = 7 THEN reveal.status ` +
                    `WHEN operation_alpha.operation_kind = 9 THEN origination.status ` +
                    ` ELSE NULL::smallint ` +
                    `END = ${getValueForQuery(args.filter.status)}`,
            );
        }
        if (isNotNullish(args.filter.timestamp)) {
            query.appendWhereRange('block', 'block.timestamp', args.filter.timestamp);
        }
        if (isNotNullish(args.filter.level_range)) {
            query.appendWhereRange('block', 'block.level', args.filter.level_range);
        }

        return query;
    }
}
