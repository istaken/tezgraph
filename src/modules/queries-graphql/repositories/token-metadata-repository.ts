import { bigmap, PrismaClient } from '@prisma/client';
import { bytes2Char } from '@taquito/tzip16';
import { UserInputError } from 'apollo-server-express';
import { inject, singleton } from 'tsyringe';

import { Account } from '../../../entity/queries/account-record';
import { TokenMetadata } from '../../../entity/queries/token-metadata';
import { isNotNullish, isNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { JsonValue } from '../helpers/types';
import { TokenMetadataArgs } from '../resolvers/account-fields-resolver';
import { BigmapRecordKind } from '../resolvers/types/bigmap-kind';
import TaquitoRepository from './taquito-repository';

// eslint-disable-next-line @typescript-eslint/consistent-type-definitions
type TokenMetadataBigmapValue = {
    args: {
        string: string;
        bytes: string;
    }[];
    prim: string;
};

@singleton()
export default class TokenMetadataRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @inject(TaquitoRepository) private readonly taquitoRepository: TaquitoRepository,
    ) {}

    async findTokenMetadataWithTokenId(
        account: Account,
        tokenMetadataArgs: TokenMetadataArgs,
    ): Promise<TokenMetadata | null> {
        if (isNullish(tokenMetadataArgs.token_id) || !account.address.startsWith('KT')) {
            return null;
        }

        const tokenMetadataBigmap = await this.findTokenMetadataBigmap(account, tokenMetadataArgs.token_id);

        const tokenMetadata =
            this.findTokenMetadataFromStorage(tokenMetadataBigmap) ??
            (await this.taquitoRepository.getTokenMetadataFromTaquito(account, tokenMetadataArgs));

        return tokenMetadata;
    }

    findTokenMetadataFromStorage(tokenMetadataBigmap: bigmap | null): TokenMetadata | undefined {
        if (isNullish(tokenMetadataBigmap)) {
            return undefined;
        }

        const tokenMetadataBigmapValue = tokenMetadataBigmap.value as { args: JsonValue[] };
        const tokenId = Number((tokenMetadataBigmapValue.args[0] as { int: string }).int);
        const tokenData = tokenMetadataBigmapValue.args[1] as TokenMetadataBigmapValue[];
        const tokenMetadataRaw: Record<string, string> = {};

        if (!Array.isArray(tokenData)) {
            throw new UserInputError(`Token metadata is non-compliant with the TZIP-016 standards.`);
        }

        tokenData.forEach((data) => {
            const key = data.args[0]?.string;
            const value = data.args[1]?.bytes;
            if (isNotNullish(key) && isNotNullish(value)) {
                tokenMetadataRaw[key] = value;
            }
        });

        if (tokenMetadataRaw.decimals !== undefined) {
            const decimals = Number(bytes2Char(tokenMetadataRaw.decimals));
            if (isNullish(decimals) || isNaN(decimals)) {
                return;
            }
            return {
                decimals,
                name: isNotNullish(tokenMetadataRaw.name) ? bytes2Char(tokenMetadataRaw.name) : null,
                symbol: isNotNullish(tokenMetadataRaw.symbol) ? bytes2Char(tokenMetadataRaw.symbol) : null,
                token_id: tokenId,
                raw: tokenMetadataRaw,
            };
        }
        return undefined;
    }

    async findTokenMetadataBigmap(account: Account, token_id: number): Promise<bigmap | null> {
        const bigmapWithTokenMetadataAnnot = await this.prisma.bigmap.findFirst({
            where: {
                annots: '%token_metadata',
                receiver_id: account.address_id,
                kind: BigmapRecordKind.alloc,
            },
        });

        if (isNullish(bigmapWithTokenMetadataAnnot)) {
            return null;
        }

        const tokenMetadataBigmap = await this.prisma.bigmap.findFirst({
            where: {
                NOT: { kind: bigmapWithTokenMetadataAnnot.kind },
                id: bigmapWithTokenMetadataAnnot.id,
                key: {
                    path: ['int'],
                    equals: token_id.toString(),
                },
            },
            orderBy: { operation_id: 'desc' },
        });

        if (isNullish(tokenMetadataBigmap)) {
            throw new Error('token_id required to query for token metadata.');
        }

        return tokenMetadataBigmap;
    }
}
