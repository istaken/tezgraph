/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/member-ordering */
import { PrismaClient, address, Prisma } from '@prisma/client';
import { performance } from 'perf_hooks';
import { inject, singleton } from 'tsyringe';

import { AccountFilter } from '../../../entity/queries/account-record';
import { isNotNullish } from '../../../utils';
import { injectLogger, Logger } from '../../../utils/logging';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { PaginationArgs } from '../helpers/pagination';
import { sortPaginateAndRunQuery } from '../helpers/prisma-pagination';
import { OrderByDirection } from '../helpers/types';
import { AccountArgs } from '../resolvers/account-resolver';
import { AccountOrderByField, accountOrderByInfo, AccountOrderByInput } from '../resolvers/types/account-order-by';

@singleton()
export default class AddressRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @injectLogger(AddressRepository) private readonly logger: Logger,
    ) {}

    async getAddresses({
        filter,
        order_by: orderBy,
        aggregationInput,
        ...pagination
    }: AccountArgs): Promise<{ data: address[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const findManyAccountsOptions = this.getFindManyAccountOptions({ filter });

        const dataPromise = this.sortPaginateAndRunAccountQuery({
            prismaFindManyArgs: findManyAccountsOptions,
            orderBy,
            pagination,
        });
        const aggregationPromise = getAggregations<Prisma.addressCountArgs, Prisma.addressWhereInput>({
            aggregationInput,
            prismaFindManyArgs: { where: findManyAccountsOptions.where },
            prismaCountFunction: this.prisma.address.count,
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    private async sortPaginateAndRunAccountQuery<T extends Prisma.addressFindManyArgs>({
        prismaFindManyArgs,
        orderBy,
        pagination,
    }: {
        prismaFindManyArgs: Prisma.SelectSubset<T, Prisma.addressFindManyArgs>;
        orderBy: AccountOrderByInput | undefined;
        pagination: PaginationArgs;
    }): Promise<{ data: address[]; hasMorePages: boolean }> {
        return sortPaginateAndRunQuery<
            address,
            Prisma.addressFindManyArgs,
            T,
            AccountOrderByField,
            Prisma.addressGetPayload<T>
        >({
            prismaFindManyArgs,
            orderBy: orderBy ?? {
                field: AccountOrderByField.address,
                direction: OrderByDirection.asc,
            },
            sortInfo: accountOrderByInfo,
            pagination,
            maxPageSize: 100,
            prismaFindManyFunction: this.prisma.address.findMany,
            getCursorObject: async (cursor) => this.getFromCursor(cursor),
        });
    }

    private async getFromCursor(cursor: string): Promise<{ address: string }> {
        return Promise.resolve({ address: cursor });
    }

    private getFindManyAccountOptions({ filter }: { filter?: AccountFilter }): Prisma.addressFindManyArgs {
        const conditions: Prisma.addressWhereInput[] = [];
        if (filter?.addresses) {
            conditions.push({
                address: {
                    in: filter.addresses,
                },
            });
        }
        if (filter?.address_ids) {
            conditions.push({
                address_id: {
                    in: filter.address_ids,
                },
            });
        }
        if (filter?.address_prefixes) {
            const prefixConditions: Prisma.addressWhereInput[] = filter.address_prefixes.map((prefix) => ({
                address: {
                    startsWith: prefix,
                },
            }));
            conditions.push({
                OR: prefixConditions,
            });
        }
        if (filter && isNotNullish<boolean>(filter.is_contract)) {
            conditions.push({
                address: {
                    startsWith: filter.is_contract ? 'KT1' : 'tz',
                },
            });
        }
        return {
            where: {
                AND: conditions,
            },
        };
    }

    // Activated Timestamp Query
    async getActivated(pkh: string, requestId: string): Promise<Date | undefined> {
        const performanceStart = performance.now();
        this.logger.logInformation(`GetActivated started - for {requestId}`, { requestId });
        const activationData = await this.prisma.address.findUnique({
            where: {
                address: pkh,
            },
            select: {
                activation: {
                    select: {
                        operation_alpha: {
                            select: {
                                block: {
                                    select: {
                                        timestamp: true,
                                    },
                                },
                            },
                        },
                    },
                },
            },
        });
        const performanceEnd = performance.now();
        const duration = `${performanceEnd - performanceStart} ms`;
        this.logger.logInformation(`GetActivated ended - for {requestId}, {duration}.`, { requestId, duration });
        return activationData?.activation[0]?.operation_alpha.block.timestamp;
    }
}
