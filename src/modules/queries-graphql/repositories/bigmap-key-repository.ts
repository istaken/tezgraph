/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable no-undef-init */
import { Prisma, PrismaClient } from '@prisma/client';
import { UserInputError } from 'apollo-server-express';
import { inject, singleton } from 'tsyringe';

import { isNotNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { BigmapKeySelectionInBigmap } from '../resolvers/types/bigmap-filters';
import BigmapPaginationUtils from '../utils/BigmapPaginationUtils';

export interface FindManyBigmapKeyResult {
    key: Prisma.JsonValue;
    key_hash: string;
    id?: bigint | null;
}

export interface BigmapKeyParentInfo {
    bigmap_id?: bigint | null;
}

@singleton()
export default class BigmapKeyRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        private readonly bigmapPaginationUtils: BigmapPaginationUtils,
    ) {}

    async findManyBigmapKeys(
        parentInfo: BigmapKeyParentInfo,
        args: BigmapKeySelectionInBigmap,
    ): Promise<{ data: FindManyBigmapKeyResult[]; hasMorePages: boolean; aggregationResult: AggregationResult }> {
        const options = this.getFindManyBigmapKeyOptions(parentInfo, args);

        const dataPromise = this.bigmapPaginationUtils.sortPaginateAndRunBigmapKeyQuery({
            orderBy: args.order_by,
            pagination: args,
            prismaFindManyArgs: options,
        });
        const aggregationPromise = getAggregations<Prisma.bigmap_keys2CountArgs, Prisma.bigmap_keys2WhereInput>({
            prismaCountFunction: this.prisma.bigmap_keys2.count,
            prismaFindManyArgs: options,
            aggregationInput: args.aggregationInput,
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { data, hasMorePages, aggregationResult };
    }

    private getFindManyBigmapKeyOptions(
        parentInfo: BigmapKeyParentInfo,
        args: BigmapKeySelectionInBigmap,
    ): Prisma.bigmap_keys2FindManyArgs {
        let keyFilter: Prisma.JsonNullableFilter | undefined = undefined;
        if (args.filter?.keys) {
            if (args.filter.keys.length > 1) {
                throw new UserInputError('Currently, only filtering on one key is implemented');
            }
            const firstNotNullishKey = args.filter.keys.find((key) => isNotNullish(key));
            if (!firstNotNullishKey) {
                throw new UserInputError('All given values for key are nullish');
            }
            keyFilter = firstNotNullishKey as unknown as Prisma.JsonNullableFilter;
        }
        const where: Prisma.bigmap_keys2WhereInput = { key: { equals: keyFilter } };
        if (isNotNullish(parentInfo.bigmap_id)) {
            where.id = parentInfo.bigmap_id;
        }
        const select = Prisma.validator<Prisma.bigmap_keys2Select>()({
            key: true,
            key_hash: true,
            id: true,
        });
        return {
            where,
            select,
        };
    }
}
