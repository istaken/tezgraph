import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

export interface ReceiverResult {
    operation_id: bigint;
    receiver_id: bigint | null;
    receiver_address: string | null;
}

@singleton()
export default class ReceiverRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<ReceiverResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const receivers = await this.prisma.operation_sender_and_receiver.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                receiver_id: true,
                address_addressTooperation_sender_and_receiver_receiver_id: {
                    select: {
                        address: true,
                    },
                },
            },
        });

        return receivers.map((receiver) => ({
            operation_id: receiver.operation_id,
            receiver_id: receiver.receiver_id,
            receiver_address: receiver.address_addressTooperation_sender_and_receiver_receiver_id?.address ?? null,
        }));
    }
}
