import { contract_balance, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { prismaClientDIToken } from '../database/prisma';

@singleton()
export default class ContractBalanceRepository {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async findBalanceByAddressAndBlockHash(
        address_id: bigint,
        block_hash_id: number,
    ): Promise<Pick<contract_balance, 'balance'> | null> {
        return this.prisma.contract_balance.findUnique({
            where: {
                address_id_block_level: {
                    address_id,
                    block_level: block_hash_id,
                },
            },
            select: {
                balance: true,
            },
        });
    }
}
