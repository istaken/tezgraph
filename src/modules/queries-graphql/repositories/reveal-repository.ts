import { Prisma, PrismaClient, reveal as Reveal } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import {
    RevealRecord,
    RevealRecordMetadata,
    RevealResultRecord,
    RevealRecordData,
} from '../../../entity/queries/reveal-record';
import { OperationKind } from '../../../entity/subscriptions';
import { prismaClientDIToken } from '../database/prisma';
import { OperationMapper } from '../helpers/operation-mapper';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { FindManyOperationsResult } from './operation-alpha-repository';

export interface FindRevealByIdResult {
    operation_id: bigint;
    reveal_public_key: string;
    consumed_milligas: Prisma.Decimal | null;
    fee: bigint;
    status: number;
    error_trace: Prisma.JsonValue | null;
}

@singleton()
export default class RevealRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @inject(delay(() => OperationMapper))
        private readonly operationUtils: OperationMapper,
    ) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<FindRevealByIdResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const reveals = await this.prisma.reveal.findMany({
            where: { operation_id: { in: bigInts } },
            select: {
                operation_id: true,
                pk: true,
                consumed_milligas: true,
                fee: true,
                status: true,
                error_trace: true,
            },
        });
        return reveals.map((reveal) => ({
            ...reveal,
            reveal_public_key: reveal.pk,
        }));
    }

    mapResultToEntity(result: FindManyOperationsResult): RevealRecordData {
        if (result.operation_kind !== OperationRecordKind.reveal) {
            throw new Error(`Invalid reveal.mapResultToEntity invocation for operation kind: ${result.operation_kind}`);
        }

        return {
            kind: OperationKind.reveal,
            graphQLTypeName: RevealRecord.name,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            public_key: result.reveal_public_key!,
            metadata: this.operationUtils.mapResultToOperationMetadata(result, {
                metadataTypeName: RevealRecordMetadata.name,
                resultTypeName: RevealResultRecord.name,
            }),
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            fee: result.fee!,
        };
    }

    async findLatestByAddressId(address_id: bigint): Promise<Reveal | null> {
        const result = await this.prisma.reveal.findFirst({
            where: {
                source_id: address_id,
            },
            orderBy: {
                operation_id: 'desc',
            },
        });
        return result;
    }
}
