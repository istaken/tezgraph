/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable max-lines-per-function */
/* eslint-disable capitalized-comments */
import { Prisma, PrismaClient } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import { DelegationMetadata, DelegationResult } from '../../../entity/common/delegation';
import { DelegationRecord, DelegationRecordData } from '../../../entity/queries/delegation-record';
import { OperationKind } from '../../../entity/subscriptions';
import { prismaClientDIToken } from '../database/prisma';
import { OperationMapper } from '../helpers/operation-mapper';
import { OperationRecordKind } from '../resolvers/types/operation-record-kind';
import { FindManyOperationsResult } from './operation-alpha-repository';

export interface FindDelegationByIdResult {
    operation_id: bigint;
    delegation_address_pkh_address: string | null;
    delegation_address_pkh_id: bigint | null;
    consumed_milligas: Prisma.Decimal | null;
    fee: bigint;
    status: number;
    error_trace: Prisma.JsonValue | null;
}

@singleton()
export default class DelegationRepository {
    constructor(
        @inject(delay(() => OperationMapper))
        private readonly operationUtils: OperationMapper,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
    ) {}

    async getByOperationIds(operationIds: (bigint | string)[]): Promise<FindDelegationByIdResult[]> {
        const bigInts = operationIds.map((id) => (typeof id === 'bigint' ? id : BigInt(id)));
        const delegations = await this.prisma.delegation.findMany({
            where: {
                operation_id: { in: bigInts },
            },
            select: {
                operation_id: true,
                consumed_milligas: true,
                fee: true,
                status: true,
                error_trace: true,
                pkh_id: true,
                address_addressTodelegation_pkh_id: {
                    select: {
                        address: true,
                    },
                },
            },
        });
        return delegations.map((delegation) => ({
            operation_id: delegation.operation_id,
            delegation_address_pkh_address: delegation.address_addressTodelegation_pkh_id?.address ?? null,
            delegation_address_pkh_id: delegation.pkh_id,
            consumed_milligas: delegation.consumed_milligas,
            fee: delegation.fee,
            status: delegation.status,
            error_trace: delegation.error_trace,
        }));
    }

    mapResultToEntity(result: FindManyOperationsResult): DelegationRecordData {
        if (result.operation_kind !== OperationRecordKind.delegation) {
            throw new Error(
                `Invalid delegation.mapResultToEntity invocation for operation kind: ${result.operation_kind}`,
            );
        }

        return {
            kind: OperationKind.delegation,
            graphQLTypeName: DelegationRecord.name,
            ...this.operationUtils.mapBaseResultToBaseOperationData(result),
            ...this.operationUtils.mapManagerNumbersResultToOperationCostData(result),
            fee: result.fee!,
            metadata: this.operationUtils.mapResultToOperationMetadata(result, {
                metadataTypeName: DelegationMetadata.name,
                resultTypeName: DelegationResult.name,
            }),
            delegate_address: result.delegation_address_pkh_address,
            delegate_id: result.delegation_address_pkh_id,
        };
    }
}
