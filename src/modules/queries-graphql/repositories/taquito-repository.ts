import { RpcClient } from '@taquito/rpc';
import { compose, TezosToolkit } from '@taquito/taquito';
import { tzip12 } from '@taquito/tzip12';
import { MetadataProvider, tzip16 } from '@taquito/tzip16';
import { inject, singleton } from 'tsyringe';

import { Account } from '../../../entity/queries/account-record';
import { ContractMetadata, ContractMetadataError } from '../../../entity/queries/contract-metadata';
import { TokenMetadata } from '../../../entity/queries/token-metadata';
import { isNullish } from '../../../utils';
import { QueryMetricsUtils } from '../../../utils/metrics/query-metrics-utils';
import { metadataProviderDiToken } from '../../../utils/query/metadata-provider';
import { taquitoRpcClientDIToken } from '../../../utils/query/rpc-client-factory';
import { tezosToolkitDIToken } from '../../../utils/query/tezos-toolkit-factory';
import { TokenMetadataArgs } from '../resolvers/account-fields-resolver';

@singleton()
export default class TaquitoRepository {
    constructor(
        @inject(tezosToolkitDIToken) private readonly tezosToolkit: TezosToolkit,
        @inject(taquitoRpcClientDIToken) private readonly taquitoRpcClient: RpcClient,
        @inject(metadataProviderDiToken) private readonly metadataProvider: MetadataProvider,
        private readonly queryMetricsUtils: QueryMetricsUtils,
    ) {}

    async getDelegationAmountFromTaquito({
        source_address,
        block_hash,
    }: {
        block_hash: string;
        source_address: string;
    }): Promise<bigint | null> {
        const res = await this.taquitoRpcClient.getBalance(`${source_address}`, { block: `${block_hash}` });
        if (isNullish(res.c) || isNullish(res.c[0]) || isNaN(res.c[0])) {
            this.reportQuerySourceType('getDelegationAmountFromTaquito', 'taquito-rpc', 'failed');
            return null;
        }
        this.reportQuerySourceType('getDelegationAmountFromTaquito', 'taquito-rpc', 'succeeded');
        return BigInt(res.c[0]); // The delegation balance is found in the first element in the array value of the c key.
    }

    async findContractMetadataFromTaquito(
        metadataStorageString: string,
        address: string,
    ): Promise<ContractMetadata | null> {
        try {
            const contract = await this.tezosToolkit.contract.at(address);
            const { metadata } = await this.metadataProvider.provideMetadata(
                contract,
                metadataStorageString,
                // eslint-disable-next-line @typescript-eslint/no-unsafe-argument, @typescript-eslint/dot-notation
                this.tezosToolkit['_context'],
            );

            const contractMetadata: ContractMetadata = {
                ...metadata,
                errors: metadata.errors as ContractMetadataError[],
                raw: metadata,
            };
            this.reportQuerySourceType(
                'findContractMetadataFromTaquito',
                `taquito-tzip16-${metadataStorageString}`,
                'succeeded',
            );
            return contractMetadata;
        } catch (err: unknown) {
            this.reportQuerySourceType(
                'findContractMetadataFromTaquito',
                `taquito-tzip16-${metadataStorageString}`,
                'failed',
            );
            throw err;
        }
    }

    async getTokenMetadataFromTaquito(account: Account, tokenMetadataArgs: TokenMetadataArgs): Promise<TokenMetadata> {
        try {
            if (isNullish(tokenMetadataArgs.token_id)) {
                throw new Error(`unable to find a valid token_metadata record with the given address and token_id`);
            }
            const contract = await this.tezosToolkit.contract.at(account.address, compose(tzip12, tzip16));
            const tokenMetadata = await contract.tzip12().getTokenMetadata(tokenMetadataArgs.token_id);
            this.reportQuerySourceType('getTokenMetadataFromTaquito', `taquito-tzip12-rpc`, 'succeeded');
            return {
                decimals: tokenMetadata.decimals,
                name: tokenMetadata.name,
                symbol: tokenMetadata.symbol,
                token_id: tokenMetadata.token_id,
                raw: tokenMetadata,
            };
        } catch (err: unknown) {
            this.reportQuerySourceType('getTokenMetadataFromTaquito', `taquito-tzip12-rpc`, 'failed');
            throw err;
        }
    }

    private reportQuerySourceType(method: string, CallType: string, status: 'succeeded' | 'failed'): void {
        let formattedCallType = CallType.replace('\u0005\u0001\u0000\u0000\u0000\u0015', '');
        const [CallTypeWithoutKey] = formattedCallType.split(':');
        if (CallTypeWithoutKey) {
            formattedCallType = CallTypeWithoutKey;
        }
        this.queryMetricsUtils.sendQuerySourceType(method, formattedCallType, status);
    }
}
