/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/unbound-method */
/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/strict-boolean-expressions */
import { Prisma, PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { BigmapOperationKind } from '../../../entity/queries/bigmap-operation-kind';
import { AccountIdAndAddress } from '../../../entity/queries/bigmap-record';
import { BigmapValue } from '../../../entity/queries/bigmap-value-record';
import { isNotNullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationResult, getAggregations } from '../helpers/aggregation';
import { BigmapValueSelectionInKey } from '../resolvers/types/bigmap-filters';
import BigmapPaginationUtils from '../utils/BigmapPaginationUtils';

export interface BigmapValueResultBase {
    key: Prisma.JsonValue;
    value?: Prisma.JsonValue;
    kind: BigmapOperationKind;
    i: bigint;
    block_level: number;
}
export interface FindManyBigmapValueResult extends BigmapValueResultBase {
    key_json?: Prisma.JsonValue;
    key_hash?: string | null;
    value_json?: Prisma.JsonValue;
    id: bigint | null;
    address_addressTobigmap_receiver_id?: AccountIdAndAddress;
    address_addressTobigmap_sender_id?: AccountIdAndAddress;
    operation_id: bigint | null;
    block?: {
        hash: string;
    };
}

export interface FindOneBigmapValueResult extends BigmapValueResultBase {
    block_level: number;
    id: bigint;
    receiver_id?: bigint;
    sender_id?: bigint;
    operation_id: bigint;
}

export interface BigmapValueFilter {
    key?: Prisma.JsonObject;
    operationId?: bigint;
    bigmapId?: bigint;
}

@singleton()
export default class BigmapValueRepository {
    constructor(
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        private readonly bigmapPaginationUtils: BigmapPaginationUtils,
    ) {}

    async findManyBigmapValues(
        parentInfo: BigmapValueFilter,
        args: BigmapValueSelectionInKey,
    ): Promise<{
        bigmapValuesData: FindManyBigmapValueResult[];
        hasMorePages: boolean;
        aggregationResult: AggregationResult;
    }> {
        const options = this.getFindManyBigmapValueOptions(parentInfo);

        const dataPromise = this.bigmapPaginationUtils.sortPaginateAndRunBigmapValueQuery({
            orderBy: args.order_by,
            pagination: args,
            prismaFindManyArgs: options,
        });
        const aggregationPromise = getAggregations<Prisma.bigmapCountArgs, Prisma.bigmapWhereInput>({
            prismaCountFunction: this.prisma.bigmap.count,
            prismaFindManyArgs: { where: options.where },
            aggregationInput: args.aggregationInput,
        });

        const [{ data, hasMorePages }, aggregationResult] = await Promise.all([dataPromise, aggregationPromise]);
        return { bigmapValuesData: data, hasMorePages, aggregationResult };
    }

    async findPreviousValue(currentValue: BigmapValue): Promise<FindOneBigmapValueResult | null> {
        const prev = await this.prisma.bigmap.findFirst({
            where: {
                id: currentValue.id,
                key: {
                    equals: currentValue.key!,
                },
                i: { lt: currentValue.i },
                kind: {
                    in: [1, 3],
                },
            },
            orderBy: {
                i: 'desc',
            },
        });
        if (!prev) {
            return null;
        }
        return {
            key: prev.key,
            value: prev.value,
            block_level: prev.block_level,
            operation_id: prev.operation_id!,
            receiver_id: prev.receiver_id!,
            sender_id: prev.sender_id!,
            kind: prev.kind,
            i: prev.i,
            id: prev.id!,
        };
    }

    private getFindManyBigmapValueOptions(filter: BigmapValueFilter): Prisma.bigmapFindManyArgs {
        const where: Prisma.bigmapWhereInput = {
            kind: { in: [BigmapOperationKind.update, BigmapOperationKind.copy, BigmapOperationKind.clear] },
            id: {
                gt: 0,
            },
        };
        if (isNotNullish(filter.key)) {
            where.key = {
                equals: filter.key,
            };
        }
        if (filter.bigmapId) {
            where.id = filter.bigmapId;
        }
        if (filter.operationId) {
            where.operation_id = filter.operationId;
        }
        const select = Prisma.validator<Prisma.bigmapSelect>()({
            id: true,
            key: true,
            key_hash: true,
            value: true,
            operation_id: true,
            address_addressTobigmap_receiver_id: {
                select: {
                    address: true,
                    address_id: true,
                },
            },
            address_addressTobigmap_sender_id: {
                select: {
                    address: true,
                    address_id: true,
                },
            },
            kind: true,
            i: true,
            block_level: true,
            block: {
                select: {
                    hash: true,
                },
            },
        });
        const args: Prisma.bigmapFindManyArgs = {
            where,
            select,
        };
        return args;
    }
}
