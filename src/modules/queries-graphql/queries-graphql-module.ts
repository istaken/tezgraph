import { PrismaClient } from '@prisma/client';
import { RpcClient } from '@taquito/rpc';
import { TezosToolkit } from '@taquito/taquito';
import { MetadataProvider } from '@taquito/tzip16';
import { DependencyContainer, Lifecycle } from 'tsyringe';

import { BackgroundWorker, backgroundWorkersDIToken } from '../../bootstrap/background-worker';
import { graphQLResolversDIToken } from '../../bootstrap/graphql-resolver-type';
import { HealthCheck, healthChecksDIToken } from '../../utils/health/health-check';
import { metadataProviderDiToken, metadataProvider } from '../../utils/query/metadata-provider';
import { taquitoRpcClientDIToken, TaquitoRpcClientFactory } from '../../utils/query/rpc-client-factory';
import { TezosToolkitFactory, tezosToolkitDIToken } from '../../utils/query/tezos-toolkit-factory';
import { Module, ModuleName } from '../module';
import DatabaseHealthCheck from './database/database-health-check';
import IndexerHealthCheck from './database/indexer-health-check';
import IndexerStatusProvider from './database/indexer-status-provider';
import { prismaClientDIToken, PrismaClientFactory } from './database/prisma';
import { PrismaDatabaseWorker } from './database/prisma-worker';
import { BigmapDataSource, bigmapDataSourceDIToken } from './datasources/bigmap-datasource';
import { AccountFieldsResolver } from './resolvers/account-fields-resolver';
import { AccountRelationsResolver } from './resolvers/account-relations-resolver';
import { AccountResolver } from './resolvers/account-resolver';
import { BigmapKeyResolver } from './resolvers/bigmap-key-resolver';
import { BigmapResolver } from './resolvers/bigmap-resolver';
import { BigmapValueResolver } from './resolvers/bigmap-value-resolver';
import { BlockResolver } from './resolvers/block-resolver';
import { ContractMetadataResolver } from './resolvers/contract-metadata-resolver';
import { DelegationRecordResolver } from './resolvers/delegation-resolver';
import { EndorsementRecordResolver } from './resolvers/endorsement-resolver';
import { OperationResolver } from './resolvers/operation-resolver';
import { OriginationRecordResolver } from './resolvers/origination-resolver';
import { TransactionParametersResolver } from './resolvers/transaction-parameters-resolver';
import { TransactionRecordResolver } from './resolvers/transaction-resolver';

export const queriesGraphQLModule: Module = {
    name: ModuleName.QueriesGraphQL,
    dependencies: [],

    initialize(diContainer: DependencyContainer): void {
        diContainer.registerInstance(graphQLResolversDIToken, AccountResolver);
        diContainer.registerInstance(graphQLResolversDIToken, AccountFieldsResolver);
        diContainer.registerInstance(graphQLResolversDIToken, AccountRelationsResolver);
        diContainer.registerInstance(graphQLResolversDIToken, BlockResolver);
        diContainer.registerInstance(graphQLResolversDIToken, TransactionParametersResolver);
        diContainer.registerInstance(graphQLResolversDIToken, OperationResolver);
        diContainer.registerInstance(graphQLResolversDIToken, DelegationRecordResolver);
        diContainer.registerInstance(graphQLResolversDIToken, EndorsementRecordResolver);
        diContainer.registerInstance(graphQLResolversDIToken, OriginationRecordResolver);
        diContainer.registerInstance(graphQLResolversDIToken, ContractMetadataResolver);
        diContainer.registerInstance(graphQLResolversDIToken, BigmapResolver);
        diContainer.registerInstance(graphQLResolversDIToken, BigmapKeyResolver);
        diContainer.registerInstance(graphQLResolversDIToken, BigmapValueResolver);
        diContainer.registerInstance(graphQLResolversDIToken, TransactionRecordResolver);

        diContainer.register<BigmapDataSource>(bigmapDataSourceDIToken, BigmapDataSource, {
            lifecycle: Lifecycle.ContainerScoped,
        });

        diContainer.register<PrismaClient>(prismaClientDIToken, {
            useFactory: (c) => c.resolve(PrismaClientFactory).create(),
        });
        diContainer.register<HealthCheck>(healthChecksDIToken, { useToken: DatabaseHealthCheck });
        diContainer.register<HealthCheck>(healthChecksDIToken, { useToken: IndexerHealthCheck });
        diContainer.register<BackgroundWorker>(backgroundWorkersDIToken, { useToken: PrismaDatabaseWorker });
        diContainer.register<BackgroundWorker>(backgroundWorkersDIToken, { useToken: IndexerStatusProvider });
        diContainer.register<TezosToolkit>(tezosToolkitDIToken, {
            useFactory: (c) => c.resolve(TezosToolkitFactory).create(),
        });
        diContainer.register<RpcClient>(taquitoRpcClientDIToken, {
            useFactory: (c) => c.resolve(TaquitoRpcClientFactory).create(),
        });
        diContainer.register<MetadataProvider>(metadataProviderDiToken, { useValue: metadataProvider });
    },
};
