CREATE index CONCURRENTLY bigmap_contract_index ON c.bigmap (receiver_id, id) WHERE (kind <> 2 and key_type is not null);

CREATE INDEX CONCURRENTLY bigmap_id_key_index ON c.bigmap USING btree (id, "key") where (kind <> 2 and key_type is null);

CREATE INDEX CONCURRENTLY bigmap_id_key_hash_index ON c.bigmap USING btree (id, key_hash) where (kind <> 2 and key_type is null);

CREATE INDEX CONCURRENTLY bigmap_clears ON c.bigmap USING btree (id, block_hash_id desc, i desc) where kind = 2;

CREATE INDEX CONCURRENTLY endorsement_delegate_operation_idx ON c.endorsement USING btree (delegate_id, operation_id);

CREATE INDEX CONCURRENTLY activation_pkh_id_idx ON c.activation USING btree (pkh_id);

CREATE INDEX CONCURRENTLY block_timestamp_idx ON c.block USING btree ("timestamp");

CREATE index CONCURRENTLY bigmap_previous_index ON c.bigmap (id, key, i) where kind in (1, 3);

CREATE index CONCURRENTLY bigmap_allocation_index ON c.bigmap (id) WHERE (kind <> 2 and key_type is not null);

CREATE index concurrently endorsement_delegate_id_operation_id ON c.endorsement USING btree (delegate_id, operation_id);

CREATE index concurrently delegation_pkh_operation_id ON c.delegation USING btree (pkh_id, operation_id);

CREATE INDEX concurrently tx_destination_operation_id ON c.tx USING btree (destination_id, operation_id);

create or replace view c.bigmap_info as
select
	b1.id,
	b1."key_type",
	b1.value_type,
    b1.block_level as block_hash_id,
	blk.level as block_level,
	b1.operation_id,
	b1.sender_id,
	b1.receiver_id,
	b1.i,
	b1.annots,
	blk.hash as hash_of_block
from c.bigmap b1
join c.block blk on blk.level = b1.block_level
where b1.kind = 0;

CREATE OR REPLACE VIEW c.bigmap_keys
AS SELECT DISTINCT ON (b1.id, b1.key)
	b1.id,
    b1.key,
    b1.key_hash,
    b1.block_level as block_hash_id,
    b1.i,
    blk.hash as hash_of_block
   FROM c.bigmap b1
   join c.block blk on blk.level = b1.block_level
  where kind in (1, 3)
  ORDER BY b1.id, b1.key, b1.block_level asc, b1.i asc;

CREATE OR REPLACE VIEW c.bigmap_keys2
AS SELECT DISTINCT ON (b.id, b.key) b.id,
    b.key,
    b.key_hash
   FROM c.bigmap b
  WHERE b.kind <> 2 and b.key_type is null
  ORDER BY b.id, b.key, b.block_level, b.i;

CREATE OR REPLACE VIEW c.bigmap_last_change_per_key
AS SELECT DISTINCT ON (b.id, b.key)
	b.id,
    b.key,
    b.key_hash,
    b.block_level as block_hash_id,
    b.i,
    b.value,
	h.level as block_level,
    b.operation_id,
    b.sender_id,
    b.receiver_id,
    b.kind
  FROM c.bigmap b
  join c.block h on h.level = b.block_level
  where kind in (1, 3)
  ORDER BY b.id, b.key, b.block_level desc, b.i desc;

create view c.operation_alpha_info
as
select
oa.block_level as block_hash_id,
oa.hash_id,
oa.id,
oa.operation_kind,
oa.internal,
oa.autoid,
o.hash as operation_hash,
b."timestamp" as block_timestamp,
b."level" as block_level,
b.hash as block_hash,
rs.sender_id as sender_id,
sender.address as sender_address,
rs.receiver_id as receiver_id,
receiver.address as receiver_address,
deleg_addr_pkh.address as delegation_address_pkh_address,
deleg.pkh_id as delegation_address_pkh_id,
cb.balance as delegation_amount,
endors_addr.address as endorsement_address,
endors.delegate_id as endorsement_delegate_id,
reveal.pk as reveal_public_key,
orig_k_addr.address as origination_k_address,
orig_delegate_addr.address as origination_delegate_address,
orig.delegate_id as origination_delegate_id,
orig.credit as origination_balance,
tran.destination_id as transaction_destination_id,
tran.entrypoint as transaction_entrypoint,
tran.amount as transaction_amount,
tran.parameters as transaction_parameters,
dest.address as transaction_destination_address,
case
	when oa.operation_kind = 8 then tran.consumed_milligas
	when oa.operation_kind = 10 then deleg.consumed_milligas
	when oa.operation_kind = 7 then reveal.consumed_milligas
	when oa.operation_kind = 9 then orig.consumed_milligas
end as consumed_milligas,
case
	when oa.operation_kind = 8 then tran.fee
	when oa.operation_kind = 10 then deleg.fee
	when oa.operation_kind = 7 then reveal.fee
	when oa.operation_kind = 9 then orig.fee
end as fee,
case
	when oa.operation_kind = 8 then tran.error_trace
	when oa.operation_kind = 10 then deleg.error_trace
	when oa.operation_kind = 7 then reveal.error_trace
	when oa.operation_kind = 9 then orig.error_trace
end as error_trace,
case
	when oa.operation_kind = 8 then tran.status
	when oa.operation_kind = 10 then deleg.status
	when oa.operation_kind = 7 then reveal.status
	when oa.operation_kind = 9 then orig.status
end as status,
case
	when oa.operation_kind = 8 then tran.storage_size
	when oa.operation_kind = 9 then orig.storage_size
end as storage_size,
mannum.gas_limit as manager_numbers_gas_limit,
mannum.counter as manager_numbers_counter,
mannum.storage_limit as manager_numbers_storage_limit,
endors.slots as endorsement_slots,
cs.script as script
from c.operation_alpha oa
left join c.operation o on o.hash_id = oa.hash_id
left join c.block b on b.level = oa.block_level
left join c.operation_sender_and_receiver rs on rs.operation_id = oa.autoid
left join c.addresses sender on sender.address_id = rs.sender_id
left join c.addresses receiver on receiver.address_id = rs.receiver_id
left join c.delegation deleg on deleg.operation_id = oa.autoid
left join c.contract_balance cb on cb.address_id = sender.address_id and cb.block_level = b.level
left join c.addresses deleg_addr_pkh on deleg_addr_pkh.address_id = deleg.pkh_id
left join c.endorsement endors on endors.operation_id = oa.autoid
left join c.addresses endors_addr on endors_addr.address_id = endors.delegate_id
left join c.reveal reveal on reveal.operation_id = oa.autoid
left join c.origination orig on orig.operation_id = oa.autoid
left join c.addresses orig_k_addr on orig_k_addr.address_id = orig.k_id
left join c.addresses orig_delegate_addr on orig_delegate_addr.address_id = orig.delegate_id
left join c.tx tran on tran.operation_id = oa.autoid
left join c.addresses dest on dest.address_id = tran.destination_id
left join c.manager_numbers mannum on mannum.operation_id = oa.autoid
left join c.contract_script cs on cs.address_id = orig.k_id;

CREATE OR REPLACE VIEW c.transaction_info
AS SELECT 'transaction'::text AS kind,
    oa.autoid AS id,
    b.level,
    b."timestamp",
    b.hash AS block,
    op.hash,
    sourceaddr1.address AS source,
    t.fee,
    m.counter,
    m.gas_limit,
    m.storage_limit,
    oa.id AS op_id,
    oa.internal,
    t.nonce,
    NULL::text AS public_key,
    t.amount,
    destaddr1.address AS destination,
    t.parameters,
    NULL::smallint[] AS slots,
    NULL::smallint as slot,
    t.entrypoint,
    NULL::text AS contract_address,
    NULL::text AS delegate,
    t.consumed_milligas
   FROM c.tx t
     LEFT JOIN c.operation_alpha oa ON oa.autoid = t.operation_id
     LEFT JOIN c.manager_numbers m ON m.operation_id = oa.autoid
     LEFT JOIN c.operation op ON op.hash_id = oa.hash_id
     LEFT JOIN c.block b ON b.level = op.block_level
     LEFT JOIN LATERAL ( SELECT sourceaddr.address
           FROM c.addresses sourceaddr
          WHERE sourceaddr.address_id = t.source_id) sourceaddr1 ON true
     LEFT JOIN LATERAL ( SELECT destaddr.address
           FROM c.addresses destaddr
          WHERE destaddr.address_id = t.destination_id) destaddr1 ON true;

CREATE INDEX CONCURRENTLY IF NOT EXISTS endorsement_delegate_id ON C.endorsement USING btree (delegate_id);
