import { singleton } from 'tsyringe';

import { Names } from '../../../utils/configuration/env-config';
import { EnvConfigProvider } from '../../../utils/configuration/env-config-provider';

@singleton()
export class PrismaEnvConfig {
    readonly databaseConnectionString: string;

    constructor(env: EnvConfigProvider) {
        this.databaseConnectionString = env.getString(Names.DatabaseConnectionString);
    }
}
