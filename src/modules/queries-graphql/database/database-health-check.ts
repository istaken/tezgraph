/* eslint-disable capitalized-comments */
/* eslint-disable multiline-comment-style */
import { PrismaClient, Prisma } from '@prisma/client';
import { delay, inject, singleton } from 'tsyringe';

import { isNullish } from '../../../utils';
import { HealthStatus, HealthCheck, HealthCheckResult } from '../../../utils/health/health-check';
import { Clock } from '../../../utils/time/clock';
import DatabaseHealthCheckRepository from '../repositories/database-health-check-repository';
import { prismaClientDIToken } from './prisma';
import { PrismaDatabaseWorker } from './prisma-worker';

enum ConnectionStatus {
    NOT_CONNECTED = 'NOT_CONNECTED',
    CONNECTED = 'CONNECTED',
    UNABLE_TO_CONNECT = 'UNABLE_TO_CONNECT',
    DISCONNECTED = 'DISCONNECTED',
}

function getHealthStatus(status: ConnectionStatus): HealthStatus {
    switch (status) {
        case ConnectionStatus.CONNECTED:
            return HealthStatus.Healthy;
        case ConnectionStatus.DISCONNECTED:
            return HealthStatus.Degraded;
        case ConnectionStatus.NOT_CONNECTED:
        case ConnectionStatus.UNABLE_TO_CONNECT:
        default:
            return HealthStatus.Unhealthy;
    }
}

function getStatusDescription(status: ConnectionStatus): string {
    switch (status) {
        case ConnectionStatus.CONNECTED:
            return 'Database connection has been established successfully.';
        case ConnectionStatus.DISCONNECTED:
            return 'Database connection has been lost.';
        case ConnectionStatus.NOT_CONNECTED:
            return 'Database connection is not established yet.';
        case ConnectionStatus.UNABLE_TO_CONNECT:
            return 'Database connection is not possible to establish.';
        default:
            return 'No detailed info about database connection.';
    }
}

@singleton()
export default class DatabaseHealthCheck implements HealthCheck {
    readonly name = 'DatabaseHealth';
    private connectionStatus: ConnectionStatus = ConnectionStatus.NOT_CONNECTED;
    private lastCheckedAt?: Date;
    private indexerVersion?: string | null;
    private chainId?: string | null;

    constructor(
        private readonly clock: Clock,
        @inject(prismaClientDIToken) private readonly prisma: PrismaClient,
        @inject(delay(() => PrismaDatabaseWorker)) private readonly prismaDatabaseWorker: PrismaDatabaseWorker,
        private readonly databaseHealthCheckRepository: DatabaseHealthCheckRepository,
    ) {}

    async checkHealth(): Promise<HealthCheckResult> {
        if (isNullish(this.indexerVersion)) {
            this.indexerVersion = await this.databaseHealthCheckRepository.getIndexerVersion();
        }
        if (isNullish(this.chainId)) {
            this.chainId = await this.databaseHealthCheckRepository.getChainId();
        }
        return {
            status: getHealthStatus(this.connectionStatus),
            data: {
                statusDescription: getStatusDescription(this.connectionStatus),
                indexerVersion: this.indexerVersion,
                chainId: this.chainId,
            },
            evaluatedOn: this.lastCheckedAt,
        };
    }

    setConnected(): void {
        this.setConnectionStatus(ConnectionStatus.CONNECTED);
    }

    setUnableToConnect(): void {
        this.setConnectionStatus(ConnectionStatus.UNABLE_TO_CONNECT);
        // TODO: enable this when Prisma issue is fixed
        // https://github.com/prisma/prisma/issues/9420
        // this.prismaDatabaseWorker.scheduleReconnect();
    }

    setDisconnected(): void {
        this.setConnectionStatus(ConnectionStatus.DISCONNECTED);
        this.prismaDatabaseWorker.scheduleReconnect();
    }

    setupMonitoring(): void {
        this.prisma.$use(async (params, next) => {
            try {
                const result: unknown = await next(params);
                this.setConnected();
                return result;
            } catch (err: unknown) {
                if (err instanceof Prisma.PrismaClientKnownRequestError && err.code === 'P1001') {
                    this.setDisconnected();
                } else if (err instanceof Prisma.PrismaClientInitializationError) {
                    this.setUnableToConnect();
                }
                throw err;
            }
        });
    }

    private setConnectionStatus(status: ConnectionStatus): void {
        this.lastCheckedAt = this.clock.getNowDate();
        this.connectionStatus = status;
    }
}
