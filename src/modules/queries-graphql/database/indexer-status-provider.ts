import { singleton } from 'tsyringe';

import { BackgroundWorker } from '../../../bootstrap/background-worker';
import { Clock, injectLogger, Logger } from '../../../utils';
import BlockRepository from '../repositories/block-repository';

export enum IndexerStatus {
    NOT_CHECKED = 'NOT_CHECKED',
    CHECKED = 'CHECKED',
    UNABLE_TO_QUERY = 'UNABLE_TO_QUERY',
}

const LastBlockCheckingInterval = 10000; // 10 seconds

@singleton()
export default class IndexerStatusProvider implements BackgroundWorker {
    name = 'IndexerStatusWorker';

    status = IndexerStatus.NOT_CHECKED;
    lastBlockLevel: number | null = null;
    lastCheckedAt?: Date;

    private timeoutHandle?: NodeJS.Timeout;

    constructor(
        private readonly clock: Clock,
        @injectLogger(IndexerStatusProvider) private readonly logger: Logger,
        private readonly blockRepository: BlockRepository,
    ) {}

    start(): void {
        void this.checkLastBlock();
    }

    stop(): void {
        if (this.timeoutHandle) {
            clearTimeout(this.timeoutHandle);
        }
    }

    private async checkLastBlock(): Promise<void> {
        try {
            const lastBlock = await this.blockRepository.findLastBlock();
            if (!lastBlock) {
                this.status = IndexerStatus.UNABLE_TO_QUERY;
                this.lastBlockLevel = null;
            } else {
                this.status = IndexerStatus.CHECKED;
                this.lastBlockLevel = lastBlock.level;
            }
        } catch (error: unknown) {
            this.lastBlockLevel = null;
            this.status = IndexerStatus.UNABLE_TO_QUERY;
            this.logger.logError('Failed to query last indexed block from the database. {error}', {
                error,
            });
        }
        this.lastCheckedAt = this.clock.getNowDate();
        this.timeoutHandle = setTimeout(() => void this.checkLastBlock(), LastBlockCheckingInterval);
    }
}
