import { DataSource } from 'apollo-datasource';
import DataLoader from 'dataloader';
import { injectable, InjectionToken, Lifecycle, scoped } from 'tsyringe';

import { isNotNullish, isNullish, Nullish } from '../../../utils';
import BigmapRawRepository from '../repositories/bigmap-raw-repository';
import { FindManyBigmapsResult } from '../repositories/bigmap-repository';

export const bigmapDataSourceDIToken: InjectionToken<BigmapDataSource> = 'bigmapDataSource';

@injectable()
@scoped(Lifecycle.ContainerScoped)
export class BigmapDataSource extends DataSource {
    private readonly loader: DataLoader<bigint, Nullish<FindManyBigmapsResult>>;

    constructor(private readonly bigmapRepository: BigmapRawRepository) {
        super();
        this.loader = new DataLoader(async (keys) => this.loadBigmaps(keys));
    }

    didEncounterError(error: Error): void {
        throw error;
    }

    async get(id: bigint): Promise<FindManyBigmapsResult> {
        const bigmap = await this.loader.load(BigInt(id));
        if (isNullish(bigmap)) {
            throw new Error(`Could not load Bigmap for id: ${id}`);
        }
        return bigmap;
    }

    private async loadBigmaps(ids: readonly bigint[]): Promise<Nullish<FindManyBigmapsResult>[]> {
        const bigmaps = await this.bigmapRepository.findManyBigmaps(
            {
                filter: {
                    ids: [...ids],
                },
            },
            { bypassPageSizeLimit: true },
        );
        return ids.map((id) => bigmaps.data.find((bigmap) => isNotNullish(bigmap.id) && BigInt(bigmap.id) === id));
    }
}
