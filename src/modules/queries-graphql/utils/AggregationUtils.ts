import { PrismaClient } from '@prisma/client';
import { inject, singleton } from 'tsyringe';

import { isNullish, Nullish } from '../../../utils';
import { prismaClientDIToken } from '../database/prisma';
import { AggregationInput, AggregationResult } from '../helpers/aggregation';
import { QueryInfo } from '../helpers/query-builder';
import { ValueOrArray } from '../helpers/types';

@singleton()
export default class AggregationUtils {
    constructor(@inject(prismaClientDIToken) private readonly prisma: PrismaClient) {}

    async buildAndRunAggregationsQuery<TOrderByField extends string>({
        args: { aggregationInput },
        query,
        requiredTables,
        columns,
    }: {
        args: { aggregationInput?: AggregationInput };
        query: QueryInfo<unknown, TOrderByField>;
        requiredTables: Nullish<ValueOrArray<string>>;
        requiredEquivalentColumnGroupNames?: Nullish<ValueOrArray<string>>;
        columns: string[];
    }): Promise<AggregationResult> {
        if (aggregationInput?.totalCount !== true) {
            return {};
        }
        const aggregationQuery = query.clone<AggregationResult>();
        aggregationQuery.addSelectColumns(requiredTables, columns);
        const sql = aggregationQuery.build();
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        const aggregateRows = await this.prisma.$queryRawUnsafe<AggregationResult[]>(sql, ...query.values);
        if (aggregateRows.length > 1) {
            throw new Error(
                `The following aggregations query is expected to return exactly one row, but returned ${aggregateRows.length}:\n ${sql}`,
            );
        }
        const aggregates = aggregateRows[0];
        if (isNullish(aggregates)) {
            throw new Error(
                `The following aggregations query is expected to return exactly one row, but returned none:\n ${sql}`,
            );
        }
        return aggregates;
    }
}
