import * as rpc from '@taquito/rpc';

import { isWhiteSpace } from '../../../utils';

/** Data downloaded from Tezos node mempool monitor. */
export interface RpcMempoolOperationGroup {
    protocol: string;
    branch: string;
    signature: string;

    // Slightly adapted to make it compatible with block RPC -> common deserialization is easier.
    contents: (rpc.OperationContents | rpc.OperationContentsAndResult)[];
    chain_id?: undefined;
    hash?: undefined;
}

export function getRpcSignature(group: RpcMempoolOperationGroup): string {
    const operation = group.contents[0];
    if (!operation) {
        throw new Error('Unexpectedly, there are no operations in the mempool operation group.');
    }

    // EndorsementWithSlot uses a dummy signature -> wouldn't deduplicate the group -> fallback to inner signature.
    const signature =
        operation.kind === rpc.OpKind.ENDORSEMENT_WITH_SLOT ? operation.endorsement.signature : group.signature;

    if (isWhiteSpace(signature)) {
        throw new Error('Unexpectedly, there is no signature on the first operation in the mempool operation group.');
    }
    return signature;
}
