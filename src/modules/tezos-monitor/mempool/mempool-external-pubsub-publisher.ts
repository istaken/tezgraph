import { singleton } from 'tsyringe';

import { getSignature, MempoolOperationGroup, OperationOrigin } from '../../../entity/subscriptions';
import { ExternalPubSub, externalTriggers, ItemProcessor } from '../../../utils';
import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';

/** Publishes GraphQL mempool operation groups to external PubSub. */
@singleton()
export class MempoolExternalPubSubPublisher implements ItemProcessor<MempoolOperationGroup> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(MempoolExternalPubSubPublisher) private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
    ) {}

    async processItem(mempoolGroup: MempoolOperationGroup): Promise<void> {
        this.logger.logInformation('Publishing mempool operation group with {signature} and {operationCount}.', {
            signature: getSignature(mempoolGroup),
            operationCount: mempoolGroup.length,
        });
        this.metrics.tezosMonitorOperationCount.inc({ origin: OperationOrigin.mempool }, mempoolGroup.length);
        await this.externalPubSub.publish(externalTriggers.mempoolOperationGroups, mempoolGroup);
    }
}
