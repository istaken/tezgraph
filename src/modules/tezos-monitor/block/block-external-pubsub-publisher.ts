import { singleton } from 'tsyringe';

import { BlockNotification, OperationOrigin } from '../../../entity/subscriptions';
import { ExternalPubSub, externalTriggers, ItemProcessor } from '../../../utils';
import { injectLogger, Logger } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';

/** Publishes GraphQL blocks to external PubSub. */
@singleton()
export class BlockExternalPubSubPublisher implements ItemProcessor<BlockNotification> {
    constructor(
        private readonly externalPubSub: ExternalPubSub,
        @injectLogger(BlockExternalPubSubPublisher) private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
    ) {}

    async processItem(block: BlockNotification): Promise<void> {
        this.logger.logInformation('Publishing block {hash} with {operationCount}.', {
            hash: block.hash,
            operationCount: block.operations.length,
        });
        this.metrics.tezosMonitorBlockLevel.set(block.header.level);
        this.metrics.tezosMonitorOperationCount.inc({ origin: OperationOrigin.block }, block.operations.length);
        await this.externalPubSub.publish(externalTriggers.blocks, block);
    }
}
