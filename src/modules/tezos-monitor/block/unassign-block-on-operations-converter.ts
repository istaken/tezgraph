import { singleton } from 'tsyringe';

import { BlockNotification } from '../../../entity/subscriptions';
import { ItemConverter } from '../../../utils';

/** Un-assigns block on operations because it's a circular reference and can't be JSON-stringified. */
@singleton()
export class UnassignBlockOnOperationsConverter implements ItemConverter<BlockNotification, BlockNotification> {
    convert(block: BlockNotification): BlockNotification {
        return {
            ...block,
            operations: block.operations.map((operation) => ({
                ...operation,
                block: null,
            })),
        };
    }
}
