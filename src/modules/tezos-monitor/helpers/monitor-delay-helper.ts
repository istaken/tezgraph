import { assert } from 'ts-essentials';
import { injectable } from 'tsyringe';

import { SleepHelper } from '../../../utils/time/sleep-helper';
import { TezosMonitorEnvConfig } from '../tezos-monitor-env-config';

@injectable() // B/c it's used by multiple places.
export class MonitorDelayHelper {
    private isInInfiniteLoopFlag = false;
    private delays: number[] = [];
    private retryIndex = 0;

    constructor(private readonly envConfig: TezosMonitorEnvConfig, private readonly sleepHelper: SleepHelper) {
        this.reset();
    }

    get isInInfiniteLoop(): boolean {
        return this.isInInfiniteLoopFlag;
    }

    async wait(): Promise<[delay: number, index: number]> {
        const delay = this.delays[0];
        assert(delay, 'The last delay should not be removed b/c it is used indefinitely.');

        if (this.delays.length === 1) {
            this.isInInfiniteLoopFlag = true;
        } else {
            this.delays.shift();
        }

        await this.sleepHelper.sleep(delay);
        this.retryIndex++;
        return [delay, this.retryIndex];
    }

    reset(): void {
        this.delays = this.envConfig.rpcMonitorReconnectDelaysMillis.slice();
        this.isInInfiniteLoopFlag = false;
        this.retryIndex = 0;
    }
}
