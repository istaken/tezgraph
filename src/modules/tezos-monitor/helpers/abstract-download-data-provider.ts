import { AbortSignal } from 'abort-controller';
import { trimEnd, trimStart } from 'lodash';

import { AsyncIterableProvider } from '../../../utils';
import { Logger } from '../../../utils/logging';
import { TezosMonitorClient } from './tezos-monitor-client';

export abstract class AbstractDownloadDataProvider<TItem> implements AsyncIterableProvider<TItem> {
    constructor(
        private readonly monitorClient: TezosMonitorClient,
        private readonly logger: Logger,
        readonly url: string,
    ) {}

    async *iterate(abortSignal: AbortSignal): AsyncIterableIterator<TItem> {
        for await (const data of this.monitorClient.iterateMonitor(this.url, abortSignal)) {
            const items = this.tryCastData(data);

            if (items) {
                yield* items;
            } else {
                this.logger.logError(
                    `Data from {url} is invalid (nullish, no ID etc.). Related subscriptions won't be published. {data}`,
                    {
                        url: this.url,
                        data,
                    },
                );
            }
        }
    }

    protected abstract tryCastData(data: unknown): TItem[] | undefined;
}

/** Resolves trailing/leading/missing slashes. */
export function appendUrl(baseUrl: string, relativeUrl: string): string {
    return `${trimEnd(baseUrl, '/')}/${trimStart(relativeUrl, '/')}`;
}
