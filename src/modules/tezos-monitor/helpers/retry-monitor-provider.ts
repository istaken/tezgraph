import { AbortSignal } from 'abort-controller';
import { DependencyContainer, InjectionToken } from 'tsyringe';

import { AsyncIterableProvider } from '../../../utils';
import { isAbortError } from '../../../utils/abortion';
import { ComponentHealthState } from '../../../utils/health/component-health-state';
import { HealthStatus } from '../../../utils/health/health-check';
import { Logger, LoggerFactory, LogLevel } from '../../../utils/logging';
import { MetricsContainer } from '../../../utils/metrics/metrics-container';
import { MonitorDelayHelper } from './monitor-delay-helper';

export const healthMessages = {
    healthy: 'Everything is fine.',
    temporaryFailure: 'Monitoring temporarily failed. It will reconnect.',
    persistentFailure: 'Monitoring failed multiple times. Now trying with the last delay indefinitely.',
} as const;

/**
 * Retries Tezos monitor connection on an end or an error because it's quite unstable.
 * This makes it infinite until return() on the iterator or underlying abort signal is aborted.
 * Retries are delayed according to config and the last delay also fails the related health check.
 */
export class RetryMonitorProvider<TItem> implements AsyncIterableProvider<TItem> {
    constructor(
        private readonly innerProvider: AsyncIterableProvider<TItem>,
        private readonly healthState: ComponentHealthState,
        private readonly delayHelper: MonitorDelayHelper,
        private readonly logger: Logger,
        private readonly metrics: MetricsContainer,
    ) {}

    async *iterate(abortSignal: AbortSignal): AsyncIterableIterator<TItem> {
        // It can be cancelled by calling return() on iterator.
        // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
        while (true) {
            // eslint-disable-line @typescript-eslint/no-unnecessary-condition
            try {
                for await (const item of this.innerProvider.iterate(abortSignal)) {
                    this.setHealthy();
                    yield item;
                }
                throw new Error('Connection ended which should not happen for a monitor.');
            } catch (error: unknown) {
                if (isAbortError(error)) {
                    this.logger.logInformation('Monitoring was aborted.');
                    throw error;
                }

                if (this.delayHelper.isInInfiniteLoop) {
                    this.setError(HealthStatus.Unhealthy, error, LogLevel.Error, healthMessages.persistentFailure);
                    this.metrics.tezosMonitorInfiniteLoopCount.inc();
                } else {
                    this.setError(HealthStatus.Degraded, error, LogLevel.Debug, healthMessages.temporaryFailure);
                    this.metrics.tezosMonitorReconnectCount.inc();
                }

                const [delay, index] = await this.delayHelper.wait();

                this.metrics.tezosMonitorRetryCount.inc({
                    endpoint: this.healthState.name,
                    delay: delay.toString(),
                    index: index.toString(),
                });
            }
        }
    }

    private setHealthy(): void {
        this.healthState.set(HealthStatus.Healthy, healthMessages.healthy);
        this.delayHelper.reset();
    }

    private setError(status: HealthStatus, error: unknown, logLevel: LogLevel, reason: string): void {
        this.logger.log(logLevel, `${reason} {error}`, { error });
        this.healthState.set(status, { reason, error });
    }
}

/** See corresponding provider. */
export function createRetryMonitorProvider<TItem>(
    diContainer: DependencyContainer,
    innerProviderDIToken: InjectionToken<AsyncIterableProvider<TItem>>,
    healthStateDIToken: InjectionToken<ComponentHealthState>,
): AsyncIterableProvider<TItem> {
    const healthState = diContainer.resolve(healthStateDIToken);
    return new RetryMonitorProvider(
        diContainer.resolve(innerProviderDIToken),
        healthState,
        diContainer.resolve(MonitorDelayHelper),
        diContainer.resolve(LoggerFactory).getLogger(`Retry${healthState.name}`),
        diContainer.resolve(MetricsContainer),
    );
}
