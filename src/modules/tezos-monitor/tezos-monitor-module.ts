import { DependencyContainer } from 'tsyringe';

import { numberOfLastBlocksToDeduplicate, numberOfLastMempoolGroupsToDeduplicate } from '../../entity/subscriptions';
import { OperationGroupConverter } from '../../rpc/converters/operation-group-converter';
import {
    ConvertItemProcessor,
    DeduplicateItemProcessor,
    FreezeItemProcessor,
    registerAsyncIterableWorker,
} from '../../utils';
import { healthChecksDIToken } from '../../utils/health/health-check';
import { Module, ModuleName } from '../module';
import { registerTezosRpcHealthWatcher, RequirePubSubDependency } from '../module-helpers';
import { BlockExternalPubSubPublisher } from './block/block-external-pubsub-publisher';
import { BlockMonitorHealth, DownloadBlocksProvider } from './block/download-blocks-provider';
import { RpcMonitorBlockConverter } from './block/rpc-monitor-block-converter';
import { RpcMonitorBlockHeader } from './block/rpc-monitor-block-header';
import { RpcMonitorBlockHeaderInfoProvider } from './block/rpc-monitor-block-header-info-provider';
import { UnassignBlockOnOperationsConverter } from './block/unassign-block-on-operations-converter';
import { createRetryMonitorProvider } from './helpers/retry-monitor-provider';
import { DownloadMempoolProvider, MempoolMonitorHealth } from './mempool/download-mempool-provider';
import { MempoolExternalPubSubPublisher } from './mempool/mempool-external-pubsub-publisher';
import { getRpcSignature } from './mempool/rpc-mempool-operation-group';
import { RpcMempoolOperationGroupInfoProvider } from './mempool/rpc-mempool-operation-group-info-provider';

export const tezosMonitorModule: Module = {
    name: ModuleName.TezosMonitor,

    dependencies: [new RequirePubSubDependency({ reason: 'notifications can be published to a PubSub' })],

    initialize(diContainer: DependencyContainer): void {
        registerBlockPipeline(diContainer);
        registerMempoolPipeline(diContainer);
        registerTezosRpcHealthWatcher(diContainer);
    },
};

function registerBlockPipeline(diContainer: DependencyContainer): void {
    registerAsyncIterableWorker<RpcMonitorBlockHeader>(
        diContainer,
        'BlockPublisher',
        (c) => createRetryMonitorProvider(c, DownloadBlocksProvider, BlockMonitorHealth),
        (c) =>
            new DeduplicateItemProcessor(
                numberOfLastBlocksToDeduplicate,
                (b) => b.hash,
                new ConvertItemProcessor(
                    c.resolve(RpcMonitorBlockConverter),
                    new ConvertItemProcessor(
                        c.resolve(UnassignBlockOnOperationsConverter),
                        new FreezeItemProcessor(c.resolve(BlockExternalPubSubPublisher)),
                    ),
                ),
            ),
        RpcMonitorBlockHeaderInfoProvider,
    );
    diContainer.register(healthChecksDIToken, { useToken: BlockMonitorHealth });
}

function registerMempoolPipeline(diContainer: DependencyContainer): void {
    registerAsyncIterableWorker(
        diContainer,
        'MempoolPublisher',
        (c) => createRetryMonitorProvider(c, DownloadMempoolProvider, MempoolMonitorHealth),
        (c) =>
            new DeduplicateItemProcessor(
                numberOfLastMempoolGroupsToDeduplicate,
                getRpcSignature,
                new ConvertItemProcessor(c.resolve(OperationGroupConverter), c.resolve(MempoolExternalPubSubPublisher)),
            ),
        RpcMempoolOperationGroupInfoProvider,
    );
    diContainer.register(healthChecksDIToken, { useToken: MempoolMonitorHealth });
}
