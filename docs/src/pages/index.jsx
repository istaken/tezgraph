import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';


function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
            <div className='row'>
                <div className={clsx('text--left col col--6')}>
                    <h1>Create Richer and Faster Applications on premise and in the cloud</h1>
                    <p>TezGraph is a simple, highly compatible, and reliable open-source API that provides access to historic and real-time Tezos blockchain data with the convenience of GraphQL.</p>
                    <div className={styles.buttons}>
                    <Link
                        className="button button--secondary button--lg"
                        to="/docs/">
                        Get Started
                    </Link>
                    </div>
                </div>
                <div className={clsx('col col--6 header-image')}>
                    <img src='/img/homepage-illustration.svg' alt='diagram' height="80%"/>
                </div>
            </div>

            <div className='row'>
                <div className={clsx('text--left col col--6 ')}>
                    <div className="homepage-left-list">
                    <h1>Features</h1>
                    <ul>
                        <li>Near real-time notifications</li>
                        <li>Efficiently query accounts and manager operations</li>
                        <li>Use out of the box Indexer node with Public API</li>
                        <li>Set up Indexer node on premise</li>
                        <li>Continous integration and delivery</li>
                    </ul>
                    </div>
                </div>
                <div className={clsx('text--left col col--6')}>
                    <div className="homepage-list-highlight">
                        <h1>Dev Benefits</h1>
                        <ul>
                            <li>Indexed blockchain data at your fingertips</li>
                            <li>Simple, discoverable and efficient API</li>
                            <li>Compatible with major programming languages</li>
                            <li>Helps build richer end-user experiences</li>
                            <li>Faster development cycle and quicker adoption</li>
                    </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
    pageClassName="homepage"
      title={siteConfig.title}
      description="TezGraph is a GraphQL API that provides access to historic and real-time data from the Tezos blockchain. TezGraph is opensource (Apache-2.0), offered as a public API, and packaged to operate privately.">
      <HomepageHeader />

      <main className="split-color">

      <div className="container">
            <div className='row item shadow--tl floating-box'>

                <div className={clsx('text--left col col--8')}>
                    <h2>Get started with TezGraph</h2>
                    <p>Check out the Quick Start Documentation to learn more about getting started with TezGraph.</p>
                </div>
                <div className={clsx('text--centre col col--4')}>
                <Link
                        className="button button--secondary button--lg"
                        to="/docs/">
                        Get Started
                    </Link>
                </div>

            </div>
        </div>

      </main>
    </Layout>
  );
}
