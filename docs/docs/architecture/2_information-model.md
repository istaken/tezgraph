---
title: 'Information Model'
metaTitle: 'Information Model | TezGraph'
metaDescription: 'This is the meta description'
---

# Overview

This document provides an Information Model for TezGraph product.

While [Tezos glossary](https://tezos.gitlab.io/user/glossary.html) was used as a foundation for definitions, this document clarifies Tezos terms further and adds new terms applicable to TezGraph.

# Tezos entities

![image info](./images/informationModel/1_account.png)

**Account** (aka Contract) is an agreement between a blockchain participant and a blockchain that is uniquely identified through its address and may carry a balance.

**Implicit Account** is a type of Account associated with a person, an institution, or its representative. Implicit Account is identifiable by address format: tz[1, 2, 3]&lt;public key hash&gt;.

**Originated Account** (aka Smart Contracts, KT1&lt;...&gt;) is a type of Account associated with a program. A program is a Michelson script stored and executed on the blockchain to update the blockchain's context. Originated Account is identifiable by address format: KT1&lt;public key hash&gt;.

![image info](./images/informationModel/2_operation.png)

**Operation** is an action that changes the state of the blockchain data.

**Block** is a collection of Operations. Operations are grouped into Blocks so that the chain progresses in batches.

**Cycle** is a set of consecutive blocks. Cycles are used as a unit of “time” in the blockchain.

**Baking rights** are an allowance to create and progress a number of new Blocks. Baking Rights are limited to Blocks assigned within a Cycle.

**Endorsement rights** are an allowance to validate a number of new Blocks. Endorsement rights are limited to the Blocks assigned within a Cycle.

![image info](./images/informationModel/3_delegate.png)

**Delegate** is a type of Implicit Account registered for the opportunity to get Baking Rights and/or Endorsement Rights assigned for the upcoming Cycle.

Delegate is registered by creating a transaction to itself.

Other Implicit Account can give their Baking and Endorsement rights to a Delegate through fund lending. In this case, the Delegate does not control the funds from that Implicit Account.

**Delegate Registry** is a collection of current Delegates.

![image info](./images/informationModel/4_baker.png)

**Baker** is a Delegate with Baking rights. The amount of Baking rights is assigned based on the Delegate's seed_nonce, the total balance of tez (# of rolls), and timeliness/legitimacy of past operations. Baker can also submit proposals and vote.

![image info](./images/informationModel/5_endorser.png)

**Endorser** is a Delegate with Endorsement rights. The amount of Endorsement rights is assigned based on the Delegate's seed_nonce, the total balance of tez (# of rolls), and timeliness/legitimacy of past operations.

**Voting Cycle** is a time period during which voting on a proposal is accepted.

# TezGraph Model

## Accounts

### Implicit Accounts

![image info](./images/informationModel/6_implicit_account.png)

![image info](./images/informationModel/7_implicit_state.png)

![image info](./images/informationModel/8_delegate_state.png)

Implicit Account can be in one of two states: Active and Inactive. Once the Implicit Account is revealed, it becomes Active. Once its balance is drained, it becomes Inactive.

Implicit Account also can be registered as a Delegate, in which case it becomes an Active Delegate. It can be automatically unregistered and become an Inactive Delegate.

You can see both Active and Inactive Delegates referred to in Operations.

### Originated Accounts

TezGraph uses the definition of Originated Account above.

## Operations

Operations on Tezos blockchain can be grouped into Account Operations, Validation Operation, and Voting Operations.

**Account Operations** include Reveal, Activation, Transaction, Delegation, Origination.

**Validation Operations** include Endorsement, Double Baking Evidence, and Double Endorsement Evidence.

**Voting Operations** include Ballot, Proposal.

**IMPORTANT NOTE:** In the current TezGraph release, all operations are available for subscription, but only Reveal, Transaction, Delegation, Origination, and Endorsement are available for querying.

### Account Operations

**Account Operation** is an Operation targeting to change the state of a particular Account.

![image info](./images/informationModel/9_reveal.png)

**Reveal** is an Operation that publicly reveals the address of the Implicit Account. It usually happens before any other Operation can be performed on the Implicit Account.

![image info](./images/informationModel/10_activate.png)

**Activation** is an Operation that reactivates an Account after deactivation. An account gets deactivated when it is drained.

![image info](./images/informationModel/11_transaction.png)

**Transaction** is an Operation to transfer tokens from one Implicit Account to another or execute an Originated Account (smart contract).

![image info](./images/informationModel/12_delegation.png)

**Delegation** is an Operation in which an Implicit Account's balance is lent to a Delegate. This increases the Delegate’s balance and, consequently, its Baking and Endorsement rights.

Note, Delegation is an Operation of lending to an Implicit Account. This Implicit Account does not become a Delegate due to Delegation Operation, but it has to be a Delegate (see definition above) in the first place.

![image info](./images/informationModel/13_origination.png)

**Origination** is an Operation to register a Smart Contract (aka Originated Account). The smart contract must be originated before execution (see Transaction).

### Validation Operations

**Validation Operation** is an Operation targeting validation of creation and endorsement of Blocks.

![image info](./images/informationModel/14_endorsement.png)

**Endorsement**/**Endorsement with Slot** is an Operation confirming the validity and legitimacy of the Block being created and propagated on the blockchain. A Delegate that emits an Endorsement is deemed the Endorser \*\*\*\*of the Block.

Since Endorsement rights are assigned randomly, a Baker might be allowed to endorse a given Block more than once.

![image info](./images/informationModel/15_double_baking_evidence.png)

**Pre-endoresement** is an Operation happening before final Endorsement operation. This operation was introduced to support Tenderbake as part of Ithaca protocol update.

**Double Baking Evidence** is an Operation that proves an intention to create multiple identical Blocks by a Baker.

![image info](./images/informationModel/16_double_endorsement_evidence.png)

**Double Endorsement Evidence** is an Operation that proves multiple Endorsements for a Block.

![image info](./images/informationModel/17_seed_nonce_revelation.png)

**Seed Nonce Revelation** is an Operation executed for a Delegate to reveal the cryptographic nonce they committed in their Blocks from previous Cycles. This information is used to calculate Baking and Endorsement rights for the next Cycle.

### Voting Operations

**Voting Operation** is an Operation that executes a Delegate's voting power.

![image info](./images/informationModel/18_proposal.png)

**Proposal** is a Voting Operation executed by a Delegate to submit and/or upvote proposals to amend the Tezos protocol.

![image info](./images/informationModel/19_ballot.png)

**Ballot** is a Voting Operation submitted by a Delegate to vote for a proposal in a given Voting Cycle.
