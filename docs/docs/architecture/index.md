---
title: 'Architecture'
metaTitle: 'Architecture | TezGraph'
metaDescription: 'This is the meta description'
---

**Components:** [Components](./components)

**Information Model:** [Information Model](./information-model)
