---
title: 'Monitoring and Observability'
metaTitle: 'Monitoring and Observability | TezGraph'
metaDescription: 'This page contains the Tezgraph Accounts documentation and examples.'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Endpoints

### /health Endpoint

At the `/health` endpoint, the health of the Tezgraph components are

On this page, you will find a the version details of the Tezgraph instance as well as the health details of the Tezgraph components.

The health details are reported for the following components:
- BlockPublisher
    - Reports on the health of the BlockPublisher, responsible for publishing block data.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - The last block published along with its hash, level, and timestamp.
        - An error if available.
- BlockSubscriber
    - Reports on the health of the BlockSubscriber, responsible for block subscriptions.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - The last subscribable block and its hash, level, and timestamp.
        - An error if available.
- BlockTezosMonitor
    - Reports on the health of the BlockTezosMonitor, responsible for downloading block data for the BlockPublisher.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - An error if available.
- MempoolPublisher
    - Reports on the health of the MempoolPublisher, responsible for publishing mempool operation data.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - The last mempool operation batch published along with its signature and the operation count.
        - An error if available.
- MempoolSubscriber
    - Reports on the health of the MempoolSubscriber, responsible for mempool operation subscriptions.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - The last subscribable mempool operation batch and its signature and the operation count.
        - An error if available.
- MempoolTezosMonitor
    - Reports on the health of the MempoolTezosMonitor, which is responsible for downloading blocks data for the BlockPublisher.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The reason for the health status.
        - An error if available.
- TezosRpcWatcher
    - Reports on the health of the TezosRpcWatcher, responsible for checking the connection with the RPC.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The head block data retrieved from the RPC, including its hash, level, chainId, and timestamp.
        - The timestamp of when Tezgraph retrieved this head block data.
    - An error if available.
- DatabaseHealth
    - Reports on the health of the database.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The Tezos-Indexer version used to index the database.
        - The chainId of the Tezos network being indexed to the database.
    - An error if available.
- IndexerHealth
    - Reports on the health of the indexer.
    - Under this block, you may find the details on:
        - The health status of this component.
        - The last indexed block level.
        - The number lag in levels between the last indexed block and the current head block of the chain.
    - An error if available.


### /check Endpoint

The `/check` endpoint provides a quick one word answer to whether the Tezgraph instance is health. If any component on the `/health` page is reported as unhealthy, the `/check` endpoint will return `Unhealthy`. If everything is healthy, it will return `OK`.

### /metrics Endpoint

The `/metrics` endpoint provides measureable details on the processes of Tezgraph. All details on the page come with a description explaining what it is measuring.

The metrics can also be connected to and used by Prometheus and Grafana to provide

The metrics can be connected to Prometheus and Grafana for querying and visual representations.


---


# Prometheus and Grafana

If you are new to Prometheus and Grafana, you can find a getting started guide [here](https://grafana.com/docs/grafana/next/getting-started/get-started-grafana-prometheus/).

If you are looking for a quick way to spin up Prometheus and Grafana using Docker-Compose, try using [dockprom](https://github.com/stefanprodan/dockprom).

Once the services are set up, add a scrape job config for Tezgraph to the Prometheus configurations.

Example Configuration:
```
promtheus.yml

global:
  scrape_interval:     15s
  evaluation_interval: 15s

scrape_configs:
  - job_name: 'tezgraph'
    scrape_interval: 10s
    static_configs:
      - targets: ['localhost:3000']
```

### Grafana Dashboard

To get you started quickly on the Grafana dashboard for Tezgraph metrics, [here](https://gitlab.com/tezgraph/tezgraph/-/blob/master/grafana/dashboards/tezgraph.json) is a JSON model that can be imported to Grafana.
- Instructions on importing this JSON model can be found [here](https://gitlab.com/tezgraph/tezgraph/-/blob/master/grafana/dashboards/tezgraph.json)

#### Grafana Dashboard Panels

**Uptime**
- Displays the uptime of the Tezgraph instance.
- Query: `tezgraph_uptime {instance=~"${env}"}`

**Unhealthy Components**
- Displays the number of unhealthy components aggregated.
- Query: `unhealthy_components{instance=~"${env}",name="aggregated"}`

**Warnings**
- Displays the number of warning logs.
- Query: `increase(logger_logs_total{instance=~"${env}",level="warning"}[$__range])`

**Indexer**
- Displays the number of unhealthy components under IndexerHealth.
- Query: `unhealthy_components{instance=~"${env}",name="IndexerHealth"}`

**Block Mon**
- Displays the number of unhealthy components under BlockTezosMonitor.
- Query: `unhealthy_components{instance=~"${env}",name="BlockTezosMonitor"}`

**Mempool Mon**
- Displays the number of unhealthy components under MempoolTezosMonitor.
- Query: `unhealthy_components{instance=~"${env}",name="MempoolTezosMonitor"}`

**Block Pub**
- Displays the number of unhealthy components under BlockPublisher.
- Query: `unhealthy_components{instance=~"${env}",name="BlockPublisher"}`

**Block Sub**
- Displays the number of unhealthy components under BlockSubscriber.
- Query: `unhealthy_components{instance=~"${env}",name="BlockSubscriber"}`

**Mempool Pub**
- Displays the number of unhealthy components under MempoolPublisher.
- Query: `unhealthy_components{instance=~"${env}",name="MempoolPublisher"}`

**Mempool Sub**
- Displays the number of unhealthy components under MempoolSubscriber.
- Query: `unhealthy_components{instance=~"${env}",name="MempoolSubscriber"}`

**Health**
- Displays the total number of unhealthy components.
- Query: `sum(unhealthy_components{instance=~"${env}"})`

**Block Level**
- Displays the last block level processed by tezos monitor.
- Query: `tezos_monitor_block_level {instance=~"${env}"}`

**Http Connections**
- Displays the number of active http connections.
- Query: `http_server_connections_active {instance=~"${env}"}`

**Errors**
- Displays the number of error logs.
- Query: `increase(logger_logs_total{instance=~"${env}",level="error"}[$__range])`

**Errors Heatmap**
- A heatmap displaying the errors that occurred.
- Query: `increase(logger_logs_total{instance=~"${env}",level="error"}[$interval])`

**Unhealthy Components Time**   Series
- A time series graph displaying the times when components were unhealthy.
- Query: `unhealthy_components{instance=~"${env}", name!="aggregated"}`

**RPC Health**
- A state timeline graph displaying the times when components were unhealthy.
- Query: `unhealthy_components{instance=~"${env}"}`

**Reconnects**
- Displays the number of tezos monitor reconnects.
- Query: `ceil(rate(tezos_monitor_reconnects_total{instance=~"${env}"}[$interval]))`

**Infinite Loops**
- Displays the number of tezos monitor infinite retry loops.
- Query: `ceil(rate(tezos_monitor_infinite_loops_total{instance=~"${env}"}[$interval]))`

**Mempool Retry**
- Displays the number of tezos monitor call retries for MempoolTezosMonitor.
- Query: `increase(tezos_monitor_retries_total {job=~"${env}",endpoint="MempoolTezosMonitor"}[$interval])`

**Block Retry**
- Displays the numbers of tezos monitor call retries for BlockTezosMonitor.
- Query: `increase(tezos_monitor_retries_total {instance=~"${env}",endpoint="BlockTezosMonitor"}[$interval])`

**Mempool Retry**
- A heatmap displaying the tezos monitor call retries for MempoolTezosMonitor.
- Query: `tezos_monitor_retries_total {instance=~"${env}",endpoint="MempoolTezosMonitor"}`

**Subscriptions Rate**
- The rate of subscriptions.
- Query: `rate(subscriptions_total {instance=~"${env}"}[$interval])`

**Notifications Sent Rate**
- The rate of notifications sent.
- Query: `rate(graphql_subscription_method_total{instance=~"${env}"}[$interval])`

**Subscriptions with Mempool**   Rate
- The rate of subscriptions with mempool.
- Query: `rate(graphql_subscription_method_total {instance=~"${env}", mempool="true"}[$interval])`

**Subscriptions with Replay**   from Block Rate
- The rate of graphql_subscriptions with replay.
- Query: `rate(graphql_subscription_method_total {job=~"${env}", replay="true"}[$interval])`

**Active Subscriptions**
- Displays the number of active subscriptions.
- Query: `subscriptions_active {instance=~"${env}"}`

**Total Subscriptions**
- Displays the number of total subscriptions.
- Query: `subscriptions_total {instance=~"${env}"}`

**Total Notifications Sent**
- Displays the number of total notifications sent.
- Query: `graphql_subscription_method_total{instance=~"${env}"}`

**PubSub Processing Relative**   Duration
- The duration it took to execute all processing from block receive (ms).
- Query: `histogram_quantile(0.95, sum(rate(process_notification_duration_relative_bucket{instance=~"${env}"}[$interval])) by (le))`

**PubSub Processing Absolute**   Duration
- The duration it took to execute all processing from block create (ms).
- Query: `histogram_quantile(0.95, sum(rate(process_notification_duration_absolute_bucket{instance=~"${env}"}[$interval])) by (le))`

**RPC Client GetBlock**   Duration
- The duration it took to execute RPC client get endpoint (ms).
- Query: `histogram_quantile(0.95, sum(rate(rpc_client_get_duration_bucket{endpoint="getBlock",instance=~"${env}"}[$interval])) by (le))`

**Block Operations Processed**
- Displays the number of block operations processed.
- Query: `increase(tezos_monitor_operations_total{instance=~"${env}",source="block"}[$interval])`

**Mempool Operations Processed**
- Displays the number of mempool operations processed.
- Query: `increase(tezos_monitor_operations_total{instance=~"${env}",source="mempool"}[$interval])`

**Query Counter**
- Displays the number of times specified queries were used.
- Query: `delta(graphql_query_method_total{instance=~"${env}", service="tezgraph"}[$__range])`

**Query Field Counter**
- Displays the number of times the specified query fields were requested.
- Query: `delta(graphql_query_type_field_total{instance="${env}", job="tezgraph"}[$__range])`

