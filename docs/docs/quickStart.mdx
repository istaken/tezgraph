---
title: 'Quick Start'
metaTitle: 'Quick Start | TezGraph'
metaDescription: 'Use the publicly available TezGraph instance'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Introduction

This guide serves to help you start using a publicly available instance of TezGraph. You can also consider hosting your own instance. The instructions for hosting your own instance can be found under the ["Self-Hosting" section](./self-Hosting/index.mdx).

Also, in this guide, you will access TezGraph through a web user interface. It makes GraphQL calls behind the scene. For your actual usage, you might need to make GraphQL queries/subscription calls using an existing GraphQL library for your favourite programming language or even make calls directly using HTTP.

---

# Public TezGraph Links

It is not necessary to set up TezGraph locally. You can use our public TezGraph servers linked here.

<table>
    <tbody>
        <tr>
            <th>Mainnet</th>
            <td>
                <a href="https://mainnet.tezgraph.ecadinfra.com/graphql">https://mainnet.tezgraph.ecadinfra.com/graphql</a>
                <br />
                <a href="https://tezgraph-mainnet.tezoslive.io/graphql">
                    https://tezgraph-mainnet.tezoslive.io/graphql
                </a>
            </td>
        </tr>
        <tr>
            <th>Jakartanet</th>
            <td>
                <a href="https://jakartanet.tezgraph.ecadinfra.com/graphql">https://jakartanet.tezgraph.ecadinfra.com/graphql</a>
            </td>
        </tr>
    </tbody>
</table>

---

# What is GraphQL, and why should I care?

GraphQL is a standard query language to access data remotely. An ecosystem of client and server libraries, best practices and knowledge bases make it easy to adopt, especially on the client-side.

Using GraphQL, you can:

- Query historical data with filtering, sorting and pagination (as made available by the GraphQL server developer)
- Subscribe to changes
- Get related data in the same query
- Only get the data that you need
- Discover the API using GraphQL Schema
- Use the existing tooling/libraries in the GraphQL ecosystem
- Write clients that keep working after new features are added to the GraphQL server (less versioning)
- Write clients that are faster and more responsive as they make fewer calls to the servers
- Have better scalability

---

# What is TezGraph

TezGraph is a GraphQL interface to Tezos data. It connects to a Tezos node to enable real-time subscriptions and to a historical database of indexed Tezos data (updated by Nomadic Labs' Tezos Indexer).

To use TezGraph effectively, familiarity with Tezos concepts (like baking, endorsing, block, implicit and originated addresses, contracts, operations, transactions, bigmaps, metadata, etc.) is assumed.


Tezgraph uses the [*Relay Cursor Connections Specification*](https://relay.dev/graphql/connections.htm) for pagination (further details can be found under the ["Pagination" section](https://tezgraph.com/docs/documentation/queries/pagination)). This means that all queries have the following structure.

```
connection {
    edges {
        node {
            ...
        }
    }
}
```

Inside the `connection` (operations query), we have an `edges` sub-selection. Edges consist of two queryable fields, `cursor` and `node`. The `node` field is a sub-selection in which we request the operation fields of interest.

---

# Your First Tezgraph Query

Let's assume you want to check out the 3 latest operations performed by a specific account.
You can head to one of the public TezGraph links or use the user interface below to issue the following query.

<GraphiQLComponent
        query={`query {
  accounts(
    first: 1
    filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }
  ) {
    page_info {
      has_next_page
      has_previous_page
      start_cursor
      end_cursor
    }
    edges {
      node {
        operations(
          first: 3
          filter: { relationship_type: source }
          order_by: { field: chronological_order, direction: desc }
        ) {
          edges {
            cursor
            node {
              hash
              kind
            }
          }
        }
      }
    }
  }
}
`}/>

You can learn about the `accounts` query in greater detail under the ["Accounts" section](https://tezgraph.com/docs/documentation/queries/2accounts).

### Let's have a closer look at the query:

`query`: This signifies that a query is about to begin. Another value that can go here is `subscription`. TezGraph gives you a read-only view of the blockchain, so there is no `mutation`.

`accounts`: This is the root query we are using.
- TezGraph also provides: `bigmaps`, `bigmap_keys`, `bigmap_values`, `operations`, and `version`, which can be used to query information about TezGraph's version and release. All of the available queries can be found under the DOCS tab on the right side of the playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).

The account query accepts arguments. Here we ask for the `first` matching `account` with the address `tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS`. In TezGraph queries, precisely one of the `first` or `last` arguments are required, as it limits the number of rows you can query in each GraphQL call. If more data is needed, you can use the pagination arguments to get the following pages or rows. That is, by passing the `cursor` values you get from your response to `after` and `before`.
That is, by using the `cursor` values provided by the Tezgraph results for the arguments `before` or `after`. This is the standard of relay pagination, a common practice in the GraphQL ecosystem, further explored below.

`operations`: One of the benefits of GraphQL is that you can query related data in the same query. Here, for each `account` returned by the query, we ask for the **first** 3 of the operations it has initiated (it's their **source**), in reverse **chronological** order.

`edges`: This is part of the relay pagination standard. The query returns a relay `connection` which has the following information:

- `page_info`: Tells you if there are previous and next pages and the cursor to use to query those pages. Further explained [in the "Pagination" section](https://tezgraph.com/docs/documentation/queries/pagination).
- `edges`: Contains an array of objects, each containing one row of the information you requested inside the `node` field and a `cursor` field of type `string`.

The whole boilerplate around relay pagination might seem overkill at first, but it becomes second nature after a few queries.

This concludes the quick start example on Tezgraph queries. Details on paginating Tezgraph results are documented [here](https://tezgraph.com/docs/documentation/queries/pagination). For more information on queries, please refer to their dedicated documentation.

More details and examples on Tezgraph queries:
- [Tezgraph GraphQL Schema](https://tezgraph.com/schema/)
- [Tezgraph Pagination](https://tezgraph.com/docs/documentation/queries/pagination)
- [Account Queries](https://tezgraph.com/docs/documentation/queries/2accounts)
- [Operation Queries](https://tezgraph.com/docs/documentation/queries/operations)
- [Bigmap Queries](https://tezgraph.com/docs/documentation/queries/4bigmaps)

---

## Advanced Tezgraph Query with Pagination

The previous example returned 3 operations. In this advanced query example, we will now add in the `block` field to reveal which block these operations were performed on. Adding the following to the query, after the `kind` field.

```
block {
  level
  hash
}
```

Here, because each operation is related to only one block, the `block` field is simple to query.

For performance reasons, we limit the complexity and depth of queries that TezGraph accepts. If you are not satisfied with the limits, you can contact us about your use-cases or alternatively choose to self-host TezGraph with more relaxed limits for your usage.

In this advanced query example, we will also add in pagination fields (`page_info` and `cursor`) and arguments (`first` and `after`) to the query. Tezgraph uses the Relay Cursor Connections Specification for pagination, if you are unfamiliar with this, it is recommended that you read the [pagination documentation](https://tezgraph.com/docs/documentation/queries/pagination).

In `operations` sub-query, just above `edges`, we have added the following lines:

```
page_info {
  has_next_page
  has_previous_page
  start_cursor
  end_cursor
}
```

We have also added the `cursor` field right inside the `operations.edges` sub-query.

Now you will see that the query has no previous page, but it will have a next page.

To query the next page, we have to use `after` argument in the `operations` sub-query and pass the `page_info.end_cursor` value to it:

<GraphiQLComponent
    query={`query{
  accounts(
    first: 1
    filter: { addresses: ["tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS"] }
  ) {
    edges {
      node {
        operations(
          first: 3
          after: "ooY4G16jWGTTxMCS1BWwh4PtmhhVfyTKA18mpnpDRQcqUjyftr8:0:0"
          filter: { relationship_type: source }
          order_by: { field: chronological_order, direction: desc }
        ) {
          page_info {
            has_next_page
            has_previous_page
            start_cursor
            end_cursor
          }
          edges {
            cursor
            node {
              hash
              kind
              block {
                level
                hash
              }
            }
          }
        }
      }
    }
  }
}
`}
/>

---

# Your First Tezgraph Subscription

GraphQL queries allow you to get historical data. In some other technologies (namely REST services), if you need to stay informed of new changes, you need to make repeated calls to the service backend. If these queries are too much separated in time, the user can be using stale data for a while, but if you repeat queries too frequently, a lot of network and server resources will be wasted to serve these repeated queries. Enter GraphQL subscriptions.

In this example, we subscribe to entire new blocks and also get reveals which can be filtered too.

This allows clients to make sure they have not missed any updates as they can easily keep track of the last seen block level on their side. For example, if a connection is dropped, it is easy for the client to reconnect and get all the updates from the last seen block using `replayFromBlockLevel` parameter.

<GraphiQLComponent
    query={`subscription {
  blockAdded {
    hash
    header {
      level
    }
    reveals(filter: { source: { notEqualTo: "tz1MBidfvWhJ64MuJapKExcP5SV4HQWyiJwS" } }) {
      fee
      public_key
    }
  }
}
`}
/>

More details and examples on Tezgraph subscriptions can be found [here](https://tezgraph.com/docs/documentation/subscriptions).

---

# Next steps

- You can learn more about GraphQL by checking out the [official GraphQL website](https://graphql.org/learn/).
- You can learn more about [pagination](./documentation/queries/pagination) in Tezgraph queries.
- You can further explore the [Tezgraph queries](./documentation).
- You can explore the TezGraph schema by checking out the **DOCS** button in Graph*i*QL interfaces embedded in this page. You may also explore the **DOCS** on our Tezgraph GraphQL playground [here](https://mainnet.tezgraph.tez.ie/queries/graphql) and [here](https://tezgraph.com/schema/).
- You can connect your software to TezGraph by leveraging any of the client libraries written for at least 23 different languages. A starting point is [here](https://graphql.org/code).
