---
title: 'Bigmaps'
metaTitle: 'Bigmaps | TezGraph'
metaDescription: 'This page contains the Tezgraph Bigmaps documentation and examples.'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Bigmaps

:::tip
If you are new to Tezgraph, it is recommended that you read the [quick start](https://tezgraph.com/docs/quickStart) and [pagination documentation](https://tezgraph.com/docs/documentation/queries/pagination) first.
:::

### Introduction

Smart contracts on the Tezos blockchain are written in Michelson ([https://tezos.gitlab.io/michelson-reference/](https://tezos.gitlab.io/michelson-reference/)). Michelson bigmaps ([https://tezos.gitlab.io/michelson-reference/#type-big_map](https://tezos.gitlab.io/michelson-reference/#type-big_map)) are used as storage for these smart contracts.

Tezgraph bigmap queries provide the ability to retrieve these bigmaps and other related bigmaps.

### Tezgraph Bigmap Queries

#### bigmaps
- This will return the bigmap records that meet the query conditions.
- Supported Filters:
    - by contract addresses
    - by annotations
    - by bigmap ids
#### bigmap_keys
- This will return the records for the available keys of the bigmaps that meet the query conditions.
- Supported Filters:
    - by bigmap keys
    - by bigmap ids
#### bigmap_values
- This will return all of the records of the changes of the bigmaps that meet the query conditions.
- Supported Filters:
    - by specific bigmap key
    - by specific bigmap id

All `BigmapRecords` have an id (bigmap id). All of the `BigmapKeyRecords` and `BigmapValueRecords` that are related to a `BigmapRecord` would share the same id (bigmap id) as that `BigmapRecord`.

Example of a `BigmapRecord`:
```json
{
    "node": {
        "id": "27966",
        "annots": "%balances",
        "contract": {
            "address": "KT1JherLR5WrarWLPxg1e2frRhULkrsscf7i"
        },
        "key_type": {
            "prim": "address"
        },
        "value_type": {
            "args": [
                {
                    "prim": "nat",
                    "annots": [
                        "%enr"
                    ]
                }
            ],
            "prim": "pair"
        }
    }
},
```

The `BigmapKeyRecords` and `BigmapValueRecords`, even though they share the same id (bigmap id), will look different.

Example of a `BigmapKeyRecord`:
```json
{
  "cursor": "26:expruriMSZUhpscq7C7B67gjrWfbTYXk5qptTfUUN1DBiPXyWmAUUc",
  "node": {
    "key": "tz1NMfZdAFgojChrfPh9owpax1ykpgoxtncR",
    "key_michelson": "\"tz1NMfZdAFgojChrfPh9owpax1ykpgoxtncR\"",
    "key_micheline_json": "{\"string\":\"tz1NMfZdAFgojChrfPh9owpax1ykpgoxtncR\"}",
    "current_value": {
      "value": {
        "int": "7475000000"
      },
      "value_michelson": "7475000000",
      "value_micheline_json": "{\"int\":\"7475000000\"}",
      "block": {
        "timestamp": "2020-03-03T16:24:02.000Z",
        "level": 849852,
        "hash": "BLAPmJB83vr9q2JRasYFQhrRdo1FMd8jwNvJxQqewY3bVc6GyaU"
      }
    }
  }
},
```

Example of a `BigmapValueRecord`:
```json
{
    "node": {
        "key": {
            "bytes": "0000b532558a9c90ae5d4fdc7bc70243c1b58f9f3303"
        },
        "key_hash": "exprueiStGaYMk2L7xhSSDhWzytvf3xe3KuTigPopLATqHZLw7747G",
        "value": {
            "int": "31200000000"
        },
        "block": {
            "level": 923518
        },
        "source": {
            "address": "tz1cA7J7pCQVErdHBdRAf6GLmF19b84YsDpt"
        }
    }
}
```


:::info

The example records above contain only some of the available fields. All of the queryable fields can be found under the `DOCS` tab on the right side of the playground (https://mainnet.tezgraph.tez.ie/graphql).
 - `DOCS` > `bigmaps`/`bigmap_keys`/`bigmap_values` > `edges` > `node`

:::

------

### Bigmaps Query
This will return the bigmaps that meet the conditions of the query.

<GraphiQLComponent
    query={`query BigmapQuery {
  bigmaps(filter: { annots: "%balances" }, first: 10) {
    total_count
    edges {
      cursor
      node {
        id
        annots
        block {
          hash
          timestamp
          level
        }
      }
    }
  }
}
`}/>


In this example, we are querying for the fields of `id`, `annots`, `block.hash`, `block.timestamp`, `block.level`, we can ignore `cursor` until the pagination example.

As for the arguments, we are using a `annots` filter in the `filter` argument, along with the `first` argument. Together, these arguments will set the conditions of the results and return the first 10 bigmaps with the `annots` of `"%balances"`.

#### Arguments:
- `filter: { annots: "%balances" }` - This will filter for bigmaps with a `annots` of `%balances`.
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `first: 10` - This will return the first 10 bigmaps that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination).

In this query we are looking for bigmaps with an `annots` value of `%balances`. Each record in the result is a `BigmapRecord` that defines the usage and structure of the bigmap. The returned values can be used in the `bigmap_keys` or `bigmap_values` queries, which are explored below. In those examples, we will use the `id` values returned from the results of this example as query arguments.

:::info

The bigmap queries provide many arguments to sort and filter query results. You can find all of the available query options under the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql).
 - `DOCS` > `bigmaps`/`bigmap_keys`/`bigmap_values` > Arguments
 - `DOCS` > `bigmaps`/`bigmap_keys`/`bigmap_values` > `filter`
 - `DOCS` > `bigmaps`/`bigmap_keys`/`bigmap_values` > `order_by`

:::

If no optional arguments are given, Tezgraph will return the results in the default condition. The default condition for bigmap query results is ordered by the id (bigmap id) in ascending order. The record with the smallest id first, and the record with the largest id last.

------

### Bigmap Keys Query
This query will return all of the available keys for the bigmaps that meet the query conditions.

<GraphiQLComponent
    query={`query BigmapKeysQuery {
  bigmap_keys(filter: { bigmap_id: "26" }, first: 10) {
    total_count
    edges {
      cursor
      node {
        key
        key_michelson
        key_micheline_json
        current_value {
          value
          value_michelson
          value_micheline_json
          block {
            timestamp
            level
            hash
          }
        }
      }
    }
  }
}`}/>


In this example, we are querying for the fields of `key`, `key_michelson`, `key_micheline_json`, `current_value.value`, `current_value.value_michelson`, `current_value.value_micheline_json`, `block.timestamp`, `block.level`, and `block.hash`.

As for the arguments, we are using a `bigmap_id` filter in the `filter` argument, along with the `first` argument. Together, these arguments will set the conditions of the results and return the first 10 `BigmapKeyRecords` with the `bigmap_id` of `26`.

#### Arguments:
- `filter: { bigmap_id: "26" }` - This will filter for bigmap keys with a `bigmap_id` of `26`.
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `first: 10` - This will return the first 10 bigmap keys that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination).

This query will show us the available keys in the bigmap with the bigmap_id of 26. Each record in the results represent a unique key in the bigmap. These details can be useful for finding the data of interest, and can be used in the `bigmap_values` query to explore the history of updates. The `bigmap_values` query is further explored in the next section.

:::info
The bigmap queries provide many ways to the same data in different formats. You can find all of the available query options under the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql).
 - `DOCS` > `bigmaps`/`bigmap_keys`/`bigmap_values` > `edges` > `node`
:::

If no optional arguments are given, Tezgraph will return the results in the default condition. The default condition for bigmap query results is ordered by the id (bigmap id) in ascending order. The record with the smallest id first, and the record with the largest id last.


------

### Bigmap Values Query
This query will return all of the records that represent changes to bigmaps that meet the conditions of the query.

<GraphiQLComponent
    query={`query BigmapValuesQuery {
  bigmap_values(first: 10, filter: { bigmap_id: "26", key: "tz1PwiRzT46gPsfJkxNMjsECDDqxmxPZvWTJ" }) {
    total_count
    edges {
      cursor
      node {
        key
        key_michelson
        value
        value_michelson
        kind
        block {
          timestamp
          level
          hash
        }
        source {
          address
        }
      }
    }
  }
}`}/>

In this example, we are querying for the fields of `key`, `key_michelson`, `value`, `value_michelson`, `kind`, `block.timestamp`, `block.level`, `block.hash`, and `source.address`.

As for the arguments, we are using a `bigmap_id` and `key_micheline_json` filters in the `filter` argument, along with the `first` argument. Together, these arguments will set the conditions of the results and return the first 10 `BigmapValueRecords` with the `bigmap_id` of "26" and a `key` of `tz1PwiRzT46gPsfJkxNMjsECDDqxmxPZvWTJ`.

#### Arguments:
- `filter: { bigmap_id: "26" }` - This will filter for bigmap values with a `bigmap_id` of `26`.
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `filter: { key: "tz1PwiRzT46gPsfJkxNMjsECDDqxmxPZvWTJ" }` - This will filter for bigmap values with a `key` of `tz1PwiRzT46gPsfJkxNMjsECDDqxmxPZvWTJ`.
  - __OPTIONAL__ - This is an optional argument used to filter the query results. Please note that if you filter by `key`, you also need to filter by `bigmap_id`
- `first: 10` - This will return the first 10 bigmap values that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination).

This query will show us how the data for this key changed over time. Each record in the results indicate an operation that performed an action on the bigmap.

If no optional arguments are given, Tezgraph will return the results in the default condition. The default condition for bigmap query results is ordered by the id (bigmap id) in ascending order. The record with the smallest id first, and the record with the largest id last.

------

### Advanced Bigmap Query

Inside the `bigmap` query, Tezgraph is able to access related details from other queries(accounts, operations, bigmaps). In the query below, we will be using the `bigmaps` query to retrieve related data for:
- `block`,
- `keys` (BigmapKeyRecordConnection)
- `values_history` (BigmapValueRecordConnection)
- `current_value` (BigmapValueRecord)
- `contract` (AccountRecord)
- `operations` (OperationRecordConnection)

<GraphiQLComponent
    query={`query BigmapQuery {
  bigmaps(filter: { annots: "%balances" }, first: 15) {
    total_count
    edges {
      cursor
      node {
        id
        annots
        block {
          hash
          timestamp
          level
        }
        keys (first: 10) {
          edges {
            node {
              key_hash
              key
              values_history (first: 10) {
                edges {
                  node {
                    key
                    value
                  }
                }
              }
              current_value {
                key
                value
                value_michelson
                value_micheline_json
              }
            }
          }
        }
        contract {
          address
          contract_metadata {
            name
            description
          }
          operations(
            first: 1
            filter: { kind: origination, relationship_type: originated_contract }
          ) {
            edges {
              node {
                ... on OriginationRecord {
                  contract {
                    address
                  }
                }
                kind
                source {
                  address
                }
                hash
              }
            }
          }
        }
      }
    }
  }
}`}/>

In the bigmap query above, we have added the following:

#### Block
```
  block {
      hash
      timestamp
      level
  }
```
In this `block` sub-query, we are asking for the block details, specifically  `hash` (block hash), `timestamp`, and `level`.

#### Bigmap Keys and Bigmap Values

```
  keys(first: 10) {
    edges {
      node {
        key_hash
        key
        values_history(first: 10) {
          edges {
            node {
              key
              value
            }
          }
        }
        current_value {
          key
          value
          value_michelson
          value_micheline_json
        }
      }
    }
  }
```
This `keys` sub-query provides a way to query for `bigmap_keys` records (BigmapKeyRecord) from inside the `bigmaps` query. Through this `keys` sub-query, we can go deeper and query for related `bigmap_values` records (BigmapValueRecord).

1. The outer `bigmaps` query (displayed in the GraphiQL interface) will return `bigmap` records that meet the query conditions.
2. Inside each of the `bigmap` records returned by the above (1), the `keys` sub-query will find and return `bigmap_keys` records. In this example, we are using the argument `first: 10`. This will return the first 10 keys, if available, for each of the `bigmap` records.
3. Now inside these `keys` records, we can go even further and query for their `bigmap_values` using the `values_history` sub-query or the `current_value` sub-query.
    - `values_history` - This sub-query will return all of the records that represent changes or updates to the storage, the bigmap values, of the bigmaps that meet the conditions of the query. This sub-query requires the use of the `first` or `last` arguments.
    - `current_value` - This sub-query will return the current value (`bigmap_values`) of the `bigmap_keys` record.

#### Contract (Accounts) and Operations

```
 contract {
   address
   contract_metadata {
     name
     description
   }
   operations(
     first: 1
     filter: { kind: origination, relationship_type: originated_contract }
   ) {
     edges {
       node {
         ... on OriginationRecord {
           contract {
             address
           }
         }
         kind
         source {
           address
         }
         hash
       }
     }
   }
 }

```

Here we are using the `contract` sub-query to retrieve `account` records. Through this `contract` sub-query, we can go deeper and query for related `operation` records.
1. This `contract` sub-query will return the `account` details for the `contract` that the bigmap belongs to.
    - Inside this `contract` sub-query, we have two more sub-queries, `contract_metadata` and `operations`.

2. Smart contracts often have contract metadata which may provide more contract details. These details can be retrieved using the `contract_metadata` sub-query.
3. In the `operations` sub-query, we have applied some arguments. Together, these arguments will find and return the `OperationRecord` for the  origination operation that created the related contract (the contract returned by the `contract` sub-query).

#### Conclusion

In this Advanced Bigmap Query example, we:
- Found 15 `BigmapRecords` with the annotation of `%balances`.
    - For each of these `BigmapRecords`, we retrieved their first 10 available `BigmapKeyRecords`.
        - For each of these `BigmapKeyRecords`, we retrieved their first 10 available updates as `BigmapValueRecords`.
        - For each of these `BigmapValueRecords`, we retrieved their current value as a `BigmapValueRecord`.
    - For each of these `BigmapRecords`, we retrieved their contract account details as `AccountRecords`.
        - For each of these `AccountRecords`, using a combination of arguments, we retrieved the `OperationRecord` containing the contract origination details.


:::info
Tezgraph records are all connected in different ways through different fields. To explore other queries, records, and fields, visit the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).
:::
