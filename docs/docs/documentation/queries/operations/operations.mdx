---
title: 'Operations'
metaTitle: 'Operations | TezGraph'
metaDescription: 'This page contains the Tezgraph Operations documentation and examples.'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Operations

:::tip
If you are new to Tezgraph, we recommend reading the [quick start](https://tezgraph.com/docs/quickStart) and [pagination documentation](https://tezgraph.com/docs/documentation/queries/pagination) first.
:::

### Introduction

Operations are actions that change the state of the Tezos Blockchain. There are many types of operations, like transactions, reveals, delegations, etc. In this document, we present an overall understanding of how operations can be queried and subscribed to in TezGraph and how to work with specific operation types.

To work with Operations effectively, we recommend getting familiar with the [relevant concepts in Tezos](https://tezgraph.com/docs/architecture/information-model/), so here we only focus on using Operations in TezGraph.

:::info
All of the queryable fields can be found under the DOCS tab on the right side of the playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).
 - `DOCS` > `operations` > `edges` > `node`
:::

------

### Operations Query

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {hash: "opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo"}, first: 10) {
    edges {
      node {
        hash
        batch_position
        internal
        kind
        block {
          hash
          level
          timestamp
        }
        source {
          address
        }
      }
    }
  }
}`}/>

In this example, the fields of interest are `hash`, `batch_position`, `internal`, `kind`, `block.hash`, `block.level`, `block.timestamp`, and `source.address`.

As for the arguments, we are using a `hash` filter in the `filter` argument, along with the `first` argument. Together, these arguments will set the conditions of the results and return the first 10 `OperationRecords` with the `hash` (operation_hash) of "opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo".

#### Arguments:
- `filter: {hash: "opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo"}` - This will filter for `OperationRecords` with a `hash` (operation hash) of `opEcd7DjnF81mEJPeQMp1iDugBqaaEFRFaNGMz6rd2hcHiV7Xbo`.
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `first: 10` - This will return the first 10 operations that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination), which will be explained in the pagination example.


:::info

You can find all of the available query options under the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql).
 - `DOCS` > `operations` > Arguments
 - `DOCS` > `operations` > `filter`
 - `DOCS` > `operations` > `order_by`

:::

If no optional arguments are given, Tezgraph will return the results in the default ordering. The default ordering for operation query results is to be ordered by the reverse chronological order in which they were applied to the blockchain. The most recent operation first, and the oldest operation last.

------

### Operations Query with Fragments

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(
    first: 10
    filter: {
      kind:transaction
    }
  ) {
    edges {
      cursor
      node {
        hash
        batch_position
        internal
        kind
        block {
          hash
          level
          timestamp
        }
        source {
          address
        }
        ... on TransactionRecord {
          fee
          counter
          gas_limit
          storage_limit
          storage_size
          amount
          destination
        }
      }
    }
  }
}`}/>

This example is similar to the one above, except we are no longer using a `hash` filter in the `filter` argument. Instead, we are using the `kind` filter and have added a `TransactionRecord` fragment.

Using the `kind` filter will filter the query results and only return `OperationRecords` with the given argument value.

Fragments allow us to query sets of fields when applicable. By adding the `TransactionRecord` fragment, we can query for transaction-specific fields that would not have been retrievable otherwise.

#### Arguments:
- `filter: { kind: transaction }` - This will filter the operation query results for `OperationRecords` with `kind: transaction` key value pair.
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `first: 10` - This will return the first 10 operations that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and [pagination](https://tezgraph.com/docs/documentation/queries/pagination), which will be explained in the pagination example.

:::info

All fragment implementations can be found under the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql).
 - `DOCS` > `operations` > `edges` > `node` > Implementations

:::
------

### Operations Query with Sorting

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(
    order_by: { field: source, direction: asc }
    last: 3
    filter: {
      timestamp: {
        gte: "2021-01-01T00:00:00.000Z"
        lte: "2021-01-01T00:00:10.000Z"
      }
    }
  ) {
    edges {
      node {
        hash
        batch_position
        internal
        source {
          address
        }
      }
    cursor
    }
    page_info {
      has_previous_page
      has_next_page
      start_cursor
      end_cursor
    }
  }
}`}/>

In this example, we are querying for the fields of `hash`, `batch_position`, `internal`, and `source.address` of the `OperationRecords`. The `cursor` and `page_info` fields are used for pagination and will be explained in the pagination example.

In this query, we use a `timestamp` filter in the `filter` argument, along with the `order_by` and `last` arguments. Together, these arguments will sort the operations by the `source` (`source.address`) and return the last 3 operations with timestamps between "2021-01-01T00:00:00.000Z" and "2021-01-01T00:00:10.000Z".

#### Arguments:
- `order_by: { field: source, direction: asc }` - This will sort the operations by `source` in ascending order.
  - __OPTIONAL__ - If this argument were not provided, by default, the results would be ordered by the chronological order in which they were indexed. The most recent operation first, and the oldest operation last.
- `filter: { timestamp: { gte: "2021-01-01T00:00:00.000Z" lte: "2021-01-01T00:00:10.000Z" } }` - This will filter for records with timestamps greater than or equal to "2021-01-01T00:00:00.000Z" and less than or equal to "2021-01-01T00:00:10.000Z".
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `last: 3` - This will return the last 3 operations that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and may be used to paginate these results. Pagination will be explained in the following example.

------

### Operations Query with Pagination

Tezgraph uses the [*Relay Cursor Connections Specification*](https://relay.dev/graphql/connections.htm) for pagination. If you are unfamiliar with this specification or pagination in Tezgraph, it is recommended that you read the [pagination documentation](https://tezgraph.com/docs/documentation/queries/pagination) before continuing.

Simply put, each record will be given a cursor. This cursor value can be used for the `before`/`after` query arguments.

Using the `first`/`last` arguments and the `before`/`after` arguments, you can paginate through the results.

For this example, we will be using the same example query from above (Operations Query with Sorting) with the addition of the `before` argument.

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(
    order_by: { field: source, direction: asc }
    last: 3
    filter: {
      timestamp: {
        gte: "2021-01-01T00:00:00.000Z"
        lte: "2021-01-01T00:00:10.000Z"
      }
    }
    before: "oomUph7hoPriHd3ZmcJgpLAa4VhTiex2cWWhBLLhN6pWvaPtHgQ:0:0"
  ) {
    edges {
      node {
        hash
        batch_position
        internal
        source {
          address
        }
      }
      cursor
    }
    page_info {
      has_previous_page
      has_next_page
      start_cursor
      end_cursor
    }
  }
}`}/>

By adding this, the results will be paginated in a way that will return the last 3 `OperationRecords` in the given order, that are before the record with the cursor of `oomUph7hoPriHd3ZmcJgpLAa4VhTiex2cWWhBLLhN6pWvaPtHgQ:0:0`.

#### Arguments:
- `order_by: { field: source, direction: asc }` - This will sort the operations by `source` in ascending order.
  - __OPTIONAL__ - If this argument were not provided, by default, the results would be ordered by the chronological order in which they were indexed. The most recent operation first, and the oldest operation last.
- `filter: { timestamp: { gte: "2021-01-01T00:00:00.000Z" lte: "2021-01-01T00:00:10.000Z" } }` - This will filter for records with timestamps greater than or equal to "2021-01-01T00:00:00.000Z" and less than or equal to "2021-01-01T00:00:10.000Z".
  - __OPTIONAL__ - This is an optional argument used to filter the query results.
- `before: "oomUph7hoPriHd3ZmcJgpLAa4VhTiex2cWWhBLLhN6pWvaPtHgQ:0:0"` - This argument will paginate the results and return only the records before the record with the cursor of `oomUph7hoPriHd3ZmcJgpLAa4VhTiex2cWWhBLLhN6pWvaPtHgQ:0:0`.
  - __OPTIONAL__ - This is an optional argument used to paginate the query results.
- `last: 3` - This will return the last 3 operations that meet the given conditions.
  - __REQUIRED__ - The `first`/`last` arguments are used to set the number of returned records in the results and may be used to paginate these results.

:::info
Tezgraph records are all connected in different ways through different fields. To explore other queries, records, and fields, visit the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).
:::

