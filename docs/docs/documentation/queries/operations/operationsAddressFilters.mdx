---
title: 'Operations Address Filters'
metaTitle: 'Operations Filters | TezGraph'
metaDescription: 'This page contains the Tezgraph Operations Filters documentation and examples.'
---

import GraphiQLComponent from '@site/components/graphiql-component';

# Operations Address Filters

:::tip
This page explains the address filters of the Operations query. We recommend reading the [Operations documentation](https://tezgraph.com/docs/documentation/queries/operations/3operations) first.
:::

### Introduction

The Operations query provides address filters that filter through operation fields to find the specified addresses. Each of these filters takes in an array of addresses in string format.

```
sources: [Address!]
destinations: [Address!]
endorsement_delegates: [Address!]
delegation_delegates: [Address!]
originated_contracts: [Address!]
senders: [Address!]
receivers: [Address!]
```

#### Filters:

-  ### **sources: [Address!]**:

    -   This filter will return operations in which one of the given addresses are found under the `source` field.
    -   Operation Kinds returned by this filter:
        -   Reveal
        -   Delegation
        -   Endorsement
        -   Origination
        -   Transaction

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {sources: ["tz3gGCrSvKfJpUd3w6ckSvBFbRJ5RjWU9zEw"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        source {
          address
        }
      }
    }
  }
}`}
/>

-  ### **destinations: [Address!]**:

    -   This filter will return operations in which one of the given addresses are found under the `destination` field.
    -   Operation Kinds returned by this filter:
        -   Transaction

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {destinations: ["tz3gGCrSvKfJpUd3w6ckSvBFbRJ5RjWU9zEw"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        ... on TransactionRecord {
          destination {
            address
          }
        }
      }
    }
  }
}`}
/>

-  ### **endorsement_delegates: [Address!]**:

    -   This filter will return `Endorsement` operations in which one of the given addresses are found under the `delegate` field.
    -   Operation Kinds returned by this filter:
        -   Endorsement

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {endorsement_delegates: ["tz1WnfXMPaNTBmH7DBPwqCWs9cPDJdkGBTZ8"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        ... on EndorsementRecord {
          metadata {
            delegate {
              address
            }
          }
        }
      }
    }
  }
}`}
/>

-  ### **delegation_delegates: [Address!]**:

    -   This filter will return `Delegation` operations in which one of the given addresses are found under the `delegate` field.
    -   Operation Kinds returned by this filter:
        -   Delegation


<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {delegation_delegates: ["tz1Wit2PqodvPeuRRhdQXmkrtU8e8bRYZecd"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        ... on DelegationRecord {
          delegate {
            address
          }
        }
      }
    }
  }
}`}
/>

-  ### **originated_contracts: [Address!]**:

    -   This filter will return `Origination` operations in which one of the given addresses are found under the `contract` field.
    -   Operation Kinds returned by this filter:
        -   Origination

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(
    filter: {originated_contracts: ["KT1HedYVpGi5N2NVTXhRY5zw6PRjf8NFy5te"]}
    first: 10
  ) {
    edges {
      node {
        hash
        batch_position
        kind
        ... on OriginationRecord {
          contract {
            address
          }
        }
      }
    }
  }
}`}
/>

-  ### **senders: [Address!]**:

    -   The Tezos-Indexer provides a table called `operation-sender-and-receivers`. Tezos-Indexer uses this table to organize each operation with a `sender` and a `receiver` if present. This filter will look through this table and return the operations with one of the given addresses as the `sender`.
    -   Operation Kinds returned by this filter:
        -   Reveal
        -   Delegation
        -   Endorsement
        -   Origination
        -   Transaction


<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {senders: ["tz3gGCrSvKfJpUd3w6ckSvBFbRJ5RjWU9zEw"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        sender {
          address
        }
      }
    }
  }
}`}
/>

-  ### **receivers: [Address!]**:

    -   The Tezos-Indexer provides a table called `operation-sender-and-receivers`. Tezos-Indexer uses this table to organize each operation with a `sender` and a `receiver` if present. This filter will look through this table and return the operations with one of the given addresses as the `receiver`.
    -   Operation Kinds returned by this filter:
        -   Delegation
        -   Origination
        -   Transaction

<GraphiQLComponent
    query={`query OperationsQuery {
  operations(filter: {receivers: ["tz3gGCrSvKfJpUd3w6ckSvBFbRJ5RjWU9zEw"]}, first: 10) {
    edges {
      node {
        hash
        batch_position
        kind
        receiver {
          address
        }
      }
    }
  }
}`}
/>

####

:::info
Tezgraph records are all connected in different ways through different fields. To explore other queries, records, and fields, visit the `DOCS` tab on the right side of the Tezgraph GraphQL Playground (https://mainnet.tezgraph.tez.ie/graphql) or [here](https://tezgraph.com/schema/).
:::
