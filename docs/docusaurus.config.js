// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const sectionPrefix = require('./src/remark/section-prefix');

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'TezGraph',
    tagline: 'Open-source GraphQL API for Tezos data',
    url: 'https://tezgraph.com',
    baseUrl: '/',
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    favicon: 'img/favicon.ico',
    organizationName: 'ECAD Labs', // Usually your GitHub org/user name.
    projectName: 'TezGraph', // Usually your repo name.

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                    // Please change this to your repo.
                    editUrl: 'https://gitlab.com/-/ide/project/tezgraph/tezgraph/edit/master/-/docs',
                    remarkPlugins: [],
                },
                blog: {
                    showReadingTime: true,
                    // Please change this to your repo.
                    editUrl: 'https://gitlab.com/tezgraph/tezgraph',
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
                gtag: {
                    trackingID: 'G-HF8CLLV68K',
                    anonymizeIP: true,
                },
            }),
        ],
    ],

    themeConfig:
        /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            defaultMode: 'light',
            colorMode: {
                defaultMode: 'light',
                disableSwitch: true,
                respectPrefersColorScheme: false,
            },
            navbar: {
                logo: {
                    alt: 'Tezgraph Logo',
                    src: 'img/logo.svg',
                    srcDark: 'img/logo-white.svg',
                },
                items: [
                    {
                        type: 'doc',
                        docId: 'quickStart',
                        position: 'right',
                        label: 'Quick Start',
                    },
                    {
                        type: 'doc',
                        docId: 'index',
                        position: 'right',
                        label: 'Docs',
                    },
                    {
                        href: 'https://gitlab.com/tezgraph/tezgraph',
                        label: 'GitLab',
                        position: 'right',
                    },
                ],
            },
            footer: {
                style: 'light',
                links: [
                    {
                        title: '   ',
                        items: [
                            {
                                html: "<img src='/img/logo.svg' class='footer-logo'/><br/><p>TezGraph is a simple, highly compatible, and reliable open-source API that provides access to historic and real-time Tezos blockchain data with the convenience of GraphQL.</p>",
                            },
                        ],
                    },
                    {
                        title: '    ',
                        items: [],
                    },
                    {
                        title: '     ',
                        items: [
                            {
                                label: 'Get Started',
                                to: '/docs/quickStart',
                            },
                            {
                                label: 'Documentation',
                                to: '/docs/',
                            },
                            {
                                label: 'Find us on GitLab',
                                href: 'https://gitlab.com/tezgraph/tezgraph',
                            },
                            {
                                html: '<a href="https://gitlab.com/tezgraph/tezgraph" target="_blank"><img src="/img/gitlab-small.svg" class="footer-icon"/></a><a href="https://discord.gg/sRUjuEPJjS" target="_blank"><img src="/img/facebook.svg" class="footer-icon"/></a><a href="https://twitter.com/TezGraph" target="_blank"><img src="/img/twitter.svg" class="footer-icon"/></a>',
                            },
                        ],
                    },
                ],
                copyright: undefined,
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
            },
        }),
    plugins: [require.resolve('./plugins/webpack5plugin/index.js')],
};

module.exports = config;
